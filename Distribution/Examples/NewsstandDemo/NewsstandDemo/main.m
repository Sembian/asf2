//
//  main.m
//  NewsstandDemo
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    int retVal = UIApplicationMain(argc, argv, nil, @"NewsstandDemoAppDelegate");
    [pool release];
    return retVal;
}
