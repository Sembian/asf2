//
//  NewsstandDemoAppDelegate.h
//  NewsstandDemo
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NewsstandDemoViewController;
@interface NewsstandDemoAppDelegate : NSObject <UIApplicationDelegate> {

}

@property (nonatomic, retain) IBOutlet UIWindow *window;

@property (nonatomic, retain) IBOutlet NewsstandDemoViewController *viewController;

@end