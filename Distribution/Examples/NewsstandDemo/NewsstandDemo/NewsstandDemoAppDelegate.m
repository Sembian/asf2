//
//  NewsstandDemoAppDelegate.m
//  NewsstandDemo
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "NewsstandDemoAppDelegate.h"
#import "NewsstandDemoViewController.h"
#import <ASFKit/ASFKit.h>

@interface NewsstandDemoAppDelegate() <ASFSessionDelegate>

@end

@implementation NewsstandDemoAppDelegate
@synthesize window=_window, viewController=_viewController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Configuration options for ASFKit
    [ASFSession startSessionUsingLicenseKey:@"PUT LICENSE HERE"
                             andEnvironment:ASFEnvironmentQA
                                   delegate:self];
    
    // Override point for customization after application launch.
    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}

# pragma mark - ASFSession Delegate methods
- (void)sessionDidEnd: (ASFSession *)session
{
    /*
     Called when the ASFSession was ended by this application.
     Remove sensitive data from memory and revert the interface to a state
     where the user can log in again.
     If appropriate for your app, you may call [[ASFSession sharedSession] startSession] here.
     */
    
    NSLog(@"ASFSession ended.");
    [self.viewController updateInterface];
}

- (void)sessionDidExpire: (ASFSession *)session
{
    /*
     Called when either the authentication token expired, another application ended the ASFSession,
     or the integrity of the device cannot be verified.
     Remove sensitive data from memory and revert the interface to a state
     where the user can log in again.
     If appropriate for your app, you may call [[ASFSession sharedSession] startSession] here.
     */
    
    NSLog(@"ASFSession expired.");
    [self.viewController updateInterface];
}

- (void)sessionDidStart: (ASFSession *)session
{
    /*
     Called when the ASFSession successfully started--the device has been secured and user data is available.
     This will only be called after an attempt to start the session from [[ASFSession sharedSession] startSession].
     You may begin downloading data or updating the interface to include personal user information.
     User properties are now available on the ASFSession singleton (e.g. [ASFSession sharedSession].upi).
     */
    
    NSLog(@"ASFSession started.");
    [self.viewController updateInterface];
}

- (void)sessionFailedToStartWithError:(NSError *)error
{
    /*
     Called when the ASFSession failed to start.
     error will contain the details about the error preventing the ASFSession from starting.
     The description of the error should be presented to the user in a UIAlertView.
     Do NOT place a call to startSession until the error has been resolved.
     */
    
    NSLog(@"ASFSession failed to start: %@.", error.localizedDescription);
    [self.viewController updateInterface];
}

- (void)dealloc
{
    [_window release];
    [_viewController release];
    [super dealloc];
}

@end
