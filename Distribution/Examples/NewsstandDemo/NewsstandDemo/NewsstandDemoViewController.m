//
//  NewsstandDemoAppDelegate.h
//  NewsstandDemo
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "NewsstandDemoViewController.h"
#import <ASFKit/ASFKit.h>

@implementation NewsstandDemoViewController
@synthesize loginButton, statusButton;

- (void)dealloc
{
    [loginButton release];
    [statusButton release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    statusButton.enabled = NO;
    [super viewDidLoad];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

# pragma mark - Interface actions
/// <summary>
/// Updates the interfaced based on whether the user is logged in or not.
/// </summary>
- (void) updateInterface
{
    ASFSession *session = [ASFSession sharedSession];
    
    if (session.sessionStarted)
    {
        statusButton.title = [NSString stringWithFormat:@"Logged in: %@", session.displayName];
        statusButton.enabled = YES;
        loginButton.title = @"Logout";
    }
    else
    {
        statusButton.title = @"Not Logged In";
        statusButton.enabled = NO;
        loginButton.title = @"Login";
    }
}

/// <summary>
/// Receives the event when the user presses the "Login" or "Logout" button in the bottom right of the screen.
/// Determines if the session has been started and uses that to determine if it should end the user's session or start a new one.
/// </summary>
- (IBAction) toggleLogin:(id)sender
{
    if([ASFSession sharedSession].sessionStarted)
    {
        [[ASFSession sharedSession] endSession];
    }
    else
    {
        [[ASFSession sharedSession] startSession];
    }
}

/// <summary>
/// Displays an UIAlertView containing the basic information about the current session.
/// </summary>
- (IBAction) displayStatus:(id)sender
{
    [[[[UIAlertView alloc] initWithTitle:@"ASFSession Status" message:[[ASFSession sharedSession] description] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] autorelease] show];
}

@end
