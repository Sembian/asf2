//
//  NewsstandDemoAppDelegate.h
//  NewsstandDemo
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsstandDemoViewController : UIViewController {
    IBOutlet UIBarButtonItem *loginButton;
    IBOutlet UIBarButtonItem *statusButton;
}

@property (nonatomic,retain) UIBarButtonItem *loginButton;
@property (nonatomic,retain) UIBarButtonItem *statusButton;

- (IBAction) toggleLogin:(id)sender;
- (IBAction) displayStatus:(id)sender;
- (void) updateInterface;

@end
