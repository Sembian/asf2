#!/bin/bash

# Move the documentation to the current user's home folder
rm -rf ~/Library/Developer/Shared/Documentation/DocSets/com.abbvie.asfkit.docset
mv /tmp/com.abbvie.asfkit.docset ~/Library/Developer/Shared/Documentation/DocSets
exit 0