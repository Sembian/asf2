About ASFKit
-----------------------
ASFKit was developed to help you make your applications more secure. By simply dropping it into your application and following the handful of configuration steps, your application has become tightly knitted with AbbVie.

At-A-Glance
-----------------------
While the framework has a number of components and features, most will adjust themselves accordingly to your application. At the outset, you're probably most interested in these classes:

* ASFSession -- establishes the user's identity and enforces basic security requires on the device.
* ASFProfile -- tells you about the current user.
* ASFActivityLog -- allows you to remotely log (and query) the user's activity.
* ASFActivityLogQueryResult -- provides access to the statements received from queries made to the server.

As you work more with activity logging, you'll want to explore the capabilities of these classes:

* ASFActivityStatement -- individual records of user activity that are exchanged with the server.
* ASFActivityObject -- represents "things" that are related to the activity (the "thing" that was created, for example).
* ASFActivityActor -- information about the user performing the activity.

Finally, you may find some conveniences available to you in these utility classes for common needs:

* ASFURLRequest -- provides convenient methods for requesting data from a server.
* ASFKeychain -- provides an easy-to-understand interface for interacting with the device's keychain.
* ASFUtilities -- provides quick access to commonly used data (such as the application's documents directory).