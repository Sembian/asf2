# /bin/bash
#
# Installs Appledoc code documentation generator by GentleBytes.
# http://gentlebytes.com/appledoc/
#
#	Install script by Float Mobile Learning
# Author: Daniel Pfeiffer
#

if type "/usr/local/bin/appledoc" > /dev/null; then
    echo "It looks like appledoc is already installed."
    read -p "Do you want to install it again? (y/n) " -n 1 -r
	echo    # (optional) move to a new line
	if [[ ! $REPLY =~ ^[Yy]$ ]]
	then
		exit 1
	fi
fi

if ! type "git" > /dev/null; then
    echo "Error: You must have git installed."
    exit 1
fi

pushd ~ > /dev/null
echo "Downloading Appledoc..."
echo "You will be asked for your password in a moment."
sleep 1
curl -L https://github.com/tomaz/appledoc/releases/download/v2.2-963/appledoc.zip -o appledoc.zip
unzip -q appledoc.zip -d appledoc
cd appledoc
sudo cp appledoc /usr/local/bin
sudo cp -Rf Templates/ ~/.appledoc
cd ~
sudo rm -rf appledoc appledoc.zip
popd > /dev/null