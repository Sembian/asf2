These notifications are posted by ASFKit.

## ASFSession

<a name="ASFSessionStartedNotification"></a>
### ASFSessionStartedNotification
Name of NSNotification that is sent upon a successful session initialization.

<a name="ASFSessionFailedNotification"></a>
### ASFSessionFailedNotification
Name of NSNotification that is sent upon a failed session initialization.

<a name="ASFSessionEndedNotification"></a>
### ASFSessionEndedNotification
Name of NSNotification that is sent upon a user requesting the ending of a session.

<a name="ASFSessionWillEndNotification"></a>
### ASFSessionWillEndNotification
Name of NSNotification that is sent when a session is about to end.

#### Availability
2.0

<a name="ASFSessionExpiredNotification"></a>
### ASFSessionExpiredNotification
Name of NSNotification that is sent upon a session becoming invalid.

<a name="ASFSessionWillExpireNotification"></a>
### ASFSessionWillExpireNotification
Name of NSNotification that is sent when a session is about to become invalid.

#### Availability
2.0

<a name="ASFSessionEndedOnLaunchNotification"></a>
### ASFSessionEndedOnLaunchNotification
Name of NSNotification that is sent when the user requests the session to end when the app is launched.

<a name="ASFSessionChangedNotification"></a>
### ASFSessionChangedNotification
Name of NSNotification that is sent when the logged in user changed in the background.

<a name="ASFLoginViewWillShowNotification"></a>
### ASFLoginViewWillShowNotification
Login view is about to appear.

#### Availability
1.0.7

<a name="ASFLoginViewDidShowNotification"></a>
### ASFLoginViewDidShowNotification
Login view has finished animating onto the screen.

#### Availability
1.0.7

<a name="ASFLoginViewWillHideNotification"></a>
### ASFLoginViewWillHideNotification
Login view is about to disappear.

#### Availability
1.0.7

<a name="ASFLoginViewDidHideNotification"></a>
### ASFLoginViewDidHideNotification
Login view has finished animating off the screen.

#### Availability
1.0.7

<a name="ASFConfigurationProfileWasInstalledNotification"></a>
### ASFConfigurationProfileWasInstalledNotification
Name of the NSNotification that is sent when the configuration profile is installed.

#### Availability
2.0

<a name="ASFConfigurationProfileWasUninstalledNotification"></a>
### ASFConfigurationProfileWasUninstalledNotification
Name of the NSNotification that is sent when the configuration profile is uninstalled.

#### Availability
2.0

## ASFFileProtectionManager

### ASFProtectedFilesWillBeRemovedNotification
Posted just before the files protected by ASFFileProtectionManager are removed from the application's sandbox.

#### Availability
1.0.7

### ASFProtectedFilesWereRemovedNotification
Posted after the files protected by ASFFileProtectionManager are removed from the application's sandbox.

#### Availability
1.0.7

### ASFProtectionAttributeModifiedNotification
Posted just after a file attribute was modified (e.g. the NSFileProtectionKey was set to NSFileProtectionComplete).

#### Availability
1.0.7

## ASFIntegrityCheckController

### ASFIntegrityCheckWillBeginNotification
Notification triggered just before the integrity check is about to begin.

As of version 2.1.1, these notifications will always be posted on the main queue.

#### Availability
1.0.7

### ASFIntegrityCheckFinishedNotification
Notification triggered when the integrity check has finished. userInfo will contain the result of the integrity check.

As of version 2.1.1, these notifications will always be posted on the main queue.

#### Availability
1.0.7