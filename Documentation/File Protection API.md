ASFKit File Protection API
=================================

ASFKit provides an API for protecting files downloaded within the app. These files are protected in two ways:

1. Protected files and directories are scanned every 30 minutes verifying that the file protection attribute on the file is NSFileProtectionComplete. If it is not, the file protection attribute is changed to [NSFileProtectionComplete](http://developer.apple.com/library/ios/#DOCUMENTATION/Cocoa/Reference/Foundation/Classes/NSFileManager_Class/Reference/Reference.html#//apple_ref/doc/uid/20000305-SW109).
2. If at any time the integrity of the device becomes compromised (e.g. if the configuration profile is removed), all protected files are deleted as soon as the app is launched/resumed.

Protected files are encrypted when the device is locked or shutdown. Protected files are only available to the system when the device is unlocked (requiring the user's passcode).

ASFFileProtectionManager is intended to be used as a "utility" class and therefore you cannot create instances of this class. All public methods are class level.

Definitions
-----------
* **Location:** A path or URL that references either a directory or file.
* **Unprotected:** The location is not checked by the Authentication Framework to ensure that it has the right NSFileProtectionKey.
* **Unprotectable:** The location is checked by the Authentication Framework but cannot be encrypted with NSFileProtectionComplete (usually because the file is a symbolic link or directory).
* **Protectable:** The location is checked by the Authentication Framework but does not have its NSFileProtectionKey attribute set to NSFileProtectionComplete.
* **Protected:** The location is checked by the Authentication Framework and has the NSFileProtectionKey set to NSFileProtectionComplete.

Directories Protected by Default
--------------------------------
You do not need to add protection to files or directories within these folders:

* Documents
* Library
* tmp

You may *not* remove protection from these directories.

Adding Protection to a Directory or File
----------------------------------------
To add protection to a directory or file, execute the following line:

    [ASFFileProtectionManager beginProtectingLocation: directoryOrFilePath];

`directoryOrFilePath` should be a relative path to the desired location (e.g. "Documents/protectThisFolder). It should NOT contain a beginning slash or the scheme. If `directoryOrFilePath` is determined to be invalid, an argument error will be thrown.

Any added directory is recursively protected meaning that all child folders and child files are also protected. If you begin protecting a folder or file inside of an already protected directory, you will add redundant protection to that location which *may* cause performance issues.

These files are checked on a 30 minute interval to verify that their file protection attribute key is NSFileProtectionComplete.

`directoryOrFilePath` does not have to exist in order to protect it. This location will be watched for the creation of any files and when the location does exist, the directory or file at the location will be protected.

Removing Protection from a Directory or File
--------------------------------------------
To stop protecting a directory or file, call:

    [ASFFileProtectionManager stopProtectingLocation: directoryOrFilePath]

`directoryOrFilePath` should be a relative path to the desired location (e.g. "Documents/doNotProtectThisFolder). It should NOT contain a beginning slash or the scheme. If `directoryOrFilePath` is determined to be invalid, an argument error will be thrown.

`directoryOrFilePath` will no longer be checked every 30 minutes and will *not* be deleted if the device's integrity becomes compromised.

If you're attempting to remove protection from the Documents, Library, or tmp directory, an argument error will be thrown.

Protection for a directory or folder is determined by level of specificity. For example, if a file's parent directory is being protected but the file itself is specifically removed from protection, the file will *not* be protected.

When a location is removed from protection, the file protection attribute key is *not* modified. If the file protection attribute needs to be modified, it will need to be done through the [NSFIleManager](http://developer.apple.com/library/ios/#documentation/Cocoa/Reference/Foundation/Classes/NSFileManager_Class/Reference/Reference.html) class.

**Note:** AbbVie requires documentation outlining why a specific file or directory should not be protected. In nearly all cases, directories and files should be protected. If persisting user data and preferences is a concern, this data should be synchronized with an external server.

Check Protection Status on Location
-----------------------------------
To verify whether a location is protected or not, call:

    [ASFFileProtectionManager isLocationProtected: directoryOrFilePath];

`directoryOrFilePath` should be a relative path to the desired location (e.g. "Documents/someFolder). It should NOT contain a beginning slash or the scheme. If `directoryOrFilePath` is determined to be invalid, an argument error will be thrown.

This will return YES if the location is explicitly protected or protected by a parent directory. It will return NO if a location is not protected.

When Files are Deleted
----------------------
If the integrity of the devices comes into question, then all the protected files are removed. Directories are NOT deleted.

Special Considerations
----------------------
The file protection API checks the protection status on all protected locations on each integrity check interval. ASFKit only checks the integrity if there is a valid session. If there is not active session, then the file system is not checked. Apps should NOT be downloading data if there is no active session.

Notifications
-------------

###ASFProtectedFilesWillBeRemovedNotification
Triggered when all the protected files are about to be deleted.

###ASFProtectedFilesWereRemovedNotification
Posted after the files protected by ASFFileProtectionManager are removed from the application's sandbox.

###ASFProtectionAttributeModifiedNotification
Triggered when the file protection attribute is modified on a file to NSFileProtectionComplete. This notification is posted for *every* file that is modified. The file's location can be found in the user info dictionary.