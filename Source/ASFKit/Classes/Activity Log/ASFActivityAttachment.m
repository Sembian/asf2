//
//  ASFActivityAttachment.m
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFActivityAttachment.h"

@implementation ASFActivityAttachment

- (instancetype) initWithData:(NSData *)data andContentType:(NSString *)contentType
{
    if (self = [self init]) {
        _data = data;
        _contentType = contentType;
    }
    return self;
}

+ (ASFActivityAttachment *) attachmentWithImage:(UIImage *)image
{
    return [ASFActivityAttachment attachmentWithData:UIImagePNGRepresentation(image) ofContentType:@"image/png"];
}

+ (ASFActivityAttachment *) attachmentWithImage:(UIImage *)image andCompressionQuality:(CGFloat)quality
{
    return [ASFActivityAttachment attachmentWithData:UIImageJPEGRepresentation(image, quality) ofContentType:@"image/jpeg"];
}

+ (ASFActivityAttachment *) attachmentWithData:(NSData *)data ofContentType:(NSString *)contentType
{
    return [[self alloc] initWithData:data andContentType:contentType];
}

@end
