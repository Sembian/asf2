//
//  ASFActivityLog.m
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFActivityLog_Private.h"
#import "ASFURLRequest.h"
#import "ASFActivityStatement.h"
#import "ASFActivityObject.h"
#import "ASFActivityLogQuery_Private.h"
#import "ASFActivityLogStatementQueue.h"
#import "ASFFileProtectionManager.h"
#import "ASFReachability.h"
#import "FLOAuthUtil.h"
#import "NSString+ASFHash.h"
#import "ASFAuthorization.h"
#import "ASFProfile.h"
#import "ASFActivityLogQueryResult_Private.h"
#import "ASFUtilities_Private.h"

NSString* ASFActivityLogQueueFlushedNotification = @"ASFActivityLogQueueFlushedNotification";
NSString* const ASFActivityLogConsentStatusUpdatedNotification = @"ASFActivityLogConsentStatusUpdatedNotification";
static NSString* kASFActivityLogUserConsent = @"ASFActivityLogUserConsent";
static NSString* kTermsHash = @"ASFActivityLogTermsHash";
static NSString* kTermsAccepted = @"ASFActivityLogTermsAccepted";
static NSString* ASFActivityLogDefaultQueueStore = @"queue.plist";

@implementation ASFActivityLog

#pragma mark - Public methods
- (instancetype) init
{
    if (self = [super init]) {
        _queue = [[ASFActivityLogStatementQueue alloc] initWithFileName:ASFActivityLogDefaultQueueStore];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onSessionStatusWillChange) name:ASFSessionStartedNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onSessionStatusWillChange) name:ASFSessionChangedNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onSessionStatusWillChange) name:ASFSessionWillExpireNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onSessionStatusWillChange) name:ASFSessionWillEndNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onProtectedFilesWillBeRemoved) name:ASFProtectedFilesWillBeRemovedNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReachabilityChanged:) name:kReachabilityChangedNotification object:nil];
        [self flushStatementQueue];
    }
    return self;
}

+ (ASFActivityStatement *) logActionForCurrentUser:(NSString *)verb withObjectName:(NSString *)objectName ofType:(NSString *)objectType completionHandler:(ASFActivityLogStoreCompletionHandler)completionHandler
{
    return [self logActionForCurrentUser:verb withObjectName:objectName ofType:objectType onTargetWithComponents:nil completionHandler:completionHandler];
}

+ (ASFActivityStatement *) logActionForCurrentUser:(NSString *)verb withObject:(ASFActivityObject *)object completionHandler:(ASFActivityLogStoreCompletionHandler)completionHandler
{
    return [self logActionForCurrentUser:verb withObject:object onTarget:nil completionHandler:completionHandler];
}

+ (ASFActivityStatement *) logActionForCurrentUser:(NSString *)verb withObject:(ASFActivityObject *)object withAttachment:(ASFActivityAttachment *)attachment completionHandler:(ASFActivityLogStoreCompletionHandler)completionHandler
{
    return [self logActionForCurrentUser:verb withObject:object onTarget:nil withAttachment:attachment completionHandler:completionHandler];
}

+ (ASFActivityStatement *) logActionForCurrentUser:(NSString *)verb withObjectName:(NSString *)objectName ofType:(NSString *)objectType onTargetWithName:(NSString *)targetName completionHandler:(ASFActivityLogStoreCompletionHandler)completionHandler
{
    return [self logActionForCurrentUser:verb withObjectName:objectName ofType:objectType onTargetWithComponents:@[targetName] completionHandler:completionHandler];
}

+ (ASFActivityStatement *) logActionForCurrentUser:(NSString *)verb withObjectName:(NSString *)objectName ofType:(NSString *)objectType onTargetWithComponents:(NSArray *)targetComponents completionHandler:(ASFActivityLogStoreCompletionHandler)completionHandler
{
    ASFActivityObject *object = [ASFActivityObject activityObjectWithDisplayName:objectName andType:objectType];
    return [self logActionForCurrentUser:verb withObject:object onTargetWithComponents:targetComponents completionHandler:completionHandler];
}

+ (ASFActivityStatement *) logActionForCurrentUser:(NSString *)verb withObject:(ASFActivityObject *)object onTargetWithComponents:(NSArray *)targetComponents completionHandler:(ASFActivityLogStoreCompletionHandler)completionHandler
{
    ASFActivityObject *target = [ASFActivityObject activityObjectWithComponents:targetComponents andType:@"context"];
    return [self logActionForCurrentUser:verb withObject:object onTarget:target completionHandler:completionHandler];
}

+ (ASFActivityStatement *) logActionForCurrentUser:(NSString *)verb withObject:(ASFActivityObject *)object onTarget:(ASFActivityObject *)target completionHandler:(ASFActivityLogStoreCompletionHandler)completionHandler
{
    return [self logActionForCurrentUser:verb withObject:object onTarget:target withAttachment:nil completionHandler:completionHandler];
}

+ (ASFActivityStatement *) logActionForCurrentUser:(NSString *)verb withObject:(ASFActivityObject *)object onTarget:(ASFActivityObject *)target withAttachment:(ASFActivityAttachment *)attachment completionHandler:(ASFActivityLogStoreCompletionHandler)completionHandler
{
    ASFActivityStatement *statement = [ASFActivityStatement statementForCurrentActorWithVerb:verb withObject:object onTarget:target];
    statement.published = [NSDate date];
    statement.attachment = attachment;
    [self logStatement:statement completionHandler:completionHandler];
    return statement;
}

+ (void) setDefaultTarget:(ASFActivityObject *)target
{
    [ASFActivityStatement setDefaultTarget:target];
}

+ (void) logStatement:(ASFActivityStatement *)statement completionHandler:(ASFActivityLogStoreCompletionHandler)completionHandler
{
    NSAssert(statement!=nil, @"You cannot log a nil statement!");
    [self logStatements:@[statement] completionHandler:completionHandler];
}

+ (void) logStatements:(NSArray *)statements completionHandler:(ASFActivityLogStoreCompletionHandler)completionHandler
{
    if (statements.count == 0) {
        return;
    }
    
    ASFURLRequest *request = [self prepareRequestWithCompletionHandler:^(id response, NSError *error) {
        ASFActivityLogStoreResult result = error == nil ? ASFActivityLogStoreResultPersisted : ASFActivityLogStoreResultFailed;
        
        // If this was NOT a client error, let's queue the statements
        if ([self shouldQueueStatementsForError:error]) {
            [[[self class] sharedActivityLog].queue addStatements:statements];
            NSLog(@"Statements were queued because %@", error.localizedDescription);
            result = ASFActivityLogStoreResultQueued;
            error = nil;
        }
        
        if (completionHandler) {
            NSArray *statementIds = nil;
            if ([response isKindOfClass:[NSDictionary class]]) {
                statementIds = response[@"statement_ids"];
            } else if (result == ASFActivityLogStoreResultQueued) {
                NSMutableArray *sIds = [NSMutableArray array];
                for (ASFActivityStatement *statement in statements) {
                    [sIds addObject:statement.statementId];
                }
                statementIds = [NSArray arrayWithArray:sIds];
            }
            completionHandler(result, statementIds, error);
        }
    }];
    [request setHTTPMethod:ASFURLRequestTypePOST];
    
    // TODO: Determine if there's a way to use NSJSONSerialization
    NSError *error = nil;
    NSMutableArray *statementDicts = [NSMutableArray array];
    for (ASFActivityStatement *statement in statements) {
        if (![statement validateWithError:&error]) {
            if (completionHandler) {
                completionHandler(ASFActivityLogStoreResultFailed, @[statement.statementId], error);
            }
            return;
        }
        [statementDicts addObject:[statement dictionary]];
    }
    
    [request setHTTPBody:[NSJSONSerialization dataWithJSONObject:statementDicts options:0 error:&error]];
    if (error) {
        if (completionHandler) {
            completionHandler(ASFActivityLogStoreResultFailed, nil, error);
        }
        return;
    }
    
    [self signAndSendRequest:request];
}

+ (void) countStatementsWithQuery:(ASFActivityLogQuery *)query completionHandler:(ASFActivityLogQueryCountCompletionHandler)completionHandler
{
    [self retrieveStatementsWithQuery:query completionHandler:^(ASFActivityLogQueryResult *result, NSError *error) {
        if (completionHandler) {
            completionHandler(result.totalResultCount, error);
        }
    }];
}

+ (void) retrieveStatementsWithQuery:(ASFActivityLogQuery *)query completionHandler:(ASFActivityLogQueryCompletionHandler)completionHandler
{
    if (!query) {
        if (completionHandler) {
            completionHandler(nil, nil);
        }
        return;
    }
    
    // Before querying for new statements, attempt to flush the statement queue.
    [self flushStatementQueue];
    ASFURLRequest *request = [self prepareQueryRequestWithCompletionHandler:completionHandler];
    
    // Update the URL with the query parameters
    request.URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@", request.URL.absoluteString, [query queryString]]];
    
    [self signAndSendRequest:request];
}

+ (void) retrieveMoreStatementsWithResult:(ASFActivityLogQueryResult *)result completionHandler:(ASFActivityLogQueryCompletionHandler)completionHandler
{
    if (!result.isMoreStatements) {
        if (completionHandler) {
            completionHandler(nil, nil);
        }
        return;
    }
    
    ASFURLRequest *request = [self prepareQueryRequestWithCompletionHandler:completionHandler];
    
    // Update the URL with the query parameters
    NSString *base = [NSString stringWithFormat:@"%@://%@", request.URL.scheme, request.URL.host];
    if (request.URL.port) {
        base = [base stringByAppendingFormat:@":%@", request.URL.port];
    }
    NSString *url = [base stringByAppendingString:result.more];
    request.URL = [NSURL URLWithString:url];
    
    [self signAndSendRequest:request];
}

+ (NSUInteger) flushStatementQueue
{
    return [[ASFActivityLog sharedActivityLog] flushStatementQueue];
}

+ (ASFActivityLog *) sharedActivityLog
{
    static ASFActivityLog *activityLog;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        activityLog = [ASFActivityLog new];
    });
    return activityLog;
}

+ (BOOL) currentUserHasGivenActivityLoggingConsent
{
    return [self currentConsentStatus] == ASFActivityLogUserConsentStatusAgreed;
}

+ (BOOL) activityLoggingIsAvailable
{
    return (!ASFKitConfiguration.activityLogPrivacyNotice.length || [self currentUserHasGivenActivityLoggingConsent]) && ASFKitConfiguration.activityLogApi;
}

#pragma mark - Private methods
+ (BOOL) shouldShowActivityLogPrivacyNotice
{
    BOOL shouldResetConsent = [[NSUserDefaults standardUserDefaults] boolForKey:ResetConsentSwitchKey];
    ASFActivityLogUserConsentStatus consentStatus = [ASFActivityLog currentConsentStatus];
    
    return ASFKitConfiguration.activityLogApi
    
            && ((consentStatus == ASFActivityLogUserConsentStatusUnknown && [UIApplication sharedApplication].protectedDataAvailable)
                || consentStatus == ASFActivityLogUserConsentStatusNeedsUpdating
                || shouldResetConsent)
    
            && [ASFSession sharedSession].authorization
    
            && ASFKitConfiguration.activityLogPrivacyNotice.length > 0;
}

+ (void) updateCurrentConsentStatus:(ASFActivityLogUserConsentStatus)status
{
    NSString *currentTerms = [ASFKitConfiguration.activityLogPrivacyNotice md5Hash];
    NSDictionary *consent = @{
                                    kTermsHash : currentTerms,
                                    kTermsAccepted : @(status == ASFActivityLogUserConsentStatusAgreed)
                                    };
    
    NSMutableDictionary *preferences = [NSMutableDictionary dictionaryWithContentsOfFile:[ASFUtilities frameworkPreferencesFile]];
    if (!preferences) {
        preferences = [NSMutableDictionary dictionary];
    }
    if (!preferences[kASFActivityLogUserConsent]) {
        preferences[kASFActivityLogUserConsent] = [NSMutableDictionary dictionary];
    }
    
    NSString *userIdentifier = [[ASFSession sharedSession] getSharedAuthorization].profile.upi;
    preferences[kASFActivityLogUserConsent][userIdentifier] = consent;
    [preferences writeToFile:[ASFUtilities frameworkPreferencesFile] atomically:YES];
    
    // Update NSUserDefaults
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:ResetConsentSwitchKey];
    [[NSUserDefaults standardUserDefaults] setValue:([self activityLoggingIsAvailable] ? ASFKitLocalizedString(@"ASFEnabled") : ASFKitLocalizedString(@"ASFDisabled")) forKey:ActivityLogStatusKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    // Dispatch notification
    [[NSNotificationCenter defaultCenter] postNotificationName:ASFActivityLogConsentStatusUpdatedNotification object:nil];
    
    // Create an activity log statement
    if (status == ASFActivityLogUserConsentStatusAgreed) {
        ASFActivityObject *object = [ASFActivityObject activityObjectWithComponents:@[@"terms",currentTerms] andType:@"terms"];
        object.displayName = @"the ASF Activity Log privacy terms";
        object.content = ASFKitConfiguration.activityLogPrivacyNotice;
        [self logActionForCurrentUser:@"accepted" withObject:object completionHandler:nil];
    }
}

+ (ASFActivityLogUserConsentStatus) currentConsentStatus
{
    NSString *userIdentifier = [[ASFSession sharedSession] getSharedAuthorization].profile.upi;
    NSString *currentTerms = [ASFKitConfiguration.activityLogPrivacyNotice md5Hash];
    NSDictionary *currentAuthorization = [NSDictionary dictionaryWithContentsOfFile:[ASFUtilities frameworkPreferencesFile]][kASFActivityLogUserConsent][userIdentifier];
    
    if (!currentAuthorization) {
        return ASFActivityLogUserConsentStatusUnknown;
    } else if (![currentAuthorization[kTermsAccepted] boolValue]) {
        return ASFActivityLogUserConsentStatusDisagreed;
    } else if (![currentAuthorization[kTermsHash] isEqualToString:currentTerms]) {
        return ASFActivityLogUserConsentStatusNeedsUpdating;
    } else {
        return ASFActivityLogUserConsentStatusAgreed;
    }
}

/// Determines whether statements should be queued in response to an error
/// @param error    The error that occurred when trying to persist to the server.
/// @return         YES if the statements should be persisted; NO otherwise.
/// @since 2.1
+ (BOOL) shouldQueueStatementsForError:(NSError *)error
{
    // Don't queue statements if there was no error
    if (!error) {
        return NO;
    }
    
    return
            // Don't queue statements if there was any validation error
            error.code / 1000 != 6
    
            // Don't queue statements if the server returned an error code in the 400 range (which is a client error)
            && error.code / 100 != 4;
}

///
/// Invoked when the session status is about to change (or has just changed).
///
- (void) onSessionStatusWillChange
{
    [self flushStatementQueue];
}

///
/// Invoked when protected files are about to be deleted (because the configuration profile has just been removed).
///
- (void) onProtectedFilesWillBeRemoved
{
    [self flushStatementQueue];
}

///
/// Invoked when the device's connection status changes.
///
- (void) onReachabilityChanged:(NSNotification *)notification
{
    ASFReachability *r = notification.object;
    if ([r currentReachabilityStatus] != NotReachable) {
        [self flushStatementQueue];
    }
}

///
/// Flushes (removes) all the statements stored in the queue and attempts to persist them to the server.
/// If they're unable to be persisted, they're added back into the queue.
/// @return The number of statements that were flushed (removed) from the local queue.
///
- (NSUInteger) flushStatementQueue
{
    NSArray *statements = [self.queue flush];
    
    if (!statements.count) {
        return 0;
    }
    
    NSLog(@"Flushing %zd statements from the queue...", statements.count);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[self class] logStatements:statements completionHandler:^(ASFActivityLogStoreResult result, NSArray *statements, NSError *error) {
            if (result == ASFActivityLogStoreResultPersisted) {
                [[NSNotificationCenter defaultCenter] postNotificationName:ASFActivityLogQueueFlushedNotification object:self userInfo:@{@"statements":statements}];
            }
        }];
    });
    return statements.count;
}

///
/// Prepares a query request to send to the server.
/// @param completionHandler The handler block to execute when the requests completes (either successfully or fails).
/// @return Returns the prepare ASFURLRequest object.
///
+ (ASFURLRequest *) prepareQueryRequestWithCompletionHandler:(ASFActivityLogQueryCompletionHandler)completionHandler
{
    return [self prepareRequestWithCompletionHandler:^(id response, NSError *error){
        ASFActivityLogQueryResult *result = [ASFActivityLogQueryResult objectFromDictionary:response];
        completionHandler(result, error);
    }];
}

///
/// Prepares a request for being sent to the server.
/// @param completionHandler The handler block to execute when the requests completes (either successfully or fails).
/// @return Returns the prepare ASFURLRequest object.
///
//+ (ASFURLRequest *) prepareRequestWithCompletionHandler:(ASFActivityLogCompletionHandler)completionHandler
+ (ASFURLRequest *) prepareRequestWithCompletionHandler:(void (^)(id response, NSError *error))completionHandler
{
    NSAssert(completionHandler != nil, @"A local completion handler MUST be specified.");
    
    NSError *error = nil;
    
    if (![ASFSession sharedSession].sessionStarted) {
        error = [ASFError errorWithCode:ASFErrorSessionInvalid];
        completionHandler(nil, error);
        return nil;
    }
    
    if (ASFKitConfiguration.activityLogPrivacyNotice.length && ![self currentUserHasGivenActivityLoggingConsent]) {
        error = [ASFError errorWithCode:ASFErrorActivityLogUserDisallowed];
        completionHandler(nil, error);
        return nil;
    }
    
    if (!ASFKitConfiguration.activityLogApi) {
        error = [ASFError errorWithCode:ASFErrorActivityLogDisabled];
        completionHandler(nil, error);
        return nil;
    }
    
    if (![ASFSession sharedSession].secretKey) {
        error = [ASFError errorWithCode:ASFErrorMissingSecretKey];
        completionHandler(nil, error);
        return nil;
    }
    
    NSURL *endpoint = [[ASFSession sharedSession].configurationProfileURL URLByAppendingPathComponent:ActivityLogURI];
    ASFURLRequest *request = [ASFURLRequest requestWithURL:endpoint completionHandler:^(ASFURLRequest *request, NSData *responseData, NSError *requestError) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSError *error = requestError;
            id response = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
            
            if (requestError) {
                if ([response isKindOfClass:[NSDictionary class]]) {
                    NSInteger errorCode = 0;
                    switch (error.code) {
                        case 409:
                            errorCode = ASFErrorActivityStatementConflict;
                            break;
                        case 403:
                            errorCode = ASFErrorActivityLogNoAccess;
                            break;
                        default:
                            errorCode = error.code;
                            break;
                    }
                    NSString *reason = response[@"name"];
                    error = [ASFError errorWithCode:errorCode andLocalizedFailureReason:reason andUnderlyingError:error];
                }
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                completionHandler(response, error);
            });
        });
    }];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    return request;
}

///
/// Signs the request (using the license key as the consumer key and the secret key as the consumer secret) and sends it to the server.
/// @param request The ASFURLRequest object to sign and execute.
///
+ (void) signAndSendRequest:(ASFURLRequest *)request
{
    [request setValue:ASFOAuthHeader(request.URL, [ASFURLRequest methodWithRequestType:request.HTTPMethod], request.HTTPBody, [ASFSession sharedSession].licenseKey, [ASFSession sharedSession].secretKey) forHTTPHeaderField:@"Authorization"];
    [request start];
}

@end
