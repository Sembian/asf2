//
//  ASFActivityLogQueryResult.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "SerializableNSObject.h"

@class ASFActivityStatement;

///
/// This class represents the result returned from the server after querying for statements (assuming the query was successful).
/// It contains the statements returned as well as a pointer for retrieving more statements following the same query (if there were more).
///
/// When querying for statements, your application should keep a reference to the result returned from
/// [ASFActivityLog retrieveStatementsWithQuery:completionHandler:] so that more statements can be retrieved later.
///
/// Keep in mind that the statements property contains only the statements return from the current request--
/// it does not include statements retrieved in previous requests. Your application must hold references to all statements
/// it wants to keep around.
///
/// @since 2.1
///
@interface ASFActivityLogQueryResult : SerializableNSObject

/// @name Accessing Statements from Response

///
/// The statements retrieved from the server.
/// @note This only contains the statements that were just retrieved--not statements retrieved in previous requests.
/// @since 2.1
///
@property (nonatomic, readonly) NSArray *statements;

///
/// Indicates whether there are more statements to be retrieved from the server.
/// @since 2.1
///
@property (nonatomic, readonly) BOOL isMoreStatements;

///
/// The total number of activity statements that match the supplied query.
/// Note that this value will likely NOT match the number of statements in the statements property.
/// This value represents the total number of statements that match the query, while statements contains the statements that were just retrieved.
/// @since 2.1
///
@property (nonatomic, readonly) NSUInteger totalResultCount;

@end
