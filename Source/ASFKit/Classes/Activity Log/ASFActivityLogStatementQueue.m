//
//  ASFActivityLogStatementQueue.m
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFActivityLogStatementQueue.h"
#import "ASFActivityStatement.h"
#import "ASFKitDevice.h"
#import "ASFUtilities_Private.h"

@implementation ASFActivityLogStatementQueue

- (instancetype) initWithFileName:(NSString *)filename
{
    NSURL *filepath = [[NSURL fileURLWithPath:[ASFUtilities frameworkDirectory]] URLByAppendingPathComponent:filename];
    if (self = [self initWithFilePath:filepath]) {
        
    }
    return self;
}

- (instancetype) initWithFilePath:(NSURL *)filepath
{
    if (self = [self init]) {
        _filepath = filepath;
        [self loadFromDisk];
    }
    return self;
}
- (void) addStatement:(ASFActivityStatement *)statement
{
    return [self addStatements:@[statement]];
}

- (void) addStatements:(NSArray *)statements
{
    NSError *error = nil;
    for (ASFActivityStatement *statement in statements) {
        if (![statement validateWithError:&error]) {
            return;
        }
    }
    
    _statements = [_statements arrayByAddingObjectsFromArray:statements];
    [self saveToDisk];
}

- (void) saveToDisk
{
    BOOL result = NO;
    // Verify that configuration profile is installed
    if ([ASFKitDevice isConfigurationProfileInstalled]) {
        result = [NSKeyedArchiver archiveRootObject:_statements toFile:self.filepath.path];
    }
    if (!result) {
        NSLog(@"Failed to write statements to disk.");
    }
}

- (NSArray *) flush
{
    if (self.numberOfStatements == 0) {
        return _statements;
    }
    
    NSArray *statements = _statements;
    _statements = [NSArray array];
    [self saveToDisk];
    return statements;
}

#pragma mark - Accessors
- (NSUInteger) numberOfStatements
{
    return _statements.count;
}

#pragma mark - Private methods
///
/// Loads statements from the queue's persistent store.
///
- (void) loadFromDisk
{
    _statements = [NSKeyedUnarchiver unarchiveObjectWithFile:self.filepath.path];
    if (!_statements) {
        _statements = [NSArray array];
    }
}

@end
