//
//  ASFActivityLogQueryResult.m
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFActivityLogQueryResult.h"
#import "ASFActivityStatement.h"

@interface ASFActivityLogQueryResult ()
@property (nonatomic, strong) NSArray *statements;
@property (nonatomic, readwrite) NSInteger nextPage;
@property (nonatomic, readwrite) NSUInteger totalResultCount;
@property (nonatomic, readwrite) NSString *more;
@end

@implementation ASFActivityLogQueryResult

#pragma mark - Public methods

#pragma mark - Accessors
- (BOOL) isMoreStatements
{
    return self.more != nil;
}

#pragma mark - Serialization
- (void) setSerializedValue:(id)value forProperty:(NSString *)key
{
    if ([key isEqualToString:@"statements"]) {
        if ([value isKindOfClass:[NSArray class]]) {
            NSMutableArray *parsedStatements = [NSMutableArray array];
            for (NSDictionary *statementData in value) {
                ASFActivityStatement *statement = [ASFActivityStatement objectFromDictionary:statementData];
                [parsedStatements addObject:statement];
            }
            value = [NSArray arrayWithArray:parsedStatements];
        }
    }
    [super setSerializedValue:value forProperty:key];
}

@end
