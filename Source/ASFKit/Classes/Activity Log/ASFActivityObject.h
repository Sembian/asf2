//
//  ASFActivityObject.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "SerializableNSObject.h"

///
/// An ASFActivityStatement may contain multiple objects--the statement object, the statement target, the statement provider, and the statement generator.
/// @since 2.1
///
@interface ASFActivityObject : SerializableNSObject

/// @name Creating an Object

///
/// Creates an activity object.
///
/// @param objectId A unique identifier for the object.
/// @param displayName The display name for the receiver of the action (e.g. "a note").
/// @param objectType The type (or "category") for the receiver of the action (e.g. "note").
/// @return An instantiated activity object.
/// @since 2.1
///
- (instancetype) initWithObjectId:(NSURL *)objectId andDisplayName:(NSString *)displayName withType:(NSString *)objectType;

///
/// Creates an activity object.
///
/// @param components An array describing a hierarchy for the object's id. The components are concatenated for the object id. The last object is used as the display name.
/// @param objectType The type (or "category") for the receiver of the action (e.g. "note").
/// @return An instantiated activity object.
/// @since 2.1
///
+ (ASFActivityObject *) activityObjectWithComponents:(NSArray *)components andType:(NSString *)objectType;

///
/// Creates an activity object.
///
/// @param displayName The display name for the receiver of the action (e.g. "a note").
/// @param objectType The type (or "category") for the receiver of the action (e.g. "note").
/// @return An instantiated activity object.
/// @since 2.1
///
+ (ASFActivityObject *) activityObjectWithDisplayName:(NSString *)displayName andType:(NSString *)objectType;

/// @name Properties of an Object

///
/// A unique identifier for the object (required).
/// You may customize this identifier, but some initializers will generate it based on the displayName.
/// If multiple statements reference the same object (e.g. creating a note and then updating a note), the object ID in all statements should be identical.
/// If you need to reference the object in your own application, use this.
/// @since 2.1
///
@property (nonatomic, strong) NSURL *objectId;

///
/// The author of the object. Useful if the object is created by someone other than the statement actor.
/// @since 2.1
///
@property (nonatomic, strong) NSString *author;

///
/// Content of the object.
/// @since 2.1
///
@property (nonatomic, strong) NSString *content;

///
/// A display name for the object.
/// @since 2.1
///
@property (nonatomic, strong) NSString *displayName;

///
/// A category/type for the object (required).
/// @since 2.1
///
@property (nonatomic, strong) NSString *objectType;

///
/// Natural-language summarization of the object encoded as a single JSON String containing HTML markup. Visual elements such as thumbnail images MAY be included.
/// @since 2.1
///
@property (nonatomic, strong) NSString *summary;

///
/// The date/time the object was stored on the server.
/// @since 2.1
///
@property (nonatomic, strong, readonly) NSDate *updated;

///
/// An IRI for the resource.
/// @since 2.1
///
@property (nonatomic, strong) NSURL *url;

@end
