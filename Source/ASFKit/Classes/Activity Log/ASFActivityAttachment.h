//
//  ASFActivityAttachment.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "SerializableNSObject.h"

/// ASFActivityAttachament represents a raw data attachment to an ASFActivityStatement.
/// Each statement may specify up to 1mb of raw attachment data. The data can be anything--an image, a JSON payload, etc.
/// When specifying an attachment, you must also specify the content type (e.g. image/png or application/json) so that
/// other applications may understand what type of data has been stored.
/// @since 2.1
@interface ASFActivityAttachment : SerializableNSObject

///
/// The content type (or "MIME type") of the attachment.
/// For more information about what to place here, visit http://en.wikipedia.org/wiki/Internet_media_type
/// @since 2.1
///
@property (nonatomic, strong) NSString *contentType;

///
/// The raw data. Cannot be more than 1mb.
/// @since 2.1
///
@property (nonatomic, strong) NSData *data;

///
/// Designated initializer. Initializes the attachment with the specified data and content type.
/// @param data         The raw data to store in the attachment.
/// @param contentType  The content type of data.
/// @since 2.1
///
- (instancetype) initWithData:(NSData *)data andContentType:(NSString *)contentType;

///
/// Creates an attachment with the specified image (as a png image).
/// Sets the content type to image/png.
/// @param image    The UIImage to use as the attachment.
/// @note PNG images are *often* much larger than JPEG images; you may need to resize the UIImage before using as a PNG.
/// @since 2.1
///
+ (ASFActivityAttachment *) attachmentWithImage:(UIImage *)image;

///
/// Creates an attachment with the specified image (as a png image).
/// Sets the content type to image/png.
/// @param image    The UIImage to use as the attachment.
/// @param quality  The compression quality to use when determining the JPEG data.
/// @note The higher the compression quality, the higher the image size will be.
/// @since 2.1
///
+ (ASFActivityAttachment *) attachmentWithImage:(UIImage *)image andCompressionQuality:(CGFloat)quality;

///
/// Creates an attachment with the specified data and content type.
/// @param data         The raw data to store in the attachment.
/// @param contentType  The content type of data.
/// @since 2.1
///
+ (ASFActivityAttachment *) attachmentWithData:(NSData *)data ofContentType:(NSString *)contentType;

@end
