//
//  ASFActivityLogConsentManager.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 Responsible for displaying a prompt (UIAlertView) to the user to receive the user's consent for tracking activity with the Activity Log API.
 This class serves as the UIAlertViewDelegate to handle the user's response to the prompt for tracking his/her activity.
 @since 2.1
 */
@interface ASFActivityLogConsentManager : NSObject <UIAlertViewDelegate>

/**
 Returns the shared ASFActivityLogConsentManager for the application.
 This always returns the same object throughout the application lifecycle.
 @return The default ASFActivityLogConsentManager for the application.
 */
+ (ASFActivityLogConsentManager *) defaultManager;

/**
 Convenience method to use the defaultManager and display the consent prompt (if there is one to be shown).
 This will only display the consent prompt if the privacyNotice has been set on ASFConfiguration and if the user has not already responded to the consent prompt.
 @see [ASFActivityLog shouldShowActivityLogPrivacyNotice]
 */
+ (void) displayConsentPromptIfNeeded;

@end
