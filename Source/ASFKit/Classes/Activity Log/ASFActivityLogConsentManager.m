//
//  ASFActivityLogConsentManager.m
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFActivityLogConsentManager.h"
#import "ASFActivityLog_Private.h"
#import "ASFUtilities_Private.h"

@implementation ASFActivityLogConsentManager
{
    UIAlertView *currentPrompt;
}

- (id) init
{
    if (self = [super init]) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sessionDidEndOrChange:) name:ASFSessionEndedNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sessionDidEndOrChange:) name:ASFSessionExpiredNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sessionDidEndOrChange:) name:ASFSessionChangedNotification object:nil];
    }
    return self;
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Public methods
+ (ASFActivityLogConsentManager *) defaultManager
{
    static ASFActivityLogConsentManager *defaultManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        defaultManager = [self new];
    });
    return defaultManager;
}

+ (void) displayConsentPromptIfNeeded
{
    [[self defaultManager] displayConsentPromptIfNeeded];
}

#pragma mark - Private methods
/**
 Invoked when the session changes or ends.
 */
- (void) sessionDidEndOrChange:(NSNotification *)notification
{
    [self dismissConsentPrompt];
}

/**
 Displays the consent prompt (if there is one to be shown).
 This will only display the consent prompt if the privacyNotice has been set on ASFConfiguration and if the user has not already responded to the consent prompt.
 @see [ASFActivityLog shouldShowActivityLogPrivacyNotice]
 */
- (void) displayConsentPromptIfNeeded
{
    if ([ASFActivityLog shouldShowActivityLogPrivacyNotice] && !currentPrompt) {
        currentPrompt = [[self class] consentPromptWithPrivacyNotice:ASFKitConfiguration.activityLogPrivacyNotice delegate:self];
        [currentPrompt show];
    }
}

/**
 Dismisses the current consent prompt (if there is one).
 */
- (void) dismissConsentPrompt
{
    if (currentPrompt) {
        [currentPrompt dismissWithClickedButtonIndex:-1 animated:NO];
    }
}

/**
 Creates an alert view containing the application name and the specified privacyNotice.
 @param privacyNotice   The privacy notice to display on the prompt.
 @param delegate        The delegate handling the user response.
 @return                A UIAlertView configured with the application name as the title, the privacy notice as the message, and the specified delegate as the delegate.
 */
+ (UIAlertView *) consentPromptWithPrivacyNotice:(NSString *)privacyNotice delegate:(id<UIAlertViewDelegate>)delegate
{
    return [[UIAlertView alloc] initWithTitle:[ASFUtilities applicationDisplayName]
                                      message:privacyNotice
                                     delegate:delegate
                            cancelButtonTitle:ASFKitLocalizedString(@"ASFDeny")
                            otherButtonTitles:ASFKitLocalizedString(@"ASFOK"), nil];
}

#pragma mark - UIAlertView delegate
- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    ASFActivityLogUserConsentStatus consent = buttonIndex == alertView.cancelButtonIndex ? ASFActivityLogUserConsentStatusDisagreed : ASFActivityLogUserConsentStatusAgreed;
    [ASFActivityLog updateCurrentConsentStatus:consent];
}

- (void) alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView == currentPrompt) {
        currentPrompt = nil;
    }
}

@end
