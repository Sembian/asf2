//
//  ASFActivityLogStatementQueue.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ASFActivityStatement;

///
/// The ASFActivityLogStatementQueue class provides the ability for ASFActivityLog to locally store statements that could not be persisted to the server.
///
/// @since 2.1
///
@interface ASFActivityLogStatementQueue : NSObject

/// @name Statement Queue Properties
///
/// The filepath where the queue's persistent store is located.
/// @since 2.1
///
@property (nonatomic, readonly) NSURL *filepath;

///
/// The statements in the queue.
/// @since 2.1
///
@property (nonatomic, readonly) NSArray *statements;

///
/// The number of statements in the queue.
/// @since 2.1
///
@property (nonatomic, readonly) NSUInteger numberOfStatements;

/// @name Creating a Statement Queue
///
/// Instantiates a statement queue by creating a store in the caches directory with the specified filename.
///
/// @param filename The filename for the queue; stored in the application's caches directory.
/// @return The instantiated statement queue.
/// @since 2.1
///
- (instancetype) initWithFileName:(NSString *)filename;

///
/// Instantiates a statement queue by creating a store at the specified filepath.
///
/// @param filepath The filepath for the queue store.
/// @return The instantiate statement queue.
/// @since 2.1
///
- (instancetype) initWithFilePath:(NSURL *)filepath;

/// @name Adding Statements to a Queue
///
/// Adds a statement to the queue.
/// The statement is validated before being added to the queue. If it fails, it is thrown away.
///
/// @param statement The statement to add to the queue.
/// @since 2.1
///
- (void) addStatement:(ASFActivityStatement *)statement;

///
/// Adds an array of statements to the queue.
/// Validates each statement before being added to the queue. If ANY statement fails, all statements are thrown away.
///
/// @param statements The statements to add to the queue.
/// @since 2.1
///
- (void) addStatements:(NSArray *)statements;

/// @name Persisting a Queue
///
/// Saves the queue to it's persistent store.
/// @since 2.1
///
- (void) saveToDisk;

///
/// Removes all statements in the persistent store (for the purpose of attempting to persist all of them to the server).
/// @return The statements that were in the queue.
/// @since 2.1
///
- (NSArray *) flush;

@end
