//
//  ASFActivityObject.m
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFActivityObject.h"

@implementation ASFActivityObject

- (instancetype) initWithObjectId:(NSURL *)objectId andDisplayName:(NSString *)displayName withType:(NSString *)objectType
{
    if (self = [self init]) {
        self.objectId = objectId;
        self.displayName = displayName;
        self.objectType = objectType;
    }
    return self;
}

+ (ASFActivityObject *) activityObjectWithComponents:(NSArray *)components andType:(NSString *)objectType
{
    if (components.count == 0) {
        return nil;
    }
    
    NSString *displayName = [components lastObject];
    NSURL *objectId = [self objectIdForCurrentAppWithComponents:components];
    ASFActivityObject *object = [[self alloc] initWithObjectId:objectId andDisplayName:displayName withType:objectType];
    return object;
    
}
+ (ASFActivityObject *) activityObjectWithDisplayName:(NSString *)displayName andType:(NSString *)objectType
{
    NSURL *objectId = displayName.length ? [self objectIdForCurrentAppFromDisplayName:displayName andContext:@"object"] : nil;
    ASFActivityObject *object = [[self alloc] initWithObjectId:objectId andDisplayName:displayName withType:objectType];
    return object;
}
- (NSString *) description
{
    if (self.displayName) {
        return self.displayName;
    } else {
        return self.objectId.absoluteString;
    }
}

#pragma mark - Object serialization
- (NSDictionary *) propertyMapping
{
    return @{@"id": @"objectId"};
}

#pragma mark - Private methods
///
/// Creates an object ID from an array.
/// Concatenates the objects into a path and prepends the application's URL scheme as the protocol.
/// @param components The components to concatenate into the object ID.
/// @return The prepared object ID.
///
+ (NSURL *) objectIdForCurrentAppWithComponents:(NSArray *)components {
    NSAssert(components.count > 0, @"components cannot be empty");
    
    NSString *scheme = [ASFSession sharedSession].urlSchemes.firstObject;
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@://%@", scheme, [[self class] identiferForString:[components componentsJoinedByString:@"/"]]]];
    return url;
}

///
/// Creates an object ID using a context and the display name.
/// The object ID is prepended by the application's URL scheme as the protocol. The context and display name are concatenated into the path of the object ID.
/// For example: scheme://context/display-name
/// @param displayName The display name used for the object.
/// @param context A context to use when creating the object ID.
/// @return The prepared object ID.
///
+ (NSURL *) objectIdForCurrentAppFromDisplayName:(NSString *)displayName andContext:(NSString *)context
{
    NSString *identifier = [self identiferForString:displayName];
    return [self objectIdForCurrentAppWithComponents:@[context, identifier]];
}

///
/// Adjusts a string into a URL-compatible string.
/// Converts to lower case, replaces spaces with dashes, and adds percent encoding.
/// @param string The string to covert.
/// @return The converted string. "Display Name" would produce "display-name".
///
+ (NSString *) identiferForString:(NSString *)string
{
    return [[[string stringByReplacingOccurrencesOfString:@" " withString:@"-"] lowercaseString] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

@end
