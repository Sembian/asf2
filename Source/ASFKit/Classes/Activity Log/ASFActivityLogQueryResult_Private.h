//
//  ASFActivityLogQueryResult_Private.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFActivityLogQueryResult.h"

@interface ASFActivityLogQueryResult ()

///
/// The path to request from the server (need to supply host name) to retrieve more statements matching the query used to receive this result.
/// @since 2.1
///
@property (nonatomic, readonly) NSString *more;

@end
