//
//  ASFActivityStatement.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "SerializableNSObject.h"

@class ASFActivityObject, ASFActivityActor, ASFActivityAttachment;

///
/// The ASFActivityStatement class represents a single activity that a user has performed.
///
/// Statements are made up of an actor, a verb, and an object ("John Doe created a note"):
///
/// * The actor describes the entity that performed the activity--which in our case is always the user currently logged into the security framework.
/// * The verb describes the action of the activity (e.g. "created" or "selected"). Verbs should typically be in the past tense because each activity statement is intended to describe an activity that has already taken place.
/// * The object describes the receiver of the action (e.g. "a note"). Objects are represented by an instance of ASFActivityObject and may have additional properties such as a URL or additional content.
///
/// An activity statement may also optionally reference a target for the activity. The target describes the context in which the activity occurred.
/// This may reference a specific page of a document or a location with in the application (e.g. Module 1). The target is also an instance of ASFActivityObject.
///
/// Activity statements also reference a "provider" and a "generator." These properties are set automatically and cannot be changed.
/// The provider describes the application the user used to perform the action. The generator describes the device on which the action occurred.
///
/// @see http://activitystrea.ms/specs/json/1.0/
/// @see ASFActivityLog
/// @since 2.1
///
@interface ASFActivityStatement : SerializableNSObject

/// @name Creating an Activity Statement

///
/// Creates a new activity statement.
///
/// @param verb The verb describes the action of the activity (e.g. "created" or "selected"). Verbs should typically be in the past tense because each activity statement is intended to describe an activity that has already taken place.
/// @param object The object describes the receiver of the action (e.g. "a note"). Objects are represented by an instance of ASFActivityObject and may have additional properties such as a URL or additional content.
/// @param target The target describes the context in which the activity occurred. This may reference a specific page of a document or a location with in the application (e.g. Module 1).
/// @return The generated statement.
/// @since 2.1
///
- (instancetype) initWithVerb:(NSString *)verb andObject:(ASFActivityObject *)object forTarget:(ASFActivityObject *)target;

///
/// Creates a new activity statement.
///
/// @param verb The verb describes the action of the activity (e.g. "created" or "selected"). Verbs should typically be in the past tense because each activity statement is intended to describe an activity that has already taken place.
/// @param object The object describes the receiver of the action (e.g. "a note"). Objects are represented by an instance of ASFActivityObject and may have additional properties such as a URL or additional content.
/// @return The generated statement.
/// @since 2.1
///
+ (ASFActivityStatement *) statementForCurrentActorWithVerb:(NSString *)verb withObject:(ASFActivityObject *)object;

///
/// Creates a new activity statement.
///
/// @param verb The verb describes the action of the activity (e.g. "created" or "selected"). Verbs should typically be in the past tense because each activity statement is intended to describe an activity that has already taken place.
/// @param object The object describes the receiver of the action (e.g. "a note"). Objects are represented by an instance of ASFActivityObject and may have additional properties such as a URL or additional content.
/// @param target The target describes the context in which the activity occurred. This may reference a specific page of a document or a location with in the application (e.g. Module 1).
/// @return The generated statement.
/// @since 2.1
///
+ (ASFActivityStatement *) statementForCurrentActorWithVerb:(NSString *)verb withObject:(ASFActivityObject *)object onTarget:(ASFActivityObject *)target;

/// @name Validating Statements

///
/// Validates a statement to ensure that it contains the required data (an actor, a verb, and an object).
///
/// @param error On return, contains the error that prevented the statement from validating.
/// @return YES if the statement is valid for sending to the server; NO if did not pass validation.
/// @note A statement MAY pass validation but still be rejected by the server. This validation is rudimentary just to ensure that the required information has been specified, not that it is valid.
/// @since 2.1
///
- (BOOL) validateWithError:(NSError **)error;

/// @name Setting Statement Defaults

///
/// Set the default target for all future activity statements (unless the statement specifies it's own target).
///
/// @param target The ASFActivityObject representing the target of all future activity statements.
/// @since 2.1
///
+ (void) setDefaultTarget:(ASFActivityObject *)target;

/// @name Statement Properties

///
/// A unique identifier for the statement. It MUST be absolutely unique.
/// It is generated when the object is instantiated.
/// @since 2.1
///
@property (nonatomic, strong, readonly) NSURL *statementId;

///
/// The actor describes the entity that performed the activity--which in our case is always the user currently logged into the security framework.
/// @since 2.1
///
@property (nonatomic, readonly) ASFActivityActor *actor;

///
/// The verb describes the action of the activity (e.g. "created" or "selected").
/// Verbs should typically be in the past tense because each activity statement is intended to describe an activity that has already taken place.
/// @since 2.1
///
@property (nonatomic, strong) NSString *verb;

///
/// The date/time the activity statement was created (which should represent the date/time the activity occurred).
/// @since 2.1
///
@property (nonatomic, strong) NSDate *published;

///
/// The date/time the activity statement was stored on the server.
/// @since 2.1
///
@property (nonatomic, strong, readonly) NSDate *updated;

///
/// The object describes the receiver of the action (e.g. "a note").
/// Objects are represented by an instance of ASFActivityObject and may have additional properties such as a URL or additional content.
/// @since 2.1
///
@property (nonatomic, strong) ASFActivityObject *object;

///
/// The target describes the context in which the activity occurred.
/// This may reference a specific page of a document or a location with in the application (e.g. Module 1). The target is also an instance of ASFActivityObject.
/// @since 2.1
///
@property (nonatomic, strong) ASFActivityObject *target;

///
/// The provider describes the application the user used to perform the action.
/// This property is automatically generated and should not be changed.
/// @since 2.1
///
@property (nonatomic, strong, readonly) ASFActivityObject *provider;

///
/// The generator describes the device on which the action occurred.
/// This property is automatically generated and should not be changed.
/// @since 2.1
///
@property (nonatomic, strong, readonly) ASFActivityObject *generator;

///
/// A display title for the statement. This will be generated automatically by the server after persisting.
/// @since 2.1
///
@property (nonatomic, strong) NSString *title;

///
/// Natural-language description of the activity encoded as a single JSON String containing HTML markup. Visual elements such as thumbnail images MAY be included.
/// @since 2.1
///
@property (nonatomic, strong) NSString *content;

///
/// Raw data (e.g. an image or JSON payload) that should be attached to this statement.
/// @since 2.1
///
@property (nonatomic, strong) ASFActivityAttachment *attachment;

@end
