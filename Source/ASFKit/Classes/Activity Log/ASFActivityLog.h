//
//  ASFActivityLog.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import <Foundation/Foundation.h>

/// @name Logging Activity Statements
/// Notification is posted when the queue has successfully been persisted to the server.
const NSString* ASFActivityLogQueueFlushedNotification;

@class ASFActivityStatement, ASFActivityLogStatementQueue, ASFActivityObject, ASFActivityAttachment, ASFActivityLogQuery, ASFActivityLogQueryResult;

///
/// Represents the result of a request to store/log statements.
///
typedef NS_ENUM(NSInteger, ASFActivityLogStoreResult) {
    /// The request failed; the statements have not been stored.
    ASFActivityLogStoreResultFailed,
    /// The request succeeded; the statements have been stored on the server.
    ASFActivityLogStoreResultPersisted,
    /// The statements could not be persisted, but have been queued for later.
    ASFActivityLogStoreResultQueued
};

typedef void (^ASFActivityLogStoreCompletionHandler)(ASFActivityLogStoreResult result, NSArray *statementIds, NSError *error);
typedef void (^ASFActivityLogQueryCompletionHandler)(ASFActivityLogQueryResult *result, NSError *error);
typedef void (^ASFActivityLogQueryCountCompletionHandler)(NSUInteger count, NSError *error);

///
/// The ASFActivityLog class offers the ability to quickly log user activity and remotely store them.
/// Each activity is logged as a "statement." Statements are made up of an actor, a verb, and an object ("John Doe created a note"):
///
/// * The actor describes the entity that performed the activity--which in our case is always the user currently logged into the security framework.
/// * The verb describes the action of the activity (e.g. "created" or "selected"). Verbs should typically be in the past tense because each activity statement is intended to describe an activity that has already taken place.
/// * The object describes the receiver of the action (e.g. "a note"). Objects are represented by an instance of ASFActivityObject and may have additional properties such as a URL or additional content.
///
/// For example:
/// "John Doe downloaded a photo"
///
/// * **Actor:** John Doe (provided automatically)
/// * **Verb:** downloaded
/// * **Object:** "a photo"
///
/// The simplest way to create an activity statement is to do the following:
///
///    [ASFActivityLog logActionForCurrentUser:@"downloaded" withObjectName:@"a photo" ofType:@"image" completionHandler:completionHandler];
///
/// *John Doe downloaded a photo*
///
/// The object type ("image", in this case) provides a way to "categorize" the object of the activity. The object type should typically be a single noun (e.g. image, note, question, etc.).
///
/// You may also create more advanced statements by specifying more details about the object:
///
///    ASFActivityObject *object = [ASFActivityObject activityObjectWithDisplayName:@"a photo" andType:@"image"];
///    object.content = @"This could be an image caption.";
///    object.url = [NSURL URLWithString:@"http://example.com/images/photo.jpg"];
///    [ASFActivityLog logActionForCurrentUser:@"downloaded" withObject:object completionHandler:completionHandler];
///
/// *John Doe downloaded a photo ("This could be an image caption.")*
///
/// You may also attach raw data to the statement:
///
///    ASFActivityObject *object = [ASFActivityObject activityObjectWithDisplayName:@"a photo" andType:@"image"];
///    ASFActivityAttachment *attachment = [ASFActivityAttachment attachmentWithData:UIImageJPEGRepresentation(image, 40) ofContentType:@"image/jpeg"]
///    [ASFActivityLog logActionForCurrentUser:@"downloaded" withObject:object withAttachment:attachment completionHandler:completionHandler];
///
/// *John Doe downloaded a photo (attachment)*
///
/// The raw data may be anything you want--up to 1024kb of data. This is not intended for use as "cloud storage" but rather to add proprietary information (like an image, or JSON data) to an activity statement.
///
/// Reference ASFActivityObject for the other properties that are available to be set on the activity statement object.
///
/// An activity statement may also optionally reference a target for the activity. The target describes the context in which the activity occurred.
/// This may reference a specific page of a document or a location with in the application (e.g. Module 1). The target is also an instance of ASFActivityObject.
///
/// ASFActivityLog provides a convenient way to set the target of the activity by specifying an array describing the user's current context.
/// Let's say you want to log an activity for  a user creating a note on the fourth page of document.pdf within Module 1. You could do the following:
///
///    ASFActivityObject *object = [ASFActivityObject activityObjectWithDisplayName:@"a note" andType:@"note"];
///    object.content = @"This would contain the note's contents.";
///    [ASFActivityLog logActionForCurrentUser:@"created" withObject:object onTargetWithComponents:@[@"Module 1", @"document.pdf", @"Page 4"] completionHandler:completionHandler];
///
/// *John Doe created a note ("This would contain the note's contents") on Module 1/document.pdf/Page 4*
///
/// When logging an activity statement, ASFActivityLog attempts to immediately persist it on the server.
/// If the statement cannot be persisted (because the device is offline or the server is unavailable), the statement is stored locally until it can be persisted on the server.
///
/// Activity statements also reference a "provider" and a "generator." These properties are set automatically and cannot be changed.
/// The provider describes the application the user used to perform the action. The generator describes the device on which the action occurred.
///
/// @see ASFActivityStatement
/// @see ASFActivityObject
/// @see http://activitystrea.ms/specs/json/1.0/
/// @since 2.1
///
@interface ASFActivityLog : NSObject

/// @name Checking if Activity Log is Available

///
/// Checks if the current user has consented to the tracking of his/her activity within the application.
/// Users are asked the first time they log in if they will allow the application to track their activity.
/// If the user has NOT permitted tracking, any attempts to log activity will immediately fail.
/// @return YES if the current user has authrized activity logging on this device; NO otherwise.
/// @since 2.1
///
+ (BOOL) currentUserHasGivenActivityLoggingConsent;

///
/// Checks whether activity logging is available. This returns NO if either the user has not consented to activity logging OR if activity logging has been remotely disabled for this application.
/// If activity logging is NOT available, any attempts to log activity will immediately fail.
/// @return YES if activity logging is available; NO otherwise.
/// @since 2.1
///
+ (BOOL) activityLoggingIsAvailable;

/// @name Logging Activity Statements

///
/// Log an activity performed by the current user.
///
/// @param verb The verb describes the action of the activity (e.g. "created" or "selected"). Verbs should typically be in the past tense because each activity statement is intended to describe an activity that has already taken place.
/// @param objectName The display name for the receiver of the action (e.g. "a note").
/// @param objectType The type (or "category") for the receiver of the action (e.g. "note").
/// @param completionHandler The completion handler block to execute when the statement has succesfully persisted to the server or has been stored in the queue. It also gets executed when the statement fails to persist. A statement could fail to persist if it is missing required data, if the activity log has been disabled, or if the user has not consented to tracking his/her activity.
/// @return The generated statement that has been sent to the server.
/// @since 2.1
///
+ (ASFActivityStatement *) logActionForCurrentUser:(NSString *)verb withObjectName:(NSString *)objectName ofType:(NSString *)objectType completionHandler:(ASFActivityLogStoreCompletionHandler)completionHandler;

///
/// Log an activity performed by the current user.
///
/// @param verb The verb describes the action of the activity (e.g. "created" or "selected"). Verbs should typically be in the past tense because each activity statement is intended to describe an activity that has already taken place.
/// @param object The object describes the receiver of the action (e.g. "a note"). Objects are represented by an instance of ASFActivityObject and may have additional properties such as a URL or additional content.
/// @param completionHandler The completion handler block to execute when the statement has succesfully persisted to the server or has been stored in the queue. It also gets executed when the statement fails to persist. A statement could fail to persist if it is missing required data, if the activity log has been disabled, or if the user has not consented to tracking his/her activity.
/// @return The generated statement that has been sent to the server.
/// @since 2.1
///
+ (ASFActivityStatement *) logActionForCurrentUser:(NSString *)verb withObject:(ASFActivityObject *)object completionHandler:(ASFActivityLogStoreCompletionHandler)completionHandler;

///
/// Log an activity performed by the current user with an attachment.
///
/// @param verb The verb describes the action of the activity (e.g. "created" or "selected"). Verbs should typically be in the past tense because each activity statement is intended to describe an activity that has already taken place.
/// @param object The object describes the receiver of the action (e.g. "a note"). Objects are represented by an instance of ASFActivityObject and may have additional properties such as a URL or additional content.
/// @param attachment Raw data that may be attached to the statement. Maximum of 1mb.
/// @param completionHandler The completion handler block to execute when the statement has succesfully persisted to the server or has been stored in the queue. It also gets executed when the statement fails to persist. A statement could fail to persist if it is missing required data, if the activity log has been disabled, or if the user has not consented to tracking his/her activity.
/// @return The generated statement that has been sent to the server.
/// @since 2.1
///
+ (ASFActivityStatement *) logActionForCurrentUser:(NSString *)verb withObject:(ASFActivityObject *)object withAttachment:(ASFActivityAttachment *)attachment completionHandler:(ASFActivityLogStoreCompletionHandler)completionHandler;

///
/// Log an activity performed by the current user.
///
/// @param verb The verb describes the action of the activity (e.g. "created" or "selected"). Verbs should typically be in the past tense because each activity statement is intended to describe an activity that has already taken place.
/// @param objectName The display name for the receiver of the action (e.g. "a note").
/// @param objectType The type (or "category") for the receiver of the action (e.g. "note").
/// @param targetName The display name for the context of the activity.
/// @param completionHandler The completion handler block to execute when the statement has succesfully persisted to the server or has been stored in the queue. It also gets executed when the statement fails to persist. A statement could fail to persist if it is missing required data, if the activity log has been disabled, or if the user has not consented to tracking his/her activity.
/// @return The generated statement that has been sent to the server.
/// @since 2.1
///
+ (ASFActivityStatement *) logActionForCurrentUser:(NSString *)verb withObjectName:(NSString *)objectName ofType:(NSString *)objectType onTargetWithName:(NSString *)targetName completionHandler:(ASFActivityLogStoreCompletionHandler)completionHandler;

///
/// Log an activity performed by the current user.
///
/// @param verb The verb describes the action of the activity (e.g. "created" or "selected"). Verbs should typically be in the past tense because each activity statement is intended to describe an activity that has already taken place.
/// @param objectName The display name for the receiver of the action (e.g. "a note").
/// @param objectType The type (or "category") for the receiver of the action (e.g. "note").
/// @param targetComponents An array describing the hierarchy of the user's current context (e.g. @[@"Module 1", @"document.pdf", @"Page 4"]).
/// @param completionHandler The completion handler block to execute when the statement has succesfully persisted to the server or has been stored in the queue. It also gets executed when the statement fails to persist. A statement could fail to persist if it is missing required data, if the activity log has been disabled, or if the user has not consented to tracking his/her activity.
/// @return The generated statement that has been sent to the server.
/// @since 2.1
///
+ (ASFActivityStatement *) logActionForCurrentUser:(NSString *)verb withObjectName:(NSString *)objectName ofType:(NSString *)objectType onTargetWithComponents:(NSArray *)targetComponents completionHandler:(ASFActivityLogStoreCompletionHandler)completionHandler;

///
/// Log an activity performed by the current user.
///
/// @param verb The verb describes the action of the activity (e.g. "created" or "selected"). Verbs should typically be in the past tense because each activity statement is intended to describe an activity that has already taken place.
/// @param object The object describes the receiver of the action (e.g. "a note"). Objects are represented by an instance of ASFActivityObject and may have additional properties such as a URL or additional content.
/// @param targetComponents An array describing the hierarchy of the user's current context (e.g. @[@"Module 1", @"document.pdf", @"Page 4"]).
/// @param completionHandler The completion handler block to execute when the statement has succesfully persisted to the server or has been stored in the queue. It also gets executed when the statement fails to persist. A statement could fail to persist if it is missing required data, if the activity log has been disabled, or if the user has not consented to tracking his/her activity.
/// @return The generated statement that has been sent to the server.
/// @since 2.1
///
+ (ASFActivityStatement *) logActionForCurrentUser:(NSString *)verb withObject:(ASFActivityObject *)object onTargetWithComponents:(NSArray *)targetComponents completionHandler:(ASFActivityLogStoreCompletionHandler)completionHandler;

///
/// Log an activity performed by the current user.
///
/// @param verb The verb describes the action of the activity (e.g. "created" or "selected"). Verbs should typically be in the past tense because each activity statement is intended to describe an activity that has already taken place.
/// @param object The object describes the receiver of the action (e.g. "a note"). Objects are represented by an instance of ASFActivityObject and may have additional properties such as a URL or additional content.
/// @param target The target describes the context in which the activity occurred. This may reference a specific page of a document or a location with in the application (e.g. Module 1).
/// @param completionHandler The completion handler block to execute when the statement has succesfully persisted to the server or has been stored in the queue. It also gets executed when the statement fails to persist. A statement could fail to persist if it is missing required data, if the activity log has been disabled, or if the user has not consented to tracking his/her activity.
/// @return The generated statement that has been sent to the server.
/// @since 2.1
///
+ (ASFActivityStatement *) logActionForCurrentUser:(NSString *)verb withObject:(ASFActivityObject *)object onTarget:(ASFActivityObject *)target completionHandler:(ASFActivityLogStoreCompletionHandler)completionHandler;

///
/// Log an activity performed by the current user with an attachment.
///
/// @param verb The verb describes the action of the activity (e.g. "created" or "selected"). Verbs should typically be in the past tense because each activity statement is intended to describe an activity that has already taken place.
/// @param object The object describes the receiver of the action (e.g. "a note"). Objects are represented by an instance of ASFActivityObject and may have additional properties such as a URL or additional content.
/// @param target The target describes the context in which the activity occurred. This may reference a specific page of a document or a location with in the application (e.g. Module 1).
/// @param attachment Raw data that may be attached to the statement. Maximum of 1mb.
/// @param completionHandler The completion handler block to execute when the statement has succesfully persisted to the server or has been stored in the queue. It also gets executed when the statement fails to persist. A statement could fail to persist if it is missing required data, if the activity log has been disabled, or if the user has not consented to tracking his/her activity.
/// @return The generated statement that has been sent to the server.
/// @since 2.1
///
+ (ASFActivityStatement *) logActionForCurrentUser:(NSString *)verb withObject:(ASFActivityObject *)object onTarget:(ASFActivityObject *)target withAttachment:(ASFActivityAttachment *)attachment completionHandler:(ASFActivityLogStoreCompletionHandler)completionHandler;

///
/// Set the default target for all future activity statements (unless the statement specifies it's own target).
///
/// @param target The ASFActivityObject representing the target of all future activity statements.
/// @since 2.1
///
+ (void) setDefaultTarget:(ASFActivityObject *)target;

///
/// Sends a statement (representing an activity recently performed by the current user) to the server.
///
/// @param statement The ASFActivityStatement to persist to the server.
/// @param completionHandler The completion handler block to execute when the statement has succesfully persisted to the server or has been stored in the queue. It also gets executed when the statement fails to persist. A statement could fail to persist if it is missing required data, if the activity log has been disabled, or if the user has not consented to tracking his/her activity.
/// @since 2.1
///
+ (void) logStatement:(ASFActivityStatement *)statement completionHandler:(ASFActivityLogStoreCompletionHandler)completionHandler;

///
/// Sends an array of statements (representing activities recently performed by the current user) to the server.
///
/// @param statements An array of ASFActivityStatement objects to persist to the server.
/// @param completionHandler The completion handler block to execute when the statement has succesfully persisted to the server or has been stored in the queue. It also gets executed when the statement fails to persist. A statement could fail to persist if it is missing required data, if the activity log has been disabled, or if the user has not consented to tracking his/her activity.
/// @since 2.1
///
+ (void) logStatements:(NSArray *)statements completionHandler:(ASFActivityLogStoreCompletionHandler)completionHandler;

/// @name Querying Statements

///
/// Counts previously persisted activity statements from the server matching the specified query.
///
/// @param query The query to make to the server for counting previously persisted statements.
/// @param completionHandler The completion handler block to execute when the count has been determined or if the request failed.
/// @since 2.1
///
+ (void) countStatementsWithQuery:(ASFActivityLogQuery *)query completionHandler:(ASFActivityLogQueryCountCompletionHandler)completionHandler;

///
/// Requests a set of previously persisted activity statements from the server matching the specified query.
///
/// @param query The query to make to the server for previously persisted statements.
/// @param completionHandler The completion handler block to execute when statements have been returned from the server or if the request failed.
/// @since 2.1
///
+ (void) retrieveStatementsWithQuery:(ASFActivityLogQuery *)query completionHandler:(ASFActivityLogQueryCompletionHandler)completionHandler;

///
/// Retrieves *more* statements from the server following a query previously made by retrieveStatementsWithQuery:completionHandler:.
/// After retrieving a result from your initial query, your application should store the ASFActivityLogQueryResult object and pass it to this method
/// to retrieve more statements. When this query has completed, the statements property on ASFActivityLogQueryResult will only contain statements that were returned
/// on THIS request. It is up to your application to keep track of previously returned statements.
///
/// @param result An ASFActivityLogQueryResult object previously returned by retrieveStatementsWithQuery:completionHandler:.
/// @param completionHandler The completion handler block to execute when statements have been returned from the server or if the request failed.
/// @since 2.1
///
+ (void) retrieveMoreStatementsWithResult:(ASFActivityLogQueryResult *)result completionHandler:(ASFActivityLogQueryCompletionHandler)completionHandler;

/// @name Queuing Statements for Peristence

///
/// The queue holding onto activity statements until they can be persisted to the server.
/// @since 2.1
///
@property (nonatomic, strong, readonly) ASFActivityLogStatementQueue *queue;

///
/// Flushes (removes) all the statements stored in the queue and attempts to persist them to the server.
/// If they're unable to be persisted, they're added back into the queue.
///
/// @return The number of statements that were flushed (removed) from the local queue.
/// @since 2.1
///
+ (NSUInteger) flushStatementQueue;

///
/// A reference to the shared ASFActivityLog instance.
/// @since 2.1
///
+ (ASFActivityLog *) sharedActivityLog;

@end
