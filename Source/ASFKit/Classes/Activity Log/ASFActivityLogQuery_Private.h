//
//  ASFActivityLogQuery_Private.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFActivityLogQuery.h"

@interface ASFActivityLogQuery ()
///
/// Returns a query string matching the parameters passed into the object.
/// @return The query string (param=value&param2=value2).
///
- (NSString *) queryString;
@end
