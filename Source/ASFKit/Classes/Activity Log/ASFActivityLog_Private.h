//
//  ASFActivityLog_Private.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFActivityLog.h"

extern NSString* const ASFActivityLogConsentStatusUpdatedNotification;

/// The current status of the user's consent to having his/her activity tracked.
typedef NS_ENUM(NSInteger, ASFActivityLogUserConsentStatus) {
    /// There is no logged response for the current user
    ASFActivityLogUserConsentStatusUnknown,
    /// The current user HAS consented to the use of the activity log
    ASFActivityLogUserConsentStatusAgreed,
    /// The current user has NOT consented to the use of the activity log
    ASFActivityLogUserConsentStatusDisagreed,
    /// The current user has previously accepted the terms, but the terms have since changed
    ASFActivityLogUserConsentStatusNeedsUpdating
};

@interface ASFActivityLog ()

///
/// Determines whether the privacy notice for the activity log should be displayed.
/// @return YES if the login view should be displayed with the privacy terms; NO otherwise.
/// @since 2.1
///
+ (BOOL) shouldShowActivityLogPrivacyNotice;

///
/// Updates the logged user response for the current consent status.
/// Triggers an activity log statement.
/// @param status The new consent status.
/// @since 2.1
///
+ (void) updateCurrentConsentStatus:(ASFActivityLogUserConsentStatus)status;

///
/// Checks the current privacy notice terms for the activity log and determines if the user has given consent to the application to track his/her activity.
/// @return The current consent status for the current user.
/// @since 2.1
///
+ (ASFActivityLogUserConsentStatus) currentConsentStatus;

@end
