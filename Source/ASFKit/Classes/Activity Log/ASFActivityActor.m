//
//  ASFActivityActor.m
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFActivityActor.h"
#import "ASFAuthorization.h"
#import "ASFProfile.h"

@implementation ASFActivityActor

- (instancetype) initWithCurrentUser
{
    ASFProfile *profile = [[ASFSession sharedSession] getSharedAuthorization].profile;
    if (self = [self initWithProfile:profile]) {
        
    }
    return self;
}

- (instancetype) initWithProfile:(ASFProfile *)profile
{
    if (self = [self initWithDictionary:[profile dictionary]]) {
        if (profile.upi) {
            _actorId = [NSURL URLWithString:[NSString stringWithFormat:@"http://abbvie.com/user/%@", profile.upi]];
        }
    }
    return self;
}

- (NSString *) description
{
    if (self.displayName.length) {
        return self.displayName;
    } else if (self.upi.length) {
        return self.upi;
    } else if (self.actorId) {
        return self.actorId.absoluteString;
    }
    return [super description];
}

#pragma mark - Object serialization
- (NSDictionary *) dictionary
{
    NSMutableDictionary *dictionary = [[super dictionary] mutableCopy];
    dictionary[@"id"] = self.actorId.absoluteString;
    return [NSDictionary dictionaryWithDictionary:dictionary];
}
- (NSDictionary *) propertyMapping
{
    return @{
             @"id" : @"actorId"
             };
}

@end
