//
//  ASFActivityStatement.m
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFActivityStatement.h"
#import "ASFActivityObject.h"
#import "ASFActivityActor.h"
#import "ASFActivityAttachment.h"
#import "ASFSession.h"
#import "ASFAuthorization.h"
#import "ASFProfile.h"
#import "ASFKitDevice.h"

@interface ASFActivityStatement ()
@property (nonatomic, strong) ASFActivityActor *actor;
@end

static ASFActivityObject *defaultTarget = nil;

@implementation ASFActivityStatement

- (instancetype) initWithVerb:(NSString *)verb andObject:(ASFActivityObject *)object forTarget:(ASFActivityObject *)target
{
    if (self = [self init]) {
        self.actor = [[ASFActivityActor alloc] initWithCurrentUser];
        
        // Check if the verb has a space; it's okay if it does,
        // but really it probably shouldn't
        if ([verb rangeOfString:@" "].location != NSNotFound) {
            NSLog(@"Verbs should be only one word.");
        }
        
        self.verb = verb;
        self.object = object;
        self.target = target ? target : defaultTarget;
        
        _statementId = [[self class] generateStatementId];
        _generator = [[self class] determineGenerator];
    }
    return self;
}

- (BOOL) validateWithError:(NSError **)error
{
    if (*error == NULL) {
        *error = nil;
    }
    
    if (!self.actor.actorId) {
        *error = [ASFError errorWithCode:ASFErrorActivityStatementInvalid andLocalizedFailureReason:ASFKitLocalizedString(@"There must be an actor for the statement.")];
        return NO;
    } else if (!self.verb.length) {
        *error = [ASFError errorWithCode:ASFErrorActivityStatementInvalid andLocalizedFailureReason:ASFKitLocalizedString(@"You must specify a verb for the statement.")];
        return NO;
    } else if (!self.object) {
        *error = [ASFError errorWithCode:ASFErrorActivityStatementInvalid andLocalizedFailureReason:ASFKitLocalizedString(@"You must specify an object for the statement.")];
        return NO;
    } else if (!self.object.objectId) {
        *error = [ASFError errorWithCode:ASFErrorActivityStatementInvalid andLocalizedFailureReason:ASFKitLocalizedString(@"The object is missing an object ID.")];
        return NO;
    } else if (!self.object.objectType.length) {
        *error = [ASFError errorWithCode:ASFErrorActivityStatementInvalid andLocalizedFailureReason:ASFKitLocalizedString(@"The object is missing an object type.")];
        return NO;
    } else if (self.attachment.data && self.attachment.contentType.length == 0) {
        *error = [ASFError errorWithCode:ASFErrorActivityStatementInvalid andLocalizedFailureReason:ASFKitLocalizedString(@"The attachment must specify a content type.")];
        return NO;
    } else if (self.attachment.data.length > AttachmentMaxSizeBytes) {
        *error = [ASFError errorWithCode:ASFErrorActivityStatementTooLarge];
        return NO;
    } else {
        if (self.target) {
            if (!self.target.objectId) {
                *error = [ASFError errorWithCode:ASFErrorActivityStatementInvalid andLocalizedFailureReason:ASFKitLocalizedString(@"The target is missing an object ID.")];
                return NO;
            } else if (!self.target.objectType.length) {
                *error = [ASFError errorWithCode:ASFErrorActivityStatementInvalid andLocalizedFailureReason:ASFKitLocalizedString(@"The target is missing an object type.")];
                return NO;
            }
        }
    }
    
    return YES;
}

+ (void) setDefaultTarget:(ASFActivityObject *)target
{
    defaultTarget = target;
}

+ (ASFActivityStatement *) statementForCurrentActorWithVerb:(NSString *)verb withObject:(ASFActivityObject *)object
{
    return [self statementForCurrentActorWithVerb:verb withObject:object onTarget:nil];
}

+ (ASFActivityStatement *) statementForCurrentActorWithVerb:(NSString *)verb withObject:(ASFActivityObject *)object onTarget:(ASFActivityObject *)target
{
    ASFActivityStatement *statement = [[self alloc] initWithVerb:verb andObject:object forTarget:target];
    return statement;
}

- (NSString *) description
{
    if (self.title) {
        return self.title;
    } else if (self.actor && self.verb && self.object) {
        return [@[[self.actor description], self.verb, [self.object description]] componentsJoinedByString:@" "];
    } else {
        return self.statementId.absoluteString;
    }
}

- (NSString *) debugDescription
{
    return [[self dictionary] description];
}

#pragma mark - Object serialization
- (NSDictionary *) propertyMapping
{
    return @{@"id": @"statementId"};
}

#pragma mark - Private methods
///
/// Generates a statement ID.
/// @return The generated statement ID in the format "scheme://statement/UUID".
///
+ (NSURL *) generateStatementId
{
    NSString *UUID = [[NSUUID UUID] UUIDString];
    return [[[ASFSession sharedSession].configurationProfileURL URLByAppendingPathComponent:ActivityLogURI] URLByAppendingPathComponent:UUID];
}

///
/// Uses device info to determine a generator object for the statement.
/// @return The generator object.
///
+ (ASFActivityObject *) determineGenerator
{
    // The statement "generator" contains data about the current device
    // Object family, object type, OS, connection
    NSArray *components = [[ASFKitDevice sharedASFKitDevice] environmentDetails];
    NSString *displayName = [NSString stringWithFormat:@"%@ running %@ (%@)", components[0], components[1], components[2]];
    ASFActivityObject *object = [ASFActivityObject activityObjectWithComponents:components andType:@"device"];
    object.displayName = displayName;
    return object;
}

@end
