//
//  ASFActivityLogQuery.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "SerializableNSObject.h"

///
/// The ASFActivityLogQuery provides an interface for querying statements from the server.
/// By default, the query will be filtered to querying the current application for the current user. You may query the entire activity stream for the current user by using the queryAllApplications or queryAllApplicationsWithLimit convenience methods. Note that only the statements created by applications that are sharing their stream with your application will appear in the shared stream.
///
/// @since 2.1
///
@interface ASFActivityLogQuery : SerializableNSObject

/// @name Query Properties

///
/// The number of statements to return from the server.
/// @since 2.1
///
@property (nonatomic, readwrite) NSUInteger limit;

///
/// The bundle identifier of the application from which to retrieve the activity stream (defaults to the current application).
/// This is for retrieving the activity stream of another application.
/// @since 2.1
///
@property (nonatomic, strong) NSString *providerBundleIdentifier;

///
/// The UPI of the user's activity stream to retrieve (defaults to the current user).
/// @since 2.1
///
@property (nonatomic, strong) NSString *actorUPI;

///
/// Retrieve statements matching the same verb.
/// @since 2.1
///
@property (nonatomic, strong) NSString *verb;

///
/// Retrieve statements matching the same object identifer.
/// @since 2.1
///
@property (nonatomic, strong) NSString *objectId;

///
/// Retrieve statements matching the same object type.
/// @since 2.1
///
@property (nonatomic, strong) NSString *objectType;

///
/// Retrieve statements matching the same target identifier.
/// @since 2.1
///
@property (nonatomic, strong) NSString *targetId;

///
/// Retrieve statements matching the same target type.
/// @since 2.1
///
@property (nonatomic, strong) NSString *targetType;

///
/// Retrieve statements matching the same generator identifier.
/// @since 2.1
///
@property (nonatomic, strong) NSString *generatorId;

///
/// Whether to include attachments on the response (default=YES).
/// Omitting attachments may increase response speed.
/// @since 2.1
///
@property (nonatomic, readwrite) BOOL attachments;

/// @name Creating a Query

///
/// Instantiates a query with the specified limit.
///
/// @param limit The number of statements to retrieve from the server.
/// @return The instantiated query.
/// @since 2.1
///
- (instancetype) initWithLimit:(NSUInteger)limit;

///
/// Instantiates a query with the default limit (25).
///
/// @return The instantiated query.
/// @since 2.1
///
+ (instancetype) query;

///
/// Instantiates a query with the default limit (25) filtered to statements created by the current application.
///
/// @return The instantiated query.
/// @since 2.1
///
+ (instancetype) queryAllApplications;

///
/// Instantiates a query with the specified limit.
///
/// @param limit The number of statements to retrieve from the server.
/// @return The instantiated query.
/// @since 2.1
///
+ (instancetype) queryWithLimit:(NSUInteger)limit;

///
/// Instantiates a query with the specified limit filtered to statements created by the current application.
///
/// @param limit The number of statements to retrieve from the server.
/// @return The instantiated query.
/// @since 2.1
///
+ (instancetype) queryAllApplicationsWithLimit:(NSUInteger)limit;

///
/// Instantiates a query with the specified limit and bundle identifier.
///
/// @param limit The number of statements to retrieve from the server.
/// @param bundleIdentifier Retrieves statements with a provider matching the bundle identifier.
/// @return The instantiated query.
/// @since 2.1
///
+ (instancetype) queryWithLimit:(NSUInteger)limit andProviderBundleIdentifier:(NSString *)bundleIdentifier;

///
/// Instantiates a query with the specified limit and object type.
///
/// @param limit The number of statements to retrieve from the server.
/// @param objectType Retrieve statements matching the same object type.
/// @return The instantiated query.
/// @since 2.1
///
+ (instancetype) queryWithLimit:(NSUInteger)limit andObjectType:(NSString *)objectType;

///
/// Instantiates a query with the specified limit, verb, and object type.
///
/// @param limit The number of statements to retrieve from the server.
/// @param verb Retrieve statements matching the same verb.
/// @param objectType Retrieve statements matching the same object type.
/// @return The instantiated query.
/// @since 2.1
///
+ (instancetype) queryWithLimit:(NSUInteger)limit andVerb:(NSString *)verb andObjectType:(NSString *)objectType;

@end
