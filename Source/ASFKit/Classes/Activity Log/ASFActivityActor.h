//
//  ASFActivityActor.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFProfile.h"

///
/// The ASFActivityActor class represents the actor of the statement.
/// There are no customizable properties on this class; all it's properties are determined
/// by an underling profile object.
///
/// @since 2.1
///
@interface ASFActivityActor : ASFProfile

/// @name Actor Properties

///
/// @since 2.1
///
@property (nonatomic, readonly) NSURL *actorId;

/// @name Creating an Actor

///
/// Instantiates an actor with the currently logged in user.
/// @return The instantiated actor object.
/// @since 2.1
///
- (instancetype) initWithCurrentUser;

///
/// Instantiates an actor with the specified profile object.
/// @param profile The user profile for the actor.
/// @return The instantiated actor object.
/// @since 2.1
///
- (instancetype) initWithProfile:(ASFProfile *)profile;

@end
