//
//  ASFActivityLogQuery.m
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFActivityLogQuery.h"
#import "NSString+ASFEncoding.h"

@implementation ASFActivityLogQuery

- (instancetype) initWithLimit:(NSUInteger)limit
{
    if (self = [self init]) {
        self.limit = limit;
        // Default the user to the current user
        self.actorUPI = [ASFSession sharedSession].upi;
        self.providerBundleIdentifier = [NSBundle mainBundle].infoDictionary[(NSString *)kCFBundleIdentifierKey];
        self.attachments = YES;
    }
    return self;
}
+ (instancetype) query
{
    return [self queryWithLimit:25];
}
+ (instancetype) queryAllApplications
{
    ASFActivityLogQuery *query = [self query];
    query.providerBundleIdentifier = nil;
    return query;
}
+ (instancetype) queryWithLimit:(NSUInteger)limit
{
    return [[self alloc] initWithLimit:limit];
}
+ (instancetype) queryAllApplicationsWithLimit:(NSUInteger)limit
{
    ASFActivityLogQuery *query = [self queryWithLimit:limit];
    query.providerBundleIdentifier = nil;
    return query;
}
+ (instancetype) queryWithLimit:(NSUInteger)limit andProviderBundleIdentifier:(NSString *)bundleIdentifier
{
    ASFActivityLogQuery *query = [self queryWithLimit:limit];
    query.providerBundleIdentifier = bundleIdentifier;
    return query;
}
+ (instancetype) queryWithLimit:(NSUInteger)limit andObjectType:(NSString *)objectType
{
    ASFActivityLogQuery *query = [self queryWithLimit:limit];
    query.objectType = objectType;
    return query;
}
+ (instancetype) queryWithLimit:(NSUInteger)limit andVerb:(NSString *)verb andObjectType:(NSString *)objectType
{
    ASFActivityLogQuery *query = [self queryWithLimit:limit];
    query.verb = verb;
    query.objectType = objectType;
    return query;
}

#pragma mark - Object serialization
- (NSDictionary *) propertyMapping
{
    return @{
             @"provider.bundle_identifier" : @"providerBundleIdentifier",
             @"actor.id" : @"actorUPI",
             @"object.id" : @"objectId",
             @"object.objectType" : @"objectType",
             @"target.id" : @"targetId",
             @"target.objectType" : @"targetType",
             @"generator.id" : @"generatorId",
             };
}

#pragma mark - Private methods
- (NSString *) queryString
{
    NSDictionary *query = [self dictionary];
    NSMutableArray *queryComponents = [NSMutableArray array];
    [query enumerateKeysAndObjectsUsingBlock:^(NSString *property, id value, BOOL *stop) {
        if (value) {
            NSString *stringValue = [NSString stringWithFormat:@"%@", value];
            [queryComponents addObject:[NSString stringWithFormat:@"%@=%@", [property asf_urlEncodedString], [stringValue asf_urlEncodedString]]];
        }
    }];
    return [queryComponents componentsJoinedByString:@"&"];
}
@end
