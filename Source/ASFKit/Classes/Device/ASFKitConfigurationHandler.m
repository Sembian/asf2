//
//  ASFConfigurationHandler.m
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFKitConfigurationHandler.h"
#import "ASFLog.h"
#import "ASFUtilities_Private.h"
#import "ASFKitDevice.h"
#import "ASFFileProtectionManager.h"

NSString* const kConfigurationProcessURL    =   @"configurationProcessURL";
NSString* const kLeafCertificateURL         =   @"leafCertificateURL";

@implementation ASFKitConfigurationHandler

+ (void) load
{
    // Don't protect the leaf certificate
    [ASFFileProtectionManager stopProtectingLocation:[[self class] leafCertificatePath]];
}

#pragma mark Object lifecycle
- (id) initWithDelegate: (id<ASFKitConfigurationHandlerDelegate>)theDelegate forConfigurationURL: (NSURL *)theConfigurationURL andAppKey: (NSString *)theAppKey
{
    self = [self init];
    if (self) 
    {
        _delegate               =   theDelegate;
        _configurationURL       =   theConfigurationURL;
        _appKey                 =   theAppKey;
    }
    return self;
}


#pragma mark Private Methods
+ (NSString *) leafCertificatePath
{
    return [[ASFUtilities frameworkDirectory] stringByAppendingPathComponent:LeafCertificateName];
}

- (void) parseEndpointResponse: (NSData *)responseData
{
    [ASFLog message:@"Succesfully created endpoints."];
    _endpointsCreated = YES;
    
    // Parse data
    NSError *error = nil;
    NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
    
    _leafCeritificateURL     =   [NSURL URLWithString:[response valueForKeyPath:@"configuration.leaf"]];
    _configurationProcessURL =   [NSURL URLWithString:[response valueForKeyPath:@"configuration.mobileconfig"]];
    
    if (!self.configurationProcessURL || !self.leafCeritificateURL)
    {
        NSError *error = [ASFError errorWithCode:ASFErrorConfigurationServerFailure];
        [self.delegate didFailCreatingConfigurationEndpoints:self withError:error];
        return;
    }
    
    [self.delegate didCreateConfigurationEndpoints:self];
}

# pragma mark Public Methods
+ (ASFKitConfigurationHandler *) handlerWithDelegate: (id<ASFKitConfigurationHandlerDelegate>)delegate forConfigurationURL: (NSURL *)configurationURL andAppKey: (NSString *)appKey
{
    return [[ASFKitConfigurationHandler alloc] initWithDelegate:delegate forConfigurationURL:configurationURL andAppKey:appKey];
}

- (void) createConfigurationEndpointsForUser: (NSDictionary *)userInfo
{
    if (self.endpointsCreated)
    {
        [ASFLog message:@"Endpoints already created."];
        return;
    }
    // Create the endpoint generate URL by appending the app key and the "generate" action
    NSURL *endpointGenerateURL = [[self.configurationURL URLByAppendingPathComponent:ConfigurationRequestURI] URLByAppendingPathComponent:self.appKey];
    
    // Create a request
    ASFURLRequest *endpointRequest = [ASFURLRequest requestWithURL:endpointGenerateURL completionHandler:^(ASFURLRequest *request, NSData *responseData, NSError *error)
    {
        if (!error)
            [self parseEndpointResponse:responseData];
        else
            [self.delegate didFailCreatingConfigurationEndpoints:self withError:error];
    }];
    
    // Add the appropriate headers to the request:
    // -App Version
    NSString *version       = [[[NSBundle mainBundle] infoDictionary] valueForKey:@"CFBundleVersion"];
    // -Unique device identifier
    NSString *identifier    = [ASFKitDevice uniqueIdentifier];
    // -User's UPI
    NSString *upi           = [userInfo valueForKey:@"upi"];
    
    [endpointRequest setValue:version    forHTTPHeaderField:@"X-APP-VERSION"];
    [endpointRequest setValue:identifier forHTTPHeaderField:@"X-DEVICE-IDENTIFIER"];
    [endpointRequest setValue:upi        forHTTPHeaderField:@"X-UPI"];
    [endpointRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [endpointRequest start];
}

- (void) downloadLeafCertificate
{
    if (!self.endpointsCreated)
    {
        [self.delegate didFailDownloadingLeafCertificate:self withError:[ASFError errorWithCode:ASFErrorConfigurationServerFailure]];
        return;
    }
    
    [ASFLog message:[NSString stringWithFormat:@"Downloading leaf certificate: %@", self.leafCeritificateURL]];
    
    [[ASFURLRequest downloadFileFromURL:self.leafCeritificateURL toFilePath:[[self class] leafCertificatePath] completionHandler:^(ASFURLRequest *request, NSData *responseData, NSError *error)
    {
        if (!error)
        {
            [ASFLog message:@"Successfully saved leaf certificate."];
            // By default, ASFURLRequest will set the file proteciton to complete;
            // We'll change it here.
            [[NSFileManager defaultManager] setAttributes:@{NSFileProtectionKey:NSFileProtectionCompleteUntilFirstUserAuthentication} ofItemAtPath:[[self class] leafCertificatePath] error:nil];
            [self.delegate didDownloadLeafCertificate:self];
        }
        else
        {
            [ASFLog message:@"Failed to save leaf certificate."];
            [self.delegate didFailDownloadingLeafCertificate:self withError:[ASFError errorWithCode:ASFErrorConfigurationLeafDownloadFailed andLocalizedFailureReason:nil andUnderlyingError:error]];
        }
    }] start];
}

@end
