//
//  ASFKitDevice.m
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import <UIKit/UIKit.h>

// Macro for easily creating a singleton out of this object
#import "SynthesizeSingleton.h"

#import "ASFKitDevice.h"
#import "ASFUtilities_Private.h"
#import "ASFSession.h"
#import "ASFLog.h"
#import "ASFFileProtectionManager.h"
#import "ASFAuthorization.h"
#import "ASFProfile.h"
#import "UIDevice+IdentifierAddition.h"
#import "ASFReachability.h"
#import <sys/utsname.h>

@interface ASFKitDevice ()
@property (nonatomic, readonly) NSData *leafData;
@end

@implementation ASFKitDevice
{
    ASFReachability *reachability;
}
@synthesize leafData;
SYNTHESIZE_SINGLETON_FOR_CLASS(ASFKitDevice)

- (id) init
{
    self = [super init];
    if (self)
    {
        // Listen for the protected files to be removed
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onProtectedFilesRemoved) name:ASFProtectedFilesWereRemovedNotification object:nil];
        
        attemptedToDownloadLeaf          =  NO;
        attemptedToDownloadConfigProfile =  NO;
        reachability = [ASFReachability reachabilityWithHostName:@"mobileappserver.net"];
    }
    return self;
}

# pragma mark - Private methods

/// <devdoc>
/// If ever the protected files are removed, then the leaf certificate will be removed too,
/// so ASFKitDevice needs to clear from memory it's stored value of the leaf certificate.
/// </devdoc>
- (void) onProtectedFilesRemoved
{
    leafData = nil;
}

/// <devdoc>If the device passed validation, call the callback with ASFDeviceConfigInstalled as the result.</devdoc>
- (void) onDeviceConfigured 
{
    configurationHandler = nil;
    attemptedToDownloadLeaf = NO;
    attemptedToDownloadConfigProfile = NO;
    self.completionHandler(ASFDeviceConfigInstalled);
}

/// <devdoc>If the device passed validation, call the callback with ASFDeviceConfigFailed as the result.</devdoc>
- (void) onDeviceFailedConfigurationWithReason:(ASFDeviceConfigResult)reason 
{
    configurationHandler = nil;
    attemptedToDownloadLeaf = NO;
    attemptedToDownloadConfigProfile = NO;
    self.completionHandler(reason);
}

- (void) configureDevice
{
    ASFSession *session = [ASFSession sharedSession];
    ASFProfile *profile = [[ASFSession sharedSession] getSharedAuthorization].profile;
    // If the user is not logged in, then the device cannot be configured
    if (!profile.upi)
    {
        [self onDeviceFailedConfigurationWithReason:ASFDeviceConfigFailed];
        return;
    }
    
    // Check for available configuration endpoints
    if (!configurationHandler)
    {
        configurationHandler = [[ASFKitConfigurationHandler alloc] initWithDelegate:self forConfigurationURL:session.configurationProfileURL andAppKey:session.licenseKey];
        [configurationHandler createConfigurationEndpointsForUser:[NSDictionary dictionaryWithObject:profile.upi forKey:@"upi"]];
        return;
    }
    
    // If the endpoints aren't created, attempt to create them again
    else if (!configurationHandler.endpointsCreated)
    {
        [configurationHandler createConfigurationEndpointsForUser:[NSDictionary dictionaryWithObject:profile.upi forKey:@"upi"]];
        return;
    }
    
    switch ([ASFKitDevice verifyConfiguration]) 
    {
        case ASFDeviceConfigInstalled:
            [self onDeviceConfigured];
            break;
        case ASFDeviceConfigMissingLeaf:
            if (attemptedToDownloadLeaf)
            {
                [self onDeviceFailedConfigurationWithReason:ASFDeviceConfigServerError];
            }
            else
            {
                [configurationHandler downloadLeafCertificate];
                attemptedToDownloadLeaf = YES;
            }
            break;
        case ASFDeviceConfigNotInstalled:
            if (!attemptedToDownloadConfigProfile) 
            {
                attemptedToDownloadConfigProfile = YES;
                [ASFKitDevice downloadConfigurationProfile];
            }
            else
            {
                [self onDeviceFailedConfigurationWithReason:ASFDeviceConfigFailed];
            }
            break;
        default:
            [self onDeviceFailedConfigurationWithReason:ASFDeviceConfigFailed];
            break;
    }
    
}

/// <devdoc>
/// When the app regains focus after attempting to install the configuration profile, 
/// this checks to make sure it was installed succesfully.
/// </devdoc>
- (void) checkConfiguration:(NSNotification*)notification 
{
    // Remove listener in the notifcation center for when the app regains focus
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
    
    [self configureDevice];
}

- (void) removeLeafData
{
    leafData = nil;
}

- (void) saveLeafData:(NSData *)data
{
    [self removeLeafData];
    leafData = data;
}

# pragma mark - Public methods
- (ASFKitConfigurationHandler *)configurationHandler
{
    return configurationHandler;
}

+ (void) configureDevice:(ASFDeviceConfigurationHandler)completion;
{
    // Save a reference to the callback
    [ASFKitDevice sharedASFKitDevice].completionHandler = completion;
    
    // Attempt to configure the device
    [[ASFKitDevice sharedASFKitDevice] configureDevice];
}

/// <devdoc>
/// If the leaf certificate is stored in memory it assumes that is a valid leaf certificate
/// and returns YES. If the leaf certificate is not loaded in memory, it looks on the file system
/// for the leaf certificate; if it exists and is a valid certificate then the method will return YES.
/// If the file does not exist or refers to an invalid file, it returns NO.
/// </devdoc>
+ (BOOL) isLeafCertificateAvailable
{
#if TARGET_IPHONE_SIMULATOR
    return YES;
#endif
    
    if ([ASFKitDevice sharedASFKitDevice].leafData)
    {
        return YES;
    }
    
    // Attempt to load the data from file
    NSString *filePath = [[ASFUtilities frameworkDirectory] stringByAppendingPathComponent:LeafCertificateName];
    NSData *leafFileData = [NSData dataWithContentsOfFile:filePath];
    
    if (!leafFileData)
    {
        [[ASFKitDevice sharedASFKitDevice] removeLeafData];
        return NO;
    }
    
    // Verify that the loaded data is valid .cer data
    SecCertificateRef cert = SecCertificateCreateWithData(NULL, (__bridge CFDataRef) leafFileData);
    if (!cert)
    {
        [[ASFKitDevice sharedASFKitDevice] removeLeafData];
        return NO;
    }
    
    CFRelease(cert);
    [[ASFKitDevice sharedASFKitDevice] saveLeafData:leafFileData];
    return YES;
}

+ (void) downloadConfigurationProfile
{
    [ASFLog message:@"Downloading configuration profile."];
    
    // Listen for the application to regain focus
    [[NSNotificationCenter defaultCenter] addObserver:[ASFKitDevice sharedASFKitDevice] selector:@selector(checkConfiguration:) name:UIApplicationWillEnterForegroundNotification object:nil];
    
    // Download configuration profile
    [[UIApplication sharedApplication] openURL:[ASFKitDevice sharedASFKitDevice].configurationHandler.configurationProcessURL];
}

+ (BOOL) isConfigurationProfileInstalled
{
    // The simulator can't install a configuration profile, so for development purposes, we'll always return "YES"
    #if TARGET_IPHONE_SIMULATOR
    return YES;
    #endif
    
    // Preliminary check for the leaf certificate
    if (![ASFKitDevice isLeafCertificateAvailable]) 
    {
        [ASFLog message:@"Leaf certifiate does not exist."];
        return NO;
    }
    
    OSStatus            err;
    NSData *            certData = [ASFKitDevice sharedASFKitDevice].leafData;
    SecCertificateRef   cert;
    SecPolicyRef        policy;
    SecTrustRef         trust;
    SecTrustResultType  trustResult;
    
    // Creates a certificate using the .cer file
    cert = SecCertificateCreateWithData(NULL, (__bridge CFDataRef) certData);
    assert(cert != NULL);
    
    // Define the policy as x509
    policy = SecPolicyCreateBasicX509();
    assert(policy != NULL);
    
    // Create a trust with the certificate
    err = SecTrustCreateWithCertificates((__bridge CFArrayRef) [NSArray arrayWithObject:(__bridge id)cert], policy, &trust);
    assert(err == noErr);
    assert(trust != NULL);
    
    // Initialize the trustResult
    trustResult = -1;
    
    // Evaluate whether the trust is trusted by the device
    SecTrustEvaluate(trust, &trustResult);
    
    // Clean up
    CFRelease(trust);
    CFRelease(policy);
    CFRelease(cert);
    
    // If the trust relationship evaluated as kSecTrustResultUnspecified,
    // then the device trusts the certificate by default so the configuration profile must be installed.
    return trustResult==kSecTrustResultUnspecified;
}

+ (ASFDeviceConfigResult) verifyConfigurationFromScratch
{
    [[ASFKitDevice sharedASFKitDevice] removeLeafData];
    return [self verifyConfiguration];
}
+ (ASFDeviceConfigResult) verifyConfiguration 
{
    if ([ASFKitDevice isConfigurationProfileInstalled]) 
    {
        return ASFDeviceConfigInstalled;
    }
    else if (![ASFKitDevice isLeafCertificateAvailable])
    {
        return ASFDeviceConfigMissingLeaf;
    }
    else
    {
        return ASFDeviceConfigNotInstalled;
    }
}

+ (NSString *) uniqueIdentifier
{
    UIDevice *device = [UIDevice currentDevice];
    if ([device respondsToSelector:@selector(identifierForVendor)]) {
        return [[device identifierForVendor] UUIDString];
    } else {
        return [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
    }
}

- (NSString *) deviceModel
{
    struct utsname systemInfo;
    uname(&systemInfo);
    
    return [NSString stringWithCString:systemInfo.machine
                              encoding:NSUTF8StringEncoding];
}
- (NSArray *) environmentDetails
{
    NSString *connection = nil;
    switch ([reachability currentReachabilityStatus])
    {
        case ReachableViaWiFi:
            connection = @"WiFi";
            break;
        case ReachableViaWWAN:
            connection = @"WWAN";
            break;
        case NotReachable:
            connection = @"offline";
            break;
        default:
            connection = @"Unknown";
            break;
    }
    
    NSString *deviceModel   = [self deviceModel];
    NSString *systemVersion = [[UIDevice currentDevice] systemVersion];
    
    return @[deviceModel,systemVersion,connection];
}

# pragma mark - ASFKitConfigurationHandler delegate methods
- (void) didCreateConfigurationEndpoints: (ASFKitConfigurationHandler *)handler
{
    [self configureDevice];
}

- (void) didFailCreatingConfigurationEndpoints: (ASFKitConfigurationHandler *)handler withError:(NSError *)error
{
    configurationHandler = nil;
    
    switch (error.code) 
    {
        case 403:
            [self onDeviceFailedConfigurationWithReason:ASFDeviceConfigLicenseRevoked];
            break;
        case 404:
            [self onDeviceFailedConfigurationWithReason:ASFDeviceConfigLicenseInvalid];
            break;
        default:
            [self onDeviceFailedConfigurationWithReason:ASFDeviceConfigServerError];
            break;
    }
}

- (void) didDownloadLeafCertificate: (ASFKitConfigurationHandler *)handler
{
    [self configureDevice];
}

- (void) didFailDownloadingLeafCertificate: (ASFKitConfigurationHandler *)handler withError:(NSError *)error
{
    configurationHandler = nil;
    
    switch (error.code) 
    {
        case 403:
            [self onDeviceFailedConfigurationWithReason:ASFDeviceConfigKeyExpire];
            break;
        default:
            [self onDeviceFailedConfigurationWithReason:ASFDeviceConfigServerError];
            break;
    }
}

@end
