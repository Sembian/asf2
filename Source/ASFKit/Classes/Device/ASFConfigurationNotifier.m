//
//  ASFConfigurationNotifier.m
//  ProfileCheckPatch
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFConfigurationNotifier.h"
#import "ASFKitDevice.h"

NSString* const ASFConfigurationProfileWasInstalledNotification      =   @"ASFConfigurationProfileWasInstalledNotification";
NSString* const ASFConfigurationProfileWasUninstalledNotification    =   @"ASFConfigurationProfileWasUninstalledNotification";
NSString* const kASFConfigurationProfileStatus                              =   @"ASFConfigurationProfileStatus";

@implementation ASFConfigurationNotifier

#pragma mark - Private methods
+ (ASFConfigurationNotifier *) sharedInstance
{
    static ASFConfigurationNotifier *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [ASFConfigurationNotifier new];
    });
    return sharedInstance;
}
- (void) checkConfiguration
{
    BOOL configProfileIsInstalled = [ASFKitDevice isConfigurationProfileInstalled];
    if (configProfileWasInstalled != configProfileIsInstalled)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:configProfileIsInstalled ? ASFConfigurationProfileWasInstalledNotification : ASFConfigurationProfileWasUninstalledNotification object:nil];
        configProfileWasInstalled = configProfileIsInstalled;
        [[NSUserDefaults standardUserDefaults] setBool:configProfileIsInstalled forKey:kASFConfigurationProfileStatus];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}
- (void) beginChecking
{
    // Check the configuration from the last time the notifier was running
    [[NSUserDefaults standardUserDefaults] synchronize];
    configProfileWasInstalled = [[NSUserDefaults standardUserDefaults] boolForKey:kASFConfigurationProfileStatus];
    [self checkConfiguration];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkConfiguration) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkConfiguration) name:ASFSessionExpiredNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkConfiguration) name:ASFSessionStartedNotification object:nil];
}
- (void) stopChecking
{
    [self checkConfiguration];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:ASFSessionExpiredNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:ASFSessionStartedNotification object:nil];
}

#pragma mark - Public methods
+ (void) beginChecking
{
    [[self sharedInstance] beginChecking];
}

+ (void) stopChecking
{
    [[self sharedInstance] stopChecking];
}

@end
