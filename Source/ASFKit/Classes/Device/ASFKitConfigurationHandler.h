//
//  ASFConfigurationHandler.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASFURLRequest.h"

extern NSString* const kConfigurationProcessURL;
extern NSString* const kLeafCertificateURL;

@protocol ASFKitConfigurationHandlerDelegate;

/// 
/// Iteracts with the configuration server to obtain the configuration information,
/// download the leaf certificate, and redirect to Safari to install the configuration profile.
/// There should be a valid user because it needs to send the user details to the configuration server (for logging).
/// This class should be instantiated each time a configuration needs to be completed.
/// 
@interface ASFKitConfigurationHandler : NSObject <ASFURLRequestDelegate>

/// YES if the configuration endpoints have successfully been created; NO otherwise.
@property (nonatomic, readonly) BOOL endpointsCreated;

/// The configuration process URL.
@property (nonatomic, readonly) NSURL *configurationProcessURL;

/// The leaf certificate URL.
@property (nonatomic, readonly) NSURL *leafCeritificateURL;

/// The URL from which to request the configuration endpoints.
@property (nonatomic, strong) NSURL *configurationURL;

/// The app key.
@property (nonatomic, strong) NSString *appKey;

/// The instance that will receive delegate methods.
@property (nonatomic, weak) id<ASFKitConfigurationHandlerDelegate> delegate;

- (id) initWithDelegate: (id<ASFKitConfigurationHandlerDelegate>)theDelegate forConfigurationURL: (NSURL *)theConfigurationURL andAppKey: (NSString *)theAppKey;

/// 
/// Convenience method to instantiate one-time use handler used to configure the device.
/// Events should be handled by the supplied delegate.
/// 
/// @param delegate The delegate that will receive notifications.
/// @param configurationURL The URL from which to request the configuration endpoints.
/// @param appKey The application's license key.
/// @return An instantiated ASFConfigurationHandler
+ (ASFKitConfigurationHandler *) handlerWithDelegate: (id<ASFKitConfigurationHandlerDelegate>)delegate forConfigurationURL: (NSURL *)configurationURL andAppKey: (NSString *)appKey;

/// 
/// Requests configuration endpoints for the configuration profile and 
/// leaf certificate from the configuration server. Sends user and device data 
/// securely to the configuration server.
/// If the request succeeds, the didCreateConfigurationEndpoints: method is triggered on the delegate.
/// If the request fails, the didFailCreatingConfigurationEndpoints: method is triggered on the delegate.
/// 
/// @param userInfo A dictionary of user information (must include the user's UPI number).
- (void) createConfigurationEndpointsForUser: (NSDictionary *)userInfo;

/// 
/// Downloads the leaf certificate to the caches directory for the current app.
/// If the request succeeds, the didDownloadLeafCertificate: method is triggered on the delegate.
/// If the request fails, the didFailDownloadingLeafCertificate: method is triggered on the delegate.
/// If configuration endpoints have not been configured, this will immediately fail.
/// 
- (void) downloadLeafCertificate;

@end

@protocol ASFKitConfigurationHandlerDelegate <NSObject>

@required
/// Triggered when a configuration URL has successfully been created.
/// @param handler The ASFConfigurationHandler that successfully created configuration endpoints.
- (void) didCreateConfigurationEndpoints: (ASFKitConfigurationHandler *)handler;

/// Triggered when a configuration URL was not successfully created.
/// @param handler The ASFConfigurationHandler that failed creating the configuration endpoints.
/// @param error The error received from the server when attempting to create the endpoints.
- (void) didFailCreatingConfigurationEndpoints: (ASFKitConfigurationHandler *)handler withError:(NSError *)error;

/// Triggered when the leaf certificate was successfully downloaded.
/// @param handler The ASFConfigurationHandler that successfully downloaded the leaf certificate.
- (void) didDownloadLeafCertificate: (ASFKitConfigurationHandler *)handler;

/// Triggered when the leaf certificate was NOT successfully downloaded.
/// @param handler The ASFConfigurationHandler that failed downloading the leaf certificate.
/// @param error The error received from teh server when attempting to download the leaf certificate.
- (void) didFailDownloadingLeafCertificate: (ASFKitConfigurationHandler *)handler withError:(NSError *)error;

@end