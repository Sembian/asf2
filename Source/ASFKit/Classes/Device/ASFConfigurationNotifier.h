//
//  ASFConfigurationNotifier.h
//  ProfileCheckPatch
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ASFConfigurationNotifier : NSObject
{
    BOOL configProfileWasInstalled;
}

+ (void) beginChecking;
+ (void) stopChecking;

@end
