//
//  ASFKitDevice.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASFKitConfigurationHandler.h"

# pragma mark - Constants
typedef NS_ENUM(NSInteger,ASFDeviceConfigResult) {
    ASFDeviceConfigInstalled,
    ASFDeviceConfigNotInstalled,
    ASFDeviceConfigFailed,
    ASFDeviceConfigMissingLeaf,
    ASFDeviceConfigServerError,
    ASFDeviceConfigLicenseInvalid,
    ASFDeviceConfigLicenseRevoked,
    ASFDeviceConfigKeyExpire
};

typedef void(^ASFDeviceConfigurationHandler)(ASFDeviceConfigResult result);

/// 
/// Manages the configuration of the device; determines if the configuration profile is installed
/// and provides the ability to install the configuration profile if it doesn't exist.
/// 
@interface ASFKitDevice : NSObject <ASFKitConfigurationHandlerDelegate> {
    @private
    /// Interface for interacting with the configuration server.
    ASFKitConfigurationHandler *configurationHandler;
    /// YES if the device has recently attempted to download the leaf certificate.
    BOOL attemptedToDownloadLeaf;
    /// YES if the device has recently attempted to download a configuration profile.
    BOOL attemptedToDownloadConfigProfile;
    
    NSData *leafData;
}

@property (strong, nonatomic, readonly) ASFKitConfigurationHandler *configurationHandler;
@property (nonatomic, strong) ASFDeviceConfigurationHandler completionHandler;

/// 
/// Attempts to download the configuration profile to the device--the leaf certificate must already be download
/// otherwise this immediately trigger a failure.
/// 
/// @param completion The completion handler.
+ (void) configureDevice:(ASFDeviceConfigurationHandler)completion;

/// 
/// Opens Mobile Safari to download the configuration profile.
/// Sets a observer on NSNotificationCenter waiting for the app to regain focus.
/// 
+ (void) downloadConfigurationProfile;

/// Determines if the leaf certificate is available in the caches directory.
/// @return YES if the leaf certificate is available; NO otherwise.
+ (BOOL) isLeafCertificateAvailable;

/// Determines whether the configuration profile is installed.
/// @return YES if the configuration profile is installed, or NO if it is not. This will also return NO if the leaf certificate is not installed. It is better to use the verifyConfiguration method to get a more accurate answer.
+ (BOOL) isConfigurationProfileInstalled;

/// Same as isConfigurationProfileInstalled but responds with an integer correlating to the constants defined in the class.
/// @return An ASFDeviceConfigResult (either ASFDeviceConfigInstalled, ASFDeviceConfigMissingLeaf, or ASFDeviceConfigNotInstalled).
+ (ASFDeviceConfigResult) verifyConfiguration;
+ (ASFDeviceConfigResult) verifyConfigurationFromScratch;

/**
 Generates and returns a unique identifier for the current device (NOT the UDID).
 On iOS 6 and later, this uses the -identifierForVendor property on UIDevice.
 On iOS 5, it returns a custom property based on the device's MAC address.
 It's important to note that the same device will have a different identifier on iOS 5 than iOS 6 and later.
 
 @note It's possible that this method may return empty string or nil if an identifier is not yet available. See https://developer.apple.com/library/ios/documentation/uikit/reference/UIDevice_Class/Reference/UIDevice.html#//apple_ref/occ/instp/UIDevice/identifierForVendor for more information.
 
 @return A unique identifier for the current device.
 */
+ (NSString *) uniqueIdentifier;

- (NSString *) deviceModel;
- (NSArray *) environmentDetails;

+ (ASFKitDevice *) sharedASFKitDevice;

@end