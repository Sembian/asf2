//
//  UIImage+ASFLaunchImage.m
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "UIImage+ASFLaunchImage.h"

@implementation UIImage (ASFLaunchImage)
#pragma mark - Public methods
+ (UIImageOrientation) imageOrientationForInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    switch (interfaceOrientation) {
        case UIInterfaceOrientationPortrait:
            return UIImageOrientationUp;
        case UIInterfaceOrientationPortraitUpsideDown:
            return UIImageOrientationDown;
        case UIInterfaceOrientationLandscapeLeft:
            return UIImageOrientationLeft;
        case UIInterfaceOrientationLandscapeRight:
            return UIImageOrientationRight;
        default:
            return UIImageOrientationUp;
    }
}

+ (UIImage *) bestLaunchImageForOrientation:(UIInterfaceOrientation)desiredInterfaceOrientation isForInterfaceOrientation:(UIInterfaceOrientation *)actualInterfaceOrientation
{
    UIInterfaceOrientation interfaceOrientation = desiredInterfaceOrientation;
    UIImage *image = [self launchImageForOrientation:interfaceOrientation];
    if (!image) {
        interfaceOrientation = UIInterfaceOrientationIsLandscape(interfaceOrientation) ? UIInterfaceOrientationPortrait : UIInterfaceOrientationLandscapeRight;
        image = [self launchImageForOrientation:interfaceOrientation];
    }
    
    *actualInterfaceOrientation = interfaceOrientation;
    return image;
}

+ (UIImage *) launchImageForOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    NSArray *launchImages = [NSBundle mainBundle].infoDictionary[@"UILaunchImages"];
    if (launchImages.count == 0) {
        return [self legacyLaunchImageForOrientation:interfaceOrientation];
    }
    
    NSString *orientation = UIInterfaceOrientationIsLandscape(interfaceOrientation) ? @"Landscape" : @"Portrait";
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    NSString *size = NSStringFromCGSize(screenSize);
    NSString *flippedSize = NSStringFromCGSize(CGSizeMake(screenSize.height, screenSize.width));
    
    NSArray *appropriateLaunchImages = [launchImages filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"UILaunchImageOrientation = %@ AND (UILaunchImageSize = %@ OR UILaunchImageSize = %@)", orientation, size, flippedSize]];
    
    for (NSDictionary *launchImage in appropriateLaunchImages) {
        UIImage *image = [UIImage imageNamed:launchImage[@"UILaunchImageName"]];
        if (image) {
            return image;
        }
    }
    
    return [self legacyLaunchImageForOrientation:interfaceOrientation];
}

#pragma mark - Private method
+ (UIImage *) legacyLaunchImageForOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    NSString *orientation   =  UIInterfaceOrientationIsLandscape(interfaceOrientation) ? @"Landscape" : @"Portrait";
    
    NSMutableArray *launchImageComponents = [NSMutableArray arrayWithObject:@"Default"];
    
    // Check if this is a 4" retina screen
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone && [UIScreen mainScreen].bounds.size.height > 480)
    {
        [launchImageComponents addObject:[NSString stringWithFormat:@"-%.0fh",[UIScreen mainScreen].bounds.size.height]];
    }
    // Otherwise, add an orientation
    else
    {
        [launchImageComponents addObject:[NSString stringWithFormat:@"-%@", orientation]];
    }
    
    NSUInteger i = launchImageComponents.count+1;
    UIImage *image = nil;
    
    while (!image && i--)
    {
        NSString *imageName = [[launchImageComponents subarrayWithRange:(NSRange){0,i}] componentsJoinedByString:@""];
        image = [UIImage imageNamed:imageName];
    }
    
    return image;
}

@end
