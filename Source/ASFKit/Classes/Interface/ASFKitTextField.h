//
//  ASFTextField.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import <UIKit/UIKit.h>

/// Extends UITextField to set the default properties for the login screen.
/// <history>
///     05/09/11    [dpfeiffer.float]   Created.
/// </history>
@interface ASFKitTextField : UITextField 
/// Creates a UITextField, setting the preferred properties, and sets the specified placeholder.
/// @param frame The area to create the UITextField.
/// @param placeholder The placeholder to fill the UITextField if the user hasn't entered any value.
/// @return Pointer to UITextField
- (id) initWithFrame:(CGRect)frame andPlaceholder:(NSString *)placeholder;

@property (nonatomic,readonly,getter=isTextfieldEmpty) BOOL isEmpty;
@property (nonatomic, readwrite) BOOL canResignFirstResponder;

@end
