//
//  ASFKitLoginViewController.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASFKitDomainController.h"
#import "ASFSession.h"

@class ASFKitTextField;

@protocol ASFKitLoginViewControllerDelegate <NSObject>
@required
- (void) userDidLoginWithUsername:(NSString *)username password:(NSString *)password domain:(NSString *)domain;
- (void) userDidTapButton:(id)sender;
@optional
- (void) textFieldDidBecomeActive:(UITextField *)textField;
- (void) textFieldDidResignActive:(UITextField *)textField;
@end

/// The view controller for the login window.
/// <history>
///     [dpfeiffer.float]   05/04/11    Created.
///     [dpfeiffer.float]   05/22/11    Added legal agreement and copyright notice; combined with status view.
/// </history>
@interface ASFKitLoginViewController : UIViewController <ASFDomainControllerDelegate, UITextFieldDelegate, UIPopoverControllerDelegate> {
    // Text fields on login screen
    ASFKitTextField *txt_username, *txt_password, *txt_domain, *txt_dummy;
    
    // Login button on login view and continue button on status view
    UIButton *btn_login, *btn_continue, *btn_altcontinue;
    
    // Legal agreement, copyright notice, and login status labels
    UILabel *lbl_legalAgreement, *lbl_copyright, *lbl_status;
    
    // Logo
    UIImageView *img_logo;
    
    // View that covers login during authentication
    UIView *status;
    
    // Activity indicator
    UIActivityIndicatorView *activityInd;
    
    @private
    /// Window to create on external monitor to "blank" the screen while logging in.
    UIWindow *_blankWindow;
    /// The popover controller that lists the possible domains to connect to.
    UIPopoverController *_domainPickerPopover;
    ASFKitDomainController *domainPicker;
    /// Deprecated
    BOOL _useDefaultInterface;
    
    BOOL _keyboardVisible;
    CGFloat containerViewY;
    
    UIView *containerView;
    
    /// YES if the login view is completely on the screen (done animating open); NO otherwise.
    BOOL isOpen;
}

/// The text field for the username.
@property(nonatomic, strong) IBOutlet ASFKitTextField *txt_username;

/// The text field for the password.
@property(nonatomic, strong) IBOutlet ASFKitTextField *txt_password;

/// The text field for the domain.
@property(nonatomic, strong) IBOutlet ASFKitTextField *txt_domain;

/// The continue button (on the right side of the status view).
@property (nonatomic, strong) UIButton *btn_continue;

/// The alternate continue button (on the left side; should be used to indicate a NO response).
@property (nonatomic, strong) UIButton *btn_altcontinue;

@property (nonatomic, weak) id<ASFKitLoginViewControllerDelegate> delegate;

/// Black out any external screens so onlookers cannot spy on the user's password!
- (void) blankExternalScreen;

/// The text field for the username.
- (IBAction) doLogin: (id) sender;

/// 
/// Shows a message on the login window. The message will slide
/// in from the top and slide out after four seconds.
/// 
/// @param message The message to show on the login window (can be nil to just show a yellow band).
- (void) showMessage:(NSString *)message;

/// Dismisses the statusView modal view.
- (void) hideStatusView;

/// Sets the message inside the status view while its authenticating the user.
/// @param message The message to display on the status view.
/// @param withActivity Whether to show the activity spinner.
/// @param withButton Whether to show the button.
- (void) setStatusMessage:(NSString *)message withActivitySpinner:(BOOL)withActivity andButton:(BOOL)withButton;

/// Sets the message inside the status view while its authenticating the user.
/// @param message The message to display on the status view.
/// @param withActivity Whether to show the activity spinner.
/// @param withButton Whether to show the button.
/// @param altButton Whether to show the alternate continue button.
- (void) setStatusMessage:(NSString *)message withActivitySpinner:(BOOL)withActivity andButton:(BOOL)withButton andAltButton:(BOOL)withAltButton;

@end
