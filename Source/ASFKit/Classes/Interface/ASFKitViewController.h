//
//  ASFLoginViewController.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASFKitLoginViewController.h"

@protocol ASFKitViewControllerDelegate <NSObject>
@required
- (void) userDidLoginWithUsername:(NSString *)username password:(NSString *)password domain:(NSString *)domain;
- (void) userDidTapButton:(id)sender expectingAction:(ASFSessionAction)action;
@end

@interface ASFKitViewController : UIViewController <ASFKitLoginViewControllerDelegate>
{
    @private
    UIView *backgroundView;
    UIImageView *launchImageView;
    UIWindow *loginWindow;
}

@property (nonatomic, weak) UIView *backgroundView;
@property (nonatomic, weak) id<ASFKitViewControllerDelegate> delegate;
@property (nonatomic, strong) UIViewController *rootViewController;
@property (nonatomic, readonly) ASFKitLoginViewController *loginView;
@property (nonatomic, readonly) BOOL loginViewShowing;
@property (nonatomic, readonly) BOOL errorShowing;
@property (nonatomic, readonly) ASFSessionAction nextAction;

- (void) presentLoginView;
- (void) dismissLoginView;

- (void) showLoginScreen;
- (void) showMessageWithActivityIndicator:(NSString *)message;
- (void) showMessage:(NSString *)message withButtonTitle:(NSString *)buttonTitle andNextAction:(ASFSessionAction)action;
- (void) showConfirmation:(NSString *)message withNoTitle:(NSString *)noTitle andYesTitle:(NSString *)yesTitle responseHandler:(void(^)(BOOL response))handler;
- (void) showError:(NSError *)error withNextAction:(ASFSessionAction)action;
- (void) showNotification:(NSString *)message;

@end
