//
//  ASFKitDomainField.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFKitTextField.h"

@interface ASFKitDomainField : ASFKitTextField

@end
