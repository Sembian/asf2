//
//  ASFLoginViewController.m
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFKitViewController.h"
#import "ASFLog.h"
#import "UIImage+ASFLaunchImage.h"

@interface ASFKitViewController ()
- (UIWindow *) findApplicationWindow;
@end

@implementation ASFKitViewController
{
    void(^responseHandler)(BOOL response);
    
    /**
     Set to YES while the login view and ASF view controller are being dismissed.
     */
    BOOL closingLoginView;
}
@synthesize rootViewController, loginView, loginViewShowing;

- (id) init
{
    self = [super init];
    if (self)
    {
        loginWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        loginView   = [ASFKitLoginViewController new];
        loginView.delegate = self;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            loginView.modalPresentationStyle = UIModalPresentationFormSheet;
        }
        loginViewShowing = NO;
        backgroundView = nil;
    }
    return self;
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.backgroundView = nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Setup the launch image frame
    CGRect f = self.view.frame;
    f.origin = CGPointMake(0, 0);
    launchImageView = [[UIImageView alloc] initWithFrame:f];
    launchImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    launchImageView.contentMode = UIViewContentModeBottom;
    launchImageView.clipsToBounds = YES;
    launchImageView.image = [self imageForOrientation:[UIApplication sharedApplication].statusBarOrientation];
    launchImageView.backgroundColor = [UIColor blackColor];
    
    self.backgroundView = backgroundView;
    self.view.alpha = 0;
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    launchImageView.image = [UIImage launchImageForOrientation:self.interfaceOrientation];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self presentLoginViewController];
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    launchImageView.image = [self imageForOrientation:toInterfaceOrientation];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    NSArray *orientations = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"UISupportedInterfaceOrientations"];
    NSString *orientation = nil;
    switch (interfaceOrientation) 
    {
        case UIInterfaceOrientationLandscapeLeft:
            orientation = @"UIInterfaceOrientationLandscapeLeft";
            break;
        case UIInterfaceOrientationLandscapeRight:
            orientation = @"UIInterfaceOrientationLandscapeRight";
            break;
        case UIInterfaceOrientationPortraitUpsideDown:
            orientation = @"UIInterfaceOrientationPortraitUpsideDown";
            break;
        case UIInterfaceOrientationPortrait:
        default:
            orientation = @"UIInterfaceOrientationPortrait";
            break;
    }
    
	return [orientations containsObject:orientation];
}

#pragma mark - Private methods
- (UIImage *) imageForOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    UIInterfaceOrientation actualLaunchImageOrientation;
    UIImage *image = [UIImage bestLaunchImageForOrientation:interfaceOrientation isForInterfaceOrientation:&actualLaunchImageOrientation];
    
    if (interfaceOrientation != actualLaunchImageOrientation) {
        // Swap left and right orientations
        // Because, if the portrait image for landscape right, I need to rotate it left
        image = [UIImage imageWithCGImage:image.CGImage
                                    scale:[UIScreen mainScreen].scale
                              orientation:(interfaceOrientation - 1)];
    }
    
    return image;
}

- (void) showView
{
    [UIView animateWithDuration:0.5 animations:^{
        self.view.alpha = 1;
    }];
}

- (void) hideView
{
    [UIView animateWithDuration:0.5 delay:0.15 options:0 animations:^{
        self.view.alpha = 0;
    } completion: nil];
}

- (UIWindow *) findApplicationWindow
{
    for (UIWindow *window in [UIApplication sharedApplication].windows)
    {
        if (window.windowLevel==UIWindowLevelNormal && !window.hidden && window!=loginWindow)
        {
            return window;
        }
    }
    return nil;
}

/**
 Presents a modal view containing this class's ASFKitLoginViewController instance.
 This is triggered on viewDidAppear:. It will skip presenting the login view controller if the containing view controller (this class) is in the process of dismissing.
 @since 2.1
 */
- (void) presentLoginViewController
{
    if (closingLoginView) {
        return;
    }
    
    UIWindow *currentApplicationWindow = [self findApplicationWindow];
    [currentApplicationWindow.rootViewController viewWillDisappear:YES];
    [loginWindow.rootViewController presentViewController:loginView animated:YES completion:^{
        [currentApplicationWindow.rootViewController viewDidDisappear:YES];
        if (!loginWindow.hidden)
        {
            loginWindow.backgroundColor = [UIColor blackColor];
            // Set window level back to normal so that alerts can still appear over top of it.
            // On iOS 7, having two windows with the windowLevel as UIWindowLevelNormal seems to confuse the
            // text magnifier (when tapping and holding on a UITextField) so we set the window level to 1.2
            loginWindow.windowLevel = 1.2;
        }
    }];
}

#pragma mark - Public methods

- (void) presentLoginView
{
    // Don't attempt to show the login view again if it is already showing.
    if (loginViewShowing)
        return;
    
    UIViewController *viewController;
    if (rootViewController)
    {
        viewController = rootViewController;
    }
    else
    {
        viewController = self;
        loginWindow.rootViewController = viewController;
        loginWindow.backgroundColor = [UIColor clearColor];
        // Set the window level to alert to ensure that it covers any other windows.
        loginWindow.windowLevel = UIWindowLevelAlert;
        [loginWindow makeKeyAndVisible];
        [self showView];
    }
    
    // The login view itself will be presented by viewDidAppear:.
    loginViewShowing = YES;
}

- (void) dismissLoginView
{
    UIWindow *currentApplicationWindow = [self findApplicationWindow];
    
    // Check if the view is currently presenting
    // If so, wait for it to finish being presented and close it
    if (loginView.isBeingPresented)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self dismissLoginView];
        });
        return;
    }
    
    closingLoginView = YES;
    [currentApplicationWindow.rootViewController viewWillAppear:YES];
    [loginView dismissViewControllerAnimated:YES completion:^{
        [currentApplicationWindow.rootViewController viewDidAppear:YES];
        if (!loginWindow.hidden)
        {
            loginWindow.hidden = YES;
        }
        loginWindow.rootViewController = nil;
        loginViewShowing = NO;
        closingLoginView = NO;
        
        // When the login view opened, it set it's window as the key window
        // Now that the window is hidden, we need a new key window.
        // There's no reliable way to simply return the key window to the proper window
        // (since the previous key window may have been an alert)
        // So we'll search the application windows and find the first visible window at the normal level
        // and assume that it should be the key window.
        [ASFLog message:[NSString stringWithFormat:@"Making %@ the key window. It's root view controller is %@.",currentApplicationWindow,currentApplicationWindow.rootViewController]];
        [currentApplicationWindow makeKeyWindow];
    }];
    
    if (!loginWindow.hidden)
    {
        loginWindow.backgroundColor = [UIColor clearColor];
        [self hideView];
    }
}

- (void) showLoginScreen
{
    // Only show the login screen if there is no error message visible.
    // This method will get called from reloadSession in ASFSession.
    // If we don't check for an error, any error that may be visible will disappear
    // before the user gets a chance to read it.
    // Users must acknowledge error messages.
    if (!self.errorShowing)
    {
        [self.loginView hideStatusView];
    }
}
- (void) showMessageWithActivityIndicator:(NSString *)message
{
    _errorShowing = NO;
    [self.loginView setStatusMessage:message withActivitySpinner:YES andButton:NO];
}
- (void) showMessage:(NSString *)message withButtonTitle:(NSString *)buttonTitle andNextAction:(ASFSessionAction)action
{
    _errorShowing = NO;
    [UIView setAnimationsEnabled:NO];
    [self.loginView.btn_continue setTitle:buttonTitle forState:UIControlStateNormal];
    [UIView setAnimationsEnabled:YES];
    [self.loginView setStatusMessage:message withActivitySpinner:NO andButton:YES];
    _nextAction = action;
}
- (void) showConfirmation:(NSString *)message withNoTitle:(NSString *)noTitle andYesTitle:(NSString *)yesTitle responseHandler:(void(^)(BOOL response))handler
{
    responseHandler = handler;
    _errorShowing = NO;
    [UIView setAnimationsEnabled:NO];
    [self.loginView.btn_continue setTitle:yesTitle forState:UIControlStateNormal];
    [self.loginView.btn_altcontinue setTitle:noTitle forState:UIControlStateNormal];
    [UIView setAnimationsEnabled:YES];
    [self.loginView setStatusMessage:message withActivitySpinner:NO andButton:YES andAltButton:YES];
    _nextAction = nil;
}
- (void) showError:(NSError *)error withNextAction:(ASFSessionAction)action
{
    [self showMessage:error.localizedDescription withButtonTitle:ASFKitLocalizedString(@"ASFTryAgain") andNextAction:action];
    _errorShowing = YES;
}
- (void) showNotification:(NSString *)message
{
    [self.loginView showMessage:[message uppercaseString]];
}

#pragma mark - Accessor methods
- (void) setBackgroundView:(UIView *)aBackgroundView
{
    if (backgroundView)
    {
        [backgroundView removeFromSuperview];
    }
    
    if (aBackgroundView)
    {
        [launchImageView removeFromSuperview];
        backgroundView = aBackgroundView;
        [self.view addSubview:backgroundView];
    }
    else
    {
        [self.view addSubview:launchImageView];
    }
}
- (UIView *) backgroundView
{
    return backgroundView;
}

#pragma mark - ASFKitLoginViewControllerDelegate delegate methods
- (void) userDidLoginWithUsername:(NSString *)username password:(NSString *)password domain:(NSString *)domain
{
    [self.delegate userDidLoginWithUsername:username password:password domain:domain];
}
- (void) userDidTapButton:(id)sender
{
    if (responseHandler) {
        responseHandler(sender == loginView.btn_continue);
        responseHandler = nil;
        return;
    }
    _errorShowing = NO;
    [self.delegate userDidTapButton:sender expectingAction:self.nextAction];
}
- (void) textFieldDidBecomeActive:(UITextField *)textfield
{
    // Verify that the window is key
    if (!loginWindow.isKeyWindow) {
        [loginWindow makeKeyWindow];
    }
}
- (void) textFieldDidResignActive:(UITextField *)textField
{
    // Do nothing
}
@end
