//
//  UIImage+ASFLaunchImage.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ASFLaunchImage)
+ (UIImageOrientation) imageOrientationForInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation;
+ (UIImage *) launchImageForOrientation:(UIInterfaceOrientation)interfaceOrientation;
+ (UIImage *) bestLaunchImageForOrientation:(UIInterfaceOrientation)desiredInterfaceOrientation isForInterfaceOrientation:(UIInterfaceOrientation *)actualInterfaceOrientation;
@end
