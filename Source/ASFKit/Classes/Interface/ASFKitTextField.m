//
//  ASFKitTextField.m
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFKitTextField.h"

@implementation ASFKitTextField
@synthesize canResignFirstResponder=_canResignFirstResponder;

- (id) initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        // Set the default properties of the text field
        self.borderStyle            = UITextBorderStyleRoundedRect;
        self.autocorrectionType     = UITextAutocorrectionTypeNo;
        self.autocapitalizationType = UITextAutocapitalizationTypeNone;
        self.clearButtonMode        = UITextFieldViewModeWhileEditing;
        self.autoresizingMask       = UIViewAutoresizingFlexibleWidth;
        _canResignFirstResponder    = YES;
    }
    return self;
}
- (id) initWithFrame:(CGRect)frame andPlaceholder:(NSString *)placeholder
{
    self = [self initWithFrame:frame];
    if (self) 
    {
        self.placeholder            = placeholder;
    }
    return self;
}

- (CGRect) rightViewRectForBounds:(CGRect)bounds {
    
    CGRect textRect = [super rightViewRectForBounds:bounds];
    textRect.origin.x -= 10;
    return textRect;
}

- (BOOL) isTextfieldEmpty
{
    if ([self.text length]>0) 
    {
        return NO;
    }
    return YES;
}

@end
