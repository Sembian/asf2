//
//  ASFKitLoginViewController.m
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFKitLoginViewController.h"
#import "ASFKitDomainField.h"
#import "ASFLog.h"
#import "ASFUILabel.h"
#define IS_IPAD UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad

NSString* const ASFLoginViewWillShowNotification =   @"ASFLoginViewWillShow";
NSString* const ASFLoginViewDidShowNotification  =   @"ASFLoginViewDidShow";
NSString* const ASFLoginViewWillHideNotification =   @"ASFLoginViewWillHide";
NSString* const ASFLoginViewDidHideNotification  =   @"ASFLoginViewDidHide";

@interface ASFKitLoginViewController ()
- (void) unblankExternalScreen;
@end

@implementation ASFKitLoginViewController
{
    UIStatusBarStyle initialStatusBarStyle;
    BOOL viewReady;
    NSString *queuedMessage;
}
@synthesize txt_username, txt_password, txt_domain, delegate=_delegate, btn_continue=btn_continue, btn_altcontinue=btn_altcontinue;

#pragma mark - overridden UIViewController methods

- (id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) 
    {
        // Assume we're not using the default interface
        _useDefaultInterface = NO;
        
        // If there is no interface builder file defined, build the default interface programatically
        if (!nibNameOrNil) 
        {
            _useDefaultInterface = YES;
            
            CGFloat totalWidth = 480;
            CGFloat margin = 35;
            CGFloat w = totalWidth - margin;
            CGFloat l = (totalWidth-w)/2;
            CGFloat spacing = 5;
            CGFloat textFieldHeight = 30;
            CGFloat fontSize = textFieldHeight * 0.6;
            
            // Create the text fields
            // Username
            txt_username    = [[ASFKitTextField alloc] initWithFrame:CGRectMake(0, 0, w, textFieldHeight) andPlaceholder:ASFKitLocalizedString(@"ASFLoginPromptUsername")];
            txt_username.delegate = self;
            txt_username.font = [UIFont systemFontOfSize:fontSize];
            txt_username.returnKeyType = UIReturnKeyNext;
            
            
            // Password
            txt_password    = [[ASFKitTextField alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(txt_username.frame)+spacing, w, textFieldHeight) andPlaceholder:ASFKitLocalizedString(@"ASFLoginPromptPassword")];
            txt_password.secureTextEntry = YES;
            txt_password.delegate = self;
            txt_password.font = [UIFont systemFontOfSize:fontSize];
            txt_password.returnKeyType = UIReturnKeyGo;
            
            // Domain
            domainPicker    = [ASFKitDomainController new];
            domainPicker.delegate = self;
            txt_domain      = [[ASFKitDomainField alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(txt_password.frame)+spacing, w, textFieldHeight) andPlaceholder:ASFKitLocalizedString(@"ASFLoginPromptDomain")];
            txt_domain.clearButtonMode = UITextFieldViewModeNever;
            txt_domain.font = [UIFont systemFontOfSize:fontSize];
            // Create an image view for the right view
            NSString *branding = ASFKitConfiguration.branding;
            NSString *bundle = @"ASFKitResources.bundle";
            
            NSString *dropdownArrowName = [bundle stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_dropdown_arrow.png",branding]];
            UIImageView *img_dropdownArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:dropdownArrowName]];
            txt_domain.rightView = img_dropdownArrow;
            txt_domain.rightViewMode = UITextFieldViewModeAlways;
            txt_domain.delegate = self;
            
            // Create an invisible field to hold the focus while the user is selecting a domain
            txt_dummy = [[ASFKitTextField alloc] initWithFrame:CGRectZero];
            txt_dummy.alpha = 0;
            txt_dummy.autocorrectionType = UITextAutocorrectionTypeNo;
            txt_dummy.autocapitalizationType = UITextAutocapitalizationTypeNone;
            txt_dummy.delegate = self;
            txt_dummy.canResignFirstResponder = YES;
            
            // Login button
            btn_login       = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            [btn_login setTitle:ASFKitLocalizedString(@"ASFLogin") forState:UIControlStateNormal];
            [btn_login setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
            [btn_login addTarget:self action:@selector(doLogin:) forControlEvents:UIControlEventTouchUpInside];
            btn_login.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
            [btn_login sizeToFit];
            CGFloat btnWidth = btn_login.frame.size.width;
            btn_login.frame = CGRectMake(w-btnWidth, CGRectGetMaxY(txt_domain.frame)+spacing, btnWidth, 35);
            
            // Disable login button until all fields are filled in
            btn_login.enabled = NO;
            
            // Labels
            
            lbl_copyright = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(txt_domain.frame)+spacing, w-btn_login.frame.size.width, 35)];
            lbl_copyright.adjustsFontSizeToFitWidth = YES;
            lbl_copyright.backgroundColor = [UIColor clearColor];
            lbl_copyright.font = [UIFont systemFontOfSize:10];
            lbl_copyright.numberOfLines = 0;                            // Unlimited number of lines
            lbl_copyright.textColor = [UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:1.0];
            lbl_copyright.autoresizingMask = UIViewAutoresizingFlexibleWidth;
            
            // Logo
            img_logo = [[UIImageView alloc] initWithFrame:CGRectMake(l, [self topMargin] + 15, 170, 45)];
            img_logo.contentMode = UIViewContentModeScaleAspectFit;
            img_logo.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
            
            // Legal agreement
            lbl_legalAgreement = [[UILabel alloc] initWithFrame:CGRectMake(l, CGRectGetMaxY(img_logo.frame) + spacing, w, 80)];
            lbl_legalAgreement.adjustsFontSizeToFitWidth = YES;
            lbl_legalAgreement.backgroundColor = [UIColor clearColor];
            lbl_legalAgreement.font = [UIFont systemFontOfSize:11];
            lbl_legalAgreement.numberOfLines = 0;                       // Unlimited number of lines
            lbl_legalAgreement.textColor = [UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:1.0];
            lbl_legalAgreement.autoresizingMask = UIViewAutoresizingFlexibleWidth;
            
            // Container for username, password, and domain
            containerView = [[UIView alloc] initWithFrame:CGRectMake(l, CGRectGetMaxY(lbl_legalAgreement.frame)+10, w, CGRectGetMaxY(btn_login.frame))];
            containerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
            
            // View overlay
            status = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 480, 320)];
            status.backgroundColor = [UIColor colorWithWhite:0.95 alpha:1];
            status.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
            
            // Status label
            lbl_status = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 400, 60)];
            lbl_status.center = status.center;
            lbl_status.backgroundColor = [UIColor clearColor];
            lbl_status.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:fontSize];
            lbl_status.numberOfLines = 0;
            lbl_status.textAlignment = NSTextAlignmentCenter;
            lbl_status.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            
            // Continue button
            btn_continue = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            [btn_continue setTitle:ASFKitLocalizedString(@"ASFContinue") forState:UIControlStateNormal];
            [btn_continue addTarget:self action:@selector(onContinue:) forControlEvents:UIControlEventTouchUpInside];
            btn_continue.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin;
            btn_altcontinue = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            [btn_altcontinue setTitle:ASFKitLocalizedString(@"ASFContinue") forState:UIControlStateNormal];
            [btn_altcontinue addTarget:self action:@selector(onContinue:) forControlEvents:UIControlEventTouchUpInside];
            btn_altcontinue.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
            
            // Activity indicator
            activityInd = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            [activityInd startAnimating];
            activityInd.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin;
            
            _blankWindow = nil;
        }
        
    }
    return self;
}

- (void)dealloc
{
    [[ASFSession sharedSession] removeObserver:self forKeyPath:@"configuration"];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    domainPicker = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // "Hack" to ensure that the login view opens in the correct orientation.
    // While the login view is opening, isOpen is NO. It checks the position of the status bar and forces the
    // orientation of the login view to respect the orientation of the status bar (doesn't matter if status bar is visible or not).
    if (isOpen)
    {
        return YES;
    }
    else
    {
        if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation]))
        {
            return UIInterfaceOrientationIsLandscape(interfaceOrientation);
        }
        else
        {
            return UIInterfaceOrientationIsPortrait(interfaceOrientation);
        }
    }
}

- (BOOL) disablesAutomaticKeyboardDismissal
{
    return NO;
}

- (void) loadView
{
    self.view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 480, 320)];
}

- (CGSize) preferredContentSize
{
    return CGSizeMake(480, 310);
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    // Check if we're using the default interface
    if(_useDefaultInterface)
    {
        self.view.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        self.view.backgroundColor = containerView.backgroundColor = [UIColor colorWithWhite:230/255.0 alpha:1];
        
        // If we are, add the UITextField's to the view
        [self.view addSubview:img_logo];
        [self.view addSubview:lbl_legalAgreement];
        [containerView addSubview:txt_dummy];
        [containerView addSubview:txt_username];
        [containerView addSubview:txt_password];
        [containerView addSubview:txt_domain];
        [containerView addSubview:btn_login];
        [containerView addSubview:lbl_copyright];
        [self.view addSubview:containerView];
        
        containerViewY = containerView.frame.origin.y;
        
        // Prepare status view
        [status addSubview:lbl_status];
        [status addSubview:btn_continue];
        [status addSubview:btn_altcontinue];
        [status addSubview:activityInd];
        
        // If this is the iPhone, listen for the keyboard to become visible
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone)
        {
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillAppear) name:UIKeyboardWillShowNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillDisappear) name:UIKeyboardWillHideNotification object:nil];
        }
    }
    
    // Listen for the configuration to change
    [self refreshInterface];
    [[ASFSession sharedSession] addObserver:self forKeyPath:@"configuration" options:0 context:NULL];
    
    // Listen for the app to prepare to go into the background
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppWillResignActive) name:UIApplicationWillResignActiveNotification object:nil];
}

- (void) viewDidUnload
{
    [super viewDidUnload];
    [[ASFSession sharedSession] removeObserver:self forKeyPath:@"configuration"];
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if (_keyboardVisible)
    {
        [UIView animateWithDuration:duration animations:^{
            CGRect frame = containerView.frame;
            frame.origin.y = UIInterfaceOrientationIsLandscape(toInterfaceOrientation) ? [self topMargin] : CGRectGetMaxY(img_logo.frame);
            containerView.frame = frame;
        }];
    }
}

- (void) viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    if (self.modalPresentationStyle == UIModalPresentationFormSheet)
    {
        self.view.superview.bounds = (CGRect) {
            .size = self.preferredContentSize
        };
    }
}

- (void) keyboardWillAppear
{
    _keyboardVisible = YES;
    [UIView animateWithDuration:0.3 animations:^{
        CGRect frame = containerView.frame;
        frame.origin.y = UIInterfaceOrientationIsLandscape(self.interfaceOrientation) ? [self topMargin] : CGRectGetMaxY(img_logo.frame);
        containerView.frame = frame;
    }];
}
- (void) keyboardWillDisappear
{
    _keyboardVisible = NO;
    [UIView animateWithDuration:0.3 animations:^{
        CGRect frame = containerView.frame;
        frame.origin.y = containerViewY;
        containerView.frame = frame;
    }];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self blankExternalScreen];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        viewReady = YES;
        if (queuedMessage) {
            [self showMessage:queuedMessage];
            queuedMessage = nil;
        }
    });
    
    // Dispatch an event
    [[NSNotificationCenter defaultCenter] postNotificationName:ASFLoginViewWillShowNotification object:[ASFSession sharedSession]];
    
    // Listen for another screen to be connected
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(blankExternalScreen) name:UIScreenDidConnectNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(unblankExternalScreen) name:UIScreenDidDisconnectNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppWillEnterForeground) name:UIApplicationWillEnterForegroundNotification object:nil];
}
- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    viewReady = YES;
    
    initialStatusBarStyle = [UIApplication sharedApplication].statusBarStyle;
    if (!(IS_IPAD))
    {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
    }
    
    isOpen = YES;
    [[NSNotificationCenter defaultCenter] postNotificationName:ASFLoginViewDidShowNotification object:[ASFSession sharedSession]];
}
- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    viewReady = NO;
    [[UIApplication sharedApplication] setStatusBarStyle:initialStatusBarStyle animated:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:ASFLoginViewWillHideNotification object:[ASFSession sharedSession]];
}
- (void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self unblankExternalScreen];
    viewReady = NO;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:ASFLoginViewDidHideNotification object:[ASFSession sharedSession]];
    
    // Stop listening for another screen to be connected
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIScreenDidConnectNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIScreenDidDisconnectNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
    isOpen = NO;
}

#pragma mark - Private methods

/// <devdoc>
/// Checks if each field is empty; if so, disables the login button. If not, enables the login button.
/// </devdoc>
- (void) checkFieldStatus
{
    // Check if all login form fields have values
    if (!txt_username.isEmpty
        && !txt_password.isEmpty
        && !txt_domain.isEmpty) 
    {
        btn_login.enabled = YES;
    }
    else
    {
        btn_login.enabled = NO;
    }
}

/// <devdoc>Creates a UIPopover for the user to select the domain to connect to.</devdoc>
- (void) selectDomain:(id)sender
{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone)
    {
        UIPickerView *pickerView = [[UIPickerView alloc] init];
        pickerView.dataSource = domainPicker;
        pickerView.delegate = domainPicker;
        pickerView.showsSelectionIndicator = YES;
        
        txt_dummy.inputView = pickerView;
        
        UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:ASFKitLocalizedString(@"ASFDone") style:UIBarButtonItemStyleBordered target:self action:@selector(dismissDomainPicker)];
        UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIToolbar *toolbar = [[UIToolbar alloc] init];
        toolbar.barStyle = UIBarStyleBlackOpaque;
        [toolbar sizeToFit];
        toolbar.items = @[flexSpace,doneButton];
        txt_dummy.inputAccessoryView = toolbar;
    }
    else
    {
        if (!_domainPickerPopover)
        {
            // Create a popover picker
            _domainPickerPopover = [[UIPopoverController alloc] initWithContentViewController:domainPicker];
            _domainPickerPopover.delegate = self;
            
            // Let's keyboard appear before popover to ensure popover doesn't go behind/over keyboard
            dispatch_async(dispatch_get_main_queue(), ^{
                [_domainPickerPopover presentPopoverFromRect:txt_domain.frame inView:containerView permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
            });
        }
    }
    
    // For whatever reason, iOS doesn't like to change the first responder immediately after getting a new one.
    // So, we'll use this ugly fix to make it wait just a split second before making our dummy field
    // become the first responder.
    dispatch_async(dispatch_get_main_queue(), ^{
        [txt_dummy becomeFirstResponder];
    });
}

/// <devdoc>Recieves the delegate notification for when a user selects a domain.</devdoc>
- (void) domainSelected:(NSString *)domain
{
    txt_domain.text = domain;
    [_domainPickerPopover dismissPopoverAnimated:YES];
    
    if (_domainPickerPopover)
        [self popoverControllerDidDismissPopover:_domainPickerPopover];
    
    [self checkFieldStatus];
}

- (void) popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    txt_dummy.canResignFirstResponder = YES;
    _domainPickerPopover = nil;
}

- (void) dismissDomainPicker
{
    [txt_dummy resignFirstResponder];
}

/// <devdoc>Fades the status view over the login view.</devdoc>
- (void) showStatusView
{
    CGRect frame = status.frame;
    frame.size = self.view.bounds.size;
    status.frame = frame;
    

    // Make sure the button has enough width to fit it's content
    [btn_continue sizeToFit];
    btn_continue.frame = ({
        CGRect frame = btn_continue.frame;
        frame.size.height = btn_login.frame.size.height;
        frame.origin.x = status.frame.size.width - frame.size.width - 17;
        frame.origin.y = status.frame.size.height - frame.size.height - 15;
        frame;
    });
    [btn_altcontinue sizeToFit];
    btn_altcontinue.frame = ({
        CGRect frame = btn_altcontinue.frame;
        frame.size.height = btn_login.frame.size.height;
        frame.origin.x = 17;
        frame.origin.y = status.frame.size.height - frame.size.height - 15;
        frame;
    });
    activityInd.center = CGPointMake(CGRectGetMidX(btn_continue.frame) + 10, CGRectGetMidY(btn_continue.frame));
    
    if (status.superview) return;
    
    status.alpha = 0;
    
    // Place the status view below the notification (if there is one)
    UIView *notification = [self.view viewWithTag:400];
    if (notification)
        [self.view insertSubview:status belowSubview:notification];
    else
        [self.view addSubview:status];
    
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionTransitionFlipFromLeft
                     animations:^{
                         status.alpha = 1;
                     }
                     completion:nil
     ];
}

/// <devdoc>Remove the "splash" from the external screen (if connected).</devdoc>
- (void) unblankExternalScreen
{
    if (_blankWindow) 
    {
        [_blankWindow setHidden:YES];
        [_blankWindow setScreen:nil];
    }
}

/// <devdoc>
/// Invoked when the application is about to enter the foreground. Used to check whether an external monitor is connected and, if so, blank the external screen.
/// </devdoc>
- (void) onAppWillEnterForeground
{
    [self blankExternalScreen];
}

/// <devdoc>User pressed the "login" button.</devdoc>
- (IBAction) doLogin: (id) sender
{
    // Verify that the username/password fields have text
    if ([txt_username.text length]<1 || [txt_password.text length]<1) 
    {
        [ASFLog message:@"No username/password."];
        [[[UIAlertView alloc] initWithTitle:ASFKitLocalizedString(@"ASFErrorMissingCredentials") message:nil delegate:nil cancelButtonTitle:ASFKitLocalizedString(@"ASFOK") otherButtonTitles: nil] show];
        return;
    }
    
    [txt_username resignFirstResponder];
    [txt_password resignFirstResponder];
    [txt_dummy resignFirstResponder];
    
    [self.delegate userDidLoginWithUsername:txt_username.text password:txt_password.text domain:txt_domain.text];
    
    // Immediately destroy password value
    txt_password.text = @"";
}

/// <devdoc>User pressed the "Continue" button.</devdoc>
- (void) onContinue:(id)sender
{
    [self.delegate userDidTapButton:sender];
}

/**
 Updates the interface elements to reflect the current values in the session configuration.
 The logo, domain list, copyright, and legal agreement can change based on the values in the configuration.
 This gets triggered by a KVO notification on configuration and whenever the login view is created.
 */
- (void) refreshInterface
{
    // Update the logo
    NSString *logoPath = [ASFKitResourcesBundle() pathForResource:[NSString stringWithFormat:@"%@_LoginViewLogo",ASFKitConfiguration.branding] ofType:@"png"];
    img_logo.image = [UIImage imageWithContentsOfFile:logoPath];
    
    // Determine the default domain--
    // The default domain is the first item in the domains plist
    txt_domain.text = domainPicker.defaultDomain;
    
    // Update the legal agreement
    lbl_legalAgreement.text = ASFKitConfiguration.legalNotice;
    
    // Update the copyright
    lbl_copyright.text = [ASFKitLocalizedString(@"ASFLoginCopyright") stringByReplacingOccurrencesOfString:@"%@" withString:ASFKitConfiguration.companyName];
}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    // The configuration has changed; refresh the interface!
    [self refreshInterface];
}

/// <devdoc>
/// Triggered when the app is about to go into the background.
/// It removes the contents of the password field so when the app returns
/// to the foreground, the password field is blank.
/// </devdoc>
- (void) onAppWillResignActive
{
    // Clear out the password field
    self.txt_password.text = @"";
    
    // Remove the message (if it exists)
    UILabel *warningLabel = (UILabel *)[self.view viewWithTag:400];
    [warningLabel removeFromSuperview];
    warningLabel = nil;
}

- (CGFloat) topMargin
{
    if (IS_IPAD)
    {
        return 0;
    }
    
    return (int)[self respondsToSelector:@selector(edgesForExtendedLayout)] * 20;
}

# pragma mark - Public methods

- (void) blankExternalScreen
{
    // The iPad 1 doesn't have mirroring, so we don't need to worry about blanking the screen
    if ([[UIDevice currentDevice].model isEqualToString:@"iPad1,1"]) {
        return;
    }
    
    // If the external screen is already blanked, don't do anything
    if (_blankWindow && _blankWindow.hidden==NO)
    {
        return;
    }
    
    // Temporarily disable mirroring to prevent password from showing on external screen
    if ([[UIScreen screens] count]>1) 
    {
        if (!_blankWindow) 
        {
            _blankWindow = [[UIWindow alloc] init];
            _blankWindow.backgroundColor = [UIColor blackColor];
        }
        _blankWindow.screen = [[UIScreen screens] objectAtIndex:1];
        [_blankWindow setHidden:NO];
        
        // Add logo to the center of the screen
        NSString *logoName = [@"ASFKitResources.bundle" stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_BlankScreenLogo.png",ASFKitConfiguration.branding]];
        UIImageView *logo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:logoName]];
        logo.center = CGPointMake(_blankWindow.frame.size.width/2, _blankWindow.frame.size.height/2);
        [_blankWindow addSubview:logo];
    }
}

- (void) showMessage:(NSString *)message
{
    if (!viewReady) {
        queuedMessage = message;
        return;
    }
    
    ASFUILabel *warningLabel = [ASFUILabel new];
    warningLabel.tag = 400;
    warningLabel.text = message;
    warningLabel.textAlignment = NSTextAlignmentCenter;
    warningLabel.font = [UIFont boldSystemFontOfSize:11];
    warningLabel.backgroundColor = [UIColor colorWithRed:240/255.0 green:225/255.0 blue:100/255.0 alpha:1.0];
    
    CGFloat topMargin = [self topMargin];
    
    warningLabel.frame = CGRectMake(0, -18, self.view.frame.size.width, 18 + topMargin);
    warningLabel.insets = UIEdgeInsetsMake(topMargin, 0, 0, 0);
    warningLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    [self.view addSubview:warningLabel];
    
    [UIView animateWithDuration:0.5 delay:0 options:0 animations:^
    {
        CGRect frame = warningLabel.frame;
        frame.origin.y = 0;
        warningLabel.frame = frame;
    }
    completion:^(BOOL finished){
        [UIView animateWithDuration:0.5 delay:4.0 options:UIViewAnimationOptionCurveEaseOut animations:^
         {
             CGRect frame = warningLabel.frame;
             frame.origin.y -= frame.size.height;
             warningLabel.frame = frame;
         }
         completion:^(BOOL finished)
         {
             [warningLabel removeFromSuperview];
         }];
    }];
}

- (void) hideStatusView
{
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionTransitionFlipFromRight
                     animations:^{
                         status.alpha = 0;
                     }
                     completion:^(BOOL result){
                         [status removeFromSuperview];
                     }
     ];
    [self checkFieldStatus];
}

- (void) setStatusMessage:(NSString *)message withActivitySpinner:(BOOL)withActivity andButton:(BOOL)withButton
{
    [self setStatusMessage:message withActivitySpinner:withActivity andButton:withButton andAltButton:NO];
}

- (void) setStatusMessage:(NSString *)message withActivitySpinner:(BOOL)withActivity andButton:(BOOL)withButton andAltButton:(BOOL)altButton
{
    lbl_status.text = message;
    btn_continue.hidden = !withButton;
    btn_altcontinue.hidden = !altButton;
    if (withActivity)
        [activityInd startAnimating];
    else
        [activityInd stopAnimating];
    [self showStatusView];
}

# pragma mark - UITextField delegate methods
- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == txt_domain) {
        [self selectDomain:textField];
    }
    
    [self checkFieldStatus];
    if ([self.delegate respondsToSelector:@selector(textFieldDidBecomeActive:)])
    {
        [self.delegate textFieldDidBecomeActive:textField];
    }
}
- (void) textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == txt_dummy) {
        [self domainSelected:txt_domain.text];
    }
    
    [self checkFieldStatus];
    if ([self.delegate respondsToSelector:@selector(textFieldDidResignActive:)])
    {
        [self.delegate textFieldDidResignActive:textField];
    }
}
- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    if (textField==txt_username)
        [txt_password becomeFirstResponder];
    else if (textField==txt_password)
        [self doLogin:nil];
    
    return YES;
}
- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    [self checkFieldStatus];
    return YES;
}


@end
