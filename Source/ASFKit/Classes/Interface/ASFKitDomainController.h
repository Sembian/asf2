//
//  ASFKitDomainController.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import <UIKit/UIKit.h>

/// Specifies delegate callbacks for user events within the domain picker.
@protocol ASFDomainControllerDelegate
/// Fired when the user selects a domain from the UITableView.
/// @param domain The selected domain.
- (void)domainSelected:(NSString *)domain;
@end

/// The UITableView listing the possible domains the user can connect to.
/// <history>
///     05/13/11    [dpfeiffer.float]   Created.
/// </history>
@interface ASFKitDomainController : UITableViewController <UIPickerViewDataSource, UIPickerViewDelegate>

/// Array of possible domains.
@property (nonatomic, readonly) NSArray *domains;
/// Receiver for delegate events.
@property (nonatomic, weak) id<ASFDomainControllerDelegate> delegate;

@property (nonatomic, strong, readonly) NSString *defaultDomain;

@end
