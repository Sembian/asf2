//
//  ASFUILabel.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ASFUILabel : UILabel

@property (nonatomic, readwrite) UIEdgeInsets insets;

@end
