//
//  ASFUserProfileGroupController.m
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFUserProfileGroupController.h"
#import "ASFAuthorization.h"
#import "ASFProfile_Private.h"
#import "ASFLog.h"

@interface ASFUserProfileGroupController ()
@property (nonatomic, strong) NSURL *url;
@property (nonatomic, strong) ASFAuthorization *authorization;
@property (nonatomic, strong) ASFUserProfileGroupRequestCompletion completion;
@end

@implementation ASFUserProfileGroupController

- (instancetype) initWithURL:(NSURL *)url;
{
    if (self = [self init]) {
        self.url = url;
    }
    return self;
}
+ (ASFUserProfileGroupController *) controllerWithURL:(NSURL *)url
{
    return [[self alloc] initWithURL:url];
}

- (void) profileGroupForAuthorization:(ASFAuthorization *)authorization completion:(ASFUserProfileGroupRequestCompletion)completion
{
    self.completion = completion;
    self.authorization = authorization;
    self.authorization.profile.userType = ASFKitLocalizedString(@"ASFUserTypeUnknown"); // Prime the value
    
    ASFURLRequest *request = [ASFURLRequest requestWithURL:self.url delegate:self];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    request.HTTPMethod = ASFURLRequestTypePOST;
    request.HTTPBody = authorization.profile.JSONData;
    
    [ASFLog message:[NSString stringWithFormat:@"Retrieving profile group from %@...", self.url]];
    [request start];
}

#pragma mark - Connection delegate
- (void) requestDidFinish:(ASFURLRequest *)request
{
    NSDictionary *response = [NSJSONSerialization JSONObjectWithData:request.responseData options:0 error:nil];
    
    // If there is no response, but the status code was 200, consider that a failure.
    if (!response) {
        self.completion(NO, [ASFError errorWithCode:ASFErrorProfileGroupRetrievalFailure andLocalizedDescription:ASFKitLocalizedString(@"The profile group service responded with no data.")], nil);
    } else if (response[@"error"]) {
        self.completion(NO, [ASFError errorWithCode:ASFErrorProfileGroupRetrievalFailure andLocalizedDescription:ASFKitLocalizedString(response[@"error"])], nil);
    } else {
        self.authorization.profile.userType = response[@"profile"][@"user_type"];
        self.completion(YES, nil, self.authorization);
    }
}
- (void) request:(ASFURLRequest *)request didFailWithError:(NSError *)error
{
    self.completion(NO, [ASFError errorWithCode:ASFErrorProfileGroupRetrievalFailure andLocalizedFailureReason:nil andUnderlyingError:error], nil);
}

@end
