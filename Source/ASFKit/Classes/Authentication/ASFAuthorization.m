//
//  Authorization.m
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFAuthorization.h"
#import "ASFProfile.h"
#import "ASFAuthorization_Private.h"

@implementation ASFAuthorization
@synthesize accessToken=_accessToken, expiration=_expiration, profile=_profile, refreshToken=_refreshToken;

- (id) initWithAuthorizationResponse:(NSDictionary *)authResponse
{
    if (self = [super init])
    {
        _accessToken = [authResponse objectForKey:@"access_token"];
        _refreshToken = [authResponse objectForKey:@"refresh_token"];
        _expiration = [NSDate dateWithTimeIntervalSinceNow:[[authResponse objectForKey:@"expires_in"] intValue]];
    }
    return self;
}

+ (ASFAuthorization *) authorizationWithAuthorizationResponse:(NSDictionary *)authResponse
{
    return [[ASFAuthorization alloc] initWithAuthorizationResponse:authResponse];
}

- (NSString *) description
{
    return [NSString stringWithFormat:@"%@", self.dictionary];
}

#pragma mark - "Protected" methods
- (BOOL) isValid
{
    // An authorization is considered valid if it has...
    //  1. A user profile with a UPI
    return self.profile.upi.length;
}
- (void) setProfile:(ASFProfile *)profile
{
    _profile = profile;
}
- (ASFAuthorization *) publicAuthorization
{
    NSDictionary *publicData = @{
                                  @"profile" : @{
                                          @"upi" : self.profile.upi
                                          }
                                  };
    ASFAuthorization *auth = [[[self class] alloc] initWithDictionary:publicData];
    return auth;
}

#pragma mark - NSCoding implementation
- (void) encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeRootObject:self.dictionary];
    [aCoder encodeObject:_refreshToken forKey:@"refresh_token"];
}
- (id) initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithDictionary:[aDecoder decodeObject]])
    {
        _refreshToken = [aDecoder decodeObjectForKey:@"refresh_token"];
    }
    return self;
}

@end
