//
//  ASFUserProfileGroupController.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASFURLRequest.h"

typedef void(^ASFUserProfileGroupRequestCompletion)(BOOL result, NSError *error, ASFAuthorization *authorization);

/// Retrieves the user's profile group (user type).
/// The licensing server is currently responsible for handling profile groups.
/// @since 2.1
@interface ASFUserProfileGroupController : NSObject <ASFURLRequestDelegate>

- (instancetype) initWithURL:(NSURL *)url;
+ (ASFUserProfileGroupController *) controllerWithURL:(NSURL *)url;

- (void) profileGroupForAuthorization:(ASFAuthorization *)authorization completion:(ASFUserProfileGroupRequestCompletion)completion;

@end
