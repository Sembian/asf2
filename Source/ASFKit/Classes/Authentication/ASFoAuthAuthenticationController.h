//
//  oAuthAuthenticationController.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASFURLRequest.h"

@class ASFAuthorization;
typedef void (^ASFoAuthLoginResponse)(BOOL result, NSError *error, ASFAuthorization *authorization);

@interface ASFoAuthAuthenticationController : NSObject <ASFURLRequestDelegate>

- (id) initWithURL:(NSURL *)url clientId:(NSString *)clientId andClientPassword:(NSString *)clientPassword;
+ (ASFoAuthAuthenticationController *) controllerWithURL:(NSURL *)url clientId:(NSString *)clientId andClientPassword:(NSString *)clientPassword;
- (void) loginWithUsername:(NSString *)username andPassword:(NSString *)password forDomain:(NSString *)domain completion:(ASFoAuthLoginResponse)onComplete;
- (void) refreshAccessToken:(ASFAuthorization *)authorization completion:(ASFoAuthLoginResponse)onComplete;

@end
