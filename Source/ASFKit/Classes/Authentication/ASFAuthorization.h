//
//  Authorization.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SerializableNSObject.h"

@class ASFProfile;
@interface ASFAuthorization : SerializableNSObject <NSCoding>

@property (nonatomic, readonly) NSString *accessToken;
@property (nonatomic, readonly) NSDate *expiration;
@property (nonatomic, readonly) ASFProfile *profile;

@property (nonatomic, readonly) BOOL isRefreshed;

- (id) initWithAuthorizationResponse:(NSDictionary *)authResponse;
+ (ASFAuthorization *) authorizationWithAuthorizationResponse:(NSDictionary *)authResponse;

@end
