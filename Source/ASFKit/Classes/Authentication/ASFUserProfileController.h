//
//  UserProfileController.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASFURLRequest.h"

@class ASFAuthorization;

typedef void(^ASFUserProfileRequestCompletion)(BOOL result, NSError *error, ASFAuthorization *authorization);

@interface ASFUserProfileController : NSObject <ASFURLRequestDelegate>

- (id) initWithURL:(NSURL *)url;
+ (ASFUserProfileController *) controllerWithURL:(NSURL *)url;

- (void) profileForAuthorization:(ASFAuthorization *)authorization completion:(ASFUserProfileRequestCompletion)completion;

@end
