//
//  oAuthAuthenticationController.m
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFoAuthAuthenticationController.h"
#import "ASFAuthorization.h"
#import "ASFKitDevice.h"
#import "ASFLog.h"
#import "NSString+ASFEncoding.h"

#define ContentType     @"application/x-www-form-urlencoded"

@interface ASFAuthorization()
@property (nonatomic, retain) NSString *refreshToken;
@end

@interface ASFoAuthAuthenticationController ()
@property (nonatomic, strong) NSURL *endpoint;
@property (nonatomic, strong) NSString *clientId;
@property (nonatomic, strong) NSString *clientPassword;
@property (nonatomic, strong) ASFoAuthLoginResponse onComplete;
@end

@implementation ASFoAuthAuthenticationController

- (id) initWithURL:(NSURL *)url clientId:(NSString *)clientId andClientPassword:(NSString *)clientPassword
{
    if (self = [super init])
    {
        _endpoint = url;
        _clientId = clientId;
        _clientPassword = clientPassword;
    }
    return self;
}

+ (ASFoAuthAuthenticationController *) controllerWithURL:(NSURL *)url clientId:(NSString *)clientId andClientPassword:(NSString *)clientPassword
{
    return [[ASFoAuthAuthenticationController alloc] initWithURL:url clientId:clientId andClientPassword:clientPassword];
}

- (void) loginWithUsername:(NSString *)username andPassword:(NSString *)password forDomain:(NSString *)domain completion:(ASFoAuthLoginResponse)onComplete
{
    self.onComplete = onComplete;
    
    // Prepare the grant request
    NSString *grantRequest = [NSString stringWithFormat:@"grant_type=password&username=%@\\%@&password=%@&scope=profile_name profile_email profile_network profile_contact profile_location profile_org profile_sales&state=%@", [domain asf_urlEncodedString], [username asf_urlEncodedString], [password asf_urlEncodedString], [[self stateParameterForCurrentApplication] asf_urlEncodedString]];
    
    // Prepare the authorization header
    // Basic (Base64-encoded clientId: clientPassword)
    NSString *authorization = [NSString stringWithFormat:@"Basic %@", [[[NSString stringWithFormat:@"%@:%@",self.clientId, self.clientPassword] dataUsingEncoding:NSUTF8StringEncoding] base64Encoding]];
    
    // Prepare the request
    ASFURLRequest *request = [ASFURLRequest requestWithURL:self.endpoint delegate:self];
    request.HTTPMethod = ASFURLRequestTypePOST;
    request.serverErrorsAreFailures = NO;
    [request setValue:authorization forHTTPHeaderField:@"Authorization"];
    [request setHTTPBody:[grantRequest dataUsingEncoding:NSUTF8StringEncoding]];
    [ASFLog message:[NSString stringWithFormat:@"Sending credentials to %@...", self.endpoint]];
    [request start];
}

- (void) refreshAccessToken:(ASFAuthorization *)authorization completion:(ASFoAuthLoginResponse)onComplete
{
    self.onComplete = onComplete;
    
    // Prepare the grant request
    NSString *grantRequest = [NSString stringWithFormat:@"grant_type=refresh_token&refresh_token=%@&state=%@",authorization.refreshToken,[[self stateParameterForCurrentApplication] asf_urlEncodedString]];
    
    // Prepare the authorization header
    // Basic (Base64-encoded clientId: clientPassword)
    NSString *auth = [NSString stringWithFormat:@"Basic %@", [[[NSString stringWithFormat:@"%@:%@",self.clientId, self.clientPassword] dataUsingEncoding:NSUTF8StringEncoding] base64Encoding]];
    
    // Prepare the request
    ASFURLRequest *request = [ASFURLRequest requestWithURL:self.endpoint delegate:self];
    request.HTTPMethod = ASFURLRequestTypePOST;
    request.serverErrorsAreFailures = NO;
    [request setValue:auth forHTTPHeaderField:@"Authorization"];
    [request setHTTPBody:[grantRequest dataUsingEncoding:NSUTF8StringEncoding]];
    [ASFLog message:[NSString stringWithFormat:@"Sending refresh token to %@...", self.endpoint]];
    [request start];
}

- (NSString *) stateParameterForCurrentApplication
{
    // The state parameter should be: [Unique device identifier]:[Application Bundle ID]
    // Once determined for the current application, it should NEVER change during runtime
    static NSString *state = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        state = [NSString stringWithFormat:@"%@:%@", [ASFKitDevice uniqueIdentifier], [NSBundle mainBundle].infoDictionary[(NSString *)kCFBundleIdentifierKey]];
    });
    return state;
}

#pragma mark - Connection Delegate methods
- (void) requestDidFinish: (ASFURLRequest *)request
{
    NSDictionary *response = [NSJSONSerialization JSONObjectWithData:request.responseData options:0 error:nil];
    
    // Check the response for an error
    if ([response valueForKey:@"error"])
    {
        NSError *error = [ASFError errorWithCode:ASFErrorAuthorizationFailed
                         andLocalizedDescription:ASFKitLocalizedString([response valueForKey:@"error_description"])];
        self.onComplete(NO, error, nil);
    }
    else if (request.responseCode >= 500)
    {
        NSError *underlyingError = [ASFError errorWithCode:request.responseCode andUserInfo:@{
                                                                                                     NSLocalizedDescriptionKey : [NSHTTPURLResponse localizedStringForStatusCode:request.responseCode],
                                                                                                     NSLocalizedFailureReasonErrorKey : [[NSString alloc] initWithData:request.responseData encoding:NSUTF8StringEncoding]
                                                                                                     }];
        [self request:request didFailWithError:underlyingError];
    }
    else
    {
        self.onComplete(YES, nil, [ASFAuthorization authorizationWithAuthorizationResponse:response]);
    }
}

- (void) request: (ASFURLRequest *)connection didFailWithError:(NSError *)error
{
    NSError *authError = [ASFError errorWithCode:ASFErrorAuthorizationFailed andLocalizedFailureReason:nil andUnderlyingError:error];
    self.onComplete(NO, authError, nil);
}

@end
