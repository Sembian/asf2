//
//  UserProfileController.m
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFUserProfileController.h"
#import "ASFAuthorization_Private.h"
#import "ASFProfile.h"
#import "ASFLog.h"

@interface ASFUserProfileController ()
@property (nonatomic, strong) NSURL *url;
@property (nonatomic, strong) ASFAuthorization *authorization;
@property (nonatomic, strong) ASFUserProfileRequestCompletion completion;
@end

@implementation ASFUserProfileController

- (id) initWithURL:(NSURL *)url
{
    if (self = [super init])
    {
        _url = url;
    }
    return self;
}
+ (ASFUserProfileController *) controllerWithURL:(NSURL *)url
{
    return [[ASFUserProfileController alloc] initWithURL:url];
}

- (void) profileForAuthorization:(ASFAuthorization *)authorization completion:(ASFUserProfileRequestCompletion)completion
{
    self.completion = completion;
    self.authorization = authorization;
    
    // Prepare the authorization header
    // Bearer <<access token>>
    NSString *a = [NSString stringWithFormat:@"Bearer %@", authorization.accessToken];
    
    ASFURLRequest *request = [ASFURLRequest requestWithURL:self.url delegate:self];
    [request setValue:a forHTTPHeaderField:@"Authorization"];
    
    [ASFLog message:[NSString stringWithFormat:@"Retrieving profile from %@...", self.url]];
    [request start];
}

#pragma mark - Connection delegate
- (void) requestDidFinish:(ASFURLRequest *)request
{
    NSDictionary *response = [NSJSONSerialization JSONObjectWithData:request.responseData options:0 error:nil];
    
    // If there is no response, but the status code was 200, consider that a failure.
    if (!response) {
        self.completion(NO, [ASFError errorWithCode:ASFErrorProfileRetrievalFailed andLocalizedDescription:ASFKitLocalizedString(@"The profile server responded with no data.")], nil);
    } else {
        [self.authorization setProfile:[[ASFProfile alloc] initWithDictionary:response]];
        self.completion(YES, nil, self.authorization);
    }
}
- (void) request:(ASFURLRequest *)request didFailWithError:(NSError *)error
{
    self.completion(NO, [ASFError errorWithCode:ASFErrorProfileRetrievalFailed andLocalizedFailureReason:nil andUnderlyingError:error], nil);
}

@end
