//
//  ASFProfile_Private.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import <ASFKit/ASFKit.h>

@interface ASFProfile ()
@property (nonatomic, strong) NSString *userType;
@end
