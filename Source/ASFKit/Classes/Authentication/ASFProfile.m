//
//  Profile.m
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFProfile_Private.h"

@implementation ASFProfile
{
    /**
     Keep a record of all profile values retrieved from the profile server in case a user needs
     to access a non-standard property that has not been defined in the ASFProfile class.
     */
    NSDictionary *allProfileValues;
}

- (id) initWithDictionary:(NSDictionary *)dict
{
    if (self = [super initWithDictionary:dict])
    {
        allProfileValues = [dict copy];
    }
    return self;
}

- (NSDictionary *) dictionary
{
    NSMutableDictionary *dictionary = [allProfileValues mutableCopy];
    if (self.userType) {
        [dictionary setObject:self.userType forKey:@"userType"];
    }
    return [NSDictionary dictionaryWithDictionary:dictionary];
}

- (NSString *) description
{
    return [NSString stringWithFormat:@"%@", self.displayName];
}

- (id) valueForUndefinedKey:(NSString *)key
{
    // Check if the key is defined in any unhandled profile values
    NSString *value;
    if ((value = [allProfileValues objectForKey:key])) {
        return value;
    }
    return [super valueForUndefinedKey:key];
}

- (NSString *) profileValueForKey:(NSString *)key
{
    return [self valueForKey:key];
}

@end
