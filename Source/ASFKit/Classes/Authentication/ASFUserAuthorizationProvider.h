//
//  UserAuthorizationProvider.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ASFAuthorization, ASFUserProfileController;
@protocol ASFUserAuthorizationProviderDelegate <NSObject>

@required
- (void) authorizationDidSucceed:(ASFAuthorization *)authorization duration:(NSTimeInterval)duration;
- (void) authorizationDidRefresh:(ASFAuthorization *)authorization duration:(NSTimeInterval)duration;
- (void) authorizationDidFailWithError:(NSError *)error;
- (void) profileRetrievalFromURL:(NSURL *)url didSucceed:(ASFAuthorization *)authorization duration:(NSTimeInterval)duration;
- (void) profileRetrievalFromURL:(NSURL *)url didFailWithError:(NSError *)error;

@end

@interface ASFUserAuthorizationProvider : NSObject

@property (nonatomic, weak) id<ASFUserAuthorizationProviderDelegate> delegate;

- (id) initWithDelegate:(id<ASFUserAuthorizationProviderDelegate>)delegate;
+ (ASFUserAuthorizationProvider *) authenticateUser:(NSString *)username withPassword:(NSString *)password forDomain:(NSString *)domain delegate:(id<ASFUserAuthorizationProviderDelegate>)delegate;
+ (ASFUserAuthorizationProvider *) updateProfileForAuthorization:(ASFAuthorization *)authorization delegate:(id<ASFUserAuthorizationProviderDelegate>)delegate;
+ (ASFUserAuthorizationProvider *) refreshAuthorizationTokenWithAuthorization:(ASFAuthorization *)authorization delegate:(id<ASFUserAuthorizationProviderDelegate>)delegate;

@end
