//
//  UserAuthorizationProvider.m
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFUserAuthorizationProvider.h"
#import "ASFoAuthAuthenticationController.h"
#import "ASFUserProfileController.h"
#import "ASFUserProfileGroupController.h"
#import "ASFConfiguration.h"

@interface ASFUserAuthorizationProvider ()
@property (nonatomic, strong) ASFoAuthAuthenticationController *authController;
@property (nonatomic, strong) ASFUserProfileController *profileController;
@property (nonatomic, strong) ASFUserProfileGroupController *profileGroupController;
@end

static NSMutableArray *activeInstances;

@implementation ASFUserAuthorizationProvider
@synthesize delegate=_delegate, authController=_authController, profileController=_profileController;

+ (void) initialize
{
    activeInstances = [NSMutableArray array];
}

#pragma mark - private methods
- (void) retrieveProfileForAuthorization:(ASFAuthorization *)authorization
{
    [self hold];
    NSDate *start = [NSDate date];
    // Credentials were verified, now get the profile and the profile group
    __weak ASFUserAuthorizationProvider *provider = self;
    NSURL *url = ASFKitConfiguration.profileEndpoint;
    self.profileController = [ASFUserProfileController controllerWithURL:url];
    NSURL *profileGroupURL = [[ASFSession sharedSession].configurationProfileURL URLByAppendingPathComponent:ProfileGroupsURI];
    self.profileGroupController = [ASFUserProfileGroupController controllerWithURL:profileGroupURL];
    
    [self.profileController profileForAuthorization:authorization completion:^(BOOL result, NSError *error, ASFAuthorization *authorization)
    {
        if (!result)
        {
            [provider.delegate profileRetrievalFromURL:url didFailWithError:error];
        }
        else
        {
            NSTimeInterval duration = [[NSDate date] timeIntervalSinceDate:start];
            [self.profileGroupController profileGroupForAuthorization:authorization completion:^(BOOL result, NSError *error, ASFAuthorization *authorization) {
                if (!result) {
                    [provider.delegate profileRetrievalFromURL:url didFailWithError:error];
                } else {
                    [provider.delegate profileRetrievalFromURL:url didSucceed:authorization duration:duration];
                }
            }];
        }
        [provider cleanup];
    }];
}

- (void) authenticateUser:(NSString *)username withPassword:(NSString *)password forDomain:(NSString *)domain
{
    [self hold];
    NSDate *start = [NSDate date];
    __weak ASFUserAuthorizationProvider *provider = self;
    self.authController = [ASFoAuthAuthenticationController controllerWithURL:[ASFSession sharedSession].configuration.authenticationEndpoint
                                                                     clientId:[ASFSession sharedSession].configuration.clientid
                                                            andClientPassword:[ASFSession sharedSession].configuration.clientSecret];
                           
     [self.authController loginWithUsername:username andPassword:password forDomain:domain completion:^(BOOL result, NSError *error, ASFAuthorization *authorization)
     {
         if (!result)
         {
             [provider.delegate authorizationDidFailWithError:error];
             [provider cleanup];
         }
         else
         {
             NSTimeInterval duration = [[NSDate date] timeIntervalSinceDate:start];
             [provider.delegate authorizationDidSucceed:authorization duration:duration];
             [provider retrieveProfileForAuthorization:authorization];
         }
     }];
}
- (void) refreshAuthorizationToken:(ASFAuthorization *)authorization
{
    [self hold];
    NSDate *start = [NSDate date];
    __weak ASFUserAuthorizationProvider *provider = self;
    self.authController = [ASFoAuthAuthenticationController controllerWithURL:[ASFSession sharedSession].configuration.authenticationEndpoint
                                                                     clientId:[ASFSession sharedSession].configuration.clientid
                                                            andClientPassword:[ASFSession sharedSession].configuration.clientSecret];
    [self.authController refreshAccessToken:authorization completion:^(BOOL result, NSError *error, ASFAuthorization *authorization)
    {
        if (!result)
        {
            [provider.delegate authorizationDidFailWithError:error];
            [provider cleanup];
        }
        else
        {
            NSTimeInterval duration = [[NSDate date] timeIntervalSinceDate:start];
            [provider.delegate authorizationDidRefresh:authorization duration:duration];
            [provider retrieveProfileForAuthorization:authorization];
        }
    }];
}

- (void) cleanup
{
    [activeInstances removeObject:self];
}
- (void) hold
{
    if (![activeInstances containsObject:self])
    {
        [activeInstances addObject:self];
    }
}

#pragma mark - public methods

- (id) initWithDelegate:(id<ASFUserAuthorizationProviderDelegate>)delegate
{
    if (self = [super init])
    {
        _delegate = delegate;
    }
    return self;
}

+ (ASFUserAuthorizationProvider *) authenticateUser:(NSString *)username withPassword:(NSString *)password forDomain:(NSString *)domain delegate:(id<ASFUserAuthorizationProviderDelegate>)delegate
{
    ASFUserAuthorizationProvider *provider = [[ASFUserAuthorizationProvider alloc] initWithDelegate:delegate];
    [provider authenticateUser:username withPassword:password forDomain:domain];
    return provider;
}
+ (ASFUserAuthorizationProvider *) updateProfileForAuthorization:(ASFAuthorization *)authorization delegate:(id<ASFUserAuthorizationProviderDelegate>)delegate
{
    ASFUserAuthorizationProvider *provider = [[ASFUserAuthorizationProvider alloc] initWithDelegate:delegate];
    [provider retrieveProfileForAuthorization:authorization];
    return provider;
}
+ (ASFUserAuthorizationProvider *) refreshAuthorizationTokenWithAuthorization:(ASFAuthorization *)authorization delegate:(id<ASFUserAuthorizationProviderDelegate>)delegate
{
    ASFUserAuthorizationProvider *provider = [[ASFUserAuthorizationProvider alloc] initWithDelegate:delegate];
    [provider refreshAuthorizationToken:authorization];
    return provider;
}

@end
