//
//  Profile.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SerializableNSObject.h"

///
/// The ASFProfile class represents information about the current user of the application.
/// It is populated with profile data received from AbbVie's server.
///
/// This data is accessible either via a delegate method when the session starts:
///
/// 	- (void) sessionDidStart:(ASFSession *)session withUserProfile:(ASFProfile *)profile
/// 	{
///
/// 	}
///
/// Or via a property on the current authorization:
///
/// 	[ASFSession sharedSession].authorization.profile
///
/// You should not create copies of this instance or of any of the data represented within this class as it could change at any time.
/// Instead, keep references to the instance and consider using [key-value observing](https://developer.apple.com/library/mac/documentation/Cocoa/Conceptual/KeyValueObserving/KeyValueObserving.html) to respond to changes.
/// @since 2.0
///
@interface ASFProfile : SerializableNSObject

/// @name Retrieving User Data

/// The user's current AD domain.
/// @since 2.0
@property (nonatomic, readonly) NSString *adDomain;

/// The user's current AD username (typically the same username used to log in to the application).
/// @since 2.0
@property (nonatomic, readonly) NSString *adLogon;

/// The user's default country.
/// @since 2.0
@property (nonatomic, readonly) NSString *c;

/// The user's co.
/// @since 2.0
@property (nonatomic, readonly) NSString *co;

/// The user's default company.
/// @since 2.0
@property (nonatomic, readonly) NSString *company;

/// The user's department.
/// @since 2.0
@property (nonatomic, readonly) NSString *department;

/// The department's code.
/// @since 2.1
@property (nonatomic, readonly) NSString *departmentcode;

/// The department's number.
/// @since 2.1
@property (nonatomic, readonly) NSString *departmentnumber;

/// The user's name.
/// @since 2.0
@property (nonatomic, readonly) NSString *displayName;

/// The user's division code.
/// @since 2.0
@property (nonatomic, readonly) NSString *divisionCode;

/// The user's division name.
/// @since 2.0
@property (nonatomic, readonly) NSString *divisionName;

/// The user's employee type.
/// @since 2.0
@property (nonatomic, readonly) NSString *employeeType;

/// The user's given name (commonly their "first" name).
/// @since 2.0
@property (nonatomic, readonly) NSString *givenName;

/// The user's initials.
/// @since 2.0
@property (nonatomic, readonly) NSString *initials;

/// The user's ledger department.
/// @since 2.1
@property (nonatomic, readonly) NSString *ledgerdepartment;

/// The user's location.
/// @since 2.0
@property (nonatomic, readonly) NSString *location;

/// The user's location code.
/// @since 2.1
@property (nonatomic, readonly) NSString *locationCode;

/// The user's email address.
/// @since 2.0
@property (nonatomic, readonly) NSString *mail;

/// The name of the user's manager.
/// @since 2.0
@property (nonatomic, readonly) NSString *managerDisplayName;

/// The email of the user's manager.
/// @since 2.0
@property (nonatomic, readonly) NSString *managerEmail;

/// The UPI of the user's manager.
/// @since 2.0
@property (nonatomic, readonly) NSString *managerUpi;

/// The user's postal code.
/// @since 2.1
@property (nonatomic, readonly) NSString *postalcode;

/// The user's preferred language.
/// @since 2.0
@property (nonatomic, readonly) NSString *preferredLanguage;

/// The user's sales area.
/// @since 2.0
@property (nonatomic, readonly) NSString *salesArea;

/// The user's sales category ID.
/// @since 2.0
@property (nonatomic, readonly) NSString *salesCategoryID;

/// The user's sales district.
/// @since 2.0
@property (nonatomic, readonly) NSString *salesDistrict;

/// The user's sales force name.
/// @since 2.0
@property (nonatomic, readonly) NSString *salesForceName;

/// The user's sales force number.
/// @since 2.0
@property (nonatomic, readonly) NSString *salesForceNumber;

/// The user's sales franchise.
/// @since 2.0
@property (nonatomic, readonly) NSString *salesFranchise;

/// The user's sales franchise ID.
/// @since 2.0
@property (nonatomic, readonly) NSString *salesFranchiseID;

/// The user's sales national.
/// @since 2.0
@property (nonatomic, readonly) NSString *salesNational;

/// The user's sales PMI.
/// @since 2.0
@property (nonatomic, readonly) NSString *salesPMI;

/// The user's primary sales responsibility.
/// @since 2.0
@property (nonatomic, readonly) NSString *salesPrimaryResponsibility;

/// The user's sales region.
/// @since 2.0
@property (nonatomic, readonly) NSString *salesRegion;

/// The user's sales sales force.
/// @since 2.0.1
@property (nonatomic, readonly) NSString *salesSalesForce;

/// The user's sales sampling.
/// @since 2.0
@property (nonatomic, readonly) NSString *salesSampling;

/// The user's sales team.
/// @since 2.0
@property (nonatomic, readonly) NSString *salesTeam;

/// The user's sales territory type ID.
/// @since 2.0.1
@property (nonatomic, readonly) NSString *salesTerritoryTypeID;

/// The user's surname (commonly the user's last name).
/// @since 2.0
@property (nonatomic, readonly) NSString *sn;

/// The user's space location ID.
/// @since 2.0
@property (nonatomic, readonly) NSString *spaceLocationID;

/// The user's space property ID.
/// @since 2.0
@property (nonatomic, readonly) NSString *spacePropertyID;

/// The user's space structure ID.
/// @since 2.0
@property (nonatomic, readonly) NSString *spaceStructureID;

/// The user's state.
/// @since 2.1
@property (nonatomic, readonly) NSString *st;

/// The user's street address.
/// @since 2.1
@property (nonatomic, readonly) NSString *streetaddress;

/// The user's business telephone number.
/// @since 2.0
@property (nonatomic, readonly) NSString *telephoneNumber;

/// The user's territory.
/// @since 2.0
@property (nonatomic, readonly) NSString *territory;

/// The user's title.
/// @since 2.0
@property (nonatomic, readonly) NSString *title;

/// The user's UPI.
/// @since 2.0
@property (nonatomic, readonly) NSString *upi;

/// The user's user type.
/// @since 2.1
@property (nonatomic, readonly) NSString *userType;

///
/// Retrieves a profile value for the provided key name. The value will match the value returned by the properties.
/// If there are extra properties returned from the Profile server that are not defined as properties, you can still retrieve those properties here.
/// The ASFProfile class keeps a copy of the response from the profile server.
///
/// @note          This method is simply a wrapper for the KVO -valueForKey: method. If trying to retrieve a value for an undefined key, this WILL throw an exception.
///
/// @param key     The key name of the property you want to retrieve.
/// @return        The profile value for the provided key.
/// @since 2.0.1
///
- (NSString *) profileValueForKey:(NSString *)key;

@end
