//
//  ASFAuthorization_Private.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFAuthorization.h"

@interface ASFAuthorization()
@property (nonatomic, strong) NSString *refreshToken;
@property (nonatomic, readwrite) BOOL isRefreshed;
- (BOOL) isValid;
- (void) setProfile:(ASFProfile *)profile;
/// The public authorization is stored in a less secure area of the device's keychain.
/// It only contains the current user's UPI. All other data will be stripped out.
/// This is intended to allow background fetches to continue to work even while the device is locked.
/// @since 2.1
- (ASFAuthorization *) publicAuthorization;
@end
