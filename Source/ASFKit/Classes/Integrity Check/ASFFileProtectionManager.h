//
//  ASFFileProtectionManager.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASFIntegrityCheckController.h"

/// Represents the protection status of a given file.
typedef NS_ENUM(NSInteger, ASFFileProtectionStatus) {
    /// The file is not protected or checked by the file protection API.
    ASFFileUnprotected,
    
    /// This file is checked by the file protection API but currently refers to a location that is unprotectable (e.g. a non-existant file or a symbolic link).
    ASFFileUnprotectable,
    
    /// The file is checked by the file protection API, but is currently not currently set to NSFileProtectionComplete.
    ASFFileProtectable,
    
    /// The file is checked by the file protection API and currently has NSFileProtectionComplete enabled.
    ASFFileProtected
};

/// Posted just before the files protected by ASFFileProtectionManager are removed from the application's sandbox.
extern NSString* const ASFProtectedFilesWillBeRemovedNotification;
/// Posted after the files protected by ASFFileProtectionManager are removed from the application's sandbox.
extern NSString* const ASFProtectedFilesWereRemovedNotification;
/// Posted just after a file attribute was modified (e.g. the NSFileProtectionKey was set to NSFileProtectionComplete).
extern NSString* const ASFProtectionAttributeModifiedNotification;
/// Dictionary key representing the NSArray containing a list of file paths that were removed. Sent in the userInfo dictionary for ASFProtectedFilesWereRemovedNotification.
extern NSString* const kRemovedFiles;

/// 
/// The file protection manager provides an API to implementing applications to control how files are protected.
/// For most applications, this API doesn't need to be used as it will automatically protect files in the way it thinks is best. By default, it will automatically protect:
/// -[Application]/Documents
/// -[Application]/Library
/// -[Application]/tmp
/// It will protect those directories recursively meaning that subdirectories and files are also protected.
///
/// The ASFFileProtectionManager implements the ASFIntegrityCheckProtocol and is execute as part of the periodic integrity check. It has two responsibilities:
/// 1. Ensuring that all the files in the application's sandbox are properly protected (typically this means setting each files NSFileProtectionKey to NSFileProtectionComplete), and
/// 2. Ensuring that the file system is still protected by ensuring that the leaf certificate and configuration profile are installed.
///
/// If point number 1 fails, the manager will fix the problem automatically. If ever point number 2 fails, the manager will signal to the ASFIntegrityCheckController that the shared session needs to be destroyed and all local files need to be removed.
///
/// Your application can subscribe to the ASFProtectedFilesWillBeRemovedNotification, ASFProtectedFilesWereRemovedNotification, and ASFProtectionAttributeModifiedNotification to be notified of when the ASFFileProtectionManager is going to make changes to the file system. Your application cannot prevent the manager from making changes, but it can ensure that it's ready to respond correctly when the changes occur (especially when files are about to be removed).
/// 
@interface ASFFileProtectionManager : NSObject <ASFIntegrityCheckProtocol>
{
    @private
    /// Directories/files protected by the authentication framework.
    NSMutableArray *protectedLocations;
    
    /// Directories/files excluded from protection.
    NSMutableArray *excludedLocations;
    
    /// Indicates whether the file system will be checked during the integriry check.
    BOOL enabled;
    
    /// YES if either the leaf certificate or configuration profile was not found.
    BOOL failed;
    
    /// The last error that was encountered when running the protection manager
    NSError *lastError;
    
    /// Indicates to the ASFIntegrityCheckController that the shared session should be destroyed.
    BOOL shouldEndSharedSession;
}

/// 
/// Adds a location for the ASFFileProtectionManager to protect.
/// By default, all locations in the application's sandbox are protected. You do not need to specifically protect any of your application's files unless you elsewhere remove protected. If you try to protect a file that is already protected, a notice will be printed in the console.
/// 
/// @param directoryOrFilePath The path of the file or directory that should be protected.
+ (void) beginProtectingLocation: (NSString *)directoryOrFilePath;

/// 
/// Prevents the ASFFileProtectionManager from protecting a directory or file.
/// By default, all locations in the application's sandbox are protected. Your application should only remove protection from a file after discussing with AbbVie the reasons why a file or directory should not be protected by the framework.
/// @note This should be placed in your application:didFinishLaunchingWithOptions: method.
/// 
/// @param directoryOrFilePath The path of the file or directory that should no longer be protected.
+ (void) stopProtectingLocation: (NSString *)directoryOrFilePath;

/// 
/// Checks a location to determine if it is currently protected or not by ASFFileProtectionManager.
/// 
/// @param directoryOrFilePath The path of the file or directory to check the protection status on.
/// @return YES if the location is protected; NO otherwise.
+ (BOOL) isLocationProtected: (NSString *)directoryOrFilePath;

/// 
/// Enables the protection of the file system.
/// This only enables the file system check during the integrity check. It doesn't change anything on the file system by simply calling this method.
/// 
+ (void) enableProtection;

/// 
/// Disables the check of the file system during the integrity check.
/// 
+ (void) disableProtection;

/// The shared ASFFileProtectionManager for this instance of the application.
+ (ASFFileProtectionManager *) defaultManager;

@end
