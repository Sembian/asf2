//
//  AuthorizationVerify.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASFIntegrityCheckController.h"

@class ASFAuthorization;
@interface ASFAuthorizationVerify : NSObject <ASFIntegrityCheckProtocol>
{
    @private
    BOOL profileRetrievalFailed;
    ASFAuthorization *newAuthorization;
}

@property (nonatomic, readwrite) BOOL didIntegrityCheckFail;
@property (nonatomic, readwrite) BOOL didEncounterTemporaryProblem;
@property (nonatomic, readwrite) BOOL didFailOnRefresh;
@property (nonatomic, strong) NSError *error;
@property (nonatomic, strong) ASFIntegrityCheckCompletionHandler completionHandler;

+ (ASFAuthorizationVerify *) authorizationVerifier;

@end
