//
//  ASFIntegrityCheckResult.m
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFIntegrityCheckResult.h"

@implementation ASFIntegrityCheckResult

- (id) initWithStatus:(ASFIntegrityCheckStatus)status errors:(NSArray *)errors failedChecks:(NSArray *)failedChecks passedChecks:(NSArray *)passedChecks endingLocalSession:(BOOL)endingLocalSession endingSharedSession:(BOOL)endingSharedSession deletingProtectedFiles:(BOOL)deletingProtectedFiles
{
    self = [super init];
    if (self)
    {
        _status                 =   status;
        _errors                 =   [errors copy];
        _failedChecks           =   [failedChecks copy];
        _passedChecks           =   [passedChecks copy];
        _endingLocalSession     =   endingLocalSession;
        _endingSharedSession    =   endingSharedSession;
        _deletingProtectedFiles =   deletingProtectedFiles;
        _completedDate          =   [NSDate date];
    }
    return self;
}

- (NSError *) error
{
    switch (self.errors.count)
    {
        case 0:
            return nil;
        case 1:
            return [self.errors lastObject];
        default:
            return [ASFError errorWithCode:ASFErrorIntegrityCheckMultipleFailures];
    }
}

- (BOOL) didCheckFail:(id<ASFIntegrityCheckProtocol>)check
{
    return [self.failedChecks containsObject:check];
}

@end
