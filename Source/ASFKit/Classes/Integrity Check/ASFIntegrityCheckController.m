//
//  ASFIntegrityCheckController.m
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFIntegrityCheckController.h"
#import "ASFIntegrityCheckResult.h"
#import "ASFReachability.h"
#import "ASFLog.h"

NSString* const ASFIntegrityCheckWillBeginNotification       =   @"IntegrityCheckWillBeginNotification";
NSString* const ASFIntegrityCheckFinishedNotification        =   @"IntegrityCheckFinishedNotification";

@interface ASFIntegrityCheckController()
- (void) onApplicationWillResignActive;
- (void) onApplicationDidBecomeActive;
- (void) onReachabilityChange;
- (void) integrityCheckFinished:(id<ASFIntegrityCheckProtocol>)check;
- (void) allIntegrityChecksFinished;
- (void) offlineFailure;
- (void) saveOfflineAttempts;
- (NSInteger) retrieveOfflineAttempts;
@property (nonatomic, strong) ASFIntegrityCheckResult *lastResult;
@end

@implementation ASFIntegrityCheckController
{
    dispatch_queue_t queue;
}
@synthesize delegate, integrityCheckInterval, checking, checksRemaining, intervalCheckingEnabled;

#pragma mark - Lifecycle
- (id) init
{
    self = [super init];
    if (self)
    {
        integrityCheckInterval = ASFKitConfiguration.defaultTimeoutInterval.intValue * 60;
        checking = NO;
        checksRemaining = 0;
        intervalCheckingEnabled = NO;
        integrityChecks = [NSMutableArray new];
        checkInterval = nil;
        reachability = [ASFReachability reachabilityWithHostName:@"mobileappserver.net"];
        [reachability startNotifier];
        queue = dispatch_queue_create("ASFIntegrityCheck", NULL);
        
        offlineAttempts = [self retrieveOfflineAttempts];
        delegate = nil;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReachabilityChange) name:kReachabilityChangedNotification object:reachability];
    }
    return self;
}
- (id) initWithDelegate:(id<ASFIntegrityCheckDelegate>)aDelegate
{
    if (!(self = [self init])) return nil;
    if (self)
    {
        self.delegate = aDelegate;
    }
    return self;
}

- (void) dealloc
{
    [self stopCheckInterval];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

#pragma mark - Private methods

- (BOOL) resumeCheckInterval
{
    // Verify that the integrity check can be resumed
    if (!checkInterval && intervalCheckingEnabled)
    {
        checkInterval = [NSTimer scheduledTimerWithTimeInterval:integrityCheckInterval target:self selector:@selector(performIntegrityCheck) userInfo:nil repeats:YES];
        [ASFLog message:@"Resumed integrity checking."];
        [checkInterval fire];
        return YES;
    }
    return NO;
}

- (void) pauseCheckInterval
{
    if (checkInterval)
    {
        [ASFLog message:@"Paused integrity checking."];
        [checkInterval invalidate];
        checkInterval = nil;
    }
}

- (void) onApplicationWillResignActive
{
    [self pauseCheckInterval];
}

- (void) onApplicationDidBecomeActive
{
    [self resumeCheckInterval];
}

- (void) onReachabilityChange
{
    if (!checkInterval) {
        return;
    }
    
    // Determine if we were offline
    BOOL wasOffline = self.offline;
    _offline = reachability.currentReachabilityStatus==NotReachable;
    
    [ASFLog message:@"The host reachability changed."];
    if (wasOffline && !self.offline && [ASFSession sharedSession].sessionStarted)
    {
        [self performIntegrityCheck];
    }
}

- (void) integrityCheckFinished:(id<ASFIntegrityCheckProtocol>)check
{
    [ASFLog message:[NSString stringWithFormat:@"Integrity check task finished: %@", check]];
    if (--checksRemaining<=0) 
    {
        [self allIntegrityChecksFinished];
    }
}

- (void) allIntegrityChecksFinished
{
    // Create a result
    NSMutableArray *passedChecks = [NSMutableArray array];
    NSMutableArray *failedChecks = [NSMutableArray array];
    NSMutableArray *errors = [NSMutableArray array];
    BOOL shouldDeleteFiles = NO;
    BOOL shouldEndLocalSession = NO;
    BOOL shouldEndSharedSession = NO;
    
    // Remember how many temporary failures there were before evalulating the last check
    NSInteger previousOfflineAttempts = offlineAttempts;
    
    for (id<ASFIntegrityCheckProtocol> check in integrityChecks) 
    {
        if ([check didIntegrityCheckFail])
        {
            [failedChecks addObject:check];
            
            if ([check shouldLocalSessionEndOnFailure])
            {
                shouldEndLocalSession = YES;
            }
            if ([check shouldSharedSessionEndOnFailure])
            {
                shouldEndSharedSession = YES;
            }
            if ([check shouldProtectedFilesBeDeletedOnFailure])
            {
                shouldDeleteFiles = YES;
            }
        }
        else
        {
            [passedChecks addObject:check];
        }
        
        if ([check didEncounterTemporaryProblem])
        {
            ++offlineAttempts;
        }
        
        if ([check error])
        {
            [errors addObject:[check error]];
        }
    }
    
    // If the number of offline attempts did not go up,
    // then the check fully succeeded and offline attempts can be cleared out.
    if (previousOfflineAttempts==offlineAttempts)
    {
        offlineAttempts = 0;
    }
    else if (offlineAttempts>ASFKitConfiguration.maxOfflineFailures.intValue)
    {
        [self offlineFailure];
        [self saveOfflineAttempts];
        return;
    }
    [self saveOfflineAttempts];
    
    ASFIntegrityCheckStatus status = [failedChecks count] ? ASFIntegrityCheckFailed : ASFIntegrityCheckPassed;
    
    ASFIntegrityCheckResult *result = [[ASFIntegrityCheckResult alloc] initWithStatus:status
                                                                                                   errors:[NSArray arrayWithArray:errors]
                                                                                             failedChecks:[NSArray arrayWithArray:failedChecks]
                                                                                             passedChecks:[NSArray arrayWithArray:passedChecks]
                                                                                       endingLocalSession:shouldEndLocalSession
                                                                                      endingSharedSession:shouldEndSharedSession
                                                                                   deletingProtectedFiles:shouldDeleteFiles];
    _lastResult = result;
    checking = NO;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:ASFIntegrityCheckFinishedNotification object:self userInfo:[NSDictionary dictionaryWithObject:result forKey:@"result"]];
        
        if (delegate)
        {
            [delegate integrityCheckFinishedWithResult:result];
        }
        
        if (shouldRecheckOnComplete) {
            [self performIntegrityCheck];
        }
    });
}

- (void) offlineFailure
{
    NSString *failureReason = [NSString stringWithFormat:@"The configuration allows for %@ offline failures, but %zd failures have occurred.", ASFKitConfiguration.maxOfflineFailures, offlineAttempts];
    NSError *error = [ASFError errorWithCode:ASFErrorIntegrityCheckMaxOfflineFailures andLocalizedFailureReason:failureReason];
    ASFIntegrityCheckResult *result = [[ASFIntegrityCheckResult alloc] initWithStatus:ASFIntegrityCheckFailed
                                                                                                   errors:[NSArray arrayWithObject:error]
                                                                                             failedChecks:nil
                                                                                             passedChecks:nil
                                                                                       endingLocalSession:YES
                                                                                      endingSharedSession:YES
                                                                                   deletingProtectedFiles:YES];
    
    _lastResult = result;
    checking = NO;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:ASFIntegrityCheckFinishedNotification object:self userInfo:[NSDictionary dictionaryWithObject:result forKey:@"result"]];
        
        if (delegate)
        {
            [delegate integrityCheckFinishedWithResult:result];
        }
    });
}

// TODO: saving/retrieving offline attempts won't work in background mode. Address.
- (void) saveOfflineAttempts
{
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInteger:offlineAttempts] forKey:@"IntegrityCheckOfflineAttempts"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSInteger) retrieveOfflineAttempts
{
    [[NSUserDefaults standardUserDefaults] synchronize];
    return [[[NSUserDefaults standardUserDefaults] valueForKey:@"IntegrityCheckOfflineAttempts"] intValue];
}

#pragma mark Public methods
- (BOOL) startCheckInterval
{
    if (intervalCheckingEnabled)
    {
        return NO;
    }
    
    [ASFLog message:[NSString stringWithFormat:@"Starting interval to check integrity every %.0f minutes.", (integrityCheckInterval/60.0)]];
    
    checkInterval = [NSTimer scheduledTimerWithTimeInterval:integrityCheckInterval target:self selector:@selector(performIntegrityCheck) userInfo:nil repeats:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onApplicationWillResignActive) name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onApplicationDidBecomeActive) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    intervalCheckingEnabled = YES;
    
    // Immediately check the session
    [checkInterval fire];
    
    return YES;
}

- (void) stopCheckInterval
{
    if (!intervalCheckingEnabled)
    {
        return;
    }
    
    [checkInterval invalidate];
    checkInterval = nil;
    intervalCheckingEnabled = NO;
    
    [ASFLog message:@"Stopping integrity check."];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
}

- (void) performIntegrityCheck
{
    dispatch_sync(queue, ^{
        if (checking)
        {
            shouldRecheckOnComplete = YES;
            [ASFLog message:@"Already performing integrity check."];
            return;
        }
        
        checking = YES;
        shouldRecheckOnComplete = NO;
        checksRemaining = [integrityChecks count];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:ASFIntegrityCheckWillBeginNotification object:self userInfo:nil];
        });
        [ASFLog message:@"Checking integrity of the session..."];
        
        if ([integrityChecks count]==0)
        {
            [self allIntegrityChecksFinished];
            return;
        }
        
        for (id<ASFIntegrityCheckProtocol> check in integrityChecks) 
        {
            [ASFLog message:[NSString stringWithFormat:@"Dispatching integrity check: %@", check]];
            
            // TODO: consider renaming to restrictedMode
            BOOL offlineMode = self.offline || ![UIApplication sharedApplication].protectedDataAvailable;
            [check performCheckInOfflineMode:offlineMode completion:^{
                dispatch_async(queue, ^{
                    [self integrityCheckFinished:check];
                });
            }];
            
            [ASFLog message:@"Finished dispatching integrity check."];
        }
    });
}

- (BOOL) addIntegrityCheck:(id<ASFIntegrityCheckProtocol>)check
{
    [integrityChecks addObject:check];
    return YES;
}

- (BOOL) removeIntegrityCheck:(id<ASFIntegrityCheckProtocol>)check
{
    [integrityChecks removeObject:check];
    return YES;
}

+ (ASFIntegrityCheckController *) sharedInstance
{
    static ASFIntegrityCheckController *sharedInstance = nil;
    if (!sharedInstance)
    {
        sharedInstance = [ASFIntegrityCheckController new];
    }
    return sharedInstance;
}

@end
