//
//  AuthorizationVerify.m
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFAuthorizationVerify.h"
#import "ASFAuthorization_Private.h"
#import "ASFUserAuthorizationProvider.h"
#import "ASFSession+Private.h"

@interface ASFAuthorizationVerify() <ASFUserAuthorizationProviderDelegate>
@end

@implementation ASFAuthorizationVerify

- (void) dealloc
{
    newAuthorization = nil;
}

#pragma mark - public methods
+ (ASFAuthorizationVerify *) authorizationVerifier
{
    return [ASFAuthorizationVerify new];
}

#pragma mark - private methods

- (ASFAuthorization *) getCurrentAuthorization
{
    return [[ASFSession sharedSession] getSharedAuthorization];
}

- (BOOL) shouldIgnoreError:(NSError *)error
{
    // Check the underlying error; some errors will be considered a "temporary failure"
    NSError *underlyingError = [error.userInfo objectForKey:NSUnderlyingErrorKey];
    // If the error is a timeout error OR in the 500 range, consider it a temporary failure
    // Also ignore any NSURLError domains as that indicates the request was never sent
    return underlyingError.code==ASFErrorRequestTimedOut || underlyingError.code==ASFErrorCancelled || underlyingError.code/100==5 || [underlyingError.domain isEqualToString:NSURLErrorDomain];
}

- (void) failedWithError:(NSError *)error
{
    self.error = error;
    // Check if this is an "ignorable" error.
    if ([self shouldIgnoreError:error])
    {
        self.didEncounterTemporaryProblem = YES;
        self.completionHandler();
        return;
    }
    
    // If it's a different kind of error, end the shared session.
    self.didIntegrityCheckFail = YES;
    self.completionHandler();
}

#pragma mark - Integrity Check Implementation
- (BOOL) shouldLocalSessionEndOnFailure
{
    return YES;
}
- (BOOL) shouldSharedSessionEndOnFailure
{
    return !self.didFailOnRefresh;
}
- (BOOL) shouldProtectedFilesBeDeletedOnFailure
{
    return NO;
}
- (void) performCheckInOfflineMode:(BOOL)offlineMode completion:(ASFIntegrityCheckCompletionHandler)completion
{
    // Reset
    self.didEncounterTemporaryProblem = self.didIntegrityCheckFail = profileRetrievalFailed = NO;
    self.error = nil;
    self.completionHandler = nil;
    
    if (offlineMode)
    {
        self.didEncounterTemporaryProblem = YES;
        completion();
        return;
    }
    
    self.completionHandler = completion;
    
    // Check if the authorization is expired
    // If the expected expiration date is not less than the current date ([NSDate date])
    // Then we're assuming the access token is expired and that it needs to be refreshed
    if ([[self getCurrentAuthorization].expiration compare:[NSDate date]]!=NSOrderedDescending)
    {
        [ASFUserAuthorizationProvider refreshAuthorizationTokenWithAuthorization:[self getCurrentAuthorization] delegate:self];
    }
    else
    {
        [ASFUserAuthorizationProvider updateProfileForAuthorization:[self getCurrentAuthorization] delegate:self];
    }
}

#pragma mark - User Authorization Delegate
- (void) authorizationDidSucceed:(ASFAuthorization *)authorization duration:(NSTimeInterval)duration
{
    newAuthorization = authorization;
    [[ASFSession sharedSession] setTimeToVerifyCredentials:duration];
}
- (void) authorizationDidRefresh:(ASFAuthorization *)authorization duration:(NSTimeInterval)duration
{
    authorization.isRefreshed = YES;
    newAuthorization = authorization;
    [[ASFSession sharedSession] setTimeToVerifyCredentials:duration];
}

- (void) authorizationDidFailWithError:(NSError *)error
{
    [self failedWithError:error];
}

- (void) profileRetrievalFromURL:(NSURL *)url didSucceed:(ASFAuthorization *)authorization duration:(NSTimeInterval)duration
{
    ASFSession *session = [ASFSession sharedSession];
    
    // Verify that the session configuration has not changed and then
    // give the new authorization to the session
    if ([session.configuration.profileEndpoint isEqual:url]) {
        [session setCurrentAuthorizationForSession:authorization updatingKeychain:YES];
        [session setTimeToRetrieveProfile:duration];
    }
    
    if (authorization.isRefreshed && !ASFKitConfiguration.refreshEnabled) {
        self.didIntegrityCheckFail = YES;
        self.didFailOnRefresh = YES;
    }
    
    self.completionHandler();
}
- (void) profileRetrievalFromURL:(NSURL *)url didFailWithError:(NSError *)error
{
    if (newAuthorization.isRefreshed && !ASFKitConfiguration.refreshEnabled) {
        self.didIntegrityCheckFail = YES;
        self.didFailOnRefresh = YES;
    }
    
    // First check if this is an ignorable error.
    // Errors regarding reachability or server errors can be ignored
    if ([self shouldIgnoreError:error])
    {
        // Yes, this will check if the error should be ignored again.
        return [self failedWithError:error];
    }
    
    // This should only occur in rare instances:
    // 1. If the access token was valid when we checked, but expired between here and the server.
    // 2. The access token is no longer valid on the server (for whatever reason).
    // In either case, we'll fall back and attempt to use the refresh token to get a new access token.
    
    // In an extreme rare case that the refresh succeeds but the profile still fails, we don't want to get stuck in a loop
    // so we'll keep track if the profile retrieval fails twice.
    if (profileRetrievalFailed)
    {
        if ([ASFKitConfiguration.profileEndpoint isEqual:url]) {
            [[ASFSession sharedSession] setCurrentAuthorizationForSession:newAuthorization updatingKeychain:YES];
        }
        
        // Create a new error with this error as the underlying error
        error = [ASFError errorWithCode:ASFErrorProfileServiceFailure andLocalizedFailureReason:nil andUnderlyingError:error];
        [self failedWithError:error];
        return;
    }
    
    [ASFUserAuthorizationProvider refreshAuthorizationTokenWithAuthorization:[self getCurrentAuthorization] delegate:self];
    profileRetrievalFailed = YES;
}

@end
