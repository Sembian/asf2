//
//  ASFIntegrityCheckResult.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import <UIKit/UIKit.h>

/// Represents the result of the integrity check.
typedef NS_ENUM(NSInteger, ASFIntegrityCheckStatus)
{
    /// All checks in the integrity check have passed.
    ASFIntegrityCheckPassed,
    /// At least once check in the integrity check has failed.
    ASFIntegrityCheckFailed
};

@protocol ASFIntegrityCheckProtocol;

/// 
/// The result of an integrity check. Tells whether the check succeeded or failed,
/// the error it encountered (if any), the checks that failed, and whether the check
/// was performed in offline mode.
/// 
@interface ASFIntegrityCheckResult : NSObject

/// Whether the integrity check succeeded or failed.
@property (nonatomic, readonly) ASFIntegrityCheckStatus status;

/// The errors encountered during the integrity check.
@property (nonatomic, readonly) NSArray *errors;

/// The error (if any) that was encountered by the integrity check.
@property (nonatomic, readonly) NSError *error;

/// The checks that reported as failing.
@property (nonatomic, readonly) NSArray *failedChecks;

/// The checks that succeeded.
@property (nonatomic, readonly) NSArray *passedChecks;

/// YES if the local session (just for this application) is about to expire.
@property (nonatomic, readonly) BOOL endingLocalSession;

/// Yes if the shared session (the session data stored in the keychain used between multiple applications) is about to be removed.
@property (nonatomic, readonly) BOOL endingSharedSession;

/// YES if the protected files are about to be deleted.
@property (nonatomic, readonly) BOOL deletingProtectedFiles;

/// The date this integrity check result was completed.
@property (nonatomic, readonly) NSDate *completedDate;

/**
 Initializes the result of the integrity check.
 This is invoked by ASFIntegrityCheckController at the conclusion of the integrity check.
 
 @param status                  The status of the integrity check (whether it failed/passed).
 @param errors                  The errors that were encountered during the integrity check.
 @param failedChecks            A list of checks that failed.
 @param passedChecks            A list of checks that succeeded.
 @param endingLocalSession      Indicates whether the local session should be terminated.
 @param endingSharedSession     Indicates whether the shared session should be terminated.
 @param deletingProtectedFiles  Indicates whether protected files should be removed.
 
 @return    An intialized ASFIntegrityCheckResult.
 */
- (id) initWithStatus:(ASFIntegrityCheckStatus)status errors:(NSArray *)errors failedChecks:(NSArray *)failedChecks passedChecks:(NSArray *)passedChecks endingLocalSession:(BOOL)endingLocalSession endingSharedSession:(BOOL)endingSharedSession deletingProtectedFiles:(BOOL)deletingProtectedFiles;

/// Checks whether a specific check succeeded or failed.
/// @param check The integrity check to check the status on.
/// @return YES if the check reported a failure, NO if it was not run or if it succeeded.
- (BOOL) didCheckFail:(id<ASFIntegrityCheckProtocol>)check;

@end
