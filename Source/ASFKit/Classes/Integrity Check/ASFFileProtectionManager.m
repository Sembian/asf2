//
//  ASFFileProtectionManager.m
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFFileProtectionManager.h"
#import "ASFFileProtectionManager+Private.h"
#import "ASFUtilities_Private.h"
#import "ASFLog.h"
#import "ASFKitDevice.h"

NSString* const ASFProtectedFilesWillBeRemovedNotification    =   @"ASFProtectedFilesWillBeRemoved";
NSString* const ASFProtectedFilesWereRemovedNotification      =   @"ASFProtectedFilesRemoved";
NSString* const ASFProtectionAttributeModifiedNotification    =   @"ASFProtectionAttributeModified";
NSString* const kRemovedFiles                                           =   @"removedFiles";

static NSString* kDataProtection = @"com.apple.developer.default-data-protection";

@interface ASFFileProtectionManager()

+ (NSArray *) defaultProtectedLocations;
+ (NSURL *) applicationDirectory;
+ (NSURL *) fileURLForLocation: (NSString *) directoryOrFilePath;
- (void) beginProtectingLocation: (NSString *)directoryOrFilePath;
- (void) beginProtectingLocations: (NSArray *)directoriesOrFilePaths;
- (void) stopProtectingLocation: (NSString *)directoryOrFilePath;

- (BOOL) isLocationAtURLProtected: (NSURL *)location;
- (void) modifyFileProtectionAttributeForURL: (NSURL *)location;

- (ASFFileProtectionStatus) getFileProtectionStatus: (NSString *)filepath;

- (void) enableProtection;
- (void) disableProtection;
- (void) checkFilesystem;
- (void) checkLocationAtURL: (NSURL *)location;

@end

@implementation ASFFileProtectionManager
{
    BOOL hasPerformedCheck;
}

# pragma mark - Public methods
+ (void) beginProtectingLocation: (NSString *)directoryOrFilePath
{
    [[ASFFileProtectionManager defaultManager] beginProtectingLocation:directoryOrFilePath];
}

+ (void) stopProtectingLocation: (NSString *)directoryOrFilePath
{
    [[ASFFileProtectionManager defaultManager] stopProtectingLocation:directoryOrFilePath];
}

+ (BOOL) isLocationProtected: (NSString *)directoryOrFilePath
{
    NSURL *resolvedPath = [ASFFileProtectionManager fileURLForLocation:directoryOrFilePath];
    return [[ASFFileProtectionManager defaultManager] isLocationAtURLProtected:resolvedPath];
}
+ (void) enableProtection
{
    [[ASFFileProtectionManager defaultManager] enableProtection];
}
+ (void) disableProtection
{
    [[ASFFileProtectionManager defaultManager] disableProtection];
}

+ (ASFFileProtectionManager *) defaultManager
{
    static ASFFileProtectionManager *defaultManager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        defaultManager = [[ASFFileProtectionManager alloc] initPrivately];
    });
    
    return defaultManager;
}

# pragma mark - Object lifecycle
- (id) init
{
    return [ASFFileProtectionManager defaultManager];
}
// This has a weird name because it shouldn't be instatiated outside of the library
- (id) initPrivately
{
    self = [super init];
    if (self)
    {
        protectedLocations = [NSMutableArray new];
        excludedLocations = [NSMutableArray new];
        [self checkApplicationEntitlements];
        
        failed = NO;
        
        [self beginProtectingLocations:[ASFFileProtectionManager defaultProtectedLocations]];
    }
    return self;
}


# pragma mark - Private methods
///
/// Checks whether the entitlements section of the provisioning profile has the data protection flag (com.apple.developer.default-data-protection) enabled.
/// If it is not, it prints a warning in the console.
/// @since 2.1
///
- (void) checkApplicationEntitlements
{
    if (![self isDataProtectionEntitlementEnabled]) {
        NSLog(@"Warning: The provisioning profile SHOULD specify the data protection capability. See https://developer.apple.com/library/ios/documentation/IDEs/Conceptual/AppDistributionGuide/AddingCapabilities/AddingCapabilities.html#//apple_ref/doc/uid/TP40012582-CH26-SW30 for more information.");
    }
}

///
/// Determines whether the provisioning profile has data protection enabled.
/// @return YES if the data protection (com.apple.developer.default-data-protection) entitlement was specified (and set to NSFileProtectionComplete), NO otherwise.
/// @since 2.1
///
- (BOOL) isDataProtectionEntitlementEnabled
{
    // If this is running in the simulator, say that the entitlement is available (although that's not true)
    // Files cannot be protected in the simulator so instead of continually trying to protect them,
    // the framework will mark them as protected.
#if TARGET_IPHONE_SIMULATOR
    return YES;
#endif
    
    NSDictionary *entitlements = [ASFUtilities currentEntitlements];
    NSString *protectionStatus = entitlements[kDataProtection];
    
    return [protectionStatus isEqualToString:NSFileProtectionComplete];
}

+ (NSArray *) defaultProtectedLocations
{
    return [NSArray arrayWithObjects:
            @"Documents",
            @"Library",
            @"tmp",
            nil];
}

+ (NSURL *) applicationDirectory
{
    // The application directory will never change. We only need to determine it once.
    // Once the application directory has been determined, we can cache the determined
    // value indefinitely (for the run time of the app).
    static NSURL *applicationDirectory = nil;
    
    if (!applicationDirectory) 
    {
        NSURL *documentsDirectory = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] objectAtIndex:0];
        
        // Remove the last component (the "Documents/" part).
        applicationDirectory = [documentsDirectory URLByDeletingLastPathComponent];
    }
    return applicationDirectory;
}

+ (NSURL *) fileURLForLocation: (NSString *) directoryOrFilePath
{
    // Standardizes the form of file URL--to have a trailing slash
    // [NSURL path] removes trailing slashes then fileURLWithPath puts it back
    NSURL *fileURL;
    if ([directoryOrFilePath characterAtIndex:0]!='/')
    {
        NSString *filepath = [[[ASFFileProtectionManager applicationDirectory] URLByAppendingPathComponent:directoryOrFilePath] path];
        fileURL = [NSURL fileURLWithPath:filepath];
    }
    else
    {
        fileURL = [NSURL fileURLWithPath:directoryOrFilePath];
    }
    
    return fileURL;
}

- (void) beginProtectingLocations: (NSArray *)directoriesOrFilePaths
{
    for (NSString *directoryOrFilePath in directoriesOrFilePaths) 
    {
        [self beginProtectingLocation:directoryOrFilePath];
    }
}

- (void) beginProtectingLocation: (NSString *)directoryOrFilePath
{
    NSURL *resolvedPath = [ASFFileProtectionManager fileURLForLocation:directoryOrFilePath];
    
    // Add location to protectedLocations
    // First check to see if it already exists. If it does nothing else needs to happen.
    if ([self isLocationAtURLProtected:resolvedPath])
    {
        [ASFLog message:[NSString stringWithFormat:@"%@ is already protected.", directoryOrFilePath]];
        return;
    }
    [protectedLocations addObject:resolvedPath];
    
    // Remove location from excludedLocations (if it exists)
    [excludedLocations removeObject:resolvedPath];
    
    // Ensure file protection attribute on directoryOrFilePath is NSFileProtectionComplete
    [self modifyFileProtectionAttributeForURL:resolvedPath];
}

- (void) stopProtectingLocation: (NSString *)directoryOrFilePath
{
    if (hasPerformedCheck) {
        NSLog(@"Warning: ASFFileProtection has already begun protecting the file system. Protection will be removed from %@, but will be resumed on the next application launch. Remove protection from this file in application:didFinishLaunchingWithOptions: to silence this warning.", directoryOrFilePath);
    }
    NSURL *resolvedPath = [ASFFileProtectionManager fileURLForLocation:directoryOrFilePath];
    
    for (NSString *defaultProtectedLocation in [ASFFileProtectionManager defaultProtectedLocations])
    {
        NSString *protectedLocation = [ASFFileProtectionManager fileURLForLocation:defaultProtectedLocation].absoluteString;
        NSString *removingLocation = resolvedPath.absoluteString;
        if ([protectedLocation rangeOfString:removingLocation].location!=NSNotFound)
        {
            [NSException raise:NSInvalidArgumentException format:@"You cannot stop protecting the root folders of the application."];
            return;
        }
    }
    
    // If the location is already an excluded location, don't do anything
    if ([excludedLocations containsObject:resolvedPath])
    {
        return;
    }
    
    // Remove location from protectedLocations (if it exists)
    [protectedLocations removeObject:resolvedPath];
    
    // Add location to exlcuded locations
    [excludedLocations addObject:resolvedPath];
}

- (BOOL) isLocationAtURLProtected: (NSURL *)location
{
    // A location is protected if itself or parent locations
    // are part of the protectedLocations array
    
    // Check the provided location and check each of the location's parents to see if it is protected
    // Stop iterating backward when the application's base directory is reached.
    // If, somehow, we missed the application's base directory, stop at "file://localhost/"--this is the device's root.
    // This check is in place to prevent an infinite loop from occuring here.
    NSURL *locationToCheck = location;
    while (locationToCheck && ![locationToCheck isEqual:[ASFFileProtectionManager applicationDirectory]] && ![locationToCheck isEqual:[NSURL URLWithString:@"file://localhost/"]])
    {
        // Check if this location is included as a protectedLocation
        if ([protectedLocations containsObject:locationToCheck])
        {
            return YES;
        }
        
        // Check if this location has specified as unprotected
        if ([excludedLocations containsObject:locationToCheck])
        {
            return NO;
        }
        
        // Same process taken in [ASFFileProtectionManager fileURLForLocation] to 
        // standardize file URLs to have a trailing slash.
        locationToCheck = [NSURL fileURLWithPath:[[locationToCheck URLByDeletingLastPathComponent] path]];
    }
    
    return NO;
}

- (void) modifyFileProtectionAttributeForURL: (NSURL *)location
{
    NSError *error = nil;
    BOOL result = [[NSFileManager defaultManager] setAttributes:[NSDictionary dictionaryWithObject:NSFileProtectionComplete forKey:NSFileProtectionKey] ofItemAtPath:[location path] error:&error];
    
    // If it failed to modify the protection attribute for the location...
    if (!result)
    {
        [ASFLog message:[NSString stringWithFormat:@"Failed to modify protection attributes on location: %@\nError: %@", location, error.localizedDescription]];
        return;
    }
    
    [ASFLog message:[NSString stringWithFormat:@"Modified protection attribute on location: %@", location]];
    
    // Send the notification on the main queue
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:ASFProtectionAttributeModifiedNotification object:self userInfo:[NSDictionary dictionaryWithObject:location forKey:@"location"]];
    });
}

- (void) enableProtection
{
    [ASFLog message:@"Enabled file system protection."];
    enabled = YES;
}

- (void) disableProtection
{
    [ASFLog message:@"Disabled file system protection."];
    enabled = NO;
}

- (void) checkFilesystem
{
    hasPerformedCheck = YES;
    NSArray *locations = [protectedLocations copy];
    for (NSURL *location in locations)
    {
        [self checkLocationAtURL:location];
    }
}

- (void) checkLocationAtURL: (NSURL *)location
{
    // Obtain an enumerator for deep enumeration within the directory (checking ALL subdirectories and files)
    // Using the enumeratorAtPath instead of enumeratorAtURL because iOS 4 doesn't
    // work correctly with enumeratorAtURL.
    NSDirectoryEnumerator *dirEnumerator = [[NSFileManager defaultManager] enumeratorAtPath:[location path]];
    
    // Iterate through each item
    for (NSString *filename in dirEnumerator)
    {
        // Reconstruct the path
        NSString *path = [[location URLByAppendingPathComponent:filename] path];
        
        // If path refers to a non-existant location or the url refers to a directory, skip it.
        // Directories cannot be encyrypted...only their contents.
        BOOL isDirectory = NO;
        if (![[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:&isDirectory] || isDirectory)
        {
            continue;
        }
        
        // Determine the protection status on this item
        // If it "protectable" then it needs to have NSFileProtectionComplete set on it
        if ([self getFileProtectionStatus:path]==ASFFileProtectable)
        {
            [self modifyFileProtectionAttributeForURL:[NSURL fileURLWithPath:path]];
        }
    }    
}

- (ASFFileProtectionStatus) getFileProtectionStatus: (NSString *)filepath
{
    if (![self isLocationAtURLProtected:[NSURL fileURLWithPath:filepath]])
    {
        [ASFLog message:[NSString stringWithFormat:@"Unprotected: %@ not protected by framework.", filepath]];
        return ASFFileUnprotected;
    }
    // Attempt to get the attributes of the file
    NSError *error = nil;
    NSDictionary *attr = [[NSFileManager defaultManager] attributesOfItemAtPath:filepath error:&error];
    
    // If the attributes are empty or an error occurred, assume that the file doesn't exist
    if (!attr || error)
    {
        [ASFLog message:[NSString stringWithFormat:@"Unprotectable: %@ could not be protected--possibly because of an error:%@", filepath, error]];
        return ASFFileUnprotectable;
    }
    
    // If the filepath refers to a non-regular file, then it is not protectable
    if (![[attr fileType] isEqualToString:NSFileTypeRegular])
    {
        [ASFLog message:[NSString stringWithFormat:@"Unprotectable: %@ does not refer to regular type. Type: %@", filepath, [attr fileType]]];
        return ASFFileUnprotectable;
    }
    
    // If this is running in the simulator, say that the file is protected (although that's not true)
    // Files cannot be protected in the simulator so instead of continually trying to protect them,
    // the framework will mark them as protected.
#if TARGET_IPHONE_SIMULATOR
    return ASFFileProtected;
#endif
    
    // Check the status of NSFileProtectionKey
    if ([[attr valueForKey:NSFileProtectionKey] isEqualToString:NSFileProtectionComplete]) 
    {
        return ASFFileProtected;
    }
    else
    {
        [ASFLog message:[NSString stringWithFormat:@"Protectable: NSFileProtectionKey for %@ is %@.", filepath, [attr valueForKey:NSFileProtectionKey]]];
        return ASFFileProtectable;
    }
}

- (void) removeAllProtectedLocations
{
    NSMutableArray *removedFiles = [NSMutableArray array];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:ASFProtectedFilesWillBeRemovedNotification object:self userInfo:nil];
    
    for (NSURL *protectedLocation in protectedLocations) 
    {
        // Obtain an enumerator for deep enumeration within the directory (checking ALL subdirectories and files)
        // Using the enumeratorAtPath instead of enumeratorAtURL because iOS 4 doesn't
        // work correctly with enumeratorAtURL.
        NSDirectoryEnumerator *dirEnumerator = [[NSFileManager defaultManager] enumeratorAtPath:[protectedLocation path]];
        
        for (NSString *filename in dirEnumerator) 
        {
            // Reconstruct the path
            NSString *path = [[protectedLocation URLByAppendingPathComponent:filename] path];
            
            // If location refers to a directory, skip it. The file protection API does not remove directories.
            BOOL isDirectory = NO;
            if (![[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:&isDirectory] || isDirectory)
            {
                continue;
            }
            
            // If this location is NOT protected, skip it
            if (![self isLocationAtURLProtected:[NSURL fileURLWithPath:path]])
            {
                continue;
            }
            
            NSError *error = nil;
            BOOL result = [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
            
            if (!result || error)
            {
                [ASFLog message:[NSString stringWithFormat:@"Unable to delete file: %@ Encountered error: %@", path, error.localizedDescription]];
            }
            else
            {
                [removedFiles addObject:path];
            }
        }
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:ASFProtectedFilesWereRemovedNotification object:self userInfo:[NSDictionary dictionaryWithObject:removedFiles forKey:kRemovedFiles]];
}

#pragma mark - ASFKit Integrity Check
- (BOOL) didIntegrityCheckFail
{
    return failed;
}

- (BOOL) didEncounterTemporaryProblem
{
    return NO;
}
- (BOOL) shouldLocalSessionEndOnFailure
{
    return YES;
}
- (BOOL) shouldSharedSessionEndOnFailure
{
    return shouldEndSharedSession;
}

- (BOOL) shouldProtectedFilesBeDeletedOnFailure
{
    return YES;
}

- (NSError *) error
{
    return lastError;
}

- (void) performCheckInOfflineMode:(BOOL)offlineMode completion:(ASFIntegrityCheckCompletionHandler)completion
{
    failed = shouldEndSharedSession = NO;
    
    if (enabled)
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
           [self checkFilesystem];
            
            ASFDeviceConfigResult currentConfiguration = [ASFKitDevice verifyConfigurationFromScratch];
            failed = currentConfiguration!=ASFDeviceConfigInstalled;
            
            if (failed) {
                NSUInteger errorCode = currentConfiguration!=ASFDeviceConfigMissingLeaf ? ASFErrorConfigurationProfileMissing : ASFErrorConfigurationLeafCertificateMissing;
                lastError = [ASFError errorWithCode:errorCode];
                shouldEndSharedSession = currentConfiguration!=ASFDeviceConfigMissingLeaf;
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                completion();
            });
        });
    }
    else
    {
        completion();
    }
}

@end
