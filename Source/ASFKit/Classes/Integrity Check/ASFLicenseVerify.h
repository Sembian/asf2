//
//  ASFLicenseVerify.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASFIntegrityCheckController.h"
#import "ASFURLRequest.h"

typedef NS_ENUM(NSInteger, ASFLicenseStatus)
{
    ASFLicenseStatusUnknown,
    ASFLicenseStatusInvalid,
    ASFLicenseStatusRevoked,
    ASFLicenseStatusValid
};

typedef void(^ASFLicenseVerificationHandler)(ASFLicenseStatus status, NSError *error);

extern NSString* const LicenseStatusDidUpdateNotification;

@interface ASFLicenseVerify : NSObject <ASFIntegrityCheckProtocol, ASFURLRequestDelegate>
{
    BOOL failed;
    BOOL encounteredTemporaryProblem;
    
    ASFLicenseVerificationHandler completionHandler;
}

@property (nonatomic, readonly) NSString *licenseKey;
@property (nonatomic, strong) NSError *error;

- (id) initWithLicenseKey:(NSString *)licenseKey;
+ (ASFLicenseVerify *) licenseVerifierWithKey:(NSString *)licenseKey;
+ (ASFLicenseVerify *) verifyLicense:(NSString *)licenseKey withCompletionHandler:(ASFLicenseVerificationHandler)complete;

/// Checks the status of the application's license.
/// @param complete The code block to execute when the license status has returned from the server.
/// @return YES if the check was initiated successfully. NO if the device failed to initiate the request.
- (BOOL) verifyLicenseWithCompletionHandler:(ASFLicenseVerificationHandler)complete;

@end
