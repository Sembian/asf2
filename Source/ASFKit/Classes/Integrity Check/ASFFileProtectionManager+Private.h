//
//  ASFFileProtectionManager+Private.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//



@interface ASFFileProtectionManager (Private)

- (id) initPrivately;
- (void) removeAllProtectedLocations;

@end
