//
//  ASFIntegrityCheckController.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ASFReachability, ASFIntegrityCheckResult;
@protocol ASFIntegrityCheckProtocol;
@protocol ASFIntegrityCheckDelegate;

/// Notification triggered just before the integrity check is about to begin.
extern NSString* const ASFIntegrityCheckWillBeginNotification;

/// 
/// Notification triggered when the integrity check has finished.
/// userInfo will contain the result of the integrity check.
/// This will be run on the main queue.
/// 
extern NSString* const ASFIntegrityCheckFinishedNotification;

/// 
/// Checks the integrity of the session--this includes checking the security of the file system and checking the validity of the application license.
/// 
/// @since 1.0.7
@interface ASFIntegrityCheckController : NSObject
{
    @private
    /// 
    /// Multiple integrity checks can be run simultaneously (e.g. checking the file system, the SAML token, and the application license).
    /// This array contains the checks that will be run when checking the integrity of the ASFSession.
    /// 
    NSMutableArray *integrityChecks;
    
    /// 
    /// The NSTimer instance that controls the interval for performing the integrity check.
    /// 
    NSTimer *checkInterval;
    
    /// 
    /// A reference to the Reachability class that perodically checks for host reachability.
    /// If the Internet connection is lost, the integrity check interval will continue to run,
    /// but the checks will be run in "offline mode." As soon as a network connection is available,
    /// all the integrity checks will be triggered again in "online mode."
    /// 
    ASFReachability *reachability;
    
    /// 
    /// The number of times the integrity has been checked in "offline mode."
    /// Once this number hits the "max offline attempts" value (by default, 1000),
    /// the session will be considered invalid and the integrity check will fail.
    /// 
    NSUInteger offlineAttempts;
    
    /// YES if the integrity check should immediately run again when completed
    /// Used when the reachability status changes in the middle of a check
    BOOL shouldRecheckOnComplete;
}

/// The delegate to be notified of the latest result of an integrity check. Invoked after the notification finished posting.
@property (nonatomic, weak) id<ASFIntegrityCheckDelegate> delegate;

/// The interval on which to check the integrity of the session (in seconds). By default, 30 minutes.
@property (nonatomic, readwrite) NSTimeInterval integrityCheckInterval;

/// YES when an integrity check is currently being performed.
@property (nonatomic, readonly) BOOL checking;

/// The number of integrity checks waiting to finish. 0 if they're all done or if it is not currently checking.
@property (nonatomic, readonly) NSInteger checksRemaining;

/// YES when the integrity check is currently in offline mode.
@property (nonatomic, readonly) BOOL offline;

/// YES when the integrity checker is automatically running every x minutes (as defeined by the integrityCheckInterval).
@property (nonatomic, readonly) BOOL intervalCheckingEnabled;

/// The last result received from an integrity check.
@property (nonatomic, strong, readonly) ASFIntegrityCheckResult *lastResult;

/// 
/// The designated initializer for the ASFIntegrityCheckController.
/// Initializes an integrity check and prepares it to be started on the default interval.
/// 
/// @param aDelegate The delegate of the ASFIntegrityCheckController object.
- (id) initWithDelegate:(id<ASFIntegrityCheckDelegate>)aDelegate;

/// Begin checking the integrity every x minutes as defined by the integrityCheckInterval.
/// @note The integrity check automatically pauses when the application becomes inactive (sent to the background or the device falls asleep). When the application becomes active, an integrity check will immediately be performed and the interval will be resumed.
/// 
/// @return YES if the interval started successfully, NO otherwise.
- (BOOL) startCheckInterval;

/// Stops the checking interval.
- (void) stopCheckInterval;

/// Immediately attempts to perform an integrity check.
- (void) performIntegrityCheck;

/// Adds a check to perform during the integrity check.
/// @param check The check to add to the integrity check.
/// @return YES if the check was added succesfully, NO otherwise.
- (BOOL) addIntegrityCheck:(id<ASFIntegrityCheckProtocol>)check;

/// Removes a check to be performed during the integrity check.
/// @param check The check to remove from the integrity check.
/// @return YES if the check was removed succesfully, NO otherwise.
- (BOOL) removeIntegrityCheck:(id<ASFIntegrityCheckProtocol>)check;

/// The default instance of the integrity check controller (the reference used by ASFSession).
+ (ASFIntegrityCheckController *) sharedInstance;

@end

typedef void(^ASFIntegrityCheckCompletionHandler)();

/// 
/// Objects looking to add an additional check to the integrity check must implement the ASFIntegrityCheckProtocol.
/// 
@protocol ASFIntegrityCheckProtocol <NSObject>
@required
/// 
/// Objects implementing the ASFIntegrityCheckProtocol should return YES for this method if it considers itself to have failed.
/// This will add your object to the list of failed integrity checks, but it will not in of itself change the behavior of the application. You'll need to specify in the other protocol methods how the application should respond to the failure.
/// 
/// @return YES to indicate a failure; NO to indicate a pass.
/// @see shouldLocalSessionEndOnFailure
/// @see shouldSharedSessionEndOnFailure
/// @see shouldProtectedFilesBeDeletedOnFailure
- (BOOL) didIntegrityCheckFail;

/// 
/// Objects implementing the ASFIntegrityCheckProtocol should return YES for this method if it encountered a problem that can be considered a temporary issue (e.g. the device does not have Internet access so the license key could not be verified with the license server).
/// Sessions are allowed a certain number of "temporary problems" before forcibly ending the shared session because of a problem.
/// 
/// @return YES to indicate a temporary problem; NO otherwise.
- (BOOL) didEncounterTemporaryProblem;

/// 
/// Objects implementing the ASFIntegrityCheckProtocol should return YES for this method if the local session should end if a failure occurs.
/// This will trigger the ASFKit interface to open and take control of the application.
///
/// <strong>Important:</strong> Ending the local session alone will not log the user out of the application (because it will immediately pick up the shared session to resume). The local session should end in conjunction with another action--usually removing protected files. This will force the application to go through another license verification and local check.
/// 
/// @return YES if you want to end the local session.
/// @see If you want to force the user to log in again, you must end the shared session (shouldSharedSessionEndOnFailure).
- (BOOL) shouldLocalSessionEndOnFailure;

/// 
/// Objects implementing the ASFIntegrityCheckProtocol should return YES for this method if the shared session should end if a failure occurs.
/// Returning YES here will remove all data from the keychain. The ASFKit interface will open and the user will be forced to log in again.
/// 
/// @return YES if you want to to remove the data in the keychain and force the user to log in again.
- (BOOL) shouldSharedSessionEndOnFailure;

/// 
/// Objects implementing the ASFIntegrityCheckProtocol should return YES for this method if all the protected files in this application's sandbox should be removed on a failure.
/// 
/// @return YES if protected files should be removed.
- (BOOL) shouldProtectedFilesBeDeletedOnFailure;

/// 
/// Objects implementing the ASFIntegrityCheckProtocol should return the error that was encountered during the check.
/// 
/// @return The error that was encountered during this check.
- (NSError *) error;

/// 
/// ASFIntegrityCheckController will initiate the check by calling this method.
/// Your implementation should use this method to perform the appropriate tasks and should call the specified completion block when it is done.
/// 
/// @param offlineMode This will be YES if ASFIntegrityCheckController believes that the device is offline.
/// @param completion This will be a block that needs to be invoked when your check is finished. This block must be invoked on the main queue.
- (void) performCheckInOfflineMode:(BOOL)offlineMode completion:(ASFIntegrityCheckCompletionHandler)completion;
@end

/// 
/// The methods declared by the ASFIntegrityCheckDelegate protocol allow the adopting delegate to respond to the completion of the integrity check.
/// 
@protocol ASFIntegrityCheckDelegate <NSObject>
@required
/// 
/// Tells the delegate that all of the checks have finished and includes the result.
/// 
/// @param result The result of the integrity check.
- (void) integrityCheckFinishedWithResult:(ASFIntegrityCheckResult *)result;
@end