//
//  ASFLicenseVerify.m
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFLicenseVerify.h"
#import "ASFLog.h"
#import "ASFConfiguration.h"

#define NIL_TO_NULL(value) value != nil ? value : [NSNull null]

NSString* const LicenseStatusDidUpdateNotification = @"LicenseStatusDidUpdateNotification";

static NSMutableArray *activeVerifications;

@interface ASFLicenseVerify()
@property (weak, nonatomic, readonly) NSData *licenseVerificationToken;
@property (nonatomic, strong) ASFURLRequest *request;
@end

@implementation ASFLicenseVerify
@synthesize licenseKey=_licenseKey;

+ (void) initialize
{
    activeVerifications = [NSMutableArray array];
    [super initialize];
}

#pragma mark - Lifecycle
- (id) initWithLicenseKey:(NSString *)licenseKey
{
    self = [super init];
    if (self)
    {
        _licenseKey = [licenseKey copy];
        failed = NO;
        encounteredTemporaryProblem = NO;
    }
    return self;
}

- (void) dealloc
{
    completionHandler = nil;
    self.request.delegate = nil;
}

+ (ASFLicenseVerify *) licenseVerifierWithKey:(NSString *)licenseKey
{
    return [[ASFLicenseVerify alloc] initWithLicenseKey:licenseKey];
}

+ (ASFLicenseVerify *) verifyLicense:(NSString *)licenseKey withCompletionHandler:(ASFLicenseVerificationHandler)complete
{
    ASFLicenseVerify *verifier = [[ASFLicenseVerify alloc] initWithLicenseKey:licenseKey];
    
    if ([verifier verifyLicenseWithCompletionHandler:complete])
    {
        return verifier;
    }
    else 
    {
        return nil;
    }
}

#pragma mark - Private methods
- (NSData *) licenseVerificationToken
{
    NSDictionary *verificationToken = @{
        @"Application" : @{
            @"license_key"          :  NIL_TO_NULL(self.licenseKey),
            @"bundle_identifier"    :  NIL_TO_NULL([[[NSBundle mainBundle] infoDictionary] valueForKey:(NSString *)kCFBundleIdentifierKey]),
            @"bundle_version"       :  NIL_TO_NULL([[[NSBundle mainBundle] infoDictionary] valueForKey:(NSString *)kCFBundleVersionKey]),
            @"bundle_version_short" :  NIL_TO_NULL([[[NSBundle mainBundle] infoDictionary] valueForKey:@"CFBundleShortVersionString"])
        }
    };
    
    return [NSJSONSerialization dataWithJSONObject:verificationToken options:0 error:nil];
}

- (NSError *) errorWithStatus:(ASFLicenseStatus)status
{
    switch (status)
    {
        case ASFLicenseStatusValid:
            return nil;
        case ASFLicenseStatusInvalid:
            return [ASFError errorWithCode:ASFErrorLicenseInvalid];
        case ASFLicenseStatusRevoked:
            return [ASFError errorWithCode:ASFErrorLicenseRevoked];
        case ASFLicenseStatusUnknown:
        default:
            return [ASFError errorWithCode:ASFErrorLicenseVerifyFailure];
    }
}

#pragma mark - Public methods
- (BOOL) verifyLicenseWithCompletionHandler:(ASFLicenseVerificationHandler)complete
{
    // Only one verification can be handled per object
    if (completionHandler)
    {
        return NO;
    }
    
    [activeVerifications addObject:self];
    
    completionHandler = [complete copy];
    
    NSURL *endpoint = [[ASFSession sharedSession].configurationProfileURL URLByAppendingPathComponent:VerifyLicenseURI];
    self.request = [ASFURLRequest requestWithURL:endpoint delegate:self];
    [self.request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [self.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    self.request.HTTPBody = self.licenseVerificationToken;
    self.request.HTTPMethod = ASFURLRequestTypePOST;
    [self.request start];
    return YES;
}

- (void) finishedWithStatus:(ASFLicenseStatus)status
{
    [self finishedWithStatus:status error:[self errorWithStatus:status]];
}
- (void) finishedWithStatus:(ASFLicenseStatus)status error:(NSError *)error
{
    [self finishedWithStatus:status andConfiguration:nil error:error];
}
- (void) finishedWithStatus:(ASFLicenseStatus)status andConfiguration:(ASFConfiguration *)configuration
{
    [self finishedWithStatus:status andConfiguration:configuration error:nil];
}

- (void) finishedWithStatus:(ASFLicenseStatus)status andConfiguration:(ASFConfiguration *)configuration error:(NSError *)error;
{
#if TARGET_IPHONE_SIMULATOR
    // The license is ALWAYS valid on the simulator
    status = ASFLicenseStatusValid;
#endif
    
    // Send a notification to the notification center that the license status changed
    NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithObject:[NSNumber numberWithInt:status] forKey:@"status"];
    if (configuration)
        [userInfo setValue:configuration forKey:@"configuration"];
    
    if (error) {
        [userInfo setValue:error forKey:@"error"];
        [ASFLog message:[NSString stringWithFormat:@"Attempted to verify licese with %@ but received an error: %@", self.request.URL.absoluteString, error]];
    } else {
        [ASFLog message:[NSString stringWithFormat:@"Successfully verified license with %@.", self.request.URL.absoluteString]];
    }
         
    [activeVerifications removeObject:self];
    self.request = nil;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:LicenseStatusDidUpdateNotification object:self userInfo:userInfo];
    
    if (completionHandler)
    {
        completionHandler(status, error);
        completionHandler = nil;
    }
}

#pragma mark - ASFURLRequest delegate methods
- (BOOL) requestShouldStartAutomatically
{
    return NO;
}
- (void) requestDidStart:(ASFURLRequest *)request
{
    [ASFLog message:[NSString stringWithFormat:@"Verifying license with %@...", request.URL.absoluteString]];
}
- (void) requestDidFinish:(ASFURLRequest *)request
{
    // Verify the response was successful
    if (request.responseCode==200)
    {
        ASFConfiguration *configuration = [ASFConfiguration configurationWithData:request.responseData];
        [self finishedWithStatus:ASFLicenseStatusValid andConfiguration:configuration];
    }
    else
    {
        [self finishedWithStatus:ASFLicenseStatusUnknown];
    }
}

- (void) request: (ASFURLRequest *)aConnection didFailWithError:(NSError *)error
{
    switch (error.code)
    {
        case 403:
            [self finishedWithStatus:ASFLicenseStatusRevoked];
            break;
        case 401:
        case 404:
            [self finishedWithStatus:ASFLicenseStatusInvalid];
            break;
        case 500:
        case 503:
            [self finishedWithStatus:ASFLicenseStatusUnknown];
            break;
        default:
            [self finishedWithStatus:ASFLicenseStatusUnknown error:error];
            break;
    }
}

#pragma mark - ASF Integrity Check
- (BOOL) didIntegrityCheckFail
{
    return failed;
}

- (BOOL) didEncounterTemporaryProblem
{
    return encounteredTemporaryProblem;
}

- (BOOL) shouldLocalSessionEndOnFailure
{
    return YES;
}

- (BOOL) shouldSharedSessionEndOnFailure
{
    return NO;
}

- (BOOL) shouldProtectedFilesBeDeletedOnFailure
{
    return YES;
}

- (void) performCheckInOfflineMode:(BOOL)offlineMode completion:(ASFIntegrityCheckCompletionHandler)completion
{
    failed = NO;
    encounteredTemporaryProblem = NO;
    
    if (!offlineMode)
    {
        BOOL result = [self verifyLicenseWithCompletionHandler:^(ASFLicenseStatus status, NSError *error){
            switch (status)
            {
                case ASFLicenseStatusInvalid:
                case ASFLicenseStatusRevoked:
                    failed = YES;
                    break;
                case ASFLicenseStatusUnknown:
                    encounteredTemporaryProblem = YES;
                    break;
                default:
                    failed = encounteredTemporaryProblem = NO;
                    break;
            }
            
            self.error = error;
            completion();
        }];
        if (!result)
        {
            [ASFLog message:@"Failed to initiate license validation."];
            failed = YES;
            completion();
        }
        return;
    }
    
    [ASFLog message:@"Skipped license validation because the device appears to be offline."];
    encounteredTemporaryProblem = YES;
    completion();
}

@end
