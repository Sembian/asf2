//
//  ASFSession.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class ASFKitViewController;
@class ASFIntegrityCheckController;
@class ASFAuthorization;
@class ASFProfile;
@protocol ASFSessionDelegate;

/// Name of NSNotification that is sent upon a successful session initialization.
extern NSString* const ASFSessionStartedNotification;

/// Name of NSNotification that is sent upon a failed session initialization.
extern NSString* const ASFSessionFailedNotification;

/// Name of NSNotification that is sent upon a user requesting the ending of a session.
extern NSString* const ASFSessionEndedNotification;

/// Name of NSNotification that is sent when a session is about to end.
/// @since 2.0
extern NSString* const ASFSessionWillEndNotification;

/// Name of NSNotification that is sent upon a session becoming invalid.
extern NSString* const ASFSessionExpiredNotification;

/// Name of NSNotification that is sent when a session is about to become invalid.
/// @since 2.0
extern NSString* const ASFSessionWillExpireNotification;

/// Name of NSNotification that is sent when the user requests the session to end when the app is launched.
extern NSString* const ASFSessionEndedOnLaunchNotification;

/// Name of NSNotification that is sent when the logged in user changed in the background.
extern NSString* const ASFSessionChangedNotification;

/// Login view is about to appear.
/// @since 1.0.7
extern NSString* const ASFLoginViewWillShowNotification;

/// Login view has finished animating onto the screen.
/// @since 1.0.7
extern NSString* const ASFLoginViewDidShowNotification;

/// Login view is about to disappear.
/// @since 1.0.7
extern NSString* const ASFLoginViewWillHideNotification;

/// Login view has finished animating off the screen.
/// @since 1.0.7
extern NSString* const ASFLoginViewDidHideNotification;

/// Name of the NSNotification that is sent when the configuration profile is installed.
/// @since 2.0
extern NSString* const ASFConfigurationProfileWasInstalledNotification;

/// Name of the NSNotification that is sent when the configuration profile is uninstalled.
/// @since 2.0
extern NSString* const ASFConfigurationProfileWasUninstalledNotification;

/// Indicates the type of environment the framework should run in.
typedef NS_ENUM(NSInteger, ASFEnvironment)
{
    /// Runs in the QA environment (using the QA licensing server); the framework is a little less strict about what it enforces.
    /// Be sure to review log files during development to see if anything will turn into errors in produciton.
    ASFEnvironmentQA,
    /// Runs in the production envionment (using the production licensing server).
    /// A handful of items that were warnings in the QA environment will be errors here.
    ASFEnvironmentProduction
};

/// 
/// ASFSession is responsible for managing your application's relationship with the currently authenticated user, the device, and this current session of this application. It is the core of ASFKit. Most of your application's interactions with ASFKit will happen through ASFSession.
///
/// You should establish a relationship with ASFSession during the launch of your application:
///
///     - (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
///     {
///         // Override point for customization after application launch.
///         [ASFSession startSessionUsingLicenseKey:licenseKey
///                                    andSecretKey:secretKey
///                                   inEnvironment:ASFEnvironmentQA
///                                        delegate:self;
///
///         return YES;
///     }
///
/// Once the session is started, your application can get information about the currently logged in user--the access token used obtained from the authentication service and the user's profile--through the authorization property on ASFSession.
///
///     ASFProfile *profile = [ASFSession sharedSession].authorization.profile;
///     NSString *displayName = profile.displayName;
///
/// It's important that your application observe the different states of an ASFSession by implementing (at least) the required methods in the ASFSessionDelegate protocol. It's recommended that your application delegate should also serve as the ASFSession delegate.
/// 
/// See ASFSessionDelegate for more information about when the delegate methods will be invoked.
/// 
/// Consider using preprocessor directives to toggle between using a QA and production environment in your application. For example:
///
///     #ifdef DEBUG
///     [ASFSession configureUsingLicenseKey:qaLicenseKey
///                             andSecretKey:qaSecretKey
///                            inEnvironment:ASFEnvironmentQA
///                                 delegate:self];
///     #else
///     [ASFSession configureUsingLicenseKey:licenseKey
///                             andSecretKey:secretKey
///                            inEnvironment:ASFEnvironmentProduction
///                                 delegate:self];
///     #endif
///
/// By default, the DEBUG preprocessor macro is defined on all builds using the Debug build schema (e.g. Run or Test). You can use any different preprocessor macro and configure them using the build settings for your build configuration. You can define macros using the Preprocessor Macros (GCC_PREPROCESSOR_DEFINITIONS) build setting. For more information about build settings, see https://developer.apple.com/library/mac/documentation/DeveloperTools/Reference/XcodeBuildSettingRef/0-Introduction/introduction.html
///
@interface ASFSession : NSObject
{
    @private
    /// Indicates whether the a login attempt is being made--set to YES after the user enters his/her username and password.
    BOOL _attemptingLogin;
    
    /// Indicates whether the session has been started or not.
    BOOL _sessionStarted;
    
    /// Indicates that the ASFSession instance has finished initialization. Prevents delegate methods from being triggered prematurely while reloading session data.
    BOOL _initialized;
    
    /// Indicates that ASFSession is waiting for the device to be configured (either by downloading a leaf certificate or installing a configuration profile).
    BOOL _configuring;
    
    /// The time/date the authentication was requested from the authentication server. nil if no authentication has been requested.
    NSDate *requestedAuthentication;
    
    /// The window the login view appears in.
    ASFKitViewController *loginViewController;
}

# pragma mark - Starting and Ending Sessions
/** @name Starting and Ending Sessions */

///
/// Starts an ASFSession.
/// If a shared session is available, it will attempt to use it without asking the user for his/her credentials. If any configuration is needed or the user needs to enter his/her credentials, your application will be covered by the login window until the ASFSession is ready.
///
/// If any configuration is needed or there is no shared session available, your application will be covered by ASFKit's interface until a secure session can be established. You application should be waiting for the sessionDidStart method to be invoked on the delegate. When establishing a session, ASFKit will go through these steps:
///
/// 1. It will first attempt to verify your application's license (on the device only). If the license is invalid, your application will not be allowed to continue.
/// 2. The application will then ask for the user's credentials.
/// 3. Once the user has entered valid credentials, it will verify that the configuration profile is installed on the device. If either the leaf certificate or configuration profile is missing, it will attempt to install them. Your application will not be able to continue until both of these components are in place.
/// 4. Finally, the user will be presented with a welcome message indicating that the session is ready to start. At this time, the session data is stored in the device's keychain and is available for other applications.
/// Your application will not be notified of the new session until the user acknowledges the welcome message (by selecing "Continue").
///
/// As of version 1.0.7 of the framework, the session will attempt to start automatically at the launch of your application. See configureUsingLicenseKey:andEnvironment:delegate: if you do not want the session to start at the start of the application. Your application will be responsible for starting the session at the appropriate time.
///
/// If any failure is encountered while starting the session (invalid license, missing leaf certificate, missing configuraiton profile), the sessionFailedToStartWithError: method will be invoked on the delegate. As long as the closesLoginOnFailure property is set to YES, your application does not need to do anything--ASFKit will work with the user to resolve the error. See ASFSessionDelegate for more information.
///
- (void) startSession;

///
/// Waits for the current run loop to finish and then will invoke startSession (allowing the current context/operation to finish before attempting to start the session). You should only use this method if you want to start the session in your application's application:didFinishLaunchingWithOptions: method but have set startsSessionOnLaunch property to NO (or are using configureUsingLicenseKey:andEnvironment:delegate:). If you are trying to start a session anywhere else, use startSession.
///
/// This method is mostly used internally to prevent locking the main run loop while the application is starting or resuming (this is to prevent the watchdog on the device from terminating the application). If you're in doubt of which method to use, use startSession.
///
/// @see startSession
/// @since 1.0.7
- (void) startSessionAsynchronously;

///
/// Ends the current session;
/// removes the stored token from the keychain and local file system.
///
- (void) endSession;

#pragma mark - Configuring and Starting a Session
/** @name Configuring and Starting a Session */

///
/// Configures and starts an ASFSession in the <strong>development</strong> environment.
/// ASFSession manages your application's relationship with the authenticated user and the device.
/// If a shared session is available, it will attempt to use it without asking the user for his/her credentials.
/// If any configuration is needed or the user needs to enter his/her credentials, your application will be covered by the
/// login window until the ASFSession is ready.
///
/// This should be called in your application's application:didFinishLaunchingWithOptions: method.
///
///     - (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
///     {
///         // Override point for customization after application launch.
///         [ASFSession startSessionUsingLicenseKey:licenseKey
///                                        delegate:self;
///
///         return YES;
///     }
///
/// @param licenseKey The license key for your application. Required in order to run the application on a device.
/// @param delegate An object that responds to the required methods of ASFSessionDelegate. These methods will be invoked as the ASFSession changes status.
/// @return A pointer to the shared ASFSession.
/// @see startSessionUsingLicenseKey:andEnvironment:delegate:
/// @since 2.0
+ (ASFSession *) startSessionUsingLicenseKey:(NSString *)licenseKey delegate:(id<ASFSessionDelegate>)delegate;

///
/// Configures and starts an ASFSession in the specified environment.
/// ASFSession manages your application's relationship with the authenticated user and the device.
/// If a shared session is available, it will attempt to use it without asking the user for his/her credentials.
/// If any configuration is needed or the user needs to enter his/her credentials, your application will be covered by the
/// login window until the ASFSession is ready.
///
/// This should be called in your application's application:didFinishLaunchingWithOptions: method.
///
///     - (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
///     {
///         // Override point for customization after application launch.
///         [ASFSession startSessionUsingLicenseKey:licenseKey
///                                  andEnvironment:ASFEnvironmentDevelopment
///                                        delegate:self;
///
///         return YES;
///     }
///
/// @param licenseKey The license key for your application. Required in order to run the application on a device.
/// @param environment Whether your application is running in a development or production environment.
/// @param delegate An object that responds to the required methods of ASFSessionDelegate. These methods will be invoked as the ASFSession changes status.
/// @return A pointer to the shared ASFSession.
/// @see See configureUsingLicenseKey:andEnvironment:delegate: if you want to only configure the session at launch without immediately starting it.
/// @since 2.0
+ (ASFSession *) startSessionUsingLicenseKey:(NSString *)licenseKey andEnvironment:(ASFEnvironment)environment delegate:(id<ASFSessionDelegate>)delegate;

///
/// Configures and starts an ASFSession in the specified environment.
/// ASFSession manages your application's relationship with the authenticated user and the device.
/// If a shared session is available, it will attempt to use it without asking the user for his/her credentials.
/// If any configuration is needed or the user needs to enter his/her credentials, your application will be covered by the
/// login window until the ASFSession is ready.
///
/// This should be called in your application's application:didFinishLaunchingWithOptions: method.
///
///     - (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
///     {
///         // Override point for customization after application launch.
///         [ASFSession startSessionUsingLicenseKey:licenseKey
///                                    andSecretKey:secretKey
///                                        delegate:self];
///
///         return YES;
///     }
///
/// @param licenseKey The license key for your application. Required in order to run the application on a device.
/// @param secretKey The secret key for your application. Required to use the Activity Log API.
/// @param delegate An object that responds to the required methods of ASFSessionDelegate. These methods will be invoked as the ASFSession changes status.
/// @return A pointer to the shared ASFSession.
/// @see See configureUsingLicenseKey:andSecretKey:delegate: if you want to only configure the session at launch without immediately starting it.
/// @since 2.1
+ (ASFSession *) startSessionUsingLicenseKey:(NSString *)licenseKey andSecretKey:(NSString *)secretKey delegate:(id<ASFSessionDelegate>)delegate;

///
/// Configures and starts an ASFSession in the specified environment.
/// ASFSession manages your application's relationship with the authenticated user and the device.
/// If a shared session is available, it will attempt to use it without asking the user for his/her credentials.
/// If any configuration is needed or the user needs to enter his/her credentials, your application will be covered by the
/// login window until the ASFSession is ready.
///
/// This should be called in your application's application:didFinishLaunchingWithOptions: method.
///
///     - (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
///     {
///         // Override point for customization after application launch.
///         [ASFSession startSessionUsingLicenseKey:licenseKey
///                                    andSecretKey:secretKey
///                                  andEnvironment:ASFEnvironmentQA
///                                        delegate:self];
///
///         return YES;
///     }
///
/// @param licenseKey The license key for your application. Required in order to run the application on a device.
/// @param secretKey The secret key for your application. Required to use the Activity Log API.
/// @param environment Whether your application is running in a development or production environment.
/// @param delegate An object that responds to the required methods of ASFSessionDelegate. These methods will be invoked as the ASFSession changes status.
/// @return A pointer to the shared ASFSession.
/// @see See configureUsingLicenseKey:andSecretKey:inEnvironment:delegate: if you want to only configure the session at launch without immediately starting it.
/// @since 2.1
+ (ASFSession *) startSessionUsingLicenseKey:(NSString *)licenseKey andSecretKey:(NSString *)secretKey inEnvironment:(ASFEnvironment)environment delegate:(id<ASFSessionDelegate>)delegate;

///
/// Configures an ASFSession in the <strong>development</strong> environment, but does <strong>NOT</strong> start it. Your application is responsible for starting the session (see startSession).
///
/// This should be called in your application's application:didFinishLaunchingWithOptions: method.
///
///     - (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
///     {
///         // Override point for customization after application launch.
///         [ASFSession configureUsingLicenseKey:licenseKey
///                                     delegate:self];
///
///         return YES;
///     }
///
/// @param licenseKey The license key for your application. Required in order to run the application on a device.
/// @param delegate An object that responds to the required methods of ASFSessionDelegate. These methods will be invoked as the ASFSession changes status.
/// @return A pointer to the shared ASFSession.
/// @note Calling this method will set the startsSessionOnLaunch property to NO.
/// @see configureUsingLicenseKey:andEnvironment:delegate:
/// @see See startSessionUsingLicenseKey:andEnvironment:delegate: if you want to both start and configure the session at launch.
/// @see startSession
/// @since 2.0
+ (ASFSession *) configureUsingLicenseKey:(NSString *)licenseKey delegate:(id<ASFSessionDelegate>)delegate;

///
/// Configures an ASFSession in the specified environment, but does <strong>NOT</strong> start it. Your application is responsible for starting the session (see startSession).
///
/// This should be called in your application's application:didFinishLaunchingWithOptions: method.
///
///     - (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
///     {
///         // Override point for customization after application launch.
///         [ASFSession configureUsingLicenseKey:licenseKey
///                               andEnvironment:ASFEnvironmentQA
///                                     delegate:self];
///
///         return YES;
///     }
///
/// @param licenseKey The license key for your application. Required in order to run the application on a device.
/// @param environment Whether your application is running in a development or production environment.
/// @param delegate An object that responds to the required methods of ASFSessionDelegate. These methods will be invoked as the ASFSession changes status.
/// @return A pointer to the shared ASFSession.
/// @note Calling this method will set the startsSessionOnLaunch property to NO.
/// @see See startSessionUsingLicenseKey:andEnvironment:delegate: if you want to both start and configure the session at launch.
/// @see startSession
/// @since 2.0
+ (ASFSession *) configureUsingLicenseKey:(NSString *)licenseKey andEnvironment:(ASFEnvironment)environment delegate:(id<ASFSessionDelegate>)delegate;

///
/// Configures an ASFSession in the specified environment, but does <strong>NOT</strong> start it. Your application is responsible for starting the session (see startSession).
///
/// This should be called in your application's application:didFinishLaunchingWithOptions: method.
///
///     - (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
///     {
///         // Override point for customization after application launch.
///         [ASFSession configureUsingLicenseKey:licenseKey
///                                 andSecretKey:secretKey
///                                     delegate:self];
///
///         return YES;
///     }
///
/// @param licenseKey The license key for your application. Required in order to run the application on a device.
/// @param secretKey The secret key for your application. Required to use the Activity Log API.
/// @param delegate An object that responds to the required methods of ASFSessionDelegate. These methods will be invoked as the ASFSession changes status.
/// @return A pointer to the shared ASFSession.
/// @note Calling this method will set the startsSessionOnLaunch property to NO.
/// @see See startSessionUsingLicenseKey:andSecretKey:delegate: if you want to both start and configure the session at launch.
/// @see startSession
/// @since 2.1
+ (ASFSession *) configureUsingLicenseKey:(NSString *)licenseKey andSecretKey:(NSString *)secretKey delegate:(id<ASFSessionDelegate>)delegate;

///
/// Configures an ASFSession in the specified environment, but does <strong>NOT</strong> start it. Your application is responsible for starting the session (see startSession).
///
/// This should be called in your application's application:didFinishLaunchingWithOptions: method.
///
///     - (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
///     {
///         // Override point for customization after application launch.
///         [ASFSession configureUsingLicenseKey:licenseKey
///                                 andSecretKey:secretKey
///                               andEnvironment:ASFEnvironmentQA
///                                     delegate:self];
///
///         return YES;
///     }
///
/// @param licenseKey The license key for your application. Required in order to run the application on a device.
/// @param secretKey The secret key for your application. Required to use the Activity Log API.
/// @param environment Whether your application is running in a development or production environment.
/// @param delegate An object that responds to the required methods of ASFSessionDelegate. These methods will be invoked as the ASFSession changes status.
/// @return A pointer to the shared ASFSession.
/// @note Calling this method will set the startsSessionOnLaunch property to NO.
/// @see See startSessionUsingLicenseKey:andSecretKey:inEnvironment:delegate: if you want to both start and configure the session at launch.
/// @see startSession
/// @since 2.1
+ (ASFSession *) configureUsingLicenseKey:(NSString *)licenseKey andSecretKey:(NSString *)secretKey inEnvironment:(ASFEnvironment)environment delegate:(id<ASFSessionDelegate>)delegate;

#pragma mark - Accessing the Current Session
/** @name Accessing the Current Session */

///
/// Returns a reference to the singleton ASFSession instance or creates a new one if one has not been created yet.
/// Whenever you need to access a property of the current session, you'll used the sharedSession property.
///
///     NSString *displayName = [ASFSession sharedSession].displayName;
///
/// @return ASFSession* pointer to single ASFSession.
+ (ASFSession *) sharedSession;

#pragma mark - Accessing and Modifying Session Details
/** @name Accessing and Modifying Session Details */

/// The license key for your application. Obtain this value by contacting the Training and Development Department
/// @since 1.0.7
@property (nonatomic,strong) NSString *licenseKey;

/// The secret key for your application. You should receive this value when you receive the license key.
/// <strong>Keep this value secure!</strong> This helps determine that this app is truly this app when communicating with the server.
/// @since 2.1
@property (nonatomic, strong) NSString *secretKey;

/// The SAML audience defined in the SAML assertion (<saml:Audience />).
/// @deprecated This property is deprecated as of 2.0. There is no replacement method. It was directly related to the SAML token which is no longer used. It will return nil and will be removed in a future release.
@property (weak, nonatomic,readonly,getter=getAudience) NSString *audience DEPRECATED_ATTRIBUTE;

/// The raw base64 encoded data received from the authentication service.
/// @deprecated The raw authentication response is no longer available; there is currently no replacement.
@property (weak, nonatomic, readonly) NSString *authResponse DEPRECATED_ATTRIBUTE;

/// The URL to download the configuration profile from; this should be via HTTPS (e.g. https://mobileappserver.net/licensing/ ). In most cases, you should not be calling this method (unless instructed to do so by support). Setting this value will override the default value specified by the current environment.
/// @note As of 1.0.7, this value is set automatically. You only need to set this value if needing to override the default value.
/// @note If you override the default value, a warning message will briefly appear on the login view.
/// 
@property (nonatomic,strong) NSURL *configurationProfileURL;

/// The pointer to the object to which to send delegate methods.
@property (nonatomic,weak) id<ASFSessionDelegate>delegate;

/// The string from the SAML token that determines the expiry date for the token (the NotOnOrAfter value).
@property (weak, nonatomic,readonly,getter=getExpires) NSString *expires;

/// An NSDate object representing the time and date the current token will expire. nil if there is no active session.
/// @since 1.0.7
@property (weak, nonatomic,readonly) NSDate *expiresDate;

/// The version of the compiled framework.
@property (weak, nonatomic,readonly,getter=getFrameworkVersion) NSString *frameworkVersion;

/// The controller that manages checking the integrity of the application.
/// @since 1.0.7
@property (weak, nonatomic, readonly) ASFIntegrityCheckController *integrityCheckController;

/// Set to YES to enable outputs to the debug log.
@property (nonatomic,readwrite) BOOL logging;

/// YES if the session is valid; NO if there is no session or the session has expired.
/// @note The session is considered "valid" if the user is attempting to login (while the login view is visible on screen). If you want to determine whether the user is logged in or not, use sessionStarted.
/// 
/// @deprecated Use sessionStarted instead.
@property (nonatomic,readonly,getter=getIsSessionValid) BOOL isSessionValid DEPRECATED_ATTRIBUTE;

/// 
/// As of 1.0.7, the library will by default open the login view in a new window on top of your application.
/// You can set the background of this view by setting the loginBackgroundView property.
/// If you want to present the login view within one of your own view controllers, set this property. By default, it is nil.
/// 
@property (nonatomic,weak) UIViewController *rootViewController;

/// The SAML session index.
/// @deprecated We're no longer using SAML tokens for authentication. Currently this returns the access token, but it will be removed in a future release.
@property (weak, nonatomic,readonly,getter=getSessionIndex) NSString *sessionIndex DEPRECATED_ATTRIBUTE;

/// Boolean indicating whether the session has started or not.
@property (nonatomic,readonly) BOOL sessionStarted;

/// The saml:NameID specified in the saml:Subject node in the SAML assertion.
/// @deprecated We're no longer using SAML tokens for authentication. Currently this returns nil, but it will be removed in a future release.
@property (weak, nonatomic,readonly,getter=getSubjectNameID) NSString *subjectNameID DEPRECATED_ATTRIBUTE;

/// The time it took to receive a response from the PING federate server the last time an authentication request was made.
/// @since 1.0.7
@property (nonatomic,readonly) NSTimeInterval timeToReceiveAuthenticationResponse;

/// The time it took to parse the last response received from the PING federate authentication server.
/// @since 1.0.7
@property (nonatomic,readonly) NSTimeInterval timeToParseAuthenticationResponse DEPRECATED_ATTRIBUTE;

/// The number of seconds it took to send the username/password to the authentication service and receive an access token.
/// @since 2.0
@property (nonatomic, readonly) NSTimeInterval timeToVerifyCredentials;

/// The number of seconds it took to send the access token to the profile service and receive the user's profile information.
/// @since 2.0
@property (nonatomic, readonly) NSTimeInterval timeToRetrieveProfile;

/// If the protected files are available or are locked.
/// @since 1.0.7
@property (nonatomic, readonly) BOOL protectedFilesAvailable;

/// If set to YES, the framework will automatically attempt to start the session when the application has finished launching. By default in 1.0.7, this is set to YES.
/// @since 1.0.7
@property (nonatomic, readwrite) BOOL startsSessionOnLaunch;

/// If set to YES, the framework will automatically open the login window when an active session expires. By default in 1.0.7, this is set to YES.
/// @since 1.0.7
@property (nonatomic, readwrite) BOOL showsLoginWhenSessionExpires;

/// If set to YES, the framework will automatically open the login window when an active session ends. By default in 1.0.7, this is set to YES.
/// @since 1.0.7
@property (nonatomic, readwrite) BOOL showsLoginWhenSessionEnds;

/// If set to YES, the login view will close if there was a failure in starting the session. By default in 1.0.7, this is set to NO.
/// @since 1.0.7
/// @deprecated Although this will continue to function in version 2.0, this feature will be going away. In the future, ASFKit will not return control to your application until a session has been started.
@property (nonatomic, readwrite) BOOL closesLoginOnFailure DEPRECATED_ATTRIBUTE;

/// If set to YES, requests made through ASIHTTPRequest will use TLS 1.0 for all SSL validation.
/// @since 1.0.7
/// @deprecated This property only affects requests made by ASIHTTPRequest; ASIHTTPRequest has been removed so this property does nothing.
@property (nonatomic, strong) NSString *sslLevel DEPRECATED_ATTRIBUTE;

/// 
/// This value is passed through to the login view controller. This will be set as the background of the login view.
/// By default, the login view's background will be the application's splash screen.
/// 
/// @since 1.0.7
@property (nonatomic, weak) UIView *loginBackgroundView;

/// An array of URL schemes the application will respond to.
/// @since 1.0.7
@property (nonatomic, readonly) NSArray *urlSchemes;

///
/// Convenience method to check if the library has access to a given URL.
/// If the user is logged in, this will always return YES; otherwise it will return NO
/// unless the requested url is the same server as the configurationProfileURL.
///
/// @param url The URL to verify access to.
/// @return Whether the library can access the specified URL.
+ (BOOL) canAccessURL:(NSURL *)url;

///
/// Checks the configuration status of the device. It will determine if the
/// configuration profile is installed or not.
///
/// @param error On input, a pointer to an error object. If the configuration profile is not installed,
/// this pointer is set to an actual error object containing the error information.
/// You may specify nil for this parameter if you do not want the error information.
///
/// @return YES if the configuration profile is installed; NO if either the configuration profile or leaf certificate are missing.
/// @since 1.0.7
- (BOOL) verifyConfigurationWithError:(NSError **)error;


#pragma mark Accessing User Data
/** @name Accessing user data */

///
/// Represents the current authorization of the current user. Contains the access token and the user's profile. This data is stored and read from the device's keychain.
/// This will be nil if the session has not been started yet.
///
/// @since 2.0
@property (strong, nonatomic, readonly) ASFAuthorization *authorization;
/// The Active Directory domain the user logged into (e.g. oneabbott).
@property (weak, nonatomic,readonly,getter=getAdDomain) NSString *adDomain;
/// The username the user used to log in.
@property (weak, nonatomic,readonly,getter=getAdLogon) NSString *adLogon;
/// The ISO 3166 Country Code (e.g. US).
@property (weak, nonatomic,readonly,getter=getC) NSString *c;
/// The AbbVie Company or Affiliate the user is associated with (e.g. AbbVie).
@property (weak, nonatomic,readonly,getter=getCompany) NSString *company;
/// The AbbVie department the user belongs to (e.g. 1234-Sales).
@property (weak, nonatomic,readonly,getter=getDepartment) NSString *department;
/// The user's display name (e.g. Abbott, Wallace C)
@property (weak, nonatomic,readonly,getter=getDisplayName) NSString *displayName;
/// The AbbVie division the user belongs to (e.g. PPD-Pharmaceutical Products Division).
/// @deprecated This property is no longer supported; use divisionName instead.
@property (weak, nonatomic,readonly,getter=getDivision) NSString *division DEPRECATED_ATTRIBUTE;
/// The division code for the division the user belongs to (e.g. PPD).
@property (weak, nonatomic,readonly,getter=getDivisionCode) NSString *divisionCode;
/// The division name for the division the user belongs to (e.g. Pharmaceutical Products Division).
@property (weak, nonatomic,readonly,getter=getDivisionName) NSString *divisionName;
/// The employment type (e.g. EMPLOYEE).
@property (weak, nonatomic,readonly,getter=getEmployeeType) NSString *employeeType;
/// The user's full name (e.g. Abbott, Wallace C).
/// @deprecated This property is no longer supported; use displayName instead.
@property (weak, nonatomic,readonly,getter=getFullName) NSString *fullName DEPRECATED_ATTRIBUTE;
/// The user's first (given) name (e.g. Wallace).
@property (weak, nonatomic,readonly,getter=getGivenName) NSString *givenName;
/// The user's middle initial (e.g. C).
@property (weak, nonatomic,readonly,getter=getInitials) NSString *initials;
/// AbbVie Location (e.g. US-IL-AbbVie Park-AP30).
@property (weak, nonatomic,readonly,getter=getLocation) NSString *location;
/// The user's e-mail address (e.g. wallace.c.abbott@abbvie.com ).
@property (weak, nonatomic,readonly,getter=getMail) NSString *mail;
/// The display name of the manager the user reports to.
@property (weak, nonatomic,readonly,getter=getManagerDisplayName) NSString *managerDisplayName;
/// The e-mail address of the manager the user reports to.
@property (weak, nonatomic,readonly,getter=getManagerEmail) NSString *managerEmail;
/// The UPI of the manager the user reports to.
@property (weak, nonatomic,readonly,getter=getManagerUPI) NSString *managerUPI;
/// The preferred language of the user (e.g. en).
@property (weak, nonatomic,readonly,getter=getPreferredLanguage) NSString *preferredLanguage;
/// The user's sales area.
@property (weak, nonatomic,readonly,getter=getSalesArea) NSString *salesArea;
/// The user's sales category ID.
@property (weak, nonatomic,readonly,getter=getSalesCategoryID) NSString *salesCategoryID;
/// The user's sales district.
@property (weak, nonatomic,readonly,getter=getSalesDistrict) NSString *salesDistrict;
/// The user's sales force name.
@property (weak, nonatomic,readonly,getter=getSalesForceName) NSString *salesForceName;
/// The user's sales force number.
@property (weak, nonatomic,readonly,getter=getSalesForceNumber) NSString *salesForceNumber;
/// The user's sales franchise.
@property (weak, nonatomic,readonly,getter=getSalesFranchise) NSString *salesFranchise;
/// The user's sales franchise ID.
@property (weak, nonatomic,readonly,getter=getSalesFranchiseID) NSString *salesFranchiseID;
/// The user's sales product mix index.
@property (weak, nonatomic,readonly,getter=getSalesPMI) NSString *salesPMI;
/// The user's sales region.
@property (weak, nonatomic,readonly,getter=getSalesRegion) NSString *salesRegion;
/// Whether the rep does sales sampling.
@property (weak, nonatomic,readonly,getter=getSalesSampling) NSString *salesSampling;
/// The user's surname (last name) (e.g. AbbVie).
@property (weak, nonatomic,readonly,getter=getSn) NSString *sn;
/// The AbbVie Building Code for the user (e.g. S0256).
@property (weak, nonatomic,readonly,getter=getSpaceStructureID) NSString *spaceStructureID;
/// The user's office phone number.
@property (weak, nonatomic,readonly,getter=getTelephoneNumber) NSString *telephoneNumber;
/// The user's sales territory.
@property (weak, nonatomic,readonly,getter=getTerritory) NSString *territory;
/// The user's job title.
@property (weak, nonatomic,readonly,getter=getTitle) NSString *title;
/// The user's UPI.
@property (weak, nonatomic,readonly,getter=getUpi) NSString *upi;

/// 
/// Gets an attribute that was specified in the SAML assertion AttributeStatement.
/// 
/// @param attribute Name of attribute to get from the SAML assertion.
/// @return NSString* of the retrieve attribute from the SAML assertion AttributeStatement.
/// @deprecated We're no longer using SAML tokens for authentication. This currently attempts to find the same value in the user's profile, but it will be removed in a future release.
- (NSString *) getSAMLAttribute: (NSString *)attribute DEPRECATED_ATTRIBUTE;

/// 
/// Returns the first value matching the xpath argument.
/// 
/// @param xpath The node to search for using XPath syntax.
/// @return NSString* of the retrieved value from the SAML assertion or nil.
/// @deprecated Authentication responses no longer are returned as XML documents and therefore XPath's are now irrelevant. This method WILL return nil. It will be removed in a future release.
- (NSString *) getValueforXPath: (NSString *)xpath DEPRECATED_ATTRIBUTE;

@end;

/// 
/// The methods declared by the ASFSessionDelegate protocol allow the adopting delegate respond to messages from ASFSession--specifially significant lifecycle events such as the starting or ending of a session.
///
@protocol ASFSessionDelegate <NSObject>
@required

/// 
/// Tells the delegate that a new session has started and user data is available for use.
/// Your application should perform any set up or start up tasks that are appropriate at this point.
/// 
/// @param session A pointer to the session that was just created.
/// @see ASFSession::startSession
- (void)sessionDidStart: (ASFSession *)session;

/// 
/// Tells the delegate that the session ended <em>by request</em> of the current application.
/// When the session ends, your application should clean up any potentially sensitive data that is in memory (this includes removing any views that may contain sensitive information). All user-specific data should be released from memory. In can be stored in a cache or removed all together if it can later be retrieved from a server.  Your application should return to a view that contains no proprietary or sensitive information. A full screen UIImageView containing the splash screen is a great way to fulfill this requirement.
///
/// By default As of 1.0.7, when the session ends, the framework will attempt to start it again (triggering the login view to display). To prevent the login view from automatically showing when a session ends, add the following line to your application:didFinishLaunchingWithOptions: method:
///
///     [ASFSession sharedSession].showsLoginWhenSessionEnds = NO;
/// 
/// If you choose to prevent the login view from automatically opening, ensure that the user has some way to log back in without having to terminate and restart the app. If your app has a menu that doesn’t contain any potentially proprietary or sensitive information, it could contain a login button for when there is no active session. In most cases, it is sufficient to attempt to start a new session immediately after receiving the notification that the previous session has ended.
///
/// When your application ends the session, other running applications that were using the active session will receive the ASFSessionExpiredNotification notification and sessionDidExpire: will be called on the delegate upon returning to the foreground.
///
/// Be sure to save any peritant user data before the session ends. When this method is valled, the user information is <strong>not</strong> available. You'll need to implement the sessionWillEnd: and sessionWillExpire: methods to be notified when the session is about to end, but the user data hasn't been destroyed yet.
/// 
/// @param session A pointer to the session that ended.
/// @see ASFSession::endSession
- (void)sessionDidEnd: (ASFSession *)session;

/// 
/// Tells the delegate that the session ended <em>automatically</em> because it was no longer valid.
/// A session may expire for one of three reasons:
/// 1.	The user’s access token and refresh token are no longer valid,
/// 2.	Another application ended the session (or the user chose to “Sign out on launch” in another application), or
/// 3.	The integrity of the device is in question.
///
/// Your application will receive the ASFSessionExpiredNotification notification and sessionDidExpire: will be called on the current delegate.
/// Your application should take the same actions when a session expires as it would if it ended the session itself including cleaning up any potentially sensitive data from memory.
/// By default as of 1.0.7, when the session expires, the framework will attempt to start it again (triggering the login view to display). To prevent the login view from automatically showing when a session expires, add the following line to your application:didFinishLaunchingWithOptions: method:
///
///     [ASFSession sharedSession].showsLoginWhenSessionExpires = NO;
///
/// Be sure to save any peritant user data before the session ends. When this method is valled, the user information is <strong>not</strong> available. You'll need to implement the sessionWillEnd: and sessionWillExpire: methods to be notified when the session is about to end, but the user data hasn't been destroyed yet.
/// 
/// @param session A pointer to the session that ended.
/// @see sessionDidEnd:
- (void)sessionDidExpire: (ASFSession *)session;

@optional
/// Invoked just before the session data will be destroyed. Implementing applications can use this method to save their application state.
/// @param session A pointer to the ASFSession that is about to end.
/// @since 2.0
- (void)sessionWillEnd:(ASFSession *)session;
/// Invoked just before the session data will be destroyed. Implementing applications can use this method to save their application state.
/// @param session A pointer to the ASFSession that is about to expire.
/// @since 2.0
- (void)sessionWillExpire:(ASFSession *)session;

/// Additional convenience method for session did start to also receive the user profile with it.
/// @param session A pointer to the session that was just created.
/// @param profile A pointer to the profile that is linked to the session.
/// @since 2.1
- (void)sessionDidStart:(ASFSession *)session withUserProfile:(ASFProfile *)profile;

/// 
/// Tells the delegate that a session failed to start.
/// A session may fail to start due to
/// 1. An invalid license (or being unable to verify the license because the device does not have Internet access), or
/// 2. A missing leaf certificate or configuration profile.
///
/// As of version 2.0, as long as ASFSession::closesLoginOnFailure is set to NO (the default value), ASFKit will handle the error and work with the user to resolve the problem. Your application does not need to do anything.
/// 
/// @param error An error object describing why the session didn't start.
/// @note Optional as of 2.0.
- (void)sessionFailedToStartWithError:(NSError *)error;

/// 
/// Tells the delegate that user had enabled the <strong>Sign out on launch</strong> switch in the Settings app.
/// 
/// @param session A reference to the session that is ending.
- (void)sessionDidEndOnLaunch: (ASFSession *)session;

/// 
/// Tells the delegate that the logged in user changed while the application is in the background.
/// This method is only called if your application was using the shared session, was sent to the background, the user changed, and your application returned to the foreground. It is not for being notified between application launches that a different user is using the application.
/// 
/// @param session A reference to the session that now holds the new user information.
- (void)sessionDidChange: (ASFSession *)session;

/// Additional convenience method for session did change to also receive the user profile with it.
/// @param session A reference to the session that now holds the new user information.
/// @param profile A pointer to the profile that is linked to the session.
/// @since 2.1
- (void)sessionDidChange:(ASFSession *)session withUserProfile:(ASFProfile *)profile;

/// 
/// Tells the delegate that files that were protected by the framework were delete from the device.
/// Files are deleted if the license for your application is revoked OR the configuration profile is removed from the device.
/// 
/// @param session The currently active session.
/// @param removedFiles A list of file paths that were removed.
- (void)session: (ASFSession *)session didRemoveProtectedFiles: (NSArray *)removedFiles;

/// 
/// Tells the delegate that the implementing application's interface is about to be covered/disabled by ASFKit and the login view will appear.
/// This does not neccesarily mean that the user needs to log in again--it only means that the framework needs the user's attention (e.g. a new leaf certificate needs to be downloaded) and it is going to take control from your application.
/// 
- (void)loginViewWillShow;
/// 
/// Tells the delegate that the ASFKit interface has finished animating onto the screen. The implementing application's interface is now completely covered. It should not be attempting to regain control of the screen.
/// 
- (void)loginViewDidShow;
/// 
/// Tells the delegate that the ASFKit interface is about to begin animating off the screen. The viewWillAppear: methods will be invoked on the rootViewController of the implementing application.
/// 
- (void)loginViewWillHide;
/// 
/// Tells the delegate that the ASFKit interface has finished animating off the screen.
/// Your application should not use this as an indication that a valid session is available--it should only use sessionDidStart: to determine if a session is available or not.
/// 
- (void)loginViewDidHide;

/// 
/// Tells the delegate that the configuration profile has been installed.
/// 
- (void) configurationProfileWasInstalled;
/// 
/// Tells the delegate that the configuration profile (or leaf certificate) was uninstalled from the device.
/// Any protected files will be removed.
/// 
/// @see session:didRemoveProtectedFiles:
- (void) configurationProfileWasUninstalled;

@end;
