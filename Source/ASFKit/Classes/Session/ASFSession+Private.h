//
//  ASFSession+Private.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFSession.h"

typedef NS_ENUM(NSInteger,ASFSessionAction)
{
    ASFActionValidateLicense,
    ASFActionEnterCredentials,
    ASFActionDownloadConfiguration,
    ASFActionStartSession
};

@class ASFAuthorization;
@class ASFConfiguration;
@interface ASFSession ()

@property (nonatomic, strong) ASFConfiguration *configuration;
@property (nonatomic, readwrite) ASFEnvironment environment;
@property (nonatomic, readwrite) NSTimeInterval timeToVerifyCredentials;
@property (nonatomic, readwrite) NSTimeInterval timeToRetrieveProfile;

/// This is a "private" property on ASFSession. When set to YES, ASFURLProtocol does not check that requests are made over https.
/// This is here just in case built-in frameworks make insecure network requests and the secure checking needs to be disabled (all requests to *.apple.com are already allowed).
/// @since 2.1
@property (nonatomic, readwrite) BOOL shouldAllowInsecureNetworkRequests;

// This is somewhat strangely named (and not following conventions)
// because I don't want implementing apps to change authorization
// and this is just harder to guess (although not impossible).
- (void) setCurrentAuthorizationForSession:(ASFAuthorization *)authorization updatingKeychain:(BOOL)shouldUpdateKeychain;

- (ASFAuthorization *) getSharedAuthorization;


/// This may eventually become customizable
/// For now it is a static list of URLs that are whitelisted for use within the framework, as judged by ASFURLProtocol
/// @since 2.1
- (NSArray *) whitelistedURLs;

@end
