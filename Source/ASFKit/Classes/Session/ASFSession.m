//
//  ASFSession.m
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

// Macro for easily creating a singleton out of this object
#import "SynthesizeSingleton.h"

#import "ASFSession.h"
#import "ASFKeychain.h"
#import "ASFKitDevice.h"
#import "ASFUtilities.h"
#import "ASFLog.h"
#import "ASFKitViewController.h"
#import "ASFIntegrityCheckController.h"
#import "ASFIntegrityCheckResult.h"
#import "ASFConfigurationNotifier.h"
#import "ASFUserAuthorizationProvider.h"
#import "ASFAuthorization_Private.h"
#import "ASFProfile.h"
#import "ASFConfiguration.h"
#import "ASFActivityLog_Private.h"
#import "ASFActivityLog.h"
#import "ASFActivityLogConsentManager.h"
#import "ASFActivityObject.h"
#import "ASFActivityAttachment.h"
#import "ASFURLProtocol.h"
#import "UIImage+ASFLaunchImage.h"

// Integrity Checks
#import "ASFLicenseVerify.h"
#import "ASFAuthorizationVerify.h"

// File Protection API
#import "ASFFileProtectionManager.h"
#import "ASFFileProtectionManager+Private.h"

#import <objc/runtime.h>

#pragma mark - Constants
// NSNotification name constants
NSString* const ASFSessionStartedNotification                   = @"ASFSessionStarted";
NSString* const ASFSessionFailedNotification                    = @"ASFSessionFailed";
NSString* const ASFSessionEndedNotification                     = @"ASFSessionEnded";
NSString* const ASFSessionWillEndNotification                   = @"ASFSessionWillEnd";
NSString* const ASFSessionExpiredNotification                   = @"ASFSessionExpired";
NSString* const ASFSessionWillExpireNotification                = @"ASFSessionWillExpire";
NSString* const ASFSessionEndedOnLaunchNotification             = @"ASFSessionEndedOnLaunch";
NSString* const ASFSessionChangedNotification                   = @"ASFSessionChanged";

// Other string constants
NSString* const ASFSessionKeychainException                     = @"ASFSessionKeychainException";
NSString* const ASFPublicKeychainPrefix                         = @"Public";

#pragma mark - "Private" methods
/// <devdoc>
/// This uses an empty category name to "extend" what was already declared in the header file;
/// this allows me to declare methods without placing them in the header file.
/// For example, I don't want an implementing app to know about the "showLoginWindow" method, so I placed it here.
/// There really is no such thing as "private" methods in Objective-C;
/// instead, this is more privacy by obscurity since none of these methods are defined in the header.
/// </devdoc>
@interface ASFSession() <ASFUserAuthorizationProviderDelegate, ASFKitViewControllerDelegate>

@property (nonatomic, readonly) BOOL configurationURLIsOverridden;
@property (nonatomic, readonly) ASFLicenseStatus licenseStatus;
@property (nonatomic, readwrite) BOOL performingInitialLicenseVerify;

@end

#pragma mark -

static ASFSession *sharedSession;

@implementation ASFSession
{
    UIImageView *snapshotProtectionView;
}
@synthesize rootViewController, configurationProfileURL, protectedFilesAvailable, showsLoginWhenSessionExpires, showsLoginWhenSessionEnds, startsSessionOnLaunch, urlSchemes, integrityCheckController, closesLoginOnFailure, authorization=_authorization;

+ (ASFSession *) sharedSession
{
    @synchronized(self)
    {
        if (sharedSession==nil)
        {
            sharedSession = [ASFSession new];
            return sharedSession;
        }
    }
    return sharedSession;
}

+ (id) allocWithZone:(NSZone *)zone
{
    @synchronized(self)
    {
        if (sharedSession == nil)
        {
            sharedSession = [super allocWithZone:zone];
            return sharedSession;
        }
    }
    return nil;
}

- (id) copyWithZone:(NSZone *)zone
{
    return self;
}

- (id) init
{
    self = [super init];
    if (self) 
    {
        [NSURLProtocol registerClass:[ASFURLProtocol class]];
    
        // Assume that protected files are available
        protectedFilesAvailable = YES;
        
        showsLoginWhenSessionExpires = YES;
        showsLoginWhenSessionEnds = YES;
        startsSessionOnLaunch = YES;
        closesLoginOnFailure = NO;
        
        // Apply the default configuration
        _configuration = [ASFConfiguration loadConfiguration];
        
        [self verifyURLSchemes];
        
        // Add handlers for the different application events
        NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
        
        // Begin protecting the file system
        [ASFFileProtectionManager enableProtection];
        
        // Configure the integrity checker
        integrityCheckController = [ASFIntegrityCheckController sharedInstance];
        [integrityCheckController addIntegrityCheck:[ASFFileProtectionManager defaultManager]];
        [integrityCheckController addIntegrityCheck:[ASFAuthorizationVerify authorizationVerifier]];
        [notificationCenter addObserver:self selector:@selector(onIntegrityCheckFinished:) name:ASFIntegrityCheckFinishedNotification object:integrityCheckController];
        
        // Config notifier
        [notificationCenter addObserver:self selector:@selector(configurationProfileWasInstalled) name:ASFConfigurationProfileWasInstalledNotification object:nil];
        [notificationCenter addObserver:self selector:@selector(configurationProfileWasUninstalled) name:ASFConfigurationProfileWasUninstalledNotification object:nil];
        [ASFConfigurationNotifier beginChecking];
        
        // Reload the session data stored in the keychain
        [self reloadSession];
        
        // Set up the activity log
        [ASFActivityLog sharedActivityLog];
        
        [notificationCenter addObserver:self selector:@selector(onEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
        [notificationCenter addObserver:self selector:@selector(onEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
        [notificationCenter addObserver:self selector:@selector(onBecameActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
        [notificationCenter addObserver:self selector:@selector(onApplicationLaunched:) name:UIApplicationDidFinishLaunchingNotification object:nil];
        [notificationCenter addObserver:self selector:@selector(onDataDidBecomeAvailable:) name:UIApplicationProtectedDataDidBecomeAvailable object:nil];
        [notificationCenter addObserver:self selector:@selector(onDataWillBecomeUnavailable:) name:UIApplicationProtectedDataWillBecomeUnavailable object:nil];
        [notificationCenter addObserver:self selector:@selector(onWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
        [notificationCenter addObserver:self selector:@selector(onLicenseStatusUpdate:) name:LicenseStatusDidUpdateNotification object:nil];
        
        // Listen for internal events by ASFSession "modules"
        // and trigger a delegate call for each of them
        [notificationCenter addObserver:self selector:@selector(protectedFilesWereRemoved:) name:ASFProtectedFilesWereRemovedNotification object:nil];
        
        // Login view
        [notificationCenter addObserver:self selector:@selector(onLoginViewWillShow) name:ASFLoginViewWillShowNotification object:nil];
        [notificationCenter addObserver:self selector:@selector(onLoginViewDidShow) name:ASFLoginViewDidShowNotification object:nil];
        [notificationCenter addObserver:self selector:@selector(onLoginViewWillHide) name:ASFLoginViewWillHideNotification object:nil];
        [notificationCenter addObserver:self selector:@selector(onLoginViewDidHide) name:ASFLoginViewDidHideNotification object:nil];
        
        // iOS 7 only methods
        if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
            // In iOS 7 we can check for screenshots.
            // Unfortuantely it doesn't notify us until it has already happened but we can log the events
            [notificationCenter addObserver:self selector:@selector(onApplicationDidTakeScreenshot:) name:UIApplicationUserDidTakeScreenshotNotification object:nil];
        }
        
        _initialized = YES;
    }
    return self;
}

- (void) dealloc
{
    _delegate = nil;
}

# pragma mark - Internal Events
/// <devdoc>
/// Files protected by the framework were just deleted.
/// </devdoc>
- (void) protectedFilesWereRemoved: (NSNotification *)notification
{
    [ASFLog message:[NSString stringWithFormat:@"The files protected by the framework were deleted: \n%@", [notification.userInfo valueForKey:kRemovedFiles]]];
    
    if ([_delegate respondsToSelector:@selector(session:didRemoveProtectedFiles:)])
    {
        [_delegate session:self didRemoveProtectedFiles:[notification.userInfo valueForKey:kRemovedFiles]];
    }
}

/// <devdoc>The login view is about to animate onto the screen.</devdoc>
- (void) onLoginViewWillShow
{
    [ASFLog message:@"Login view about to open."];
    if ([_delegate respondsToSelector:@selector(loginViewWillShow)])
    {
        [_delegate loginViewWillShow];
    }
}

/// <devdoc>The login view just finished animating onto the screen.</devdoc>
- (void) onLoginViewDidShow
{
    [ASFLog message:@"Login view did open."];
    if ([_delegate respondsToSelector:@selector(loginViewDidShow)])
    {
        [_delegate loginViewDidShow];
    }
}

/// <devdoc>The login view is about to animate off the screen.</devdoc>
- (void) onLoginViewWillHide
{
    [ASFLog message:@"Login view about to close."];
    if ([_delegate respondsToSelector:@selector(loginViewWillHide)])
    {
        [_delegate loginViewWillHide];
    }
}

/// <devdoc>The login view just finished animating off the screen.</devdoc>
- (void) onLoginViewDidHide
{
    [ASFLog message:@"Login view did close."];
    if ([_delegate respondsToSelector:@selector(loginViewDidHide)])
    {
        [_delegate loginViewDidHide];
    }
}

- (void) configurationProfileWasInstalled
{
    [ASFLog message:@"The configuration profile was installed."];
    if ([_delegate respondsToSelector:@selector(configurationProfileWasInstalled)])
    {
        [_delegate configurationProfileWasInstalled];
    }
}

- (void) configurationProfileWasUninstalled
{
    [ASFLog message:@"The configuration profile was uninstalled."];
    if ([_delegate respondsToSelector:@selector(configurationProfileWasUninstalled)])
    {
        [_delegate configurationProfileWasUninstalled];
    }
}

- (void) onLicenseStatusUpdate:(NSNotification *)notification
{
    ASFLicenseStatus status = [[notification.userInfo objectForKey:@"status"] intValue];
    _licenseStatus = status;
    ASFConfiguration *newConfiguration = [notification.userInfo objectForKey:@"configuration"];
    
    // Check to see if the configuration has changed
    if (![self.configuration isEqual:newConfiguration])
    {
        self.configuration = newConfiguration;
    }
}

# pragma mark - UIApplication Events

/// <devdoc>
/// ASFSession listens for UIApplicationWillResignActive in case it needs to protect from screenshots
/// The other side of this is onEnterForeground: which listens for UIApplicationWillEnterForeground
/// </devdoc>
- (void) onWillResignActive:(NSNotification*)notification
{
    // Check for snapshot protection
    if (self.configuration.snapshotProtection) {
        // We want to protect against snapshots, so we will just show a screen over the app
        snapshotProtectionView = [[UIImageView alloc] initWithFrame:[UIApplication sharedApplication].keyWindow.frame];
        // Adding a tag to the view allows us to safely remove it when the time comes
        snapshotProtectionView.tag = 2323;
        // Just use a black screen since defaults can now be added a multitude of ways
        snapshotProtectionView.backgroundColor = [UIColor blackColor];
        // Add the application launch image
        UIInterfaceOrientation interfaceOrientation = [UIApplication sharedApplication].statusBarOrientation;
        UIInterfaceOrientation actualLaunchImageOrientation;
        UIImage *launchImage = [UIImage bestLaunchImageForOrientation:interfaceOrientation isForInterfaceOrientation:&actualLaunchImageOrientation];
        
        if (launchImage) {
            launchImage = [UIImage imageWithCGImage:launchImage.CGImage
                                              scale:[UIScreen mainScreen].scale
                                        orientation:[UIImage imageOrientationForInterfaceOrientation:actualLaunchImageOrientation]];
            snapshotProtectionView.image = launchImage;
        }
        // Add the white subview on top of the key window
        [[UIApplication sharedApplication].keyWindow addSubview:snapshotProtectionView];
        
        // iOS 7 only methods
        if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
                // We can also ignore snapshot on next application launch
                // This is particularly useful for the multitasking screen as it is presented in iOS 7
                [[UIApplication sharedApplication] ignoreSnapshotOnNextApplicationLaunch];
        }
    }
}

/// <devdoc>
/// ASFSession listens for UIApplicationWillEnterForeground to reload the session
/// from the keychain and restart any tasks that were suspended when the application moved to the background.
/// </devdoc>
- (void) onEnterForeground:(NSNotification*)notification
{
    [ASFLog message:@"Application is entering foreground."];
    
    // Reload session data from keychain
    [self reloadSession];
}

/// <devdoc>
/// ASFSession listens for UIApplicaitonDidEnterBackground to suspend any running tasks.
/// </devdoc>
- (void) onEnterBackground:(NSNotification*)notification
{
    [ASFLog message:@"Application is entering background."];
}

/// <devdoc>
/// Invoked when the application becomes active.
/// Used to automatically start the session when the application launches.
/// </devdoc>
- (void) onBecameActive:(NSNotification *)notificiation
{
    // Are we covering any screens with our snapshot blocker?
    [snapshotProtectionView removeFromSuperview];
    snapshotProtectionView = nil;
    
    // Display the Activity Log consent prompt (if neccessary)
    [ASFActivityLogConsentManager displayConsentPromptIfNeeded];
}

/// <devdoc>
/// Invoked the first time the UIApplicationDidBecomeActive notification is posted to the notification center.
/// </devdoc>
- (void) onApplicationLaunched:(NSNotification *)notification
{
    [ASFLog message:@"The application has launched."];
    
    UIApplication *application = (UIApplication *)notification.object;
    if ([application isKindOfClass:[UIApplication class]]) {
        if (startsSessionOnLaunch) {
            [self startSession];
        }
    }
}

/// <devdoc>
/// Simply here for debugging purposes while logging is enabled; 
/// useful for tracking when the data is becoming unavailable/available.
/// </devdoc>
- (void) onDataDidBecomeAvailable:(NSNotification*)notification
{
    [ASFLog message:@"Protected data has become available."];
    protectedFilesAvailable = YES;
}

/// <devdoc>
/// Simply here for debugging purposes while logging is enabled; 
/// useful for tracking when the data is becoming unavailable/available.
/// </devdoc>
- (void) onDataWillBecomeUnavailable:(NSNotification*)notification
{
    [ASFLog message:@"Protected data will become unavailable."];
    protectedFilesAvailable = NO;
}

- (void) onApplicationDidTakeScreenshot:(NSNotification*)notification
{
    if (self.configuration.snapshotProtection) {
        // Get the pixel data from the current key window
        UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
        CGSize originalSize = keyWindow.bounds.size;
        CGFloat screenScale = keyWindow.screen.scale;
        UIGraphicsBeginImageContextWithOptions(originalSize, YES, screenScale);
        [keyWindow drawViewHierarchyInRect:keyWindow.bounds afterScreenUpdates:NO];
        UIImage *screenshot = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        // Resize the screenshot so that it's small enough to send in the activity log
        // The image resizing can happen in a background queue
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            CGSize resize = (CGSize){
                .width = originalSize.width,
                .height = originalSize.height
            };
            
            UIGraphicsBeginImageContextWithOptions(resize, YES, 1.0);
            [screenshot drawInRect:CGRectMake(0, 0, resize.width, resize.height)];
            UIImage *scaledScreenshot = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            // Log the fact that a screenshot was taken
            ASFActivityObject *object = [ASFActivityObject activityObjectWithDisplayName:@"a screenshot" andType:@"screenshot"];
            ASFActivityAttachment *attachment = [ASFActivityAttachment attachmentWithImage:scaledScreenshot andCompressionQuality:50];
            [ASFActivityLog logActionForCurrentUser:@"snapped" withObject:object withAttachment:attachment completionHandler:nil];
        });
    }
}

#pragma mark - Private methods
/// <devdoc>Verifies that the application has specified URL schemes.</devdoc>
- (void) verifyURLSchemes
{
    NSMutableArray *schemes = [NSMutableArray array];
    
    NSArray *urlTypes = [[[NSBundle mainBundle] infoDictionary] valueForKey:@"CFBundleURLTypes"];
    for (NSDictionary *urlType in urlTypes)
    {
        for (NSString *scheme in [urlType objectForKey:@"CFBundleURLSchemes"])
        {
            if (scheme)
            {
                [schemes addObject:scheme];
            }
        }
    }
    
    urlSchemes = [[NSArray alloc] initWithArray:schemes];
    
    // If there are 0 URL schemes, and this is the simulator, display a warning in the log
    // If there are 0 URL schemes and this is the device, throw an exception
    if (![urlSchemes count])
    {
#if TARGET_IPHONE_SIMULATOR
        NSLog(@"WARNING: No URL schemes have been defined for the application.");
#else
        [NSException raise:@"ASFSessionConfigurationException" format:@"The application must define at least one URL scheme in the Info.plist."];
#endif
        
    }
}

/// <devdoc>
/// Updates the NSUserDefault values.
/// This always updates the logout switch to NO.
/// </devdoc>
- (void) updateSettingsBundle
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:self.frameworkVersion forKey:FrameworkVersionKey];
    [defaults setValue:self.displayName forKey:UserKey];
    [defaults setValue:self.adDomain forKey:DomainKey];
    [defaults setValue:([ASFActivityLog activityLoggingIsAvailable] ? ASFKitLocalizedString(@"Enabled") : ASFKitLocalizedString(@"Disabled")) forKey:ActivityLogStatusKey];
    
    // If there is no terms to consent to, reset the switch to NO
    if (!self.configuration.activityLogPrivacyNotice.length) {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:ResetConsentSwitchKey];
    }
    
    // Another method should have already caught whether the logout switch was flipped.
    [defaults setBool:NO forKey:LogoutSwitchKey];
    
    // Get the app version from the Info.plist
    [defaults setValue:[[[NSBundle mainBundle] infoDictionary] valueForKey:@"CFBundleShortVersionString"]  forKey:AppVersionKey];
    
    [defaults synchronize];
}

/// <devdoc>
/// Reloads the authorization information from the keychain and stores it in _authorization.
/// Sets _authorization to nil if the data in the keychain is corrupted or invalid.
/// </devdoc>
- (void) reloadSessionFromKeychain
{
    ASFAuthorization *authorization = nil;
    
    if (!self.configuration.keychainServiceName.length) {
        [NSException exceptionWithName:ASFSessionKeychainException reason:@"There is no keychain service name." userInfo:nil];
    }
    
    @try
    {
        NSString *serviceName = [UIApplication sharedApplication].protectedDataAvailable ? self.configuration.keychainServiceName : [ASFPublicKeychainPrefix stringByAppendingString:self.configuration.keychainServiceName];
        authorization = [ASFKeychain load:serviceName];
        if (![authorization isKindOfClass:[ASFAuthorization class]])
        {
            authorization = nil;
        }
    }
    @catch (NSException *e)
    {
        authorization = nil;
    }
    [self setCurrentAuthorizationForSession:authorization updatingKeychain:NO];
}

/// <devdoc>
/// Upon an application entering the foreground or launching, it checks the keychain for existing session data.
/// </devdoc>
- (void) reloadSession
{
    if (_attemptingLogin)
        return;
    
    [ASFLog message:@"Reloading session data from keychain."];
    
    // Check the value of the logout switch
    [[NSUserDefaults standardUserDefaults] synchronize];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:LogoutSwitchKey] && _sessionStarted)
    {
        [self endSessionOnLaunch];
        return;
    }
    
    // Store the current user's UPI before reloading from the keychain
    NSString *previousUPI = self.upi;
    
    // Load existing data from the keychain
    [self reloadSessionFromKeychain];
    
    if (_configuring)
        return;
    
    // Verify that the configuration profile is still installed
    switch ([ASFKitDevice verifyConfiguration])
    {
        case ASFDeviceConfigMissingLeaf:
        {
            [[ASFFileProtectionManager defaultManager] removeAllProtectedLocations];
            if (_sessionStarted)
            {
                NSError *error = [ASFError errorWithCode:ASFErrorConfigurationLeafCertificateMissing];
                [self endLocalSessionWithError:error];
                return;
            }
            break;
        }
        case ASFDeviceConfigNotInstalled:
        {
            NSError *error = [ASFError errorWithCode:ASFErrorConfigurationProfileMissing];
            [[ASFFileProtectionManager defaultManager] removeAllProtectedLocations];
            [self endSharedSessionWithError:error];
            return;
        }
        default:
            // Do nothing
            break;
    }
    
    // If we haven't finished initializing yet, then don't go further.
    if (!_initialized) return;
    
    [self updateSettingsBundle];
    
    // -------
    // Determine what delegate methods need to get called (if any).
    // -------
    
    // If the session was previously started but now there is no user,
    // then the session has expired.
    if (_sessionStarted && !_authorization)
    {
        [self endSharedSessionWithError:[ASFError errorWithCode:ASFErrorSessionInvalid]];
    }
    // If the user was attempting to login, but now there IS a valid user
    // then we're done logging in!
    else if (loginViewController && _authorization)
    {
        [self startSessionAsynchronously];
    }
    // If the user is attempting to login (perhaps is as at the welcome screen),
    // but there is no valid session; revalidate the license and show the login view
    else if (loginViewController)
    {
        [self validateLicense];
    }
    // If the previous UPI does not match the current UPI,
    // then the user changed in the background
    else if (_sessionStarted && ![self.upi isEqualToString:previousUPI])
    {
        [self sessionDidChange];
    }
}

/// <devdoc>
/// Handles the response from ASFDevice regarding the status of the device configuration.
/// </devdoc>
- (void) onInitialDeviceConfigurationWithResult:(ASFDeviceConfigResult) result {
    _configuring = NO;
    NSError *error;
    switch (result)
    {
        // If device has the configuration profile installed, create the new session.
        case ASFDeviceConfigInstalled:
            [self showWelcomeMessage];
            return;
        // If the device configuration failed or the profile is not installed, do not create a new session.
        case ASFDeviceConfigFailed:
        case ASFDeviceConfigNotInstalled:
            error = [ASFError errorWithCode:ASFErrorConfigurationProfileMissing];
            break;
        case ASFDeviceConfigMissingLeaf:
            error = [ASFError errorWithCode:ASFErrorConfigurationLeafCertificateMissing];
            break;
        case ASFDeviceConfigKeyExpire:
            error = [ASFError errorWithCode:ASFErrorLicenseExpired];
            break;
        case ASFDeviceConfigLicenseInvalid:
            error = [ASFError errorWithCode:ASFErrorLicenseInvalid];
            break;
        case ASFDeviceConfigLicenseRevoked:
            error = [ASFError errorWithCode:ASFErrorLicenseRevoked];
            break;
        case ASFDeviceConfigServerError:
            error = [ASFError errorWithCode:ASFErrorConfigurationServerFailure];
            break;
        default:
            error = [ASFError errorWithCode:ASFErrorUnknown andLocalizedFailureReason:@"The login succeeded, but during the initial device configuration check, an undocumented failure occurred. If this error continues to occur, contact support to help troubleshoot the error."];
            break;
    }
    
    [self sessionFailedToStartWithError:error withNextAction:ASFActionDownloadConfiguration];
}

- (void) downloadConfiguration
{
    [loginViewController showMessageWithActivityIndicator:ASFKitLocalizedString(@"ASFStatusSecuringDevice")];
    _configuring = YES;
    
    // Sends a request to ASFKitDevice to attempt to configure the device;
    // the response gets sent to onInitialDeviceConfigurationWithResult:
    [ASFKitDevice configureDevice:^(ASFDeviceConfigResult result){
        [self onInitialDeviceConfigurationWithResult:result];
    }];
}

/// <devdoc>
/// Handles the authentication response received from the PING federate service regarding the user's status;
/// checks for errors or faults before attempting to parse and validate the data received.
/// Finally, it checks for the configuration profile and sends a request to download it if it doesn't exist.
/// </devdoc>
- (void) onAuthenticateResponse
{
    // Determine the amount of time it took to receive a response
    _timeToReceiveAuthenticationResponse = [[NSDate date] timeIntervalSinceDate:requestedAuthentication];
    requestedAuthentication = nil;
    
    // Log the amount of time it took to get a response
    [ASFLog message:[NSString stringWithFormat:@"It took %f seconds to receive a response from the authentication server.", self.timeToReceiveAuthenticationResponse]];
    
    [self verifyDeviceConfiguration];
}

- (void) verifyDeviceConfiguration
{
    switch ([ASFKitDevice verifyConfiguration])
    {
            // If the configuration profile is installed, then the session can safely be created.
        case ASFDeviceConfigInstalled:
            [self showWelcomeMessage];
            break;
            
            // If the leaf certificate is missing, create configuration endpoints and download the leaf certificate
            // The leaf certificate is installed, but the device needs to download the configuration profile
        default:
            [self showNeedsConfigurationMessage];
            break;
    }
}

/// <devdoc>
/// After the user has entered his/her username and password, this takes the values entered and passes it off
/// to the SOAP library to create an envelope and send it to AbbVie.
/// </devdoc>
- (void) doLoginWithUsername:(NSString *)username andPassword:(NSString *)password forDomain:(NSString *)domain
{
    // Let the library know we're making a login attempt
    _attemptingLogin = YES;
    
    // Log the time just before the request is created
    requestedAuthentication = [NSDate date];
    
    [loginViewController showMessageWithActivityIndicator:ASFKitLocalizedString(@"ASFStatusAuthenticatingUser")];
    [ASFUserAuthorizationProvider authenticateUser:username withPassword:password forDomain:domain delegate:self];
}

/// <devdoc>
/// Creates the login window and pops it up in a modal presentation view in the key window.
/// </devdoc>
- (void) showLoginWindow
{
    // Don't show the login window if it's already showing
    if (loginViewController)
    {
        return;
    }
    
    loginViewController = [ASFKitViewController new];
    loginViewController.rootViewController = rootViewController;
    loginViewController.delegate = self;
    
    [loginViewController presentLoginView];
    if ([self configurationURLIsOverridden])
    {
        [loginViewController showNotification:ASFKitLocalizedString(@"ASFPromptConfigurationURLOverride")];
    }
    else if (self.environment==ASFEnvironmentQA)
    {
        [loginViewController showNotification:ASFKitLocalizedString(@"ASFPromptQAEnvironment")];
    }
}

/// <devdoc>
/// Hides the login window and releases it from memory.
/// </devdoc>
- (void) hideLoginWindow
{
    if (loginViewController.loginViewShowing) 
    {
        [loginViewController dismissLoginView];
        loginViewController = nil;
    }
}

/**
 Checks the implementing application's license key with the licensing service.
 This will display a message on the login window and handle the response from the licensing service.
 
 If the license is invalid, all protected files will be removed an error will be displayed.
 If the license is valid, the status of the session will be checked.
    If there is an authorized user and the device is configured, then the welcome message will be shown.
    If there is no authorized user, the user will be asked for his/her credentials.
    If there is an authorized user, but the device is not configured, the user will be asked to configure the device.
 */
- (void) validateLicense
{
    if (_performingInitialLicenseVerify) return;
    
    [ASFLog message:@"Validating the application's license."];
    
    // The license must be validated before a user can log in.
    _licenseStatus = ASFLicenseStatusUnknown;
    
    _performingInitialLicenseVerify = YES;
    [loginViewController showMessageWithActivityIndicator:ASFKitLocalizedString(@"ASFStatusVerifyingLicense")];
    
    [ASFLicenseVerify verifyLicense:_licenseKey withCompletionHandler:^(ASFLicenseStatus status, NSError *error)
     {
         _licenseStatus = status;
         
         // If the liecnse is revoked/invalid,
         // remove all local files
         if (status==ASFLicenseStatusInvalid || status==ASFLicenseStatusRevoked)
         {
             [ASFLog message:@"The license is not valid. Removing all protected files."];
             [[ASFFileProtectionManager defaultManager] removeAllProtectedLocations];
         }
         
         if (status!=ASFLicenseStatusValid)
         {
             [self sessionFailedToStartWithError:error withNextAction:ASFActionValidateLicense];
         }
         else
         {
             if ([self sessionCanStart])
                 [self showWelcomeMessage];
             else if (!_authorization)
                 [loginViewController showLoginScreen];
             else if ([ASFKitDevice verifyConfiguration]!=ASFDeviceConfigInstalled)
                 [self showNeedsConfigurationMessage];
             
             // This should never happen
             else
                 [self sessionFailedToStartWithError:[ASFError errorWithCode:ASFErrorUnknown] withNextAction:ASFActionEnterCredentials];
         }
         
         _performingInitialLicenseVerify = NO;
     }];
}

- (void) setCurrentAuthorizationForSession:(ASFAuthorization *)authorization updatingKeychain:(BOOL)shouldUpdateKeychain
{
    // If authorization is nil, then we are clearing out the saved authorization
    // Otherwise, we needto verify that the authorization has an access token, refresh token,
    // and that the user profile has a upi
    if (authorization && ![authorization isValid])
    {
        return;
    }
    
    [self willChangeValueForKey:@"authorization"];
    if (authorization.isRefreshed && !self.configuration.refreshEnabled) {
        _authorization = nil;
    } else {
        _authorization = authorization;
    }
    [self didChangeValueForKey:@"authorization"];
    
    // Save the new authorization in the keychain
    if (shouldUpdateKeychain) {
        [ASFKeychain save:self.configuration.keychainServiceName data:authorization availableWhenLocked:NO];
        [ASFKeychain save:[ASFPublicKeychainPrefix stringByAppendingString:self.configuration.keychainServiceName]
                     data:[authorization publicAuthorization]
        availableWhenLocked:YES];
    
        // Check the keychain just to be sure that the authorization was successfully saved
        // If the access tokens do not match, then raise an exception.
        // An access token is unique to the current user, so this is a suitable check
        // and should only be useful during development
        ASFAuthorization *checkAuthorization = [ASFKeychain load:self.configuration.keychainServiceName];
        if (authorization && ![[checkAuthorization valueForKey:@"accessToken"] isEqualToString:authorization.accessToken]) {
            NSException *exception = [NSException exceptionWithName:ASFSessionKeychainException reason:ASFKitLocalizedString(@"The current user authorization failed to save to the keychain. Perhaps it was malformed or keychain sharing is not properly configured.") userInfo:nil];
            [exception raise];
        }
    }
}

/// <devdoc>
/// Invoked when the integrity check has finished running.
/// </devdoc>
- (void) onIntegrityCheckFinished:(NSNotification *)notification
{
    ASFIntegrityCheckResult *result = [notification.userInfo objectForKey:@"result"];
    
    if (result.failedChecks.count)
    {
        [ASFLog message:[NSString stringWithFormat:@"The following integrity checks failed:\n%@", result.failedChecks]];
    }
    else if (result.errors.count)
    {
        [ASFLog message:[NSString stringWithFormat:@"The integrity check passed, but the following warnings occurred:\n%@", result.errors]];
    }
    else
    {
        [ASFLog message:@"The integrity check finished with no issues."];
    }
    
    if (result.endingSharedSession)
    {
        [self endSharedSessionWithError:result.error];
    }
    else if (result.endingLocalSession)
    {
        [self endLocalSessionWithError:result.error];
    }
    
    if (result.deletingProtectedFiles)
    {
        [[ASFFileProtectionManager defaultManager] removeAllProtectedLocations];
    }
}

/**
 Shows a message to the user that their device needs to be configured.
 */
- (void) showNeedsConfigurationMessage
{
    NSString *displayName = _authorization.profile.displayName;
    NSAssert(displayName!=nil, @"Display name should not be nil!");
    NSString *message = [NSString stringWithFormat:ASFKitLocalizedString(@"ASFPromptNeedsConfiguration"),displayName];
    [loginViewController showMessage:message withButtonTitle:ASFKitLocalizedString(@"ASFContinue") andNextAction:ASFActionDownloadConfiguration];
}

/**
 Notifies the user that their authorization was succesful and their device is configured correctly.
 Their session is ready to begin!
 */
- (void) showWelcomeMessage
{
    NSString *displayName = _authorization.profile.displayName;
    NSAssert(displayName!=nil, @"Display name should not be nil!");
    NSString *message = [NSString stringWithFormat:ASFKitLocalizedString(@"ASFPromptWelcome"),displayName];
    [loginViewController showMessage:message withButtonTitle:ASFKitLocalizedString(@"ASFContinue") andNextAction:ASFActionStartSession];
    _attemptingLogin = NO;
}

/**
 Checks whether a session could be started.
 In order for a session to be started, there must be...
    1. An authorized user,
    2. A valid application license, and
    3. A secure device.
 
 @return YES if all three checks above succeed.
 */
- (BOOL) sessionCanStart
{
    return
        // Check that the user has been authorized,
        _authorization != nil
    
        // That the license is valid,
        && _licenseStatus == ASFLicenseStatusValid
    
        // And that the device has been configured
        && [ASFKitDevice verifyConfiguration] == ASFDeviceConfigInstalled;
}

#pragma mark - Session lifecycle
/**
 Starts a secure session and hands control back to the implementing application.
 */
- (void) createSession
{
    _attemptingLogin = _configuring = NO;
    [self setSessionStarted:YES];
    
    // Display the Activity Log consent prompt (if neccessary)
    [ASFActivityLogConsentManager displayConsentPromptIfNeeded];
    
    // Hide the login window if it's showing
    [self hideLoginWindow];
    
    // Make sure the current configuration is saved
    [_configuration saveConfiguration];
    
    // Start checking the integrity of the session
    [integrityCheckController startCheckInterval];
    
    [self updateSettingsBundle];
    
    // Send a notification to the NotificationCenter
    [[NSNotificationCenter defaultCenter] postNotificationName:ASFSessionStartedNotification object:self];
    
    // Fire a method to the delegate
    if ([_delegate respondsToSelector:@selector(sessionDidStart:)])
    {
        [_delegate sessionDidStart:self];
    }
    if ([_delegate respondsToSelector:@selector(sessionDidStart:withUserProfile:)])
    {
        [_delegate sessionDidStart:self withUserProfile:self.authorization.profile];
    }
    
    [ASFLog message:@"The session successfully started."];
}

/**
 Sends a notification to the notification center when the logged in user
 changed while the app was in the background.
 */
- (void)sessionDidChange
{
    [ASFLog message:@"The current user changed while the app was in the background."];
    [[NSNotificationCenter defaultCenter] postNotificationName:ASFSessionChangedNotification object:self userInfo:nil];
    
    // Display the Activity Log consent prompt (if neccessary)
    [ASFActivityLogConsentManager displayConsentPromptIfNeeded];
    
    if ([_delegate respondsToSelector:@selector(sessionDidChange:)])
    {
        [_delegate sessionDidChange:self];
    }
    if ([_delegate respondsToSelector:@selector(sessionDidChange:withUserProfile:)])
    {
        [_delegate sessionDidChange:self withUserProfile:self.authorization.profile];
    }
}

/**
 Performs the required tasks when a session fails to start;
 hides the login window (if showing) and sends the delegate/notifications.
 
 @param error   The error that was encountered that prevented the session from starting.
 @param action  The action to take when the user taps the "Try Again" button.
 */
- (void) sessionFailedToStartWithError:(NSError *)error withNextAction:(ASFSessionAction)action
{
    [self endLocalSessionWithError:error];
    [ASFLog message:[NSString stringWithFormat:@"The session failed to start. Error: %@", error]];
    
    NSDictionary *userInfo = nil;
    if (error)
    {
        userInfo = @{@"error":error};
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:ASFSessionFailedNotification object:self userInfo:userInfo];
    if ([_delegate respondsToSelector:@selector(sessionFailedToStartWithError:)])
    {
        [_delegate sessionFailedToStartWithError:error];
    }
    
    if (closesLoginOnFailure)
    {
        // Hide the login window (if it's showing)
        [self hideLoginWindow];
    }
    else
    {
        [loginViewController showError:error withNextAction:action];
    }
}

/**
 Ending the "local" session does not clear the data from the keychain,
 it just prevents the current application from using it.
 
 This calls endSessionWithError:removingKeychainData with the endSharedSession parameter set to NO.
 
 @param error   The reason the session is ending.
 */
- (void) endLocalSessionWithError:(NSError *)error
{
    [self endSessionWithError:error removingKeychainData:NO];
}

/**
 Removes all session data from the keychain and ends the current sessin.
 
 This calls endSessionWithError:removingKeychainData with the endSharedSession parameter set to YES.
 
 @param error   The reason the session is ending.
 */
- (void) endSharedSessionWithError:(NSError *)error
{
    [self endSessionWithError:error removingKeychainData:YES];
}

/**
 Sends a notification to the notification center when the user requested the session
 to end upon launching the application.
 */
- (void) endSessionOnLaunch
{
    [self endSession];
    [ASFLog message:@"Session ended on launch."];
    [[NSNotificationCenter defaultCenter] postNotificationName:ASFSessionEndedOnLaunchNotification object:self userInfo:nil];
    
    if ([_delegate respondsToSelector:@selector(sessionDidEndOnLaunch:)])
    {
        [_delegate sessionDidEndOnLaunch:self];
    }
}

/**
 Handles the ending of a session.
 It resets all the status flags and triggers notifications and invokes delegate methods.
 If an error is supplied, it is assumed that the session expired. 
 If there is no error, then it is assumed that the user ended the session by request.
 
 Do not call this method directly; only call the endLocalSessionWithError: and endSharedSessionWithError: methods.
 
 @param error               The error that is causing the session to end. If the session is ending for any reason
                            other than the user's request, an error should be supplied. In this case, the session is considered expired.
                            If no error is provided, it is assumed the session was ended by request.
 @param endSharedSession    If this is set to YES, the data in the keychain is removed.
 */
- (void) endSessionWithError:(NSError *)error removingKeychainData:(BOOL)endSharedSession
{
    // Only trigger the delegate methods and notifications if the session had been started.
    BOOL sessionHadBeenStarted = _sessionStarted;
    
    if (sessionHadBeenStarted)
    {
        if (error)
        {
            [ASFLog message:[NSString stringWithFormat:@"The session is ending because: %@", error.localizedDescription]];
            NSError *underlyingError = error.userInfo[NSUnderlyingErrorKey];
            if (underlyingError) {
                [ASFLog message:[NSString stringWithFormat:@"Underlying error: %@", underlyingError]];
            }
            
            // Session will expire
            NSDictionary *userInfo = @{@"error":error};
            [[NSNotificationCenter defaultCenter] postNotificationName:ASFSessionWillExpireNotification object:self userInfo:userInfo];
            if ([self.delegate respondsToSelector:@selector(sessionWillExpire:)])
            {
                [self.delegate sessionWillExpire:self];
            }
        }
        else
        {
            [ASFLog message:@"The session ended by request."];
            
            // Session will end
            [[NSNotificationCenter defaultCenter] postNotificationName:ASFSessionWillEndNotification object:self];
            if ([self.delegate respondsToSelector:@selector(sessionWillEnd:)])
            {
                [self.delegate sessionWillEnd:self];
            }
        }
    }
    
    // Reset the license status to unknown to force the framework
    // to re-check with the license server the status of the app.
    if (_licenseStatus==ASFLicenseStatusValid)
    {
        _licenseStatus = ASFLicenseStatusUnknown;
    }
    
    // Reset status flags
    _attemptingLogin = _configuring = NO;
    [self setSessionStarted:NO];
    
    // Stop the integrity check
    [integrityCheckController stopCheckInterval];
    
    if (endSharedSession)
    {
        [self setCurrentAuthorizationForSession:nil updatingKeychain:YES];
    }
    
    // Update the data in the settings bundle
    [self updateSettingsBundle]; // Must come AFTER setting sessionStarted to NO
    
    if (sessionHadBeenStarted)
    {
        if (error)
        {
            // Session did expire
            NSDictionary *userInfo = @{@"error":error};
            [[NSNotificationCenter defaultCenter] postNotificationName:ASFSessionExpiredNotification object:self userInfo:userInfo];
            if ([self.delegate respondsToSelector:@selector(sessionDidExpire:)])
            {
                [self.delegate sessionDidExpire:self];
            }
            // Open the login view
            if (showsLoginWhenSessionExpires && _initialized)
            {
                [self startSession];
            }
        }
        else
        {
            // Session did end
            [[NSNotificationCenter defaultCenter] postNotificationName:ASFSessionEndedNotification object:self];
            if ([self.delegate respondsToSelector:@selector(sessionDidEnd:)])
            {
                [self.delegate sessionDidEnd:self];
            }
            // Open the login view
            if (showsLoginWhenSessionEnds && _initialized)
            {
                [self startSession];
            }
        }
    }
}

# pragma mark - Accessors
- (void) setLicenseKey:(NSString *)aLicenseKey
{
    if (!_licenseKey)
    {
        _licenseKey = aLicenseKey;
        [integrityCheckController addIntegrityCheck:[ASFLicenseVerify licenseVerifierWithKey:_licenseKey]];
    }
    else
    {
        [NSException raise:NSInternalInconsistencyException format:@"The license key cannot be changed once it is set."];
    }
}

- (void) setConfiguration:(ASFConfiguration *)configuration
{
    if (configuration && configuration!=_configuration)
    {
        // The session needs to be reloaded if the keychain service name changes
        BOOL needToReloadSession = (_configuration && ![_configuration.keychainServiceName isEqualToString:configuration.keychainServiceName]) || _configuration.refreshEnabled != configuration.refreshEnabled;
        
        _configuration = configuration;
        [_configuration saveConfiguration];
        
        // Display the Activity Log consent prompt (if neccessary)
        [ASFActivityLogConsentManager displayConsentPromptIfNeeded];
        
        // If the keychain needs to be reloaded, cancel the current integrity check
        if (needToReloadSession) {
            [self reloadSession];
        }
    }
}

- (ASFAuthorization *) authorization
{
    if (!_sessionStarted)
        return nil;
    
    return _authorization;
}

- (BOOL) logging
{
    return [ASFLog isLoggingEnabled];
}

- (void) setLogging:(BOOL)logging
{
    return [ASFLog setLoggingEnabled:logging];
}

/**
 getSharedAuthorization should only be used internally.
 It will return the current authorization data regardless of the session status.
 The authorization parameter will only return the authorization data
 if the session has been started.
 */
- (ASFAuthorization *) getSharedAuthorization
{
    return _authorization;
}

- (void) setSessionStarted:(BOOL)sessionStarted
{
    [self willChangeValueForKey:@"authorization"];
    _sessionStarted = sessionStarted;
    [self didChangeValueForKey:@"authorization"];
}

- (NSDate *) expiresDate
{
    return self.authorization.expiration;
}

- (NSURL *) configurationProfileURL
{
    if (configurationProfileURL)
    {
        return configurationProfileURL;
    }
    else
    {
        return [NSURL URLWithString:DefaultServerURL];
    }
}

- (BOOL) configurationURLIsOverridden
{
    return ![[NSURL URLWithString:DefaultServerURL] isEqual:self.configurationProfileURL];
}

- (void) setLoginBackgroundView:(UIView *)loginBackgroundView
{
    // Create the login view
    if (!loginViewController)
    {
        loginViewController = [ASFKitViewController new];
    }
    loginViewController.backgroundView = loginBackgroundView;
}

- (UIView *) loginBackgroundView
{
    return loginViewController ? loginViewController.backgroundView : nil;
}

- (NSString *) getValueforXPath: (NSString *)xpath
{
    return nil;
}

// API
- (NSString *) getAdDomain
{
    return self.authorization.profile.adDomain;
}
- (NSString *) getAdLogon
{
    return self.authorization.profile.adLogon;
}
- (NSString *) getAudience
{
    return nil;
}
- (NSString *) getC
{
    return self.authorization.profile.c;
}
- (NSString *) getCompany
{
    return self.authorization.profile.company;
}
- (NSString *) getDepartment
{
    return self.authorization.profile.department;
}
- (NSString *) getDisplayName
{
    return self.authorization.profile.displayName;
}
- (NSString *) getDivision
{
    return self.authorization.profile.divisionName;
}
- (NSString *) getDivisionCode
{
    return self.authorization.profile.divisionCode;
}
- (NSString *) getDivisionName
{
    return self.authorization.profile.divisionName;
}
- (NSString *) getEmployeeType
{
    return self.authorization.profile.employeeType;
}
- (NSString *) getExpires
{
    return [self.authorization.expiration description];
}
- (NSString *) getFrameworkVersion
{
    // The framework version is defined in ASFKitConfig.h
    return ASFKitVersion;
}
- (NSString *) getFullName
{
    return self.authorization.profile.displayName;
}
- (NSString *) getGivenName
{
    return self.authorization.profile.givenName;
}
- (NSString *) getInitials
{
    return self.authorization.profile.initials;
}
- (NSString *) getLocation
{
    return self.authorization.profile.location;
}
- (NSString *) getMail
{
    return self.authorization.profile.mail;
}
- (NSString *) getManagerDisplayName
{
    return self.authorization.profile.managerDisplayName;
}
- (NSString *) getManagerEmail
{
    return self.authorization.profile.managerEmail;
}
- (NSString *) getManagerUPI
{
    return self.authorization.profile.managerUpi;
}
- (NSString *) getPreferredLanguage
{
    return self.authorization.profile.preferredLanguage;
}
- (NSString *) getSalesArea
{
    return self.authorization.profile.salesArea;
}
- (NSString *) getSalesCategoryID
{
    return self.authorization.profile.salesCategoryID;
}
- (NSString *) getSalesDistrict
{
    return self.authorization.profile.salesDistrict;
}
- (NSString *) getSalesForceName
{
    return self.authorization.profile.salesForceName;
}
- (NSString *) getSalesForceNumber
{
    return self.authorization.profile.salesForceNumber;
}
- (NSString *) getSalesFranchise
{
    return self.authorization.profile.salesFranchise;
}
- (NSString *) getSalesFranchiseID
{
    return self.authorization.profile.salesFranchiseID;
}
- (NSString *) getSalesPMI
{
    return self.authorization.profile.salesPMI;
}
- (NSString *) getSalesRegion
{
    return self.authorization.profile.salesRegion;
}
- (NSString *) getSalesSampling
{
    return self.authorization.profile.salesSampling;
}
- (NSString *) getSessionIndex
{
    return self.authorization.accessToken;
}
- (NSString *) getSn
{
    return self.authorization.profile.sn;
}
- (NSString *) getSpaceStructureID
{
    return self.authorization.profile.spaceStructureID;
}
- (NSString *) getSubjectNameID
{
    return nil;
}
- (NSString *) getTelephoneNumber
{
    return self.authorization.profile.telephoneNumber;
}
- (NSString *) getTerritory
{
    return self.authorization.profile.territory;
}
- (NSString *) getTitle
{
    return self.authorization.profile.title;
}
- (NSString *) getUpi
{
    return self.authorization.profile.upi;
}

# pragma mark - Public methods
+ (BOOL) canAccessURL:(NSURL *)url
{
    // If the user is logged in, then they have access to the URL
    if ([ASFSession sharedSession].sessionStarted)
    {
        return YES;
    }
    else
    {
        return [[[self sharedSession] whitelistedURLs] containsObject:url.host] && ![url.pathComponents containsObject:@"demo"];
    }
}
/// <devdoc>
/// Ends a session by the user's action (e.g.: hitting the logout button).
/// </devdoc>
- (void) endSession
{
    // Sends the sessionDidEnd event; which also handles destroying the current session.
    [self endSharedSessionWithError:nil];
}

- (BOOL) verifyConfigurationWithError:(NSError **)error
{
    ASFDeviceConfigResult result = [ASFKitDevice verifyConfiguration];
    
    if (result==ASFDeviceConfigInstalled)
    {
        return YES;
    }
    
    if (error)
    {
        NSInteger errorCode = 0;
        switch (result)
        {
            case ASFDeviceConfigMissingLeaf:
                errorCode = ASFErrorConfigurationLeafCertificateMissing;
                break;
            case ASFDeviceConfigNotInstalled:
                errorCode = ASFErrorConfigurationProfileMissing;
                break;
            default:
                errorCode = ASFErrorUnknown;
                break;
        }
        *error = [ASFError errorWithCode:errorCode];
    }
    
    return NO;
}

- (NSString *) getSAMLAttribute: (NSString *)attribute
{
    return [[self.authorization.profile dictionary] objectForKey:attribute];
}

- (void) startSession
{
    // Check for license key
    if (!_licenseKey)
    {
#if TARGET_IPHONE_SIMULATOR
        NSLog(@"WARNING: No license key has been defined for the application.");
#else
        [NSException raise:@"ASFSessionConfigurationException" format:@"The application must specify a license."];
#endif
    }
    
    // configurationProfileURL MUST be set
    if (!self.configurationProfileURL)
    {
        [NSException raise:@"Missing configuration profile URL" format:@"A URL must be specified where the configuration server can be found."];
    }
    
    // If there is already an active session, don't do anything.
    if (_sessionStarted)
    {
        // Check if this is an "extra" call to this message, and let the developer know
        if (self.startsSessionOnLaunch && self.showsLoginWhenSessionEnds && self.showsLoginWhenSessionExpires && !closesLoginOnFailure) {
            [ASFLog message:@"With your current application configuration, you do not ever need to call -startSession. You should remove all calls to -startSession."];
            NSLog(@"%@",[NSThread callStackSymbols]);
        } else {
            [ASFLog message:@"The session was already started."];
        }
        return;
    }
    
    // If there looks like there is a valid session in the keychain
    // the leaf certificate is available, AND we're not using the default configuration,
    // assume the license key is valid.
    // As soon as the session starts, it will be verified for sure.
    if (_authorization && [ASFKitDevice isLeafCertificateAvailable] && !self.configuration.isDefaultConfiguration)
    {
        _licenseStatus = ASFLicenseStatusValid;
    }
    
    // Check if there is an existing session
    if ([self sessionCanStart]) 
    {
        // If there is, fire the delegate/notification
        [self createSession];
        return;
    }
    
    // Otherwise, show the login window
    [self showLoginWindow];
    [self validateLicense];
}

- (void) startSessionAsynchronously
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self startSession];
    });
}

- (NSString *)description
{
    // Initialize the description string
    NSString *description = [NSString string];
    
    // Properties to return
    NSArray *properties = [NSArray arrayWithObjects: @"licenseKey", @"adDomain", @"adLogon", @"c", @"company", @"department", @"displayName", @"division", @"divisionCode", @"divisionName", @"employeeType", @"fullName", @"givenName", @"initials", @"location", @"mail", @"managerDisplayName", @"managerEmail", @"managerUPI", @"preferredLanguage", @"salesArea", @"salesCategoryID", @"salesDistrict", @"salesForceName", @"salesForceNumber", @"salesFranchise", @"salesFranchiseID", @"salesPMI", @"salesRegion", @"salesSampling", @"sn", @"spaceStructureID", @"telephoneNumber", @"territory", @"title", @"upi", @"sessionIndex", @"expires", @"subjectNameID", @"frameworkVersion", nil];
    
    // Generate a string of properties and their value
    for (NSString *property in properties) 
    {
        description = [description stringByAppendingFormat:@"%@: %@\n", property, [self valueForKey:property]];
    }
    
    return description;
}
+ (ASFSession *) startSessionUsingLicenseKey:(NSString *)licenseKey delegate:(id<ASFSessionDelegate>)delegate
{
    return [self startSessionUsingLicenseKey:licenseKey andEnvironment:ASFEnvironmentQA delegate:delegate];
}
+ (ASFSession *) startSessionUsingLicenseKey:(NSString *)licenseKey andEnvironment:(ASFEnvironment)environment delegate:(id<ASFSessionDelegate>)delegate
{
    return [self startSessionUsingLicenseKey:licenseKey andSecretKey:nil inEnvironment:environment delegate:delegate];
}
+ (ASFSession *) startSessionUsingLicenseKey:(NSString *)licenseKey andSecretKey:(NSString *)secretKey delegate:(id<ASFSessionDelegate>)delegate
{
    return [self startSessionUsingLicenseKey:licenseKey andSecretKey:secretKey inEnvironment:ASFEnvironmentQA delegate:delegate];
}
+ (ASFSession *) startSessionUsingLicenseKey:(NSString *)licenseKey andSecretKey:(NSString *)secretKey inEnvironment:(ASFEnvironment)environment delegate:(id<ASFSessionDelegate>)delegate
{
    ASFSession *session = [self configureUsingLicenseKey:licenseKey andSecretKey:secretKey inEnvironment:environment delegate:delegate];
    [session startSessionAsynchronously];
    return session;
}
+ (ASFSession *) configureUsingLicenseKey:(NSString *)licenseKey delegate:(id<ASFSessionDelegate>)delegate
{
    return [self configureUsingLicenseKey:licenseKey andEnvironment:ASFEnvironmentQA delegate:delegate];
}
+ (ASFSession *) configureUsingLicenseKey:(NSString *)licenseKey andEnvironment:(ASFEnvironment)environment delegate:(id<ASFSessionDelegate>)delegate
{
    return [self configureUsingLicenseKey:licenseKey andSecretKey:nil inEnvironment:environment delegate:delegate];
}
+ (ASFSession *) configureUsingLicenseKey:(NSString *)licenseKey andSecretKey:(NSString *)secretKey delegate:(id<ASFSessionDelegate>)delegate
{
    return [self configureUsingLicenseKey:licenseKey andSecretKey:secretKey inEnvironment:ASFEnvironmentQA delegate:delegate];
}
+ (ASFSession *) configureUsingLicenseKey:(NSString *)licenseKey andSecretKey:(NSString *)secretKey inEnvironment:(ASFEnvironment)environment delegate:(id<ASFSessionDelegate>)delegate
{
    ASFSession *session = [ASFSession sharedSession];
    session.environment = environment;
    session.delegate = delegate;
    session.licenseKey = licenseKey;
    session.secretKey = secretKey;
    session.startsSessionOnLaunch = NO;
    return session;
}

# pragma mark - UserAuthorizationProviderDelegate methods
- (void) authorizationDidSucceed:(ASFAuthorization *)authorization duration:(NSTimeInterval)duration
{
    _timeToVerifyCredentials = duration;
}
- (void) authorizationDidFailWithError:(NSError *)error
{
    _attemptingLogin = NO;
    [self sessionFailedToStartWithError:error withNextAction:ASFActionEnterCredentials];
    [ASFLog message:[NSString stringWithFormat:@"An error occurred while trying to communicate with AbbVie's server: %@", error]];
}
- (void) authorizationDidRefresh:(ASFAuthorization *)authorization duration:(NSTimeInterval)duration
{
    _timeToVerifyCredentials = duration;
}
- (void) profileRetrievalFromURL:(NSURL *)url didSucceed:(ASFAuthorization *)authorization duration:(NSTimeInterval)duration
{
    _attemptingLogin = NO;
    [self setCurrentAuthorizationForSession:authorization updatingKeychain:YES];
    _timeToRetrieveProfile = duration;
    // Verify the authorization saved successfully
    if (!_authorization)
    {
        ASFError *error = [ASFError errorWithCode:ASFErrorAuthorizationFailed];
        [self authorizationDidFailWithError:error];
    }
    else
    {
        [self onAuthenticateResponse];
    }
}
- (void) profileRetrievalFromURL:(NSURL *)url didFailWithError:(NSError *)error
{
    _attemptingLogin = NO;
    [self sessionFailedToStartWithError:error withNextAction:ASFActionEnterCredentials];
    [ASFLog message:[NSString stringWithFormat:@"An error occurred while trying to communicate with AbbVie's server: %@", error]];
}

#pragma mark - Login View Delegate Methods
- (void) userDidLoginWithUsername:(NSString *)username password:(NSString *)password domain:(NSString *)domain
{
    [self doLoginWithUsername:username andPassword:password forDomain:domain];
}
- (void) userDidTapButton:(id)sender expectingAction:(ASFSessionAction)action
{
    // Advance the status
    switch (action)
    {
        case ASFActionValidateLicense:
            [self validateLicense];
            break;
        case ASFActionEnterCredentials:
            [loginViewController showLoginScreen];
            break;
        case ASFActionDownloadConfiguration:
            [self downloadConfiguration];
            break;
        case ASFActionStartSession:
            [self createSession];
            break;
        default:
            NSAssert(true, @"This shouldn't have happened.");
            break;
    }
}

- (NSArray *) whitelistedURLs {
    static NSArray *whitelist = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        whitelist = @[@"mobileappserver.net",@"qa.mobileappserver.net",@"hockeyapp.net",@"smetrics.abbvie.com"];
    });
    
    // Always add the licensing, profile, and authentication servers to the whitelist
    return [whitelist arrayByAddingObjectsFromArray:@[self.configurationProfileURL.host, self.configuration.authenticationEndpoint.host, self.configuration.profileEndpoint.host]];
}

@end
