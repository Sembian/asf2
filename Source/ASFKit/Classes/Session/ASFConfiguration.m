//
//  SecurityFrameworkConfiguration.m
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFConfiguration.h"
#import "ASFUtilities_Private.h"
#import "NSString+ASFHash.h"
#import "ASFFileProtectionManager.h"

NSString * const ASFKitKeychainServicePrefix                          = @"ASFSession"; // Note: Changing this value will break compatibility with previous versions.

/**
 Array of names of properties to save in a separate, protected configuration file.
 */
static NSArray *protectedProperties;

@interface ASFConfiguration ()
/**
 YES if this ASFConfiguration represents the default configuration defined in ASFKitConfig.h; NO if it was received from the licensing server.
 */
@property (nonatomic, readwrite) BOOL isDefaultConfiguration;
@end

@implementation NSArray (ASFAdditions)

- (NSArray *) arrayByRemovingObjects:(NSArray *)objectsToRemove
{
    NSMutableArray *array = [self mutableCopy];
    [array removeObjectsInArray:objectsToRemove];
    return [NSArray arrayWithArray:array];
}

@end

@implementation ASFConfiguration

+ (void) load
{
    protectedProperties = @[@"clientid", @"clientSecret"];
}

- (id) init
{
    if (self = [super init])
    {
        _companyName = @"AbbVie"; // Default company name
    }
    return self;
}

- (BOOL) isEqual:(id)object
{
    return [self.dictionary isEqualToDictionary:((ASFConfiguration *)object).dictionary];
}

#pragma mark - Public Methods
+ (ASFConfiguration *) configurationWithData:(NSData *)data
{
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSDictionary *profileDict = nil;
    
    if (!dict)
        return nil;
    
    // The response from the licensing server includes the configuration under the "profile" key.
    if ([dict objectForKey:@"profile"])
        profileDict = [dict objectForKey:@"profile"];
    
    ASFConfiguration *config = [[ASFConfiguration alloc] initWithDictionary:profileDict];
    
    
    if ([dict objectForKey:@"Application"]) {
        // We can check if refresh is enabled
        config.refreshEnabled = [[[dict objectForKey:@"Application"] objectForKey:@"refresh_enabled"] boolValue];
    }
    
    return config;
}

+ (ASFConfiguration *) loadConfiguration
{
    // Load saved configuration
    NSMutableDictionary *savedConfiguration = [NSMutableDictionary dictionaryWithContentsOfFile:[self defaultConfigurationPathForProtectedData:NO]];
    [savedConfiguration addEntriesFromDictionary:[NSDictionary dictionaryWithContentsOfFile:[self defaultConfigurationPathForProtectedData:YES]]];
    
    NSDictionary *dict = savedConfiguration;
    BOOL isDefaultConfiguration = NO;
    
    if (!dict)
    {
        dict = ASFKitDefaultConfig;
        isDefaultConfiguration = YES;
    }
    
    ASFConfiguration *configuration = [[ASFConfiguration alloc] initWithDictionary:dict];
    
    if (isDefaultConfiguration)
    {
        [configuration saveConfiguration];
    }
    
    configuration.isDefaultConfiguration = isDefaultConfiguration;
    
    return configuration;
}

- (void) saveConfiguration
{
    // Save configuration information
    [self saveConfigurationForProtectedData:NO];
    
    // Save protected configuration information
    [self saveConfigurationForProtectedData:YES];
}

#pragma mark - Accessors
- (void) setAuthenticationEndpoint:(NSURL *)authenticationEndpoint
{
    _authenticationEndpoint = authenticationEndpoint;
    if (self.sessionSharingIdentifierSuffix) {
        _keychainServiceName = [[[self class] keychainServiceNameForAuthorizationURL:authenticationEndpoint] stringByAppendingString:self.sessionSharingIdentifierSuffix];
    } else {
        _keychainServiceName = [[self class] keychainServiceNameForAuthorizationURL:authenticationEndpoint];
    }
}

#pragma mark - Private Methods
/**
 Saves a configuration file for either the protected data or the unprotected data.
 @param isProtectedData YES to saved the protected data file, NO to save the unprotected data file.
 @since 2.1
 */
- (void) saveConfigurationForProtectedData:(BOOL)isProtectedData
{
    NSDictionary *dict = [self dictionaryForProtectedData:isProtectedData];
    NSString *path = [ASFConfiguration defaultConfigurationPathForProtectedData:isProtectedData];
    [[NSFileManager defaultManager] createDirectoryAtPath:[path stringByDeletingLastPathComponent] withIntermediateDirectories:YES attributes:nil error:nil];
    [dict writeToFile:path atomically:YES];
    NSString *Protection = isProtectedData ? NSFileProtectionComplete : NSFileProtectionCompleteUntilFirstUserAuthentication;
    NSDictionary *attr = @{NSFileProtectionKey:Protection};
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [ASFFileProtectionManager stopProtectingLocation:path];
    });
    
    [[NSFileManager defaultManager] setAttributes:attr ofItemAtPath:path error:nil];
}

/**
 Prepares the information to store in the configuration file.
 @param isProtectedData YES to return the dictionary of protected data (derived from protectedProperties); NO to return the dictionary of all other properties.
 @return                The NSDictionary of key-value pairs to save to a plist.
 @since 2.1
 */
- (NSDictionary *) dictionaryForProtectedData:(BOOL)isProtectedData
{
    if (isProtectedData) {
        NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
        for (NSString *property in protectedProperties) {
            if ([self valueForKey:property]) {
                dictionary[property] = [self valueForKey:property];
            } else {
                dictionary[property] = [NSNull null];
            }
        }
        return [NSDictionary dictionaryWithDictionary:dictionary];
    } else {
        NSDictionary *dictionary = [self dictionary];
        dictionary = [dictionary dictionaryWithValuesForKeys:[dictionary.allKeys arrayByRemovingObjects:protectedProperties]];
        return dictionary;
    }
}

/**
 The file path for the configuration file.
 Uses the value of ConfigurationFileName (defined in ASFKitConfig), optionally prefixed by "protected_" if isProtectedData is YES.
 @param isProtectedData Whether to prefix the filename with "protected_"
 @return                An NSString containing the file path for the configuration file.
 @since 2.1
 */
+ (NSString *) defaultConfigurationPathForProtectedData:(BOOL)isProtectedData
{
    NSString *prefix = @"";
    if (isProtectedData) {
        prefix = @"protected_";
    }
    
    NSString *filename = [prefix stringByAppendingString:ConfigurationFileName];
    return [[ASFUtilities frameworkDirectory] stringByAppendingPathComponent:filename];
}

/**
 Provides the keychain service name derived from the authorization URL.
 The derived name is determined by taking a common prefix (defined by ASFKitKeychainServicePrefix) and appending an MD5 hash of the authorization URL.
 @param authorizationURL    The URL used to send the user's credentials (username, password, and domain).
 @return                    NSString containing the derived keychain service name.
 */
+ (NSString *) keychainServiceNameForAuthorizationURL:(NSURL *)authorizationURL
{
    NSString *hash = [authorizationURL.absoluteString md5Hash];
    return [ASFKitKeychainServicePrefix stringByAppendingString:hash];
}

@end
