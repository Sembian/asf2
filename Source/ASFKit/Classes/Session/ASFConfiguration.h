//
//  SecurityFrameworkConfiguration.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "SerializableNSObject.h"

/**
 ASFConfiguration provides the runtime configuration information for ASFKit. 
 The configuration information it holds is provided by an application profile on the licensing server or is derived from an application profile.
 
 The configuration information is only available to the framework internally and not to the implementing application. The framework has some defaults defined in ASFKitConfig.h, but all of those default are overridden as soon as a response is received from the licensing server. Configuration information is stored in two plist files--one protected, and one unprotected. Most of the configuration information is available in the unprotected file so that the framework has enough configuration in order to run in the background (e.g. in response to a background fetch).
 
 The framework should reference the current configuration through [ASFSession sharedSession].configuration, or through the macro ASFKitConfiguration.
 
 @since 2.0
 */
@interface ASFConfiguration : SerializableNSObject

/// The image name of the logo to display on the login view and any connected display during authentication.
@property (nonatomic, strong) NSString *branding;
/// The display name of the company.
@property (nonatomic, strong) NSString *companyName;
/// The URI to use to authenticate users and verify their credentials.
@property (nonatomic, strong) NSURL *authenticationEndpoint;
/// The client id to send to the authentication endpoint.
@property (nonatomic, strong) NSString *clientid;
/// The client id's corresponding secret key.
@property (nonatomic, strong) NSString *clientSecret;
/// The URI from which to request user profile details.
@property (nonatomic, strong) NSURL *profileEndpoint;
/// A comma-separatd list of domain names.
@property (nonatomic, strong) NSArray *domainList;
/// The number of minutes between each integrity check on the application while it is running with an active user.
@property (nonatomic, strong) NSNumber *integrityCheckInterval;
/// The number of temporary failures that can occur during an integrity check before the session is terminated.
@property (nonatomic, strong) NSNumber *maxOfflineFailures;
/// The number of seconds to wait before considering a request timed out. Requests can override this value.
@property (nonatomic, strong) NSNumber *defaultTimeoutInterval;
/// The text to be displayed as the legal notice on the login view.
@property (nonatomic, strong) NSString *legalNotice;
/// Whether to prevent iOS from taking snapshots of the application state.
@property (nonatomic) BOOL snapshotProtection;
/// Whether to allow implementing applications to create activity log statements.
@property (nonatomic, readwrite) BOOL activityLogApi;
/// The privacy notice text to show to users on their first log in to applications with their activity log enabled.
@property (nonatomic, strong) NSString *activityLogPrivacyNotice;
/// Allow the application to use a refreshed user authorization.
@property (nonatomic) BOOL refreshEnabled;

/// YES if the configuration has NOT been loaded from the licensing server and is using the hard-coded fallback values.
@property (nonatomic, readonly) BOOL isDefaultConfiguration;

/// The keychain service name is used to identify the shared login.
/// This uses a hash of a normalized version of the authentication endpoint prefixed by a static value to prevent conflicts between applications using different authentication endpoints.
@property (nonatomic, readonly) NSString *keychainServiceName;

/// Applications that use the same authentication server will, by default, share user sessions.
/// The session sharing identifier allows an application (or a "group" of applications) to opt-out of sharing the user session (even when using the same authentication server).
/// Under most circumstances, this field should be empty.
@property (nonatomic, strong) NSString *sessionSharingIdentifierSuffix;

/// Creates an ASFConfiguration instance from the specified JSON data.
/// The format of this data should be as supplied from the licensing server.
/// @param data     NSData representing the JSON response from the licensing server.
/// @return         Instantiated ASFConfiguration object representing the JSON data supplied.
+ (ASFConfiguration *) configurationWithData:(NSData *)data;

/// Loads ASFConfiguration from two backing files--default.plist and protected_default.plist.
/// @return The instantiated ASFConfiguration with it's properties set to the values defined in the configuration files.
+ (ASFConfiguration *) loadConfiguration;

/// Saves the current configuration to disk so that it can be reloaded the next time the framework instantiates.
/// This actually creates two files--one with protected data and the other with unprotected data.
- (void) saveConfiguration;

@end
