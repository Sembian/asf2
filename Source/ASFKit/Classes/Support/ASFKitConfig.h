//
//  ASFKitConfig.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

/// <devdoc>
/// Configuration options for ASFKit.
/// these can be changed to modify the functionality of the library,
/// but the entire library must be recompiled before the updated settings are reflected.
/// </devdoc>

// The framework version
#define ASFKitVersion               @"2.1.3"

#define ASFKitDefaultConfig         @{\
                                        @"branding"                 : @"abbvie", \
                                        @"company_name"             : @"AbbVie", \
                                        @"authentication_endpoint"  : @"https://federation.abbott.com/as/token.oauth2",\
                                        @"clientid"                 : @"ppd-digital-learning",\
                                        @"client_secret"            : @"HX44NvfQ1NMeKpJfDRch",\
                                        @"profile_endpoint"         : @"https://idp.abbott.com/upi/profile/",\
                                        @"domain_list"              : @[@"oneabbott", @"international", @"abbvienet"], \
                                        @"integrity_check_interval" : @"30",\
                                        @"max_offline_failures"     : @"1000",\
                                        @"default_timeout_interval" : @"30",\
                                        @"legal_notice"             : @"Only authorized AbbVie personnel are allowed to access this system. By accessing this system you are consenting to the system monitoring for law enforcement and other purposes. This system contains confidential AbbVie intellectual property and information. Unauthorized access may subject you to criminal prosecution and/or AbbVie disciplinary action."\
                                    }

#define ASFKitConfiguration         [ASFSession sharedSession].configuration

// The name of the leaf certificate to download from the server.
#define LeafCertificateName         @"leaf.cer"
#define FrameworkID                 @"com.abbvie.asfkit"
#define ConfigurationFileName       @"default.plist"

// The name of the key in NSUserDefaults to set the version number
// This is to display the version number in the settings bundle for the application
#define FrameworkVersionKey         @"ASFKitVersion"
#define UserKey                     @"ASFKitUser"
#define DomainKey                   @"ASFKitDomain"
#define ResetConsentSwitchKey       @"ASFKitResetConsent"
#define LogoutSwitchKey             @"ASFKitDoLogout"
#define AppVersionKey               @"appVersion"
#define ActivityLogStatusKey        @"ASFKitActivityLogStatus"

// Activity Log configuration
#define AttachmentMaxSizeBytes      1 * 1024 * 1024 // 1mb

// Licensing server information
#define ConfigurationURL_LIVE       @"https://mobileappserver.net/licensing"
#define ConfigurationURL_QA         @"https://qa.mobileappserver.net/licensing"

#define DefaultServerURL            [ASFSession sharedSession].environment ? ConfigurationURL_LIVE : ConfigurationURL_QA
#define ConfigurationRequestURI     @"configuration/request"
#define VerifyLicenseURI            @"application/verify"
#define ActivityLogURI              @"activity_log"
#define ProfileGroupsURI            @"profile_groups"