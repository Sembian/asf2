//
//  FLOAuthUtil.h
//
//  Copyright (c) 2010-2014 Float Mobile Learning. All rights reserved.
//  Included by permission in ASFKit.
//

#import <Foundation/Foundation.h>

NSString *ASFOAuthHeader(NSURL *URL, NSString *method, NSData *body, NSString *clientKey, NSString *clientSecret);
NSString *ASFOAuthHeaderWithToken(NSURL *URL, NSString *method, NSData *body, NSString *clientKey, NSString *clientSecret, NSString *tokenKey, NSString *tokenSecret);