//
//  FLOAuthUtil.m
//
//  Copyright (c) 2010-2014 Float Mobile Learning. All rights reserved.
//  Included by permission in ASFKit.
//

#import "FLOAuthUtil.h"
#import <CommonCrypto/CommonHMAC.h>

@implementation NSString (PercentEncoding)
- (NSString *) percentEncoded
{
    return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                 NULL,
                                                                                 (CFStringRef)self,
                                                                                 NULL,
                                                                                 (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                 kCFStringEncodingUTF8 ));
}
@end

@implementation NSURL (QueryUtils)
- (NSDictionary *) queryDictionary
{
    NSArray *queryComponents = [self.query componentsSeparatedByString:@"&"];
    NSMutableDictionary *queryDictionary = [NSMutableDictionary dictionary];
    for (NSString *component in queryComponents)
    {
        NSArray *keyValue = [component componentsSeparatedByString:@"="];
        if (keyValue.count == 2) {
            queryDictionary[keyValue[0]] = keyValue[1];
        }
    }
    return [NSDictionary dictionaryWithDictionary:queryDictionary];
}
- (NSString *) absoluteStringWithoutQuery
{
    // Determine the aboslute string
    NSString *absoluteString = self.absoluteString;
    // Find the location of the question mark (?) indicating the start of the query string
    NSUInteger i = [absoluteString rangeOfString:@"?"].location;
    // Return the URL leading up to the query
    return [absoluteString substringToIndex:MIN(i,absoluteString.length)];
}

@end

static char nonceCharacters[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
NSString *nonce(NSUInteger length) {
    NSString *nonce = @"";
    while (nonce.length<length)
    {
        int loc = arc4random() % sizeof(nonceCharacters);
        nonce = [nonce stringByAppendingFormat:@"%c", nonceCharacters[loc]];
    }
    return nonce;
}
NSString *oauth_timestamp() {
    return [NSString stringWithFormat:@"%.0f", (float)[[NSDate date] timeIntervalSince1970]];
}

NSString *ASFOAuthHeader(NSURL *URL, NSString *method, NSData *body, NSString *clientKey, NSString *clientSecret)
{
    return ASFOAuthHeaderWithToken(URL, method, body, clientKey, clientSecret, nil, nil);
}

NSString *ASFOAuthHeaderWithToken(NSURL *URL, NSString *method, NSData *body, NSString *clientKey, NSString *clientSecret, NSString *tokenKey, NSString *tokenSecret)
{
    // TODO: Assert!
//    NSAssert(clientKey.length>0, @"Must specify client key.");
    
    // OAuth base parameters
    NSMutableDictionary *oAuthParams = [@{
                                  @"oauth_signature_method"    :   @"HMAC-SHA256",
                                  @"oauth_version"             :   @"1.0",
                                  @"oauth_nonce"               :   nonce(10),
                                  @"oauth_timestamp"           :   oauth_timestamp(),
                                  @"oauth_consumer_key"        :   clientKey
                                  } mutableCopy];
    
    // Add the token key (if specified)
    if (tokenKey.length)
    {
        oAuthParams[@"oauth_token"] = tokenKey;
    }
    
    //
    NSString *paramaterString = @"";
    
    NSMutableDictionary *paramsToSign = [oAuthParams mutableCopy];
    [paramsToSign addEntriesFromDictionary:[URL queryDictionary]];
    
    NSArray *keys = [paramsToSign.allKeys sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
    NSMutableArray *params = [NSMutableArray array];
    for (NSString *key in keys)
    {
        NSString *value = [[paramsToSign valueForKey:key] percentEncoded];
        [params addObject:[NSString stringWithFormat:@"%@=%@",key,value]];
    }
    paramaterString = [params componentsJoinedByString:@"&"];
    
    // Create our signature base
    NSString *signatureBase = [NSString stringWithFormat:@"%@&%@&%@",
                               [method uppercaseString],
                               [[URL absoluteStringWithoutQuery] percentEncoded],
                               [paramaterString percentEncoded]
                               ];
    
    // Create our signing key
    NSString *signingKey = [NSString stringWithFormat:@"%@&",
                            [clientSecret percentEncoded]];
    
    if (tokenSecret)
    {
        signingKey = [signingKey stringByAppendingString:[tokenSecret percentEncoded]];
    }
    
    // Sign the signature with the signing key
    const char *cKey  = [signingKey cStringUsingEncoding:NSASCIIStringEncoding];
    const char *cData = [signatureBase cStringUsingEncoding:NSASCIIStringEncoding];
    
    unsigned char cHMAC[CC_SHA256_DIGEST_LENGTH];
    
    CCHmac(kCCHmacAlgSHA256, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
    
    NSData *HMAC = [[NSData alloc] initWithBytes:cHMAC
                                          length:sizeof(cHMAC)];
    
    NSString *signature = [[HMAC base64Encoding] percentEncoded];
    
    [oAuthParams setObject:signature forKey:@"oauth_signature"];
    
    // Covert the args to a string
    NSMutableArray *a = [NSMutableArray array];
    for (NSString *key in oAuthParams.allKeys)
    {
        [a addObject:[NSString stringWithFormat:@"%@=\"%@\"",key, [oAuthParams valueForKey:key]]];
    }
    NSString *authString = [a componentsJoinedByString:@" "];
    return [@"OAuth " stringByAppendingString:authString];
}