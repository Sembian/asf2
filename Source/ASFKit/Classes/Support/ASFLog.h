//
//  ASFLog.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import <Foundation/Foundation.h>


/// 
/// Handles the logging of events throughout the framework; if logging is enabled, then the messages
/// appear in the debugger log--otherwise the messages don't appear at all.
/// 
@interface ASFLog : NSObject {
    
}

+ (void) setLoggingEnabled:(BOOL)loggingEnabled;
+ (BOOL) isLoggingEnabled;

+ (void) message: (NSString *)message;

@end
