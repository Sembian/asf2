//
//  SerializableNSObject.m
//
//  Copyright (c) 2010-2014 Float Mobile Learning. All rights reserved.
//  Included by permission in ASFKit.
//

#import "SerializableNSObject.h"
#import <objc/runtime.h>
#import "NSObject+PropertyClass.h"
#import "NSDate+InternetTimestamps.h"

NSString* const kObjectType     =   @"objectType";

@implementation SerializableNSObject

- (void) setSerializedValue:(id)value forProperty:(NSString *)key
{
    NSDictionary *mapping = [self propertyMapping];
    id target = [mapping objectForKey:key];
    
    if ([value isKindOfClass:[NSNull class]])
    {
        // Do nothing
        return;
    }
    else if ([target isKindOfClass:[NSString class]])
    {
        key = target;
    }
    else if ([target respondsToSelector:@selector(objectFromDictionary:)])
    {
        value = [target objectFromDictionary:value];
    }
    
    // Attempt to get the class for the property type by name
    Class class = [self classForPropertyName:key];
    if (class && [class respondsToSelector:@selector(objectFromDictionary:)])
    {
        value = [class objectFromDictionary:value];
    }
    else if (class == [NSDate class])
    {
        if ([value isKindOfClass:[NSString class]] && [value rangeOfString:@"Z"].location != NSNotFound) {
            value = [NSDate dateFromRFC3339Timestamp:value];
        } else {
            value = [NSDate dateWithTimeIntervalSince1970:[value doubleValue]];
        }
    }
    else if (class == [NSURL class] && [value isKindOfClass:[NSString class]])
    {
        value = [NSURL URLWithString:value];
    }
    else if (class == [NSData class])
    {
        value = [[NSData alloc] initWithBase64Encoding:value];
    }
    
    [self setValue:value forKey:key];
}

- (void) setValue:(id)value forUndefinedKey:(NSString *)key
{
    // Try to convert variable_name to variableName
    if ([key rangeOfString:@"_"].location!=NSNotFound)
    {
        NSRange r = [key rangeOfString:@"_"];
        while (r.location!=NSNotFound)
        {
            // Capitalize the character after and remove the underscore
            NSString *c = [[key substringWithRange:(NSRange){r.location+1,1}] uppercaseString];
            r.length++;
            key = [key stringByReplacingCharactersInRange:r withString:c];
            
            // Next
            r = [key rangeOfString:@"_"];
        }
        [self setSerializedValue:value forProperty:key];
    }
}

- (NSDictionary *) propertyMapping
{
    return [NSDictionary dictionary];
}

- (id) initWithDictionary:(NSDictionary *)dict
{
    self = [self init];
    
    // Iterate through the dictionary values and attempt to set the right property value
    [dict enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop){
        [self setSerializedValue:obj forProperty:key];
    }];
    
    return self;
}

+ (NSDictionary *) objectMappings
{
    return [NSDictionary dictionary];
}

+ (id) objectFromDictionary:(NSDictionary *)dict
{
    // Check for the objectType key
    // If it exists, return the specific object type
    // this dictionary represents
    if ([dict objectForKey:kObjectType])
    {
        Class targetClass = [[self objectMappings] objectForKey:[dict objectForKey:kObjectType]];
        if (targetClass && [self class]!=targetClass && ![self isSubclassOfClass:targetClass])
        {
            return [targetClass objectFromDictionary:dict];
        }
    }
    
    // Subclasses can use this to determine which subclsas (e.g. agent or person) and allocate the right type
    return [[self alloc] initWithDictionary:dict];
}

- (NSArray *) classProperties
{
    NSMutableArray *retval = [NSMutableArray array];
    unsigned int outCount, i;
    Class objClass = [self class];
    
    while (objClass && objClass!=[NSObject class])
    {
        objc_property_t *properties = class_copyPropertyList(objClass, &outCount);
        for (i = 0; i < outCount; i++)
        {
            NSString *propertyName = [NSString stringWithCString:property_getName(properties[i]) encoding:NSUTF8StringEncoding];
            [retval addObject:propertyName];
        }
        free(properties);
        objClass = [objClass superclass];
    }
    
    return retval;
}

- (id) serializedValueForKey:(NSString *)key
{
    id value = [self valueForKey:key];
    
    if ([value isKindOfClass:[SerializableNSObject class]])
    {
        value = [value dictionary];
    }
    else if ([value isKindOfClass:[NSDate class]])
    {
        value = [(NSDate *)value RFC3339Timestamp];
    }
    else if ([value isKindOfClass:[NSArray class]])
    {
        if ([value count]==0) return nil;
    }
    else if ([value isKindOfClass:[NSURL class]])
    {
        value = [(NSURL *)value absoluteString];
    }
    else if ([value isKindOfClass:[NSData class]])
    {
        value = [(NSData *)value base64Encoding];
    }
    
    return value;
}

- (NSDictionary *) dictionary
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    for (NSString *property in self.classProperties)
    {
        NSDictionary *propMapping = [self propertyMapping];
        NSArray *possibleKeys = [propMapping allKeysForObject:property];
        NSString *key = possibleKeys.count ? [possibleKeys objectAtIndex:0] : property;
        
        [dict setValue:[self serializedValueForKey:property] forKey:key];
    }
    
    return [NSDictionary dictionaryWithDictionary:dict];
}

- (NSString *) JSONString
{
    return [[NSString alloc] initWithData:[self JSONData] encoding:NSUTF8StringEncoding];
}

- (NSString *) prettyJSONString
{
    NSDictionary *dictionaryRepresentation = [self dictionary];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionaryRepresentation options:NSJSONWritingPrettyPrinted error:NULL];
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}

- (NSData *) JSONData
{
    NSDictionary *dictionaryRepresentation = [self dictionary];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionaryRepresentation options:0 error:NULL];
    return jsonData;
}

- (id) initWithCoder:(NSCoder *)aDecoder
{
    NSDictionary *dict = [aDecoder decodeObjectForKey:@"dictionary"];
    if (self = [self initWithDictionary:dict]) {
        
    }
    return self;
}

- (void) encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:[self dictionary] forKey:@"dictionary"];
}

@end
