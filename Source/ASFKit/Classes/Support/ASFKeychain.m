//
//  ASFKeychain.m
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFKeychain.h"
#import "ASFLog.h"


@implementation ASFKeychain

+ (NSMutableDictionary *)getKeychainQuery:(NSString *)service 
{
    // Using a "generic password" keychain type to simply store raw data
    // Using the service as the service and account name
    return [NSMutableDictionary dictionaryWithObjectsAndKeys:
            (__bridge id)kSecClassGenericPassword, (__bridge id)kSecClass,
            service, (__bridge id)kSecAttrService,
            service, (__bridge id)kSecAttrAccount,
            nil];
}

+ (NSMutableDictionary *)getKeychainQuery:(NSString *)service withAccessGroup:(NSString *)accessGroup
{
    NSMutableDictionary *query = [ASFKeychain getKeychainQuery:service];
    if (accessGroup) 
    {
        [query setObject:accessGroup forKey:(__bridge id)kSecAttrAccessGroup];
    }
    return query;
}

+ (void)save:(NSString *)service data:(id)data availableWhenLocked:(BOOL)availableWhenLocked
{
    [ASFKeychain save:service data:data accessGroup:nil availableWhenLocked:availableWhenLocked];
}

+ (void)save:(NSString *)service data:(id)data accessGroup:(NSString *)accessGroup availableWhenLocked:(BOOL)availableWhenLocked
{
    NSMutableDictionary *keychainQuery = [self getKeychainQuery:service withAccessGroup:accessGroup];
    
    if (availableWhenLocked) {
        keychainQuery[(__bridge id)kSecAttrAccessible] = (__bridge id)kSecAttrAccessibleAfterFirstUnlockThisDeviceOnly;
    } else {
        keychainQuery[(__bridge id)kSecAttrAccessible] = (__bridge id)kSecAttrAccessibleWhenUnlockedThisDeviceOnly;
    }
    
    // Remove anything currently existing in the keychain matching the service
    SecItemDelete((__bridge CFDictionaryRef)keychainQuery);
    
    // Add the suppled data to the keychain query
    [keychainQuery setObject:[NSKeyedArchiver archivedDataWithRootObject:data] forKey:(__bridge id)kSecValueData];
    
    // Add the data to the keychain
    SecItemAdd((__bridge CFDictionaryRef)keychainQuery, NULL);
}

+ (id)load:(NSString *)service
{
    return [ASFKeychain load:service accessGroup:nil];
}

+ (id)load:(NSString *)service accessGroup:(NSString *)accessGroup 
{
    // Initialize the return value
    id ret = nil;
    
    // initialize a keychain query
    NSMutableDictionary *keychainQuery = [self getKeychainQuery:service withAccessGroup:accessGroup];
    
    // Specify search parameters (return the data of the first matched keychain item)
    [keychainQuery setObject:(id)kCFBooleanTrue forKey:(__bridge id)kSecReturnData];
    [keychainQuery setObject:(__bridge id)kSecMatchLimitOne forKey:(__bridge id)kSecMatchLimit];
    
    // Retrieve the data from the keychain item
    CFDataRef keyData = NULL;
    if (SecItemCopyMatching((__bridge CFDictionaryRef)keychainQuery, (CFTypeRef *)&keyData) == noErr)
    {
        // Try to unarchive the data from the keychain
        @try 
        {
            ret = [NSKeyedUnarchiver unarchiveObjectWithData:(__bridge NSData *)keyData];
        }
        @catch (NSException *e) 
        {
            [ASFLog message:[NSString stringWithFormat:@"Unarchive of %@ failed: %@", service, e]];
        }
    }
    if (keyData) 
    {
        CFRelease(keyData);
    }
    
    return ret;
}

+ (void)delete:(NSString *)service
{
    [ASFKeychain delete:service accessGroup:nil];
}

+ (void)delete:(NSString *)service accessGroup:(NSString *)accessGroup 
{
    NSMutableDictionary *keychainQuery = [self getKeychainQuery:service withAccessGroup:accessGroup];
    SecItemDelete((__bridge CFDictionaryRef)keychainQuery);
}

@end
