//
//  NSDate+InternetTimestamps.h
//  ASFKit
//
//  Copyright (c) 2013 Float Mobile Learning. All rights reserved.
//  http://floatlearning.com/
//

#import <Foundation/Foundation.h>

/**
 RFC 3339 http://tools.ietf.org/html/rfc3339
 Adds support for the Internet timestamp specification defined by
 The Internet Society.
 It follows the Internet profile of the ISO 8601 [ISO8601]
 standard for representation of dates and times using the Gregorian
 calendar.
 */
@interface NSDate (ASFInternetTimestamps)

+ (NSDate *) dateFromRFC3339Timestamp:(NSString *)timestamp;
- (NSString *) RFC3339Timestamp;

@end
