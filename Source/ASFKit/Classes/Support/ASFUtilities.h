//
//  ASFUtilities.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import <Foundation/Foundation.h>

/// Provides a useful set of functionality to the library and to the implementing app.
@interface ASFUtilities : NSObject

/// 
/// Loops through the device's interfaces to find the IP address that the device is currently using.
/// 
/// @return Returns the current IP address as an NSString*.
+ (NSString *) getIPAddress;

/// 
/// Determines the documents directory for the current application.
/// 
/// @return The absolute path to the documents directory for the application.
+ (NSString *) getDocumentsDirectory;

/// 
/// Returns a path to the application's caches directory.
/// Data that can be redownloaded or regenerated by your application should be stored in the caches directory.
/// 
/// @return A string containing the absolute path (suitable for appending file names and directories to) for writing data to the application's sandbox.
/// @see https://developer.apple.com/icloud/documentation/data-storage/
+ (NSString *) cachesDirectory;

@end
