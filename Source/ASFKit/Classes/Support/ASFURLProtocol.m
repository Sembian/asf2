//
//  ASFURLProtocol.m
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFURLProtocol.h"
#import "ASFLog.h"

@interface ASFURLProtocol () <NSURLConnectionDelegate>

@property (nonatomic, strong) NSURLConnection *connection;
@property (nonatomic, strong) NSMutableData *mutableData;
@property (nonatomic, strong) NSURLResponse *response;

@end

NSString* const ASFURLProtocolHandledKey = @"ASFURLProtocolHandledKey";

@implementation ASFURLProtocol

- (void) dealloc
{
    [self removeObservers];
}

+ (BOOL)canInitWithRequest:(NSURLRequest *)request {
    if ([NSURLProtocol propertyForKey:ASFURLProtocolHandledKey inRequest:request]) {
        return NO;
    }
    
    // Do not track file: (local) requests
    if ([request.URL.scheme isEqualToString:@"file"]) {
        return NO;
    }
    
    // Verify the scheme
    if(!([request.URL.scheme isEqualToString:@"http"] || [request.URL.scheme isEqualToString:@"https"])) {
        if ([ASFSession sharedSession].environment == ASFEnvironmentQA) {
            NSLog(@"Application is requesting a URL from a non-standard scheme: %@. This request will NOT be monitored by the security framework.", request.URL.scheme);
        }
        return NO;
    }
    
    return YES;
}

+ (NSURLRequest *) canonicalRequestForRequest:(NSURLRequest *)request {
    return request;
}

- (void) startLoading {
    NSMutableURLRequest *request = [self.request mutableCopy];
    [NSURLProtocol setProperty:@YES forKey:ASFURLProtocolHandledKey inRequest:request];
    
    // Disallow all requests made if there is no session
    if (![ASFSession canAccessURL:request.URL]) {
        NSError *error = [ASFError errorWithCode:ASFErrorSessionInvalid];
        [self failRequestWithError:error];
        return;
    }
    
    // Verify that this is a secure request
    if (![request.URL.scheme isEqualToString:@"https"] && ![ASFSession sharedSession].shouldAllowInsecureNetworkRequests) {
        // Explicitly allow insecure requests to any of Apple's servers
        BOOL allowInsecureConnection = [request.URL.host rangeOfString:@"apple.com"].location != NSNotFound;
        
        if (!allowInsecureConnection) {
            if (![self isDevelopmentEnvironment]) {
                NSError *error = [ASFError errorWithCode:ASFErrorInsecureRequest];
                [self failRequestWithError:error];
                return;
            }
            NSLog(@"Warning: The request to %@ is being made over an unencrypted connection. This WILL produce an error in the production environment!", request.URL);
        }
    }
    
    // Listen for the session to end
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onSessionEnded) name:ASFSessionEndedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onSessionEnded) name:ASFSessionExpiredNotification object:nil];
    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
}

- (void) stopLoading {
    [self.connection cancel];
    self.mutableData = nil;
}

#pragma mark - NSURLConnectionDelegate
- (void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [self.client URLProtocol:self didReceiveResponse:response cacheStoragePolicy:NSURLCacheStorageNotAllowed];
    
    self.response = response;
    self.mutableData = [[NSMutableData alloc] init];
}

- (void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [self.client URLProtocol:self didLoadData:data];
    
    [self.mutableData appendData:data];
}

- (void) connectionDidFinishLoading:(NSURLConnection *)connection {
    [self.client URLProtocolDidFinishLoading:self];
    [self removeObservers];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    [self failRequestWithError:error];
}

- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    // Initialize the trustResult
    SecTrustResultType trustResult;
    
    // Evaluate whether the trust is trusted by the device
    SecTrustEvaluate(challenge.protectionSpace.serverTrust, &trustResult);
    
    // Determine if there is an error when trusting the certificate
    if (trustResult > kSecTrustResultUnspecified) {
        if (![self isDevelopmentEnvironment]) {
            NSLog(@"Error: Request to %@ will be aborted because the server certificate is not trusted/valid.", self.request.URL);
            [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
        } else {
            NSLog(@"Warning: Request to %@ is being handled by a server with an untrusted/invalid certificate. If your application ignores this error, note that this request WILL be aborted in production.", self.request.URL);
            [self.client URLProtocol:self didReceiveAuthenticationChallenge:challenge];
        }
    } else {
        [self.client URLProtocol:self didReceiveAuthenticationChallenge:challenge];
    }
}

#pragma mark - Private methods
- (void) onSessionEnded
{
    if (![ASFSession canAccessURL:self.request.URL]) {
        NSError *error = [ASFError errorWithCode:ASFErrorSessionInvalid];
        [self failRequestWithError:error];
        [self stopLoading];
    }
}

- (void) failRequestWithError:(NSError *)error
{
    [self.client URLProtocol:self didFailWithError:error];
    [self removeObservers];
}

- (void) removeObservers
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:ASFSessionExpiredNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:ASFSessionEndedNotification object:nil];
}

- (BOOL) isDevelopmentEnvironment
{
    return [ASFSession sharedSession].environment == ASFEnvironmentQA;
}

@end
