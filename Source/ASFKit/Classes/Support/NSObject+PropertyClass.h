//
//  NSObject+PropertyClass.h
//
//  Copyright (c) 2010-2014 Float Mobile Learning. All rights reserved.
//  Included by permission in ASFKit.
//

#import <Foundation/Foundation.h>

@interface NSObject (PropertyClass)

- (Class) classForPropertyName:(NSString *)propertyName;

@end
