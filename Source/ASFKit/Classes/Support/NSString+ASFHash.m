//
//  NSString+ASFHash.m
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "NSString+ASFHash.h"
#import <CommonCrypto/CommonDigest.h>

@implementation NSString (ASFHash)

- (NSString *) md5Hash
{
    const char* data = [self UTF8String];
    CC_LONG length = (CC_LONG)[self lengthOfBytesUsingEncoding:NSUTF8StringEncoding];
    unsigned char md5[CC_MD5_DIGEST_LENGTH];
    
    unsigned char* digest = CC_MD5(data, length, md5);
    
    return [NSString stringWithFormat:@"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
            digest[0],digest[1],digest[2],digest[3],digest[4],digest[5],digest[6],digest[7],digest[8],digest[9],digest[10],digest[11],digest[12],digest[13],digest[14],digest[15],nil];
}

@end
