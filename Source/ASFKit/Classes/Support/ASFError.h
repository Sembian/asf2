//
//  ASFError.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, ASFErrorCode)
{
    ASFErrorUnknown         = -1,
    
    /// License Errors
    /// The application's license is invalid.
    ASFErrorLicenseInvalid          =   1500,
    /// The application's license has been revoked.
    ASFErrorLicenseRevoked          =   1501,
    /// The application's license is no longer valid.
    ASFErrorLicenseExpired          =   1502,
    /// The application's license not able to be verified (this is likely due to an Internet connection problem.
    /// @note If this error occurs while the user is attempting to login, the user will not be allowed to continue until the license can be validated. If the error occurs during the integriry check (after the user is already logged in) then the error is ignored (until it occurs more than the maximum allowed temporary failures).
    ASFErrorLicenseVerifyFailure    =   1503,
    
    // Authentication/Profile Errors
    /// 
    /// The user's credentials could not be authenticated.
    /// This is probably due to the user entering an incorrect username or password. The error will contain the response from the authorization service.
    ///
    ASFErrorAuthorizationFailed     =   2300,
    /// 
    /// The user's profile could not be retrieved from the profile service.
    /// The error will contain the response from the profile service. This may occur if the user's access token is expired and needs to be refreshed. ASFKit can normally handle that situation automatically.
    /// 
    ASFErrorProfileRetrievalFailed  =   2302,
    /// 
    /// The authorization and profile service are not responding correctly.
    /// This error should be reported to IT--it is the result of a server-side issue.
    /// 
    ASFErrorProfileServiceFailure   =   2303,
    
    /// The server providing the profile group data failed to response with valid data (or could not be contacted)
    ASFErrorProfileGroupRetrievalFailure = 2304,
    
    // Configuration Errors
    ASFErrorConfigurationProfileMissing         =   3400,
    ASFErrorConfigurationLeafCertificateMissing =   3401,
    ASFErrorConfigurationLeafDownloadFailed     =   3402,
    ASFErrorConfigurationServerFailure          =   3403,
    
    // Integrity Check
    ASFErrorIntegrityCheckMaxOfflineFailures    =   4300,
    ASFErrorIntegrityCheckMultipleFailures      =   4301,
    
    // General Errors
    ASFErrorDeviceOffline           =   5400,
    ASFErrorFileDownloadFailed      =   5401,
    ASFErrorRequestTimedOut         =   5402,
    ASFErrorSessionInvalid          =   5303,
    ASFErrorMissingSecretKey        =   5304,
    ASFErrorInsecureRequest         =   5305,
    ASFErrorCancelled               =   5403,
    
    // Activity Log Errors
    ASFErrorActivityStatementInvalid    =   6000,
    ASFErrorActivityStatementTooLarge   =   6001,
    ASFErrorActivityLogDisabled         =   6002,
    ASFErrorActivityStatementConflict   =   6003,
    ASFErrorActivityLogUserDisallowed   =   6004,
    ASFErrorActivityLogNoAccess         =   6005,
    ASFErrorActivityLogSignatureInvalid =   6006,
    
};

/// Identifier for an error returned within ASFKit.
extern NSString * const ASFErrorDomain;

/// 
/// A subclass of NSError providing ASFKit some convenient methods to create error objects.
/// 
@interface ASFError : NSError

/// 
/// Creates an error using the specified error code and userInfo dictionary.
/// This makes a call to NSError#errorWithDomain:code:userInfo: with ASFErrorDomain as the error domain.
/// 
/// @param errorCode An error code representing the type of error that occurred. Internal errors within ASFKit generally fall within the list of known errors specified above.
/// @param userInfo 
/// @return An initiated ASFError object with details about the error that occurred.
/// @see NSError#errorWithDomain:code:userInfo:
+ (ASFError *) errorWithCode:(ASFErrorCode)errorCode andUserInfo:(NSDictionary *)userInfo;

/// 
/// Creates an error using the specified error code using the specified localizedDescription as the NSLocalizedDescriptionKey in the userInfo dictionary.
/// 
/// @param errorCode An error code representing the type of error that occurred. Internal errors within ASFKit generally fall within the list of known errors specified above.
/// @param localizedDescription A description of the error that occurred.
/// @return An initiated ASFError object with details about the error that occurred.
/// @see errorWithCode:userInfo:
+ (ASFError *) errorWithCode:(ASFErrorCode)errorCode andLocalizedDescription:(NSString *)localizedDescription;

/// 
/// Creates an error using only an error code as input.
/// The error code is looked up in the list of known error codes (above) and a default error description is assigned to each one.
/// 
/// @param errorCode An error code representing the type of error that occurred. Internal errors within ASFKit generally fall within the list of known errors specified above.
/// @return An initiated ASFError object with details about the error that occurred.
/// @see errorWithCode:userInfo:
+ (ASFError *) errorWithCode:(ASFErrorCode)errorCode;

/// 
/// Creates an error using only an error code as input.
/// The error code is looked up in the list of known error codes (above) and a default error description is assigned to each one.
/// The NSLocalizedFailureKey is defined with the supplied value for localizedFailureReason.
/// 
/// @param errorCode An error code representing the type of error that occurred. Internal errors within ASFKit generally fall within the list of known errors specified above.
/// @param localizedFailureReason This string provides a more detailed explanation of the error than the description. Internally, this is used to provide more information about the error that occurred for debugging purposes.
/// @return An initiated ASFError object with details about the error that occurred.
/// @see errorWithCode:userInfo:
+ (ASFError *) errorWithCode:(ASFErrorCode)errorCode andLocalizedFailureReason:(NSString *)localizedFailureReason;

/// 
/// Creates an error using only an error code as input.
/// The error code is looked up in the list of known error codes (above) and a default error description is assigned to each one.
/// The NSLocalizedFailureKey is defined with the supplied value for localizedFailureReason.
/// The NSUnderlyingErrorKey is define with the supplied value for underlyingError.
/// 
/// @param errorCode An error code representing the type of error that occurred. Internal errors within ASFKit generally fall within the list of known errors specified above.
/// @param localizedFailureReason This string provides a more detailed explanation of the error than the description. Internally, this is used to provide more information about the error that occurred for debugging purposes.
/// @param underlyingError The corresponding value is an error that was encountered in an underlying implementation and caused the error that the receiver represents to occur.
/// @return An initiated ASFError object with details about the error that occurred.
/// @see errorWithCode:userInfo:
+ (ASFError *) errorWithCode:(ASFErrorCode)errorCode andLocalizedFailureReason:(NSString *)localizedFailureReason andUnderlyingError:(NSError *)underlyingError;

@end
