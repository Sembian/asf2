//
//  NSString+ASFHash.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (ASFHash)

- (NSString *) md5Hash;

@end
