//
//  NSString+ASFEncoding.m
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "NSString+ASFEncoding.h"

@implementation NSString (ASFEncoding)
- (NSString *) asf_urlEncodedString
{
    CFStringRef encodedString = CFURLCreateStringByAddingPercentEscapes(
                                                                        NULL,
                                                                        (CFStringRef)self,
                                                                        NULL,
                                                                        CFSTR("/%&=?$#+@<>|\\,[]{}^:;"),
                                                                        kCFStringEncodingUTF8);
    return CFBridgingRelease(encodedString);
}
@end
