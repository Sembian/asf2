//
//  ASFKeychain.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import <Foundation/Foundation.h>

/// A wrapper class for the Keychain API in the iOS SDK.
@interface ASFKeychain : NSObject

/// 
/// Saves an NSCoder complient object to the keychain identified by the specified service accessible by the default access group.
/// 
/// @param service The identifer for this keychain object.
/// @param data The data to save in the keychain.
/// @param availableWhenLocked Whether this data should be avaliable when the device is locked.
+ (void)save:(NSString *)service data:(id)data availableWhenLocked:(BOOL)availableWhenLocked;

/// 
/// Saves an NSCoder complient object to the keychain identified by the specified service accessible by the specified access group.
/// 
/// @param service The identifer for this keychain object.
/// @param data The data to save in the keychain.
/// @param accessGroup The access group identifier that can access this keychain item.
/// @param availableWhenLocked Whether this data should be avaliable when the device is locked.
+ (void)save:(NSString *)service data:(id)data accessGroup:(NSString *)accessGroup availableWhenLocked:(BOOL)availableWhenLocked;

/// 
/// Loads an item saved in the keychain.
/// 
/// @param service The identifer for this keychain object.
/// @return The stored data that was in the keychain.
+ (id)load:(NSString *)service;

/// 
/// Loads an item saved in the keychain restricted by the specified access group.
/// 
/// @param service The identifer for this keychain object.
/// @param accessGroup The access group identifier to restrict the search to.
/// @return The stored data that was in the keychain.
+ (id)load:(NSString *)service accessGroup:(NSString *)accessGroup;

/// 
/// Deletes an item saved in the keychain.
/// 
/// @param service The identifer for this keychain object.
+ (void)delete:(NSString *)service;

/// 
/// Deletes an item saved in the keychain restricted by the specified access group.
/// 
/// @param service The identifer for this keychain object.
/// @param accessGroup The access group identifier to restrict the search to.
+ (void)delete:(NSString *)service accessGroup:(NSString *)accessGroup;

@end
