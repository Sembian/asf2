//
//  SerializableNSObject.h
//
//  Copyright (c) 2010-2014 Float Mobile Learning. All rights reserved.
//  Included by permission in ASFKit.
//

#import <Foundation/Foundation.h>

@interface SerializableNSObject : NSObject <NSCoding>

- (void) setSerializedValue:(id)value forProperty:(NSString *)key;
- (NSDictionary *) propertyMapping;
- (id) initWithDictionary:(NSDictionary *)dict;
+ (NSDictionary *) objectMappings;
+ (id) objectFromDictionary:(NSDictionary *)dict;
- (NSArray *) classProperties;
- (id) serializedValueForKey:(NSString *)key;
- (NSDictionary *) dictionary;
- (NSString *) JSONString;
- (NSString *) prettyJSONString;
- (NSData *) JSONData;

@end
