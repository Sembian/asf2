//
//  ASFKitPrivate.m
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFKitPrivate.h"

NSBundle *ASFKitResourcesBundle(void)
{
    static NSBundle *resourcesBundle = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString *bundlePath = [[NSBundle mainBundle] pathForResource:@"ASFKitResources" ofType:@"bundle"];
        resourcesBundle = [NSBundle bundleWithPath:bundlePath];
    });
    
    return resourcesBundle;
}

NSString *ASFKitLocalizedString(NSString *token)
{
    // Check if the implementing app has specified a value for this key
    NSString *t = NSLocalizedString(token, @"");
    if (!t.length || [t isEqualToString:token])
    {
        if (ASFKitResourcesBundle())
        {
            return NSLocalizedStringFromTableInBundle(token, @"ASFKit", ASFKitResourcesBundle(), @"");
        }
        else
        {
            return token;
        }
    }
    return t;
}