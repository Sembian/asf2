//
//  ASFKitPrivate.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#ifndef ASFKit_ASFKitPrivate_h
#define ASFKit_ASFKitPrivate_h

NSBundle *ASFKitResourcesBundle(void);
NSString *ASFKitLocalizedString(NSString *token);

#endif
