//
//  NSObject+PropertyClass.m
//
//  Copyright (c) 2010-2014 Float Mobile Learning. All rights reserved.
//  Included by permission in ASFKit.
//

#import "NSObject+PropertyClass.h"
#import <objc/runtime.h>

@implementation NSObject (PropertyClass)

#pragma mark - Helper functions
const char * property_getTypeString( objc_property_t property )
{
	const char * attrs = property_getAttributes( property );
	if ( attrs == NULL )
		return ( NULL );
    
	static char buffer[256];
	const char * e = strchr( attrs, ',' );
	if ( e == NULL )
		return ( NULL );
    
	int len = (int)(e - attrs);
	memcpy( buffer, attrs, len );
	buffer[len] = '\0';
    
	return ( buffer );
}

- (Class) classForPropertyName:(NSString *)propertyName
{
    objc_property_t prop = class_getProperty([self class], [propertyName UTF8String]);
    if (!prop) return nil;
    
    const char * value = property_getTypeString(prop);
    
    NSString *className = [NSString stringWithCString:value encoding:NSUTF8StringEncoding];
    
    if (className.length<4) return nil;
    
    className = [[className substringToIndex:className.length-1] substringFromIndex:3];
    
    return objc_getClass([className UTF8String]);
}



@end
