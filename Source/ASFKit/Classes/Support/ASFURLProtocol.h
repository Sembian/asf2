//
//  ASFURLProtocol.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import <Foundation/Foundation.h>

///
/// The ASFURLProtocol class is an implementation of the "abstract" class, NSURLProtocol. Our class is consulted on ALL
/// network requests made by the application (provided they're made through Apple's URL loading system--NSURLConnection and NSURLSession).
///
/// The class is responsible for monitoring all network requests made by implementing applications and is watching for the following:
/// 1. Requests made while there is no user logged in
/// 2. Requests made over insecure connections (over HTTP instead of HTTPS)
/// 3. Requests made over HTTPS but the server certificate is invalid or not trusted
///
/// In scenario 1, the request will immediately fail (unless the request is being made to a whitelisted host;
/// whitelisted hosts are managed by the framework and not by the application).
///
/// In scenario 2, if the session environment is not set to ASFEnvironmentProduction, the request will immediately fail.
/// If the application is not in a production environment, request be permitted to continue, but a warning will be logged in the console.
///
/// For scenario 3, iOS will, by default reject untrusted server certificates. However, applications may choose to ignore
/// and override this. The protocol will not allow applications to ignore/override this error in a production environment.
///
/// @since 2.1
///
@interface ASFURLProtocol : NSURLProtocol

@end
