//
//  ASFLog.m
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFLog.h"
#import "ASFUtilities.h"
#import "ASFFileProtectionManager.h"

static BOOL shouldLogMessages;
@implementation ASFLog


+ (void) setLoggingEnabled:(BOOL)loggingEnabled
{
    shouldLogMessages = loggingEnabled;
}

+ (BOOL) isLoggingEnabled
{
    return shouldLogMessages;
}

+ (void) message: (NSString *)message
{
    if([ASFLog isLoggingEnabled])
    {
        NSLog(@"%@",message);
    }
}

@end
