//
//  ASFError.m
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFError.h"

// NSError domain identifier
NSString * const ASFErrorDomain           = FrameworkID;

@implementation ASFError

#pragma mark - Public methods
+ (ASFError *) errorWithCode:(ASFErrorCode)errorCode andUserInfo:(NSDictionary *)userInfo
{
    return [ASFError errorWithDomain:ASFErrorDomain code:errorCode userInfo:userInfo];
}

+ (ASFError *) errorWithCode:(ASFErrorCode)errorCode andLocalizedDescription:(NSString *)localizedDescription
{
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:localizedDescription forKey:NSLocalizedDescriptionKey];
    return [self errorWithCode:errorCode andUserInfo:userInfo];
}

+ (ASFError *) errorWithCode:(ASFErrorCode)errorCode
{
    return [self errorWithCode:errorCode andLocalizedFailureReason:nil];
}
+ (ASFError *) errorWithCode:(ASFErrorCode)errorCode andLocalizedFailureReason:(NSString *)localizedFailureReason
{
    return [self errorWithCode:errorCode andLocalizedFailureReason:localizedFailureReason andUnderlyingError:nil];
}
+ (ASFError *) errorWithCode:(ASFErrorCode)errorCode andLocalizedFailureReason:(NSString *)localizedFailureReason andUnderlyingError:(NSError *)underlyingError
{
    NSMutableDictionary *userInfo = [NSMutableDictionary dictionary];
    switch (errorCode)
    {
            // License Errors
        case ASFErrorLicenseInvalid:
            [userInfo setObject:ASFKitLocalizedString(@"ASFErrorLicenseInvalid") forKey:NSLocalizedDescriptionKey];
            break;
        case ASFErrorLicenseRevoked:
            [userInfo setObject:ASFKitLocalizedString(@"ASFErrorLicenseRevoked") forKey:NSLocalizedDescriptionKey];
            break;
        case ASFErrorLicenseExpired:
            [userInfo setObject:ASFKitLocalizedString(@"ASFErrorLicenseExpired") forKey:NSLocalizedDescriptionKey];
            break;
        case ASFErrorLicenseVerifyFailure:
            [userInfo setObject:ASFKitLocalizedString(@"ASFErrorLicenseVerifyFailure") forKey:NSLocalizedDescriptionKey];
            [userInfo setObject:ASFKitLocalizedString(@"ASFVerifyInternet") forKey:NSLocalizedRecoverySuggestionErrorKey];
            break;
            
            // Authentication/Profile Errors
        case ASFErrorAuthorizationFailed:
            [userInfo setObject:ASFKitLocalizedString(@"ASFErrorAuthorizationFailed") forKey:NSLocalizedDescriptionKey];
            break;
        case ASFErrorProfileRetrievalFailed:
            [userInfo setObject:ASFKitLocalizedString(@"ASFErrorProfileRetrievalFailed") forKey:NSLocalizedDescriptionKey];
            break;
        case ASFErrorProfileServiceFailure:
            [userInfo setObject:ASFKitLocalizedString(@"ASFErrorProfileServiceFailure") forKey:NSLocalizedDescriptionKey];
            [userInfo setObject:ASFKitLocalizedString(@"ASFErrorProfileServiceFailureReason") forKey:NSLocalizedFailureReasonErrorKey];
            break;
        case ASFErrorProfileGroupRetrievalFailure:
            [userInfo setObject:ASFKitLocalizedString(@"ASFErrorProfileGroupRetrievalFailure") forKey:NSLocalizedDescriptionKey];
            break;
            
            // Configuration Errors
        case ASFErrorConfigurationProfileMissing:
            [userInfo setObject:ASFKitLocalizedString(@"ASFErrorConfigurationProfileMissing") forKey:NSLocalizedDescriptionKey];
            [userInfo setObject:ASFKitLocalizedString(@"ASFErrorConfigurationProfileMissingReason") forKey:NSLocalizedFailureReasonErrorKey];
            break;
        case ASFErrorConfigurationLeafCertificateMissing:
            [userInfo setObject:ASFKitLocalizedString(@"ASFErrorConfigurationLeafCertificateMissing") forKey:NSLocalizedDescriptionKey];
            [userInfo setObject:ASFKitLocalizedString(@"ASFErrorConfigurationLeafCertificateMissingReason") forKey:NSLocalizedFailureReasonErrorKey];
            break;
        case ASFErrorConfigurationLeafDownloadFailed:
            [userInfo setObject:ASFKitLocalizedString(@"ASFErrorConfigurationLeafDownloadFailed") forKey:NSLocalizedDescriptionKey];
            [userInfo setObject:ASFKitLocalizedString(@"ASFErrorConfigurationLeafDownloadFailedReason") forKey:NSLocalizedFailureReasonErrorKey];
            break;
        case ASFErrorConfigurationServerFailure:
            [userInfo setObject:ASFKitLocalizedString(@"ASFErrorConfigurationServerFailure") forKey:NSLocalizedDescriptionKey];
            [userInfo setObject:ASFKitLocalizedString(@"ASFErrorConfigurationServerFailureReason") forKey:NSLocalizedFailureReasonErrorKey];
            break;
            
            // Integrity Check
        case ASFErrorIntegrityCheckMaxOfflineFailures:
            [userInfo setObject:ASFKitLocalizedString(@"ASFErrorIntegrityCheckMaxOfflineFailures") forKey:NSLocalizedDescriptionKey];
            [userInfo setObject:ASFKitLocalizedString(@"ASFVerifyInternet") forKey:NSLocalizedRecoverySuggestionErrorKey];
            break;
        case ASFErrorIntegrityCheckMultipleFailures:
            [userInfo setObject:ASFKitLocalizedString(@"ASFErrorIntegrityCheckMultipleFailures") forKey:NSLocalizedDescriptionKey];
            [userInfo setObject:ASFKitLocalizedString(@"ASFErrorIntegrityCheckMultipleFailuresReason") forKey:NSLocalizedFailureReasonErrorKey];
            break;
            
            // General Errors
        case ASFErrorDeviceOffline:
            [userInfo setObject:ASFKitLocalizedString(@"ASFErrorDeviceOffline") forKey:NSLocalizedDescriptionKey];
            [userInfo setObject:ASFKitLocalizedString(@"ASFErrorDeviceOfflineReason") forKey:NSLocalizedFailureReasonErrorKey];
            [userInfo setObject:ASFKitLocalizedString(@"ASFVerifyInternet") forKey:NSLocalizedRecoverySuggestionErrorKey];
            break;
        case ASFErrorFileDownloadFailed:
            [userInfo setObject:ASFKitLocalizedString(@"ASFErrorFileDownloadFailed") forKey:NSLocalizedDescriptionKey];
            break;
        case ASFErrorRequestTimedOut:
            [userInfo setObject:ASFKitLocalizedString(@"ASFErrorRequestTimedOut") forKey:NSLocalizedDescriptionKey];
            [userInfo setObject:ASFKitLocalizedString(@"ASFVerifyInternet") forKey:NSLocalizedRecoverySuggestionErrorKey];
            break;
        case ASFErrorSessionInvalid:
            [userInfo setObject:ASFKitLocalizedString(@"ASFErrorSessionInvalid") forKey:NSLocalizedDescriptionKey];
            [userInfo setObject:ASFKitLocalizedString(@"ASFErrorSessionInvalidReason") forKey:NSLocalizedFailureReasonErrorKey];
            break;
        case ASFErrorMissingSecretKey:
            [userInfo setObject:ASFKitLocalizedString(@"ASFErrorMissingSecretKey") forKey:NSLocalizedDescriptionKey];
            break;
        case ASFErrorInsecureRequest:
            [userInfo setObject:ASFKitLocalizedString(@"ASFErrorInsecureRequest") forKey:NSLocalizedDescriptionKey];
            break;
        case ASFErrorCancelled:
            [userInfo setObject:ASFKitLocalizedString(@"ASFErrorCancelled") forKey:NSLocalizedDescriptionKey];
            break;
            
            // Activity Log Errors
        case ASFErrorActivityStatementInvalid:
            [userInfo setObject:ASFKitLocalizedString(@"ASFErrorActivityStatementInvalid") forKey:NSLocalizedDescriptionKey];
            break;
        case ASFErrorActivityStatementTooLarge:
            [userInfo setObject:ASFKitLocalizedString(@"ASFErrorActivityStatementTooLarge") forKey:NSLocalizedDescriptionKey];
            break;
        case ASFErrorActivityLogDisabled:
            [userInfo setObject:ASFKitLocalizedString(@"ASFErrorActivityLogDisabled") forKey:NSLocalizedDescriptionKey];
            break;
        case ASFErrorActivityStatementConflict:
            [userInfo setObject:ASFKitLocalizedString(@"ASFErrorActivityStatementConflict") forKey:NSLocalizedDescriptionKey];
            break;
        case ASFErrorActivityLogUserDisallowed:
            [userInfo setObject:ASFKitLocalizedString(@"ASFErrorActivityLogUserDisallowed") forKey:NSLocalizedDescriptionKey];
            break;
        case ASFErrorActivityLogNoAccess:
            [userInfo setObject:ASFKitLocalizedString(@"ASFErrorActivityLogNoAccess") forKey:NSLocalizedDescriptionKey];
            break;
        case ASFErrorActivityLogSignatureInvalid:
            [userInfo setObject:ASFKitLocalizedString(@"ASFErrorActivityLogSignatureInvalid") forKey:NSLocalizedDescriptionKey];
            break;
        default:
            [userInfo setObject:ASFKitLocalizedString(@"ASFErrorUnknown") forKey:NSLocalizedDescriptionKey];
            break;
    }
    
    if (localizedFailureReason)
        [userInfo setObject:localizedFailureReason forKey:NSLocalizedFailureReasonErrorKey];
    
    if (underlyingError)
        [userInfo setObject:underlyingError forKey:NSUnderlyingErrorKey];
    
    return [self errorWithCode:errorCode andUserInfo:userInfo];
}

@end
