//
//  ASFURLRequest.m
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFURLRequest.h"

static NSMutableArray *activeRequests;

@interface ASFURLRequest()
// Determines if the request can start automatically
- (void) startAutomatically;
+ (NSString *) methodWithRequestType:(ASFURLRequestType)requestType;
+ (NSError *) errorWithStatusCode:(NSInteger)statusCode;
- (void) clearTimeoutTimer;
- (void) cancelAndClear;
- (void) onTimeout;
- (void) requestFinishedWithError:(NSError *)error;
- (BOOL) writeResponseDataToFileWithError:(NSError **)error;
@end

@implementation ASFURLRequest
{
    NSMutableURLRequest *_request;
    NSMutableData *_responseData;
}

#pragma mark - Lifecycle
+ (void) initialize
{
    activeRequests = [NSMutableArray array];
}

// Designated initializer
- (id) initWithURL: (NSURL *)url delegate:(id<ASFURLRequestDelegate>)delegate
{
    if (self = [super init])
    {
        _delegate   =   delegate;
        _request    =   [NSMutableURLRequest requestWithURL:url];
        _serverErrorsAreFailures = YES;
        _timeoutInterval = [ASFKitConfiguration defaultTimeoutInterval].intValue;
    }
    return self;
}

- (void) dealloc
{
    [self cancelAndClear];
}

#pragma mark - Accessors
- (void) setHTTPMethod:(ASFURLRequestType) method
{
    [_request setHTTPMethod:[ASFURLRequest methodWithRequestType:method]];
}
- (ASFURLRequestType) HTTPMethod
{
    NSString *method = [self.request HTTPMethod];
    if ([method isEqualToString:@"GET"])
    {
        return ASFURLRequestTypeGET;
    }
    else if ([method isEqualToString:@"POST"])
    {
        return ASFURLRequestTypePOST;
    }
    else if ([method isEqualToString:@"PUT"])
    {
        return ASFURLRequestTypePUT;
    }
    else if ([method isEqualToString:@"OPTIONS"])
    {
        return ASFURLRequestTypeOPTIONS;
    }
    else if ([method isEqualToString:@"DELETE"])
    {
        return ASFURLRequestTypeDELETE;
    }
    else
    {
        return ASFURLRequestTypeUNKNOWN;
    }
}
- (void) setHTTPBody:(NSData *)data
{
    [_request setHTTPBody:data];
}
- (NSData *) HTTPBody
{
    return self.request.HTTPBody;
}
- (void) setURL:(NSURL *)URL
{
    _request.URL = URL;
}
- (NSURL *) URL
{
    return self.request.URL;
}
- (NSTimeInterval) timeToCompleteRequest
{
    return [requestFinished timeIntervalSinceDate:requestStarted];
}
- (BOOL) isActive
{
    return _connection!=nil;
}
- (NSHTTPURLResponse *) response
{
    return _response;
}
- (NSInteger) responseCode
{
    return [_response statusCode];
}
- (NSURLRequest *) request
{
    return _request;
}
- (NSData *) responseData
{
    return [NSData dataWithData:_responseData];
}

+ (NSString *) methodWithRequestType:(ASFURLRequestType)requestType
{
    switch (requestType)
    {
        case ASFURLRequestTypeGET:
            return @"GET";
        case ASFURLRequestTypePOST:
            return @"POST";
        case ASFURLRequestTypePUT:
            return @"PUT";
        case ASFURLRequestTypeOPTIONS:
            return @"OPTIONS";
        case ASFURLRequestTypeDELETE:
            return @"DELETE";
        default:
            return @"GET";
    }
}

#pragma mark - Private methods
+ (NSError *) errorWithStatusCode:(NSInteger)statusCode
{
    NSMutableDictionary *userInfo = [NSMutableDictionary dictionary];
    
    switch (statusCode)
    {
        case 0:
            [userInfo setValue:ASFKitLocalizedString(@"ASFErrorRequestTimedOut") forKey:NSLocalizedDescriptionKey];
            break;
        default:
            [userInfo setValue:[NSHTTPURLResponse localizedStringForStatusCode:statusCode] forKey:NSLocalizedDescriptionKey];
            break;
    }
    
    NSError *error = [ASFError errorWithCode:statusCode andUserInfo:userInfo];
    return error;
}

- (void) clearTimeoutTimer
{
    [timeoutTimer invalidate];
    timeoutTimer = nil;
}

- (void) onTimeout
{
    _requestTimedOut = YES;
    NSString *reason = [NSString stringWithFormat:@"The request did not complete successfully within %zd seconds and was cancelled to allow the application to continue.", self.timeoutInterval];
    NSError *error = [ASFError errorWithCode:ASFErrorRequestTimedOut andLocalizedFailureReason:reason];
    [self requestFinishedWithError:error];
}

- (void) startAutomatically
{
    if ([self.delegate respondsToSelector:@selector(requestShouldStartAutomatically)])
    {
        if ([self.delegate requestShouldStartAutomatically])
        {
            [self start];
        }
    }
}

- (BOOL) writeResponseDataToFileWithError:(NSError *__autoreleasing *)error
{
    BOOL result = [[NSFileManager defaultManager] createDirectoryAtPath:[self.downloadFilepath stringByDeletingLastPathComponent] withIntermediateDirectories:YES attributes:nil error:error];
    if (result)
    {
        NSDictionary *attributes = @{
             NSFileProtectionKey : NSFileProtectionComplete
        };
        result = [[NSFileManager defaultManager] createFileAtPath:self.downloadFilepath contents:self.responseData attributes:attributes];
        if (!result)
        {
            if (error != NULL) {
                *error = [ASFError errorWithCode:ASFErrorFileDownloadFailed];
            }
        }
    }
    return result;
}

- (void) requestFinishedWithError:(NSError *)error
{
    requestFinished = [NSDate date];
    [self clearTimeoutTimer];
    [_connection cancel];
    _connection = nil;
    
    if (!error && self.downloadFilepath)
    {
        [self writeResponseDataToFileWithError:&error];
    }
    
    if (error)
    {
        _failed = YES;
        if ([self.delegate respondsToSelector:@selector(request:didFailWithError:)])
        {
            [self.delegate request:self didFailWithError:error];
        }
    }
    else
    {
        if ([self.delegate respondsToSelector:@selector(requestDidFinish:)])
        {
            [self.delegate requestDidFinish:self];
        }
    }
    
    if (self.completionHandler)
        self.completionHandler(self, self.responseData, error);
    
    // TODO: Consider replacing with queues
    @synchronized (activeRequests) {
        [activeRequests removeObject:self];
        if (activeRequests.count == 0) {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        }
    }
}

- (void) cancelAndClear
{
    BOOL wasWaitingForResponse = _connection != nil;
    
    [self clearTimeoutTimer];
    [_connection cancel];
    _connection = nil;
    requestStarted = requestFinished = nil;
    _response = nil;
    _delegate = nil;
    
    if (wasWaitingForResponse) {
        [self requestFinishedWithError:[ASFError errorWithCode:ASFErrorCancelled]];
    }
}

#pragma mark - Public methods
+ (ASFURLRequest *) requestWithURL:(NSURL *)url delegate:(id<ASFURLRequestDelegate>)delegate
{
    ASFURLRequest *request = [[ASFURLRequest alloc] initWithURL:url delegate:delegate];
    [request startAutomatically];
    return request;
}

+ (ASFURLRequest *) downloadFileFromURL:(NSURL *)url toFilePath:(NSString *)filepath delegate:(id<ASFURLRequestDelegate>)delegate
{
    ASFURLRequest *request = [[ASFURLRequest alloc] initWithURL:url delegate:delegate];
    request.downloadFilepath = filepath;
    [request startAutomatically];
    return request;
}
+ (ASFURLRequest *) requestWithURL:(NSURL *)url completionHandler:(ASFURLRequestCompletionHandler)completionHandler
{
    ASFURLRequest *request = [[ASFURLRequest alloc] initWithURL:url delegate:nil];
    request.completionHandler = completionHandler;
    return request;
}
+ (ASFURLRequest *) downloadFileFromURL:(NSURL *)url toFilePath:(NSString *)filepath completionHandler:(ASFURLRequestCompletionHandler)completionHandler
{
    ASFURLRequest *request = [[ASFURLRequest alloc] initWithURL:url delegate:nil];
    request.downloadFilepath = filepath;
    request.completionHandler = completionHandler;
    return request;
}
- (void) setValue:(NSString *)value forHTTPHeaderField:(NSString *)field
{
    [_request setValue:value forHTTPHeaderField:field];
}
- (NSString *) valueForHTTPHeaderField:(NSString *)field
{
    return [[_response allHeaderFields] objectForKey:field];
}
- (id) start
{
    if (_connection)
    {
        return self;
    }
    
    if ([self.delegate respondsToSelector:@selector(requestShouldStart:)])
    {
        if (![self.delegate requestShouldStart:self])
        {
            return self;
        }
    }
    
    // Prepare
    requestStarted = [NSDate date];
    _requestTimedOut = _failed = NO;
    
    // Start the request
    @synchronized (activeRequests) {
        [activeRequests addObject:self];
    }
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    if (self.synchronous)
    {
        NSLog(@"NOTICE: Request is being sent synchronously! It is highly recommended that you do not send requests synchronously.");
        _request.timeoutInterval = self.timeoutInterval;
        
        NSError *error = nil;
        NSURLResponse *theResponse = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:self.request returningResponse:&theResponse error:&error];
        _responseData = [data mutableCopy];
        _response = (NSHTTPURLResponse *)theResponse;
        [self requestFinishedWithError:error];
    }
    else
    {
        _responseData = [NSMutableData data];
        
        // Set up the timer
        [self clearTimeoutTimer];
        timeoutTimer = [NSTimer scheduledTimerWithTimeInterval:self.timeoutInterval target:self selector:@selector(onTimeout) userInfo:nil repeats:NO];
        
        _connection = [NSURLConnection connectionWithRequest:self.request delegate:self];
        
        // Notify the delegate
        if ([_delegate respondsToSelector:@selector(requestDidStart:)])
        {
            [_delegate requestDidStart:self];
        }
    }
    return self;
}

- (void) cancel
{
    [self cancelAndClear];
}

# pragma mark NSURLConnection Delegate Methods
- (void) connection:(NSURLConnection *)connection didReceiveResponse:(NSHTTPURLResponse *)theResponse
{
    if ([[[theResponse allHeaderFields] valueForKey:@"Content-Type"] rangeOfString:@"multipart/x-mixed-replace"].location!=NSNotFound)
    {
        NSLog(@"Warning: %@ does not support the multipart/x-mixed-replace content type. It will only take the last response received from the server.", [self class]);
    }
    
    // It's only expected to receive one response for each request
    // but in the off-chance that another response was received, then the
    // responseData container needs to be cleared out.
    [_responseData setLength:0];
    
    // Store the response from the server
    _response = (NSHTTPURLResponse *)theResponse;
}

- (void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [_responseData appendData:data];
    
    if ([self.delegate respondsToSelector:@selector(request:didMakeProgress:)] && _response.expectedContentLength>0)
    {
        [self.delegate request:self didMakeProgress:(float)self.responseData.length/(float)_response.expectedContentLength];
    }
}

- (void) connection:(NSURLConnection *)connection didSendBodyData:(NSInteger)bytesWritten totalBytesWritten:(NSInteger)totalBytesWritten totalBytesExpectedToWrite:(NSInteger)totalBytesExpectedToWrite
{
    if ([self.delegate respondsToSelector:@selector(request:didMakeProgressSendingData:)]) {
        [self.delegate request:self didMakeProgressSendingData:(float)totalBytesWritten / (float)totalBytesExpectedToWrite];
    }
}

- (void) connectionDidFinishLoading:(NSURLConnection *)connection
{
    if (self.serverErrorsAreFailures && self.responseCode>=400)
    {
        NSError *error = [ASFURLRequest errorWithStatusCode:self.responseCode];
        [self requestFinishedWithError:error];
    }
    else
    {
        [self requestFinishedWithError:nil];
    }
}

- (void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [self requestFinishedWithError:error];
}

- (void) connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    NSError *error = [ASFURLRequest errorWithStatusCode:401];
    [self requestFinishedWithError:error];
}

@end
