//
//  ASFUtilities.m
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFUtilities.h"

#include <ifaddrs.h>
#include <arpa/inet.h>

static NSString* EmbeddedProvisioningProfile = @"embedded.mobileprovision";
static NSString* ProvisioningProfileStartMarker = @"<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
static NSString* ProvisioningProfileEndMarker = @"</plist>";
static NSString* kEntitlements = @"Entitlements";

@implementation ASFUtilities

+ (NSString *) getIPAddress
{
    // The simulator will likely return 0.0.0.0 as the IP address
    NSString *address = @"0.0.0.0";
    
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0)  
    {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL)  
        {
            if(temp_addr->ifa_addr->sa_family == AF_INET)
            {
                // Check if interface is en0 which is the wifi connection on the iPhone  
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"])  
                {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                }
            }
            temp_addr = temp_addr->ifa_next;
        }
    }
    
    // Free memory
    freeifaddrs(interfaces); 
    
    return address; 
}

+ (NSString *)getDocumentsDirectory
{
    // Looks for the documents directory of the current application
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    // Returns the path for the documents directory
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return documentsDirectory;
}

+ (NSString *) cachesDirectory
{
    return [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
}

#pragma mark - Private utilities
+ (NSString *) frameworkPreferencesFile
{
    return [[self frameworkDirectory] stringByAppendingPathComponent:@"preferences.plist"];
}
+ (NSString *) frameworkDirectory
{
    return [[self cachesDirectory] stringByAppendingPathComponent:FrameworkID];
}
+ (NSDictionary *) currentEntitlements
{
    return [self currentProvisioningProfile][kEntitlements];
}

+ (NSDictionary *) currentProvisioningProfile
{
    NSString *mobileProvisionPath = [[NSBundle mainBundle] pathForResource:EmbeddedProvisioningProfile ofType:nil];
    
    // Verify that the file exists
    if (!mobileProvisionPath) {
        return nil;
    }
    
    NSData *contents = [[NSData alloc] initWithContentsOfFile:mobileProvisionPath];
    
    // Verify that data was read in from the file
    if (!contents.length) {
        return nil;
    }
    
    // Specify the start and end of the data to look for in the data
    NSData *startMarker = [ProvisioningProfileStartMarker dataUsingEncoding:NSUTF8StringEncoding];
    NSData *endMarker = [ProvisioningProfileEndMarker dataUsingEncoding:NSUTF8StringEncoding];
    
    // Parse the contents down to what exists between the start and ending markers
    NSUInteger loc = [contents rangeOfData:startMarker options:0 range:NSMakeRange(0, contents.length)].location;
    NSUInteger len = NSMaxRange([contents rangeOfData:endMarker options:0 range:NSMakeRange(0, contents.length)]) - loc;
    contents = [contents subdataWithRange:NSMakeRange(loc, len)];
    
    // Convert the plist to an NSDictioanry
    CFPropertyListRef profile = CFPropertyListCreateFromXMLData(kCFAllocatorDefault, (__bridge CFDataRef)contents, kCFPropertyListImmutable, NULL);
    NSDictionary *provisioningProfile = (__bridge NSDictionary *)profile;
    CFRelease(profile);
    return provisioningProfile;
}

+ (NSString *) applicationDisplayName
{
    return [NSBundle mainBundle].infoDictionary[@"CFBundleDisplayName"];
}

@end
