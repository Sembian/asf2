//
//  NSDate+InternetTimestamps.m
//  ASFKit
//
//  Copyright (c) 2013 Float Mobile Learning. All rights reserved.
//  http://floatlearning.com/
//

#define RFC3339Format @"yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"

#import "NSDate+InternetTimestamps.h"

@implementation NSDate (ASFInternetTimestamps)

+ (NSDateFormatter *) RFC3339DateFormatter
{
    static NSDateFormatter *dateFormat = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dateFormat = [NSDateFormatter new];
        [dateFormat setDateFormat:RFC3339Format];
        [dateFormat setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    });
    return dateFormat;
}

+ (NSDate *) dateFromRFC3339Timestamp:(NSString *)timestamp
{
    NSDateFormatter *dateFormat = [self RFC3339DateFormatter];
    NSDate *date = [dateFormat dateFromString:timestamp];
    if (!date)
    {
        NSError *error = nil;
        NSRange range = (NSRange){0,19};
        [dateFormat getObjectValue:&date forString:timestamp range:&range error:&error];
    }
    return date;
}

- (NSString *) RFC3339Timestamp
{
    NSDateFormatter *df = [NSDate RFC3339DateFormatter];
    return [df stringFromDate:self];
}

@end
