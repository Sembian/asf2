//
//  ASFUtilities_Private.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFUtilities.h"

@interface ASFUtilities ()

///
/// Reads the current provisioning profile and determines the entitlements that are currently set.
/// @return An NSDictionary representation of the current entitlements or nil if they could not be determined (for example, when running in the simulator).
/// @since 2.1
///
+ (NSDictionary *) currentEntitlements;

///
/// Parses the embedded.mobileprovision file and provides an NSDictionary representation of the embedded plist.
/// @return An NSDictionary representation of the current provisioning profile or nil if they could not be determined (for example, when running in the simulator).
/// @since 2.1
///
+ (NSDictionary *) currentProvisioningProfile;

/// Provides the file path to a "preferences" plist file.
/// Essentially used in place of NSUserDefaults since that has some quirky behavior when dealing with it when protected data is unavailable.
/// @since 2.1
+ (NSString *) frameworkPreferencesFile;

/// Provides the file path to a directory for the framework to store data.
/// Remember to make sure the directory exists before writing to it!
/// @since 2.1
+ (NSString *) frameworkDirectory;

/// Provides the display name of the application bundle as defined in the Info.plist.
/// @return The bundle display name.
/// @since 2.1
+ (NSString *) applicationDisplayName;

@end
