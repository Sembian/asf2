//
//  ASFURLRequest.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ASFURLRequestDelegate;

/// The HTTP Request method to use when sending the request to the server.
typedef NS_ENUM(NSInteger, ASFURLRequestType)
{
    /// Represents a GET request
    ASFURLRequestTypeGET,
    /// Represents a POST request
    ASFURLRequestTypePOST,
    /// Represents a PUT request
    ASFURLRequestTypePUT,
    /// Represents an OPTIONS request
    ASFURLRequestTypeOPTIONS,
    /// Represents a DELETE request
    ASFURLRequestTypeDELETE,
    /// Represents a different type of request; will end up using GET
    ASFURLRequestTypeUNKNOWN
};

@class ASFURLRequest;

/// Definition of the completion handler for an ASFURLRequest.
/// @param request The request that has completed.
/// @param responseData The data received from the request (identical to request.responseData).
/// @param error nil if the request completed successfully; otherwise it will contain the error that prevented the request from succeeding.
typedef void(^ASFURLRequestCompletionHandler)(ASFURLRequest *request, NSData *responseData, NSError *error);

/// 
/// ASFURLRequest is a wrapper class for NSURLRequest and NSURLConnection.
/// It adds convenience and security for requesting data and communicating with remote servers. A simple request can be made in just a couple lines:
///
///     ASFURLRequest *request = [[ASFURLRequest requestWithURL:[NSURL URLWithString:@"http://example.com/"]
///                  completionHandler:^(ASFURLRequest *request, NSData *responseData, NSError *error)
///      {
///          if (error)
///          {
///              // Handle the error
///          }
///          else
///          {
///              // Do something with the responseData.
///              // For example, parse JSON
///              NSError *parseError = nil;
///              NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&parseError];
///              if (!parseError)
///              {
///                  // Use response
///              }
///          }
///      }] start];
///
/// To later cancel this request:
///
///      [request cancel];
///
/// You only need to cancel a request if you no longer want to receive the data from the request. When a request is cancelled, no more delegate methods will be invoked. To start the request again, you'll need to re-create it.
///
/// If the server requires an Authorization header, that must be set before the request is sent to the server. If the request is met with an authentication challenge, the request will be considered as failed. The error will contain the error code 401.
///
/// A special feature of ASFURLRequest is that your application does not need to retain a reference to it. When a request is active, ASFURLRequest will retain references to itself so that delegate methods and response blocks will continue to be called without your application needing to retain references to an object just to prevent it from getting cleaned up by ARC when it's reference count hits zero.
///
/// @note ASFURLRequest does NOT support the multipart/x-mixed-replace content type. Please contact support if you must use this content type.
/// 
@interface ASFURLRequest : NSObject <NSURLConnectionDelegate, NSURLConnectionDataDelegate>
{
    @private
    /// The connection with the remote server.
    NSURLConnection *_connection;
    /// The response (headers and status code) received from the remote server.
    NSHTTPURLResponse *_response;
    /// An NSTimer running in the background to watch for timed out requests.
    NSTimer *timeoutTimer;
    /// The time a request started.
    NSDate *requestStarted;
    /// The time a request finished.
    NSDate *requestFinished;
}

/** @name Properties */

/// The delegate for the ASFURLRequest object.
@property (nonatomic, weak) id<ASFURLRequestDelegate> delegate;

/// The underlying NSURLRequest object.
@property (nonatomic, strong) NSURLRequest *request;

/// Contains specific HTTP protocol response (such as headers) information from a completed request.
/// The headers and status code are both available as properties on ASFURLRequest, but this is provided in case additional properties become available on NSHTTPURLResponse.
/// @since 2.1.2
@property (nonatomic, readonly) NSHTTPURLResponse *response;

/// The data received from the server. Only available after a request finishes.
@property (nonatomic, readonly) NSData *responseData;

/// The HTTP status code received from the server (e.g. 200 or 404).
/// @see http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
@property (nonatomic, readonly) NSInteger responseCode;

/// The block to be invoked when a request finishes (either successfully or unsuccessfully).
@property (nonatomic, strong) ASFURLRequestCompletionHandler completionHandler;

/// The URL the request will be sent to. This can be changed any time before the request is sent. Changing it after the request has been sent is pointless and will only confuse you.
@property (nonatomic, strong) NSURL *URL;

/// The HTTP method to use for the request (default=GET).
@property (nonatomic, readwrite) ASFURLRequestType HTTPMethod;

/// The body of the request. Only is sent with POST and PUT requests. Be sure to set before starting the request.
@property (nonatomic, weak) NSData *HTTPBody;

/// 
/// Set to YES to send the statement synchronously--default NO.
/// When a request is made synchronously, the requestDidStart: delegate method is NOT called, however requestShouldStart, request:didFailWithError:, and requestDidFinish: will all be called on the delegate. It is strongly recommended that requests <strong>not</strong> be made synchronously.
/// 
@property (nonatomic, readwrite) BOOL synchronous;

/// 
/// Set to YES to consider an error received from the server (e.g. a 4xx or 5xx error) as a failed request. If the status code is in the 400s or 500s, the request:didFailWithError:  delegate will be called even though the request was made successfully (default is YES).
/// 
@property (nonatomic, readwrite) BOOL serverErrorsAreFailures;

/// An extra property for implementations to attach an identifying value.
@property (nonatomic, readwrite) NSInteger tag;

/// YES if the request failed due to a time out.
@property (nonatomic, readonly) BOOL requestTimedOut;

/// YES if the request has been sent and is waiting for a response from the server.
@property (nonatomic, readonly) BOOL isActive;

/// YES if the request failed (the ASFURLRequestDelegate#request:didFailWithError: will have been invoked on the delegate).
@property (nonatomic, readonly) BOOL failed;
/// The number of seconds it took for a request to complete. This will be 0 if a request was not able to make it to the target server and back.
@property (nonatomic, readonly) NSTimeInterval timeToCompleteRequest;
/// The response data will be written to the specified file path. If ASFURLRequest is unable to write to the file path, the request will be considered as failed. By default, this is nil.
@property (nonatomic, strong) NSString *downloadFilepath; // Path to write the file to
/// The number of seconds to wait before considering a request as timed out. When a request times out, it is considered as failed. By default, this value is set from the configuration server.
@property (nonatomic, readwrite) NSInteger timeoutInterval;

/** @name Creating a Request */

/// 
/// The designated initializer for ASFURLRequest.
/// Initializes a request with a URL and a delegate. Does not start the request until #start is invoked.
/// 
/// @param url The targetted URL of the request.
/// @param delegate The delegate that will respond to the methods of ASFURLRequestDelegate.
/// @return An initialized ASFURLRequest.
- (id) initWithURL: (NSURL *)url delegate:(id<ASFURLRequestDelegate>)delegate;

/// 
/// Convenience method to initialize a request to a URL specifying a delegate to be notified of the request progress.
/// By default, the request will not start automatically, but your delegate can implement the ASFURLRequestDelegate#requestShouldStartAutomatically method and return YES to change that.
/// 
/// @param url The targetted URL of the request.
/// @param delegate The delegate that will respond to the methods of ASFURLRequestDelegate.
/// @return An initialized ASFURLRequest.
/// @see [ASFURLRequestDelegate requestShouldStartAutomatically]
/// @see If you simply want to download a file from a URL, see downloadFileFromURL:toFilePath:delegate:.
+ (ASFURLRequest *) requestWithURL:(NSURL *)url delegate:(id<ASFURLRequestDelegate>)delegate;

/// 
/// Convenience method to initialize a request to a URL specifiying a file path for the response data to be downloaded to and a delegate to be notified of the request progress.
/// By default, the request will not start automatically, but your delegate can implement the ASFURLRequestDelegate#requestShouldStartAutomatically method and return YES to change that.
///
/// By default, the file path will be overwritten by the data downloaded from the request. If the file path or containing directory does not exist, ASFURLRequest will attempt to create it. If for some reason the file cannot be created, the request will be considered as "failed" even if the actual request to the server succeeded. The request:didFailWithError: method will be invoked on the delegate.
/// 
/// @param url The targetted URL of the request.
/// @param filepath The response data of the request will be downloaded to this path (if writable).
/// @param delegate The delegate that will respond to the methods of ASFURLRequestDelegate.
/// @return An initialized ASFURLRequest.
/// @see [ASFURLRequestDelegate requestShouldStartAutomatically]
/// @see If you want to handle the data directly from the request instead of downloading to a file, use requestWithURL:delegate:.
+ (ASFURLRequest *) downloadFileFromURL:(NSURL *)url toFilePath:(NSString *)filepath delegate:(id<ASFURLRequestDelegate>)delegate;

/// 
/// Convenience method to initialize a request to a URL specifying a block to be invoked when the request is finished.
/// The request will not start automatically--you will need to start it.
///
/// Using blocks is an alternative to the delegate pattern. Although you can use both on the same request, it is recommend you use one or the other. The completion block will be passed the data from the request along with an error object that will be populated with an error if the request failed. The error object will be nil if the request succeeded. A possible implementation may look like:
///
///     [[ASFURLRequest requestWithURL:[NSURL URLWithString:@"http://example.com/"]
///                  completionHandler:^(ASFURLRequest *request, NSData *responseData, NSError *error)
///      {
///          if (error)
///          {
///              // Handle the error
///          }
///          else
///          {
///              // Do something with the responseData.
///              // For example, parse JSON
///              NSError *parseError = nil;
///              NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&parseError];
///              if (!parseError)
///              {
///                  // Use response
///              }
///          }
///      }] start];
/// 
/// @param url The targetted URL of the request.
/// @param completionHandler The block to be invoked when the request finishes.
/// @return An initialized ASFURLRequest.
/// @see If you simply want to download a file from a URL, see downloadFileFromURL:toFilePath:completionHandler:.
+ (ASFURLRequest *) requestWithURL:(NSURL *)url completionHandler:(ASFURLRequestCompletionHandler)completionHandler;

/// 
/// Convenience method to initialize a request to a URL specifiying a file path for the response data to be downloaded to and a block to be invoked when the request finishes.
///
/// By default, the file path will be overwritten by the data downloaded from the request. If the file path or containing directory does not exist, ASFURLRequest will attempt to create it. If for some reason the file cannot be created, the request will be considered as "failed" even if the actual request to the server succeeded. The error object in the block will be populated with whatever error was encountered when creating the file.
///
///     [[ASFURLRequest requestWithURL:[NSURL URLWithString:@"http://example.com/image.jpg"]
///                         toFilePath:[[ASFUtilities cachesDirectory] stringByAppendingPathComponent:@"image.jpg"]
///                  completionHandler:^(ASFURLRequest *request, NSData *responseData, NSError *error)
///      {
///          if (error)
///          {
///              // Handle the error
///          }
///          else
///          {
///              // You may not have to do anything with the responseData because
///              // it will already be written to disk at request.downloadFilepath.
///          }
///      }] start];
/// 
/// @param url The targetted URL of the request.
/// @param filepath The response data of the request will be downloaded to this path (if writable).
/// @param completionHandler The block to be invoked when the request finishes.
/// @return An initialized ASFURLRequest.
/// @see If you want to handle the data directly from the request instead of downloading to a file, use requestWithURL:completionHandler:.
+ (ASFURLRequest *) downloadFileFromURL:(NSURL *)url toFilePath:(NSString *)filepath completionHandler:(ASFURLRequestCompletionHandler)completionHandler;

/** @name Adjusting the Request Headers */

/// 
/// Sets the value of an HTTP header field.
/// 
/// @param value The value to set for the header field.
/// @param field The header field to set the value of.
/// @note value must not contain a linebreak--if it does, the header field will NOT be set.
- (void) setValue:(NSString *)value forHTTPHeaderField:(NSString *)field;

/// Retrieves the value for an HTTP header field <em>from the response</em>. Note that this does not retrieve values from the request headers.
/// @param field The header field to retrieve the value of.
/// @return The value of the specified field or nil if the field didn't exist.
/// @note This only retrieves values from the response to the request. It will not return values set by setValue:forHTTPHeaderField:.
- (NSString *) valueForHTTPHeaderField:(NSString *)field;

/** @name Starting and Canceling a Request */

///
/// Starts the request. This method should not be overridden.
/// This will trigger the requestShouldStart: method just before sending the request and requestDidStart: after it has started. If your application returns YES for ASFURLRequestDelegate#requestShouldStartAutomatically then you do not need to call start.
/// 
/// @return A reference to the request (for method chaining).
- (id) start;

/// Cancel the request. No more delegate methods will be triggered.
- (void) cancel;

/// Returns the string value for the ASFURLRequestType.
/// @param requestType The request type.
/// @return The string value for the ASFURLRequestType.
+ (NSString *) methodWithRequestType:(ASFURLRequestType)requestType;

@end

/// 
/// The methods declared by the ASFURLRequestDelegate protocol allow the adopting delegate respond to messages from ASFURLRequest--specifially significant changes in the request status (e.g. a request started, completed, or failed).
/// 
@protocol ASFURLRequestDelegate <NSObject>

@optional
/// 
/// The delegate can return YES for this method if the request should automatically start as soon as it's created.
/// This value is only respected when using the convenience methods and <strong>not</strong> the designated initializer. By default, this is NO.
/// 
/// @return Implementation should return YES if the request should start automatically.
- (BOOL) requestShouldStartAutomatically;

/// 
/// Tells the delegate that the request is just about to start.
/// The delegate can make any last minute determination on whether the request should be made. Delegates can return NO to prevent the request from being sent. If a delegate prevents a request from being made, then any call to ASFURLRequest#start doesn't do anything. No other delegate methods will be called.
/// 
/// @param request The request that is about to be sent.
/// @return Return NO if the request should not be sent to the server. By default, this is YES.
- (BOOL) requestShouldStart: (ASFURLRequest *)request;

/// 
/// Tells the delegate that a request was just sent to a remote server.
/// The application is waiting for a response. The other delegate methods will be called as the request makes progress.
/// 
/// @param request The current request.
- (void) requestDidStart: (ASFURLRequest *)request;

///
/// Tells the delegate that progress was made in sending the body data to the server.
/// This only reflects what percentage of body data was sent to the server; it is only invoked when requests have body data (such as a POST or PUT requests).
///
/// @param request The current request.
/// @param percentComplete A number between 0 and 1 indicating what percentage of data to send has been successfully sent.
/// @since 2.0.1
- (void) request: (ASFURLRequest *)request didMakeProgressSendingData: (float)percentComplete;

/// 
/// Tells the delegate the progress of the response being downloaded from the server.
/// The server MUST indicate the size of the response using the Content-Length header in order for this delegate method to be invoked.
///
/// This is suitable for displaying a progress bar for a large request.
/// 
/// @param request The current request.
/// @param percentComplete A number between 0 and 1 indicating the completeness of the request.
/// @note Prior to 2.0.1, this method indicated the progress of data being SENT to the server. As of 2.0.1, this now reflects the progress of data being RECEIVED.
- (void) request: (ASFURLRequest *)request didMakeProgress: (float)percentComplete;

/// 
/// Tells the delegate that the request completed successfully.
/// responseData is populated with the data received from the server. Your application can parse the data or handle it whatever way is appropriate. If ASFURLRequest#downloadFilepath was specified, then this tells the delegate that the file has downloaded and saved successfully.
/// 
/// @param request The request that just completed.
- (void) requestDidFinish: (ASFURLRequest *)request;

/// 
/// Tells the delegate that the request did not complete successfully.
/// This may indicate the server responded with a 400 or 500 level error, the file did not download properly, or simply the server could not be reached. This method will be invoked if a request is attempted while there is no authenticated user with ASFKit.
/// 
/// @param request The request that just failed.
/// @param error The error that prevented the request from completing.
/// @note By default, any response received where the status code is in the 400 or 500 range from the server will be considered a failure. See [ASFURLRequest serverErrorsAreFailures] to change this behavior.
- (void) request: (ASFURLRequest *)request didFailWithError:(NSError *)error;

@end