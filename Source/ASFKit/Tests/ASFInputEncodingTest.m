//
//  ASFInputEncodingTest.m
//  ASFKit
//
//  Created by Daniel Pfeiffer on 10/13/14.
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import <XCTest/XCTest.h>
#import "NSString+ASFEncoding.h"

@interface ASFInputEncodingTest : XCTestCase

@end

@implementation ASFInputEncodingTest

- (void)setUp
{
    [super setUp];
}

- (void)tearDown
{
    [super tearDown];
}

/**
 URL encoding is used on username and password validation as well as communication with activity log server.
 Input must be URL-encoded so all special characters (e.g. ampersands) can be sent to the server without being mistaken for a seperator.
 
 Expected result was computed using the JavaScript standard function encodeURIComponent which encodes all but the following:
 - _ . ! ~ * ' ( )
 */
- (void) testURLEncoding
{
    // 1. Test known problem chracters
    // The following characters have been determined to definitely cause log in problems: &, %, +
    NSString *knownProblems = @"&%+";
    XCTAssertEqualObjects([knownProblems asf_urlEncodedString], @"%26%25%2B");
    
    // 2. Test all basic latin special characters
    NSString *basicLatinChars = @"!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~ ";
    XCTAssertEqualObjects([basicLatinChars asf_urlEncodedString], @"!%22%23%24%25%26'()*%2B%2C-.%2F0123456789%3A%3B%3C%3D%3E%3F%40ABCDEFGHIJKLMNOPQRSTUVWXYZ%5B%5C%5D%5E_%60abcdefghijklmnopqrstuvwxyz%7B%7C%7D~%20");
}

/**
 There are two built-in methods for percent encoding strings:
 -[NSString stringByAddingPercentEncodingWithAllowedCharacters:] (new in iOS 8)
 -[NSString stringByAddingPercentEscapesUsingEncoding:]
 
 The old method fails our test; the new method only passes with a custom NSCharacterSet.
 */
- (void) testNSStringPercentEncoding {
    // The following characters have been determined to definitely cause log in problems: &, %, +
    NSString *knownProblems = @"&%+";
    // 1. Test known problem chracters using original percent endoing method (won't be equal)
    XCTAssertNotEqualObjects([knownProblems stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding], @"%26%25%2B");
    // 2. Test known problem characters using new percent encoding method with URLQueryAllowedCharacterSet (won't be equal)
    XCTAssertNotEqualObjects([knownProblems stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet], @"%26%25%2B");
    // 3. Test known problem characters using new percent encoding method with custom character set (will succeed).
    // We could also do an empty charater set, but then even letters would be encoded, and although it may work, it would increase the triple the size of the request.
    NSCharacterSet *allowedCharacters = [[NSCharacterSet characterSetWithCharactersInString:@"/%&=?$#+@<>|\\,[]{}^:;"] invertedSet];
    XCTAssertEqualObjects([knownProblems stringByAddingPercentEncodingWithAllowedCharacters:allowedCharacters], @"%26%25%2B");
}

@end
