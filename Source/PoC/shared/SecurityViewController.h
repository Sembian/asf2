//
//  SecurityDataSource.h
//  poc
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASFURLRequest.h"

extern NSString* const kPOCBackgroundFetchLog;

@class SecurityViewController;
@protocol SecurityViewControllerDelegate <NSObject>
@optional
- (BOOL) securityViewManagerShouldUpdateData:(SecurityViewController *)viewManager;
- (void) securityViewManagerWillUpdateData:(SecurityViewController *)viewManager;
- (void) securityViewManagerDidUpdateData:(SecurityViewController *)viewManager;

@end

@interface SecurityViewController : NSObject <ASFURLRequestDelegate, UIPopoverControllerDelegate>
{
    UIPopoverController *popover;
}

// Local Data
@property (nonatomic, strong) IBOutlet UITextView *cachedPasswordData;
@property (nonatomic, strong) IBOutlet UITextView *urlSchemes;
@property (nonatomic, strong) IBOutlet UITextView *applicationKey;
@property (nonatomic, strong) IBOutlet UITextView *secretKey;
@property (nonatomic, strong) IBOutlet UITextView *deviceIdentifier;
@property (nonatomic, strong) IBOutlet UITextView *fileAttributes;
@property (nonatomic, strong) IBOutlet UITextView *backgroundFetchLog;

// Server Data
@property (nonatomic, strong) IBOutlet UITextView *requestedUrl;
@property (nonatomic, strong) IBOutlet UIProgressView *downloadProgressView;
@property (nonatomic, strong) IBOutlet UITextView *httpsResponse;
@property (nonatomic, strong) IBOutlet UIImageView *pictureResponse;
@property (nonatomic, strong) IBOutlet UITextView *activityLogUserConsentStatus;
@property (nonatomic, strong) IBOutlet UITextView *activityLogStatus;

// Token
@property (nonatomic, strong) IBOutlet UITextView *displayName;
@property (nonatomic, strong) IBOutlet UITextView *accessToken;
@property (nonatomic, strong) IBOutlet UITextView *refreshToken;
@property (nonatomic, strong) IBOutlet UITextView *expiration;
@property (nonatomic, strong) IBOutlet UITextView *timeToReceiveResponse;
@property (nonatomic, strong) IBOutlet UITextView *timeToVerifyCredentials;
@property (nonatomic, strong) IBOutlet UITextView *timeToRetrieveProfile;
@property (nonatomic, strong) IBOutlet UITextView *authorizationUrl;
@property (nonatomic, strong) IBOutlet UITextView *profileUrl;

// Protection
@property (nonatomic, strong) IBOutlet UITextView *dataBecomingUnavailable;
@property (nonatomic, strong) IBOutlet UITextView *dataBecomingAvailable;
@property (nonatomic, strong) IBOutlet UITextView *frameworkVersion;
@property (nonatomic, strong) IBOutlet UITextView *lastIntegrityCheckResult;
@property (nonatomic, strong) IBOutlet UITextView *configurationProfileStatus;
@property (nonatomic, strong) IBOutlet UITextView *keychainAccessGroup;
@property (nonatomic, strong) IBOutlet UITextView *keychainData;
@property (nonatomic, strong) IBOutlet UITextView *publicKeychainData;
@property (nonatomic, strong) IBOutlet UITextView *dataProtection;

@property (weak, nonatomic) IBOutlet UIViewController *viewController;
@property (nonatomic, strong) UIBarButtonItem *loginToggleButton;
@property (nonatomic, strong) UIBarButtonItem *simulateExpirationButton;

@property (nonatomic, assign) IBOutlet id<SecurityViewControllerDelegate> delegate;
- (void) viewDidLoad;
- (void) initializeData;
- (void) updateData;

// Actions
- (IBAction) openFileBrowser:(id)sender;
- (IBAction) openWebBrowser:(id)sender;
- (IBAction) makeHTTPsRequest:(id) sender;
- (IBAction) makeHTTPsRequestInvalidCertificate: (id) sender;
- (IBAction) downloadFile:(id)sender;
- (IBAction) viewProfile:(id)sender;
- (IBAction) viewActivityStream:(id)sender;
- (IBAction) viewPDF:(id)sender;
- (IBAction) clearBackgroundFetchLog:(id)sender;

// Other
+ (NSString *) echoURLForCurrentUser;

@end
