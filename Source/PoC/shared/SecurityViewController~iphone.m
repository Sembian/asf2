//
//  SecurityViewController.m
//  poc
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "SecurityViewController~iphone.h"

@interface SecurityViewController_iphone ()

@property (nonatomic, strong) IBOutlet SecurityViewController *securityViewController;

@end

@implementation SecurityViewController_iphone
{
    BOOL isVisible;
}
@synthesize securityViewController=_dataSource;

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return YES;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    [self.securityViewController viewDidLoad];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    isVisible = YES;
    [self.securityViewController updateData];
}
- (void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    isVisible = NO;
}

#pragma mark - Security View Delegate
- (BOOL) securityViewManagerShouldUpdateData:(SecurityViewController *)viewManager
{
    return isVisible;
}

@end
