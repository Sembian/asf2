//
//  ASFMapViewController.m
//  poc
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFMapViewController.h"
#import "ASFProfile.h"

@interface ASFMapViewController ()
@property (nonatomic, strong) ASFProfile *profile;
@end

@implementation ASFMapViewController

- (id) initWithProfile:(ASFProfile *)profile
{
    if (self = [self init]) {
        self.profile = profile;
    }
    return self;
}

- (void) loadView
{
    self.mapView = [MKMapView new];
    self.view = self.mapView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.mapView.delegate = self;
    self.mapView.showsUserLocation = YES;
    
    self.contentSizeForViewInPopover = CGSizeMake(600, 600);
    self.title = self.profile.displayName;
    NSString *address = [NSString stringWithFormat:@"%@, %@, %@", self.profile.streetaddress, self.profile.st, self.profile.postalcode];
    [self centerMapOnAddress:address withName:self.profile.displayName];
}

#pragma mark - Map view delegate
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    static NSString *PinIdentifier = @"Pin";
    
    if (annotation != mapView.userLocation) {
        MKPinAnnotationView *pin = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:PinIdentifier];
        if (!pin) {
            pin = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:PinIdentifier];
        }
        
        pin.canShowCallout = YES;
        
        return pin;
    }
    
    return nil;
}

#pragma mark - Private methods
- (void) centerMapOnAddress:(NSString *)address withName:(NSString *)name
{
    static CLGeocoder *geocoder = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        geocoder = [[CLGeocoder alloc] init];
    });
    
    [geocoder geocodeAddressString:address completionHandler:^(NSArray* placemarks, NSError* error){
        if (placemarks && placemarks.count > 0) {
            CLPlacemark *topResult = placemarks.firstObject;
            MKPlacemark *placemark = [[MKPlacemark alloc] initWithPlacemark:topResult];
            
            MKCoordinateRegion region = self.mapView.region;
            region.center = [(CLCircularRegion *)placemark.region center];
            region.span.longitudeDelta /= 30.0;
            region.span.latitudeDelta /= 30.0;
            
            [self.mapView setRegion:region animated:YES];
            
            MKPointAnnotation *annotation = [MKPointAnnotation new];
            annotation.coordinate = placemark.coordinate;
            if (name) {
                annotation.title = name;
                annotation.subtitle = address;
            } else {
                annotation.title = address;
            }
            [self.mapView addAnnotation:annotation];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.75 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.mapView selectAnnotation:annotation animated:YES];
            });
        } else {
            [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"User has no location specified in his/her profile.", nil) message:nil delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
        }
    }];
}

@end
