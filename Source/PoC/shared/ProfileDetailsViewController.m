//
//  ProfileDetailsViewController.m
//  poc
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ProfileDetailsViewController.h"
#import "ASFSession+Private.h"
#import "ASFAuthorization.h"
#import "ASFProfile.h"

@interface ProfileDetailsViewController ()
@property (nonatomic, strong) NSDictionary *profileValues;
@end

@implementation ProfileDetailsViewController

- (void) dealloc
{
    [[ASFSession sharedSession] removeObserver:self forKeyPath:@"authorization.profile"];
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    self.title = NSLocalizedString(@"Profile Details", nil);
    self.tableView.allowsSelection = NO;
    self.contentSizeForViewInPopover = CGSizeMake(400, 350);
    [self updateProfileData];
    [[ASFSession sharedSession] addObserver:self forKeyPath:@"authorization.profile" options:0 context:NULL];
}

- (void) viewDidUnload
{
    [super viewDidUnload];
    [[ASFSession sharedSession] removeObserver:self forKeyPath:@"authorization.profile"];
}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    [self updateProfileData];
    [self.tableView reloadData];
}

#pragma mark - Private methods
- (void) updateProfileData
{
    ASFProfile *profile = [ASFSession sharedSession].authorization.profile;
    NSMutableDictionary *profileValues = [profile.dictionary mutableCopy];
    if (profile.userType) {
        [profileValues setObject:profile.userType forKey:@"userType"];
    } else {
        [profileValues setObject:@"" forKey:@"userType"];
    }
    self.profileValues = [NSDictionary dictionaryWithDictionary:profileValues];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.profileValues.allKeys.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell==nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:CellIdentifier];
        cell.textLabel.adjustsFontSizeToFitWidth = cell.detailTextLabel.adjustsFontSizeToFitWidth = YES;
    }
    
    // Configure the cell...
    NSArray *keys = [self.profileValues.allKeys sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    NSString *key = [keys objectAtIndex:indexPath.row];
    NSString *value = [self.profileValues valueForKey:key];
    
    cell.textLabel.enabled = cell.detailTextLabel.enabled = value.length>0;
    if (value.length==0) value = @"Not Set";
    
    cell.textLabel.text = key;
    
    if ([self.view respondsToSelector:@selector(tintColor)]) {
        cell.textLabel.textColor = self.view.tintColor;
    }
    
    cell.detailTextLabel.text = value;
    
    return cell;
}

@end
