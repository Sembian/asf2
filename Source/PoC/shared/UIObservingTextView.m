//
//  UIObservingTextView.m
//  poc
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "UIObservingTextView.h"

@implementation UIObservingTextView

- (void) observeKeyPath:(NSString *)keyPath onObject:(NSObject *)object
{
    self.text = [NSString stringWithFormat:self.format,[object valueForKeyPath:keyPath]];
    [object addObserver:self forKeyPath:keyPath options:NSKeyValueObservingOptionNew context:NULL];
}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    self.text = [NSString stringWithFormat:self.format, [change objectForKey:NSKeyValueChangeNewKey]];
}

#pragma mark - accessors
- (NSString *) format
{
    if (!_format)
        _format = @"%@";
    
    return _format;
}

@end
