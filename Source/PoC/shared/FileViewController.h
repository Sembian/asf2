//
//  FileViewController.h
//  poc1
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FileViewController : UITableViewController
{
    @private
    NSString *loadedFilePath;
    NSMutableArray *items;
}

- (id) initWithPath: (NSString *)filePath;

@end
