//
//  FileViewController.m
//  poc1
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "FileViewController.h"
#import "ASFFileProtectionManager.h"

@interface FileViewController ()
- (void) getItemsAtPath:(NSString *)filepath;
- (void) onFileUpdate;
@end

@implementation FileViewController
- (id) initWithPath: (NSString *)filePath
{
    self = [super initWithStyle:UITableViewStylePlain];
    if (self)
    {
        items = [NSMutableArray new];
        loadedFilePath = nil;
        [self getItemsAtPath:filePath];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onFileUpdate) name:ASFProtectionAttributeModifiedNotification object:nil];
    }
    return self;
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    self.title = loadedFilePath.lastPathComponent;
    if ([self.title isEqualToString:@".."])
        self.title = @"Files";
    
    self.contentSizeForViewInPopover = CGSizeMake(400, 400);
}

#pragma mark - Private methods

- (void) getItemsAtPath:(NSString *)filepath
{
    loadedFilePath = filepath;
    
    [items removeAllObjects];
    NSFileManager *defaultManager = [NSFileManager defaultManager];
    NSError *error = nil;
    NSArray *contents = [defaultManager contentsOfDirectoryAtPath:filepath error:&error];
    
    if (error)
    {
        NSLog(@"Error: %@", error);
        return;
    }
    
    for (NSString *item in contents) 
    {
        error = nil;
        NSMutableDictionary *attributes = [[defaultManager attributesOfItemAtPath:[NSString stringWithFormat:@"%@/%@", filepath,item] error:&error] mutableCopy];
        
        if (error)
        {
            NSLog(@"Error reading file: %@", error);
            continue;
        }
        
        [attributes setObject:item forKey:@"name"];
        [attributes setObject:[NSString stringWithFormat:@"%@/%@", filepath, item] forKey:@"path"];
        
        [items addObject:[NSDictionary dictionaryWithDictionary:attributes]];
    }
}
- (void) onFileUpdate
{
    [self getItemsAtPath:loadedFilePath];
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    
    NSDictionary *file = [items objectAtIndex:indexPath.row];
    cell.textLabel.text = [file objectForKey:@"name"];
    
    cell.detailTextLabel.text = @"";
    
    if ([file objectForKey:NSFileProtectionKey]!=NSFileProtectionComplete && [file objectForKey:NSFileType]!=NSFileTypeRegular)
    {
        cell.detailTextLabel.text = [NSString stringWithFormat:@"Unprotectable File Type: %@", [file objectForKey:NSFileType]];
        cell.detailTextLabel.textColor = [UIColor darkGrayColor];
    }
    else if ([file objectForKey:NSFileProtectionKey]!=NSFileProtectionComplete && [file objectForKey:NSFileType]==NSFileTypeRegular)
    {
        cell.detailTextLabel.text = [file objectForKey:NSFileProtectionKey];
        cell.detailTextLabel.textColor = [UIColor redColor];
    }
    else
    {
        cell.detailTextLabel.text = [file objectForKey:NSFileProtectionKey];
        cell.detailTextLabel.textColor = [UIColor colorWithRed:29/255.0 green:136/255.0 blue:96/255.0 alpha:1];
    }
    
    if ([file objectForKey:NSFileType]==NSFileTypeDirectory)
    {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *file = [items objectAtIndex:indexPath.row];
    return [file objectForKey:NSFileType]==NSFileTypeRegular;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        NSDictionary *file = [items objectAtIndex:indexPath.row];
        NSString *path = [loadedFilePath stringByAppendingPathComponent:[file objectForKey:@"name"]];
        if ([[NSFileManager defaultManager] removeItemAtPath:path error:nil])
        {
            [self getItemsAtPath:loadedFilePath];
            [self.tableView beginUpdates];
            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
            [self.tableView endUpdates];
        }
        else
        {
            [[[UIAlertView alloc] initWithTitle:@"This file cannot be deleted." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
    }
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *file = [items objectAtIndex:indexPath.row];
    
    if ([file objectForKey:NSFileType]==NSFileTypeDirectory)
    {
        FileViewController *vc = [[FileViewController alloc] initWithPath:[file objectForKey:@"path"]];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else
    {
        NSURL *fileURL = [NSURL fileURLWithPath:file[@"path"]];
        UIViewController *fileViewController = [UIViewController new];
        fileViewController.contentSizeForViewInPopover = self.contentSizeForViewInPopover;
        UIWebView *webView = [UIWebView new];
        
        // Special case for loading plist files; simply load them as a string and then display in UIWebView
        if ([fileURL.pathExtension isEqualToString:@"plist"]) {
            NSString *plist = [NSString stringWithContentsOfURL:fileURL encoding:NSUTF8StringEncoding error:nil];
            if (plist) {
                // Poor man's HTML encoding
                plist = [[plist stringByReplacingOccurrencesOfString:@"<" withString:@"&lt;"] stringByReplacingOccurrencesOfString:@">" withString:@"&gt;"];
                [webView loadHTMLString:[NSString stringWithFormat:@"<pre>%@</pre>", plist] baseURL:fileURL];
            }
        } else {
            [webView loadRequest:[NSURLRequest requestWithURL:fileURL]];
        }
        fileViewController.view = webView;
        fileViewController.title = file[@"name"];
        [self.navigationController pushViewController:fileViewController animated:YES];
    }
}

@end
