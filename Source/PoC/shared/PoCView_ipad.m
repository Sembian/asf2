//
//  PoCView_ipad.m
//  poc
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "PoCView_ipad.h"

@implementation PoCView_ipad

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
