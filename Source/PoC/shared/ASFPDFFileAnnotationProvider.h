//
//  ASFPDFFileAnnotationProvider.h
//  poc
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import <PSPDFKit/PSPDFKit.h>

@interface ASFPDFFileAnnotationProvider : PSPDFFileAnnotationProvider

@end
