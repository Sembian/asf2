//
//  CreateActivityStatementViewController.m
//  poc
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "CreateActivityStatementViewController.h"
#import "ASFActivityStatement.h"
#import "ASFActivityObject.h"
#import "ASFActivityLog.h"

@interface CreateActivityStatementViewController () <UITextFieldDelegate>
@property (nonatomic, strong) IBOutletCollection(UITableViewCell) NSArray *tableViewCells;
@property (nonatomic, strong) IBOutlet UITextField *verb;
@property (nonatomic, strong) IBOutlet UITextField *object;
@property (nonatomic, strong) IBOutlet UITextView *statementDisplayPreview;
@property (nonatomic, strong) IBOutlet UITextView *statementPreview;
@end

@implementation CreateActivityStatementViewController
{
    ASFActivityStatement *statement;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    self.tableView.tableFooterView = [UIView new];
    self.title = NSLocalizedString(@"Create Statement", nil);
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(sendStatement)];
    self.navigationItem.rightBarButtonItem.enabled = NO;
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setToolbarHidden:YES animated:animated];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.tableViewCells.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.frame.size.height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return self.tableViewCells[indexPath.row];
}

#pragma mark - UITextField delegate
- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    double delayInSeconds = 0.1;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self updatePreview];
    });
    return YES;
}
- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.verb) {
        [self.object becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    [self updatePreview];
    return NO;
}

#pragma mark - Private methods
- (void) updatePreview
{
    self.navigationItem.rightBarButtonItem.enabled = YES;
    statement = [self generateStatementFromFields];
    self.statementDisplayPreview.text = [statement description];
    self.statementPreview.text = [statement prettyJSONString];
}

- (ASFActivityStatement *) generateStatementFromFields
{
    ASFActivityObject *object = [ASFActivityObject activityObjectWithDisplayName:self.object.text andType:@"object"];
    return [ASFActivityStatement statementForCurrentActorWithVerb:self.verb.text withObject:object];
}

- (void) sendStatement
{
    if (!statement) {
        return;
    }
    
    [self setInterfaceEnabled:NO];
    
    [ASFActivityLog logStatement:statement completionHandler:^(ASFActivityLogStoreResult result, NSArray *statements, NSError *error) {
        if (error) {
            [[[UIAlertView alloc] initWithTitle:error.localizedDescription message:error.localizedFailureReason delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
            [self setInterfaceEnabled:YES];
        } else {
            NSString *message = nil;
            switch (result) {
                case ASFActivityLogStoreResultPersisted:
                    message = NSLocalizedString(@"The statement has been persisted on the server!", nil);
                    break;
                case ASFActivityLogStoreResultQueued:
                    message = NSLocalizedString(@"The statement has been queued locally.", nil);
                    break;
                default:
                    message = NSLocalizedString(@"An unexpected result occurred.", nil);
                    break;
            }
            [[[UIAlertView alloc] initWithTitle:message message:nil delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
}

- (void) setInterfaceEnabled:(BOOL)enabled
{
    self.verb.enabled = enabled;
    self.object.enabled = enabled;
    self.navigationItem.rightBarButtonItem.enabled = enabled;
}

@end
