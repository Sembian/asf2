//
//  pocAppDelegate.m
//  poc
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "pocAppDelegate.h"


#import "ASFSession.h"
#import "ASFProfile.h"
#import "ASFFileProtectionManager.h"
#import "SecurityViewController.h"
#import "PoCView_ipad.h"
#import "ASFUtilities.h"
#import <HockeySDK/HockeySDK.h>

// The bundle identifier for the OTHER PoC app
#if poc1
NSString* const ASFAltBundleIdentifier = @"com.abbvienet.PPD444Security2";
#elif poc2
NSString* const ASFAltBundleIdentifier = @"com.abbvienet.PPD443Security1";
#endif

@interface pocAppDelegate() <BITHockeyManagerDelegate, BITCrashManagerDelegate, BITUpdateManagerDelegate, ASFSessionDelegate>

@end

@implementation pocAppDelegate


@synthesize window=_window;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSString *licenseKey = nil, *devLicenseKey = nil, *secretKey = nil, *devSecretKey = nil, *hockeyAppId = nil;
#if poc1
    licenseKey = @"4ff70813-9254-42ea-93f4-1460adc7b1ec";
    devLicenseKey = @"4ff70813-9254-42ea-93f4-1460adc7b1ec";
    secretKey = @"pFqM3xLji/d1gYQCNqOKabEHiBEiW5m17Gmil6u2";
    devSecretKey = @"pFqM3xLji/d1gYQCNqOKabEHiBEiW5m17Gmil6u2";
    hockeyAppId = @"f6322e3a6a63b1ca65fe91078151f01a";
    [self setThemeColor:[UIColor colorWithRed:130/255.0 green:0 blue:0 alpha:1] andSecondaryColor:[UIColor colorWithRed:255/255.0 green:229/255.0 blue:174/255.0 alpha:1]];
#elif poc2
    licenseKey = @"4ff72428-9c38-4e4e-a1b6-5fa6adc7b1ec";
    devLicenseKey = @"4ff72428-9c38-4e4e-a1b6-5fa6adc7b1ec";
    secretKey = @"+Nszyqx2zTNU9EmN9Qn8A4WArzbfX1I4TgWIM7cQ";
    devSecretKey = @"+Nszyqx2zTNU9EmN9Qn8A4WArzbfX1I4TgWIM7cQ";
    hockeyAppId = @"261f7dffe3260bfe058e1feceaae3587";
    [self setThemeColor:[UIColor colorWithRed:7/255.0 green:74/255.0 blue:101/255.0 alpha:1] andSecondaryColor:[UIColor colorWithRed:255/255.0 green:229/255.0 blue:174/255.0 alpha:1]];
#endif
    
    /*
     License Key
     4fd200f8-a6b4-409c-bdaf-1f03adc7b1ec
     Secret Key
     gHD+l14ujoqFpToabiBTTWjLpRW6OKa4Oka+BcF4
     Bundle Identifier
     com.abbvienet.PPD324Security1
     */
    // Configure HockeyApp
    [[BITHockeyManager sharedHockeyManager] configureWithIdentifier:hockeyAppId];
    [BITHockeyManager sharedHockeyManager].crashManager.crashManagerStatus = BITCrashManagerStatusAutoSend;
    [BITHockeyManager sharedHockeyManager].disableUpdateManager = NO;
    [[BITHockeyManager sharedHockeyManager] startManager];
    [[BITHockeyManager sharedHockeyManager].authenticator authenticateInstallation];
    
    
    // Override point for customization after application launch.
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{
                                                              @"pocEnableLogging" : @(YES),
                                                              @"pocAutoStartsSession": @(YES),
                                                              @"pocShowsLoginOnExpire": @(YES),
                                                              @"pocShowsLoginOnEnd" : @(YES),
                                                              @"pocClosesLoginOnFailure": @(NO)
                                                              }];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults synchronize];
    
    NSLog(@"PoC Configuration:");
    NSLog(@"Production: %@ / %@", licenseKey, secretKey);
    NSLog(@"QA: %@ / %@", devLicenseKey, devSecretKey);
    if ([UIApplication sharedApplication].protectedDataAvailable) {
        NSLog(@"Protected data IS available.");
    } else {
        NSLog(@"Protected data IS NOT available.");
    }
    
    // Configure
//#ifdef DEBUG
//    [ASFSession configureUsingLicenseKey:devLicenseKey
//                            andSecretKey:devSecretKey
//                           inEnvironment:ASFEnvironmentQA
//                                delegate:self];
//#else
    [ASFSession configureUsingLicenseKey:licenseKey
                            andSecretKey:secretKey
                           inEnvironment:ASFEnvironmentProduction
                                delegate:self];
//#endif
    
    
    // Additional configuration
    [ASFSession sharedSession].startsSessionOnLaunch = [defaults boolForKey:@"pocAutoStartsSession"];
    
    // Don't protect the background fetch log
    NSString *fetchLog = [[ASFUtilities getDocumentsDirectory] stringByAppendingPathComponent:kPOCBackgroundFetchLog];
    [ASFFileProtectionManager stopProtectingLocation:fetchLog];
    
    // Start protecting a fake directory
    [ASFFileProtectionManager beginProtectingLocation:@"Documents"];
    [ASFFileProtectionManager beginProtectingLocation:@"Documents/someDirectory"];
    
    return YES;
}

#pragma mark - Public methods
- (void) setThemeColor:(UIColor *)color andSecondaryColor:(UIColor *)secondaryColor
{
    if ([self.window respondsToSelector:@selector(tintColor)])
    {
        self.window.tintColor = color;
        [[UINavigationBar appearance] setBarStyle:UIBarStyleBlack];
        [[UINavigationBar appearance] setBarTintColor:color];
        [[UINavigationBar appearance] setTintColor:secondaryColor];
        [[UINavigationBar appearanceWhenContainedIn:[UIPopoverController class], nil] setTintColor:color];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }
    else
    {
        [[UINavigationBar appearance] setTintColor:color];
        [[UISearchBar appearance] setTintColor:color];
        [[UIToolbar appearance] setTintColor:color];
        [[UISegmentedControl appearance] setTintColor:color];
    }
    [[UIButton appearanceWhenContainedIn:[UITableView class], nil]  setTitleColor:color forState:UIControlStateNormal];
    [[UIButton appearanceWhenContainedIn:[PoCView_ipad class], nil]  setTitleColor:color forState:UIControlStateNormal];
}

#pragma mark - UIApplicationDelegate methods
- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

UIBackgroundTaskIdentifier bgTask;
- (void) startBackgroundTask
{
    NSLog(@"Starting background task to check data availability.");
    [self performSelector:@selector(checkProtectedData) withObject:nil afterDelay:5];
}
- (void) endBackgroundTask
{
    NSLog(@"Ending background task.");
    [[UIApplication sharedApplication] endBackgroundTask:bgTask];
    bgTask = UIBackgroundTaskInvalid;
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(checkProtectedData) object:nil];
}

- (void) checkProtectedData
{
    if ([UIApplication sharedApplication].protectedDataAvailable) 
    {
        NSLog(@"Protected data available.");
        [self performSelector:@selector(checkProtectedData) withObject:nil afterDelay:5];
    }
    else
    {
        NSLog(@"Protected data unavailable.");
        [self endBackgroundTask];
    }
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
    bgTask = [application beginBackgroundTaskWithExpirationHandler:^{
        [self endBackgroundTask];
    }];
    [self startBackgroundTask];
}


- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
    [self endBackgroundTask];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
    // Check the settings
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults synchronize];
    
    [ASFSession sharedSession].showsLoginWhenSessionExpires = [defaults boolForKey:@"pocShowsLoginOnExpire"];
    [ASFSession sharedSession].showsLoginWhenSessionEnds    = [defaults boolForKey:@"pocShowsLoginOnEnd"];
    [ASFSession sharedSession].logging                      = [defaults boolForKey:@"pocEnableLogging"];
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated"
    [ASFSession sharedSession].closesLoginOnFailure         = [defaults boolForKey:@"pocClosesLoginOnFailure"];
#pragma GCC diagnostic pop
    
    
    // Set up iOS 7 background fetching
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
        BOOL shouldStartBackgroundFetching = YES;
        NSString *fetchLog = [[ASFUtilities getDocumentsDirectory] stringByAppendingPathComponent:kPOCBackgroundFetchLog];
        if (![[NSFileManager defaultManager] fileExistsAtPath:fetchLog]) {
            shouldStartBackgroundFetching = [[NSFileManager defaultManager] createFileAtPath:fetchLog contents:nil attributes:@{NSFileProtectionKey:NSFileProtectionCompleteUntilFirstUserAuthentication}];
        }
        
        if (shouldStartBackgroundFetching) {
            [[UIApplication sharedApplication] setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum];
        }
    }
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}

- (void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult result))completionHandler
{
    static NSString *FetchLogPath = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        FetchLogPath = [[ASFUtilities getDocumentsDirectory] stringByAppendingPathComponent:kPOCBackgroundFetchLog];
    });
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSDate *start = [NSDate date];
    NSURLSessionDataTask *task = [session dataTaskWithURL:[NSURL URLWithString:[SecurityViewController echoURLForCurrentUser]] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDate *completed = [NSDate date];
        NSString *result = error ? [error description] : [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        NSMutableArray *currentLog = [NSMutableArray arrayWithContentsOfFile:FetchLogPath];
        if (!currentLog) {
            currentLog = [NSMutableArray array];
        }
        [currentLog insertObject:[NSString stringWithFormat:@"Start: %@\nFinish: %@\nResult:\n%@", start, completed, result] atIndex:0];
        NSArray *updatedLog = [currentLog subarrayWithRange:NSMakeRange(0, MIN(25, currentLog.count))];
        BOOL writeResult = [updatedLog writeToFile:FetchLogPath atomically:NO];
        if (!writeResult) {
            NSLog(@"Failed to update the background fetch log.");
        }
        [[NSFileManager defaultManager] setAttributes:@{NSFileProtectionKey:NSFileProtectionCompleteUntilFirstUserAuthentication} ofItemAtPath:FetchLogPath error:nil];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // Dispatch a local notification
            UILocalNotification *notification = [[UILocalNotification alloc] init];
            notification.alertBody = error ? [NSString stringWithFormat:NSLocalizedString(@"Background fetch failed: %@", nil), error.localizedDescription] : NSLocalizedString(@"Background fetch completed", nil);
            notification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
            [application setScheduledLocalNotifications:@[notification]];
            
            // Fire the completion handler
            completionHandler( error ? UIBackgroundFetchResultFailed : UIBackgroundFetchResultNewData );
        });
    }];
    [task resume];
}

#pragma mark - ASFSession Delegate methods
- (void) sessionDidStart:(ASFSession *)session
{
    
}
- (void) sessionDidStart:(ASFSession *)session withUserProfile:(ASFProfile *)profile
{
 
}
- (void) sessionDidExpire:(ASFSession *)session
{
    
}
- (void) sessionDidEnd:(ASFSession *)session
{
    
}
- (void) sessionFailedToStartWithError:(NSError *)error
{
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated"
    if ([ASFSession sharedSession].closesLoginOnFailure)
#pragma GCC diagnostic pop
    {
        [[[UIAlertView alloc] initWithTitle:error.localizedDescription message:error.localizedRecoverySuggestion delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
}

@end
