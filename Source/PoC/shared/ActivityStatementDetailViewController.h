//
//  ActivityStatementDetailViewController.h
//  poc
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASFActivityStatement.h"

@interface ActivityStatementDetailViewController : UITableViewController

@property (nonatomic, strong) ASFActivityStatement *statement;

@property (nonatomic, strong) IBOutletCollection(UITableViewCell) NSArray* cells;
@property (nonatomic, strong) IBOutlet UITableViewCell *summary;
@property (nonatomic, strong) IBOutlet UITableViewCell *contents;
@property (nonatomic, strong) IBOutlet UITextView *statementJson;
@property (nonatomic, strong) IBOutlet UIImageView *attachment;


@end
