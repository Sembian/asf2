//
//  ASFPDFViewController.m
//  poc
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFPDFViewController.h"
#import "ASFSession+PoCSupport.h"
#import "ASFPDFFileAnnotationProvider.h"

@implementation ASFPDFViewController

- (id)initWithDocument:(PSPDFDocument *)document
{
    // Configure the document
    document.annotationSaveMode = PSPDFAnnotationSaveModeExternalFile;
    document.dataDirectory = [[document.dataDirectory stringByDeletingLastPathComponent] stringByAppendingPathComponent:[ASFSession sharedSession].upi];
    document.editableAnnotationTypes = [NSOrderedSet orderedSetWithObjects:PSPDFAnnotationStringHighlight, PSPDFAnnotationStringNote, PSPDFAnnotationStringUnderline, nil];
    
    // Use our custom file annotation provider so we can log activity log statements
    [document overrideClass:[PSPDFFileAnnotationProvider class] withClass:[ASFPDFFileAnnotationProvider class]];
    
    // Configure the view controller
    if (self = [super initWithDocument:document]) {
        self.pageMode = PSPDFPageModeSingle;
        self.rightBarButtonItems = @[self.searchButtonItem, self.outlineButtonItem, self.viewModeButtonItem];
        self.delegate = self;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sessionDidChange:) name:ASFSessionChangedNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sessionWillEnd:) name:ASFSessionWillEndNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sessionWillEnd:) name:ASFSessionWillExpireNotification object:nil];
    }
    return self;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor]};
}

#pragma mark - Private methods
- (void) sessionDidChange:(NSNotification *)notification
{
    PSPDFDocument *document = [PSPDFDocument documentWithURL:self.document.fileURL];
    ASFPDFViewController *pdfViewController = [[ASFPDFViewController alloc] initWithDocument:document];
    [self.navigationController setViewControllers:@[pdfViewController] animated:NO];
}
- (void) sessionWillEnd:(NSNotification *)notification
{
    [self dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark - PSPDFViewController delegate
- (NSArray *)pdfViewController:(PSPDFViewController *)pdfController shouldShowMenuItems:(NSArray *)menuItems atSuggestedTargetRect:(CGRect)rect forAnnotations:(NSArray *)annotations inRect:(CGRect)annotationRect onPageView:(PSPDFPageView *)pageView
{
    // Hides the Custom button for selecting a color
    menuItems = [menuItems filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"identifier != %@", @"Custom"]];
    return menuItems;
}

@end
