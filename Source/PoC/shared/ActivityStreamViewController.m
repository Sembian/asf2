//
//  ActivityStreamViewController.m
//  poc
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ActivityStreamViewController.h"
#import "ASFActivityLog.h"
#import "ASFActivityLogQuery.h"
#import "ASFActivityLogQueryResult.h"
#import "ASFActivityStatement.h"
#import "CreateActivityStatementViewController.h"
#import "ActivityStatementDetailViewController.h"
#import "pocAppDelegate.h"

@interface ActivityStreamViewController () {
    UISearchBar *searchBar;
    NSDictionary *suggestedBundleIds;
}
@end

@implementation ActivityStreamViewController
{
    NSArray *_statements;
    ASFActivityLogQueryResult *_result;
    ASFActivityLogQuery *query;
    NSString *otherBundleIdentifier;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;
    self.tableView.rowHeight = 54;
    self.title = NSLocalizedString(@"Activity Steam", nil);
    
#if poc1
    NSString *otherTitle = NSLocalizedString(@"Security 2", nil);
#else
    NSString *otherTitle = NSLocalizedString(@"Security 1", nil);
#endif
    otherBundleIdentifier = ASFAltBundleIdentifier;
    
    suggestedBundleIds = @{

                           @"Pronto" : @"com.abbvienet.PPD391Pronto",

    };
    
    UISegmentedControl *segementedControl = [[UISegmentedControl alloc] initWithItems:@[NSLocalizedString(@"All Apps", nil), NSLocalizedString(@"This App", nil), otherTitle,NSLocalizedString(@"Bundle ID", nil)]];
    segementedControl.segmentedControlStyle = UISegmentedControlStyleBar;
    segementedControl.frame = ({
        CGRect frame = segementedControl.frame;
        frame.size.width = self.navigationController.toolbar.frame.size.width - 45;
        frame;
    });
    segementedControl.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [segementedControl addTarget:self action:@selector(onChangeStream:) forControlEvents:UIControlEventValueChanged];
    segementedControl.selectedSegmentIndex = 0;
    [self onChangeStream:segementedControl];
    UIBarButtonItem *barItem = [[UIBarButtonItem alloc] initWithCustomView:segementedControl];
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    self.toolbarItems = @[flexibleSpace, barItem, flexibleSpace];
    self.navigationController.toolbarHidden = NO;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCompose target:self action:@selector(onSelectCreateStatement)];
    
    searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, 44.0)];
    searchBar.placeholder = NSLocalizedString(@"Enter Bundle ID",nil);
    searchBar.delegate = self;
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setToolbarHidden:NO animated:animated];
    if (!self.refreshControl.refreshing) {
        [self reloadData];
    }
}

- (void) reloadData
{
    [self.refreshControl beginRefreshing];
    [ASFActivityLog retrieveStatementsWithQuery:query completionHandler:^(ASFActivityLogQueryResult *result, NSError *error){
        if (error) {
            [[[UIAlertView alloc] initWithTitle:error.localizedDescription message:error.localizedFailureReason delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil] show];
        }
        _statements = result.statements;
        _result = result;
        [self.tableView reloadData];
        [self.refreshControl endRefreshing];
    }];
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _statements.count + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    static NSDateFormatter *df = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        df = [NSDateFormatter new];
        df.locale = [NSLocale currentLocale];
        df.dateFormat = @"dd-MMM-yyyy HH:mm:ss zzz";
    });
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell==nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.textLabel.numberOfLines = 2;
        cell.textLabel.font = [UIFont systemFontOfSize:14];
        cell.detailTextLabel.adjustsFontSizeToFitWidth = YES;
        cell.detailTextLabel.textColor = [UIColor darkGrayColor];
    }
    
    // Configure the cell...
    if (indexPath.row < _statements.count) {
        ASFActivityStatement *statement = _statements[indexPath.row];
        cell.textLabel.text = [statement description];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.detailTextLabel.text = [df stringFromDate:statement.published];
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    } else if (_result.isMoreStatements) {
        // Must be the load more row
        cell.textLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Load More (%zd total statements)...", nil), _result.totalResultCount];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.detailTextLabel.text = nil;
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        cell.accessoryType = UITableViewCellAccessoryNone;
    } else {
        // Status row
        cell.textLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%zd total statements", nil), _result.totalResultCount];
        cell.textLabel.textColor = [UIColor lightGrayColor];
        cell.detailTextLabel.text = nil;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

#pragma mark - Table view delegate
- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self isLoadMoreRow:indexPath]) {
        [self loadMore];
    } else if (indexPath.row < _statements.count) {
        ASFActivityStatement *statement = _statements[indexPath.row];
        ActivityStatementDetailViewController *detail = [ActivityStatementDetailViewController new];
        detail.statement = statement;
        [self.navigationController pushViewController:detail animated:YES];
    }
}

#pragma mark - UIActionSheet delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        searchBar.delegate = nil;
        [searchBar becomeFirstResponder];
        searchBar.delegate = self;
    } else {
        searchBar.text = suggestedBundleIds[[actionSheet buttonTitleAtIndex:buttonIndex]];
        [self searchBarSearchButtonClicked:searchBar];
    }
}

#pragma mark - UISearchBar delegate
- (BOOL) searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Bundle IDs", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Custom", nil) destructiveButtonTitle:nil otherButtonTitles:nil];
    for (NSString *app in suggestedBundleIds.allKeys) {
        [actionSheet addButtonWithTitle:app];
    }
    
    [actionSheet showInView:self.view];
    
    return NO;
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)theSearchBar
{
    query = [ASFActivityLogQuery queryWithLimit:25 andProviderBundleIdentifier:theSearchBar.text];
    [self reloadData];
    [searchBar resignFirstResponder];
}

#pragma mark - Private methods
- (void) onChangeStream:(UISegmentedControl *)sender
{
    if (self.tableView.tableHeaderView) {
        [self hideSearchBar];
    }
    
    switch (sender.selectedSegmentIndex) {
        case 0:
        default:
            query = [ASFActivityLogQuery queryAllApplications];
            break;
        case 1:
            query = [ASFActivityLogQuery query];
            break;
        case 2:
            query = [ASFActivityLogQuery queryWithLimit:25 andProviderBundleIdentifier:otherBundleIdentifier];
            break;
        case 3:
            query = nil;
            _result = nil;
            [self showSearchBar];
            break;
    }
    [self reloadData];
}
- (void) showSearchBar
{
    self.tableView.tableHeaderView = searchBar;
}
- (void) hideSearchBar
{
    [self.tableView setContentOffset:CGPointZero animated:YES];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        searchBar.text = nil;
        self.tableView.tableHeaderView = nil;
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
    });
}
- (void) onSelectCreateStatement
{
    CreateActivityStatementViewController *viewController = [CreateActivityStatementViewController new];
    [self.navigationController pushViewController:viewController animated:YES];
}
- (BOOL) isLoadMoreRow:(NSIndexPath *)indexPath
{
    return indexPath.row == _statements.count && _result.isMoreStatements;
}
- (void) loadMore
{
    [ASFActivityLog retrieveMoreStatementsWithResult:_result completionHandler:^(ASFActivityLogQueryResult *result, NSError *error){
        if (error) {
            [[[UIAlertView alloc] initWithTitle:error.localizedDescription message:error.localizedFailureReason delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil] show];
        } else {
            _statements = [_statements arrayByAddingObjectsFromArray:result.statements];
            _result = result;
            [self.tableView reloadData];
        }
    }];
}

@end
