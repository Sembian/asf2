//
//  ASFSession+PoCSupport.h
//  poc
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASFSession+Private.h"

@interface ASFSession (PoCSupport)

/// Finds the reference to the password field in the login view and checks the password stored there.
/// @return The stored password in the UITextField or nil.
- (NSString *)getCachedPassword;

- (void) simulateExpiration;

@end
