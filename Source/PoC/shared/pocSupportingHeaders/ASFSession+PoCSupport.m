//
//  ASFSession+PoCSupport.m
//  poc
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFSession+PoCSupport.h"
#import "ASFKitViewController.h"
#import "ASFKitLoginViewController.h"
#import "ASFIntegrityCheckController.h"
#import "ASFProfile.h"
#import "ASFAuthorization_Private.h"

@implementation ASFSession (PoCSupport)


- (NSString *)getCachedPassword
{
    // Checks for the existence of the login view
    return [NSString stringWithFormat:@"%@", ((UITextField *)loginViewController.loginView.txt_password).text];
}

- (void) simulateExpiration
{
    // Destroy the access and refresh tokens
    ASFProfile *p = self.authorization.profile;
    ASFAuthorization *a = [[ASFAuthorization alloc] initWithAuthorizationResponse:@{
                        @"access_token" : @"GARBAGE DATA :-)",
                        @"expires_in" : @-1,
                        @"refresh_token" : @"GARBAGE DATA :-)",
                        @"token_type" : @"Bearer"}];
    [a setProfile:p];
    [self setCurrentAuthorizationForSession:a updatingKeychain:YES];
    
    // Try to validate session by restarting the background task
    [self.integrityCheckController performIntegrityCheck];
}

@end
