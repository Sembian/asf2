//
//  ASFPDFFileAnnotationProvider.m
//  poc
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ASFPDFFileAnnotationProvider.h"
#import "ASFActivityLog.h"
#import "ASFActivityObject.h"
#import "ASFActivityStatement.h"

@implementation NSString (ASFStringTruncating)
- (NSString *) truncatedStringWithLength:(NSUInteger)length
{
    return self.length > length ? [NSString stringWithFormat:@"%@...", [self substringToIndex:length]] : self;
}
@end;

NSString* const ASFObjectTypeAnnotation = @"annotation";
static NSUInteger ASFObjectNameLength = 150;

@implementation ASFPDFFileAnnotationProvider

- (NSArray *)addAnnotations:(NSArray *)annotations
{
    for (PSPDFAnnotation *annotation in annotations) {
        
        ASFActivityObject *object = [[self class] activityObjectForAnnotation:annotation];
        
        switch (annotation.type) {
            case PSPDFAnnotationTypeUnderline:
            case PSPDFAnnotationTypeHighlight:
            {
                object.displayName = [[self class] additionalNameInformationForAnnotation:annotation];
                break;
            }
            case PSPDFAnnotationTypeNote:
                object.displayName = [NSString stringWithFormat:@"a note on %@", [[self class] additionalNameInformationForAnnotation:annotation]];
                break;
            default:
                continue;
        }
        
        [ASFActivityLog logActionForCurrentUser:[[self class] verbForCreatingAnnotation:annotation]
                                     withObject:object
                                       onTarget:[[self class] activityTargetForAnnotation:annotation]
                              completionHandler:nil];
    }
    return [super addAnnotations:annotations];
}

- (NSArray *)removeAnnotations:(NSArray *)annotations
{
    for (PSPDFAnnotation *annotation in annotations) {
        ASFActivityObject *object = [[self class] activityObjectForAnnotation:annotation];
        
        [ASFActivityLog logActionForCurrentUser:@"deleted"
                                     withObject:object
                                       onTarget:[[self class] activityTargetForAnnotation:annotation]
                              completionHandler:nil];
    }
    return [super removeAnnotations:annotations];
}

- (void)didChangeAnnotation:(PSPDFAnnotation *)annotation keyPaths:(NSArray *)keyPaths options:(NSDictionary *)options
{
    static NSArray *supportedChanges = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        supportedChanges = @[@"color", @"contents"];
    });
    
    // Determine what (supported) changes were made
    NSArray *changes = [keyPaths filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF IN (%@)", supportedChanges]];
    
    for (NSString *change in changes) {
        ASFActivityObject *object = [[self class] activityObjectForAnnotation:annotation];
        object.summary = [NSString stringWithFormat:@"%@ is now %@", change, [annotation valueForKey:change]]; // Summary of changes
        [ASFActivityLog logActionForCurrentUser:@"updated"
                                     withObject:object
                                       onTarget:[[self class] activityTargetForAnnotation:annotation]
                              completionHandler:nil];
    }
    
    [super didChangeAnnotation:annotation keyPaths:keyPaths options:options];
}

#pragma mark - Private methods
+ (ASFActivityObject *) activityObjectForAnnotation:(PSPDFAnnotation *)annotation
{
    NSArray *components = @[@"annotation",
                            [annotation.typeString lowercaseString],
                            annotation.name
                            ];
    ASFActivityObject *object = [ASFActivityObject activityObjectWithComponents:components andType:ASFObjectTypeAnnotation];
    object.displayName = [self genericNameForAnnotation:annotation];
    object.content = annotation.contents;
    return object;
}

+ (NSString *) genericNameForAnnotation:(PSPDFAnnotation *)annotation
{
    NSString *objectName = [[NSString stringWithFormat:@"the %@", [annotation.typeString lowercaseString]] stringByReplacingOccurrencesOfString:@"text" withString:@"note"];
    NSString *append = [[self class] additionalNameInformationForAnnotation:annotation];
    if (append) {
        objectName = [objectName stringByAppendingFormat:@" on %@", append];
    }
    return objectName;
}

+ (NSString *) additionalNameInformationForAnnotation:(PSPDFAnnotation *)annotation
{
    switch (annotation.type) {
        case PSPDFAnnotationTypeUnderline:
        case PSPDFAnnotationTypeHighlight:
        {
            NSString *highlightedString = [[(PSPDFAbstractTextOverlayAnnotation *)annotation highlightedString] truncatedStringWithLength:ASFObjectNameLength];
            return [NSString stringWithFormat:@"\"%@\"", highlightedString];
        }
        case PSPDFAnnotationTypeNote:
        {
            return [NSString stringWithFormat:@"page %tu", annotation.page + 1];
        }
        default:
            return nil;
    }
}

+ (ASFActivityObject *) activityTargetForAnnotation:(PSPDFAnnotation *)annotation
{
    PSPDFDocument *document = annotation.document;
    NSArray *components = @[
                            @"document",
                            document.title,
                            @(annotation.page + 1)
                            ];
    
    ASFActivityObject *target = [ASFActivityObject activityObjectWithComponents:components andType:@"document"];
    target.displayName = [NSString stringWithFormat:@"Page %tu of %@", annotation.page + 1, document.title];
    return target;
}

+ (NSString *) verbForCreatingAnnotation:(PSPDFAnnotation *)annotation
{
    switch (annotation.type) {
        case PSPDFAnnotationTypeHighlight:
            return NSLocalizedString(@"highlighted", nil);
        case PSPDFAnnotationTypeNote:
            return NSLocalizedString(@"created", nil);
        case PSPDFAnnotationTypeUnderline:
            return NSLocalizedString(@"underlined", nil);
        default:
            return nil;
    }
}

@end

