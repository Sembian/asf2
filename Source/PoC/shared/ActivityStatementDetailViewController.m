//
//  ActivityStatementDetailViewController.m
//  poc
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "ActivityStatementDetailViewController.h"
#import "ASFActivityAttachment.h"
#import "ASFActivityObject.h"

@interface ActivityStatementDetailViewController ()

@end

@implementation ActivityStatementDetailViewController
{
    NSArray *applicableCells;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = NSLocalizedString(@"Activity Detail", nil);
    self.statement = _statement; // Reload statement
    self.tableView.tableFooterView = [UIView new];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setToolbarHidden:YES animated:animated];
}

#pragma mark - Accessors
- (void) setStatement:(ASFActivityStatement *)statement
{
    _statement = statement;
    applicableCells = [self.cells copy];
    NSMutableArray *cells = [applicableCells mutableCopy];
    
    // Find the range of the verb
    NSRange verbRange = [_statement.title rangeOfString:_statement.verb];
    NSMutableAttributedString *summary = [[NSMutableAttributedString alloc] initWithString:_statement.title attributes:@{}];
    NSDictionary *verbAttributes = @{
                           NSForegroundColorAttributeName : [UIColor whiteColor],
                           NSBackgroundColorAttributeName : [UIColor colorWithRed:67/255.0 green:124/255.0 blue:59/255.0 alpha:1]
                           };
    [summary setAttributes:verbAttributes range:verbRange];
    self.summary.textLabel.attributedText = summary;
    
    
    self.contents.textLabel.text = _statement.object.content;
    if (!_statement.object.content.length) {
        [cells removeObject:self.contents];
    }
    
    UIImage *image = [UIImage imageWithData:_statement.attachment.data];
    if (image) {
        self.attachment.image = image;
    } else {
        UIView *cell = self.attachment.superview;
        while (![cell isKindOfClass:[UITableViewCell class]]) {
            cell = cell.superview;
            if (!cell) {
                break;
            }
        }
        [cells removeObject:cell];
    }
    
    self.statementJson.text = _statement.prettyJSONString;
    
    applicableCells = [NSArray arrayWithArray:cells];
}

#pragma mark - Table view datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return applicableCells.count;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = applicableCells[indexPath.row];
    
    if (cell == self.summary || cell == self.contents) {
        CGSize textSize = [cell.textLabel.text sizeWithFont:cell.textLabel.font constrainedToSize:CGSizeMake(tableView.frame.size.width - 20, CGFLOAT_MAX) lineBreakMode:cell.textLabel.lineBreakMode];
        return textSize.height + 20;
    } else {
        return cell.frame.size.height;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return applicableCells[indexPath.row];
}

@end
