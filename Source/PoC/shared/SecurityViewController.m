//
//  SecurityDataSource.m
//  poc
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import "SecurityViewController.h"
#import "ASFSession.h"
#import "ASFKeychain.h"
#import "ASFUtilities_Private.h"
#import "UIDevice+IdentifierAddition.h"
#import "FileViewController.h"
#import "ProfileDetailsViewController.h"
#import "ActivityStreamViewController.h"
#import "WebViewViewController.h"
#import "ASFSession+PoCSupport.h"
#import "ASFSession+Private.h"
#import "ASFAuthorization_Private.h"
#import "UIObservingTextView.h"
#import "ASFIntegrityCheckController.h"
#import "ASFIntegrityCheckResult.h"
#import "ASFConfiguration.h"
#import "ASFKitDevice.h"
#import "ASFActivityObject.h"
#import "ASFActivityStatement.h"
#import "ASFActivityLog_Private.h"
#import "ASFProfile.h"
#import "ASFPDFViewController.h"
#import "ASFMapViewController.h"

NSString* const kPOCBackgroundFetchLog = @"fetch.log";

@implementation NSDate (Formatting)
-(NSString *) stringWithFormat:(NSString *)format
{
    NSDateFormatter *df = [NSDateFormatter new];
    df.dateFormat = format;
    return [df stringFromDate:self];
}
@end

@implementation SecurityViewController

- (id) init
{
    if (self = [super init])
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(initializeData) name:ASFSessionStartedNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData) name:ASFSessionEndedNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData) name:ASFSessionExpiredNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData) name:ASFSessionChangedNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData) name:ASFSessionFailedNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData) name:ASFIntegrityCheckFinishedNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData) name:ASFActivityLogConsentStatusUpdatedNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData) name:UIApplicationDidBecomeActiveNotification object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onDataAvailable:) name:UIApplicationProtectedDataDidBecomeAvailable object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onDataUnavailable:) name:UIApplicationProtectedDataWillBecomeUnavailable object:nil];
        
        _loginToggleButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Login", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(toggleLogin)];
        _simulateExpirationButton = [[UIBarButtonItem alloc] initWithTitle:StringExpire style:UIBarButtonItemStyleBordered target:self action:@selector(simulateExpiration)];
    }
    return self;
}
- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

# pragma mark - Private methods
- (UIView *) createTitleView
{
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 150, 33)];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, titleView.frame.size.width, 17)];
    UILabel *subtitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(titleLabel.frame), titleView.frame.size.width, 16)];
    titleLabel.autoresizingMask = subtitleLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    titleLabel.textAlignment = subtitleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.backgroundColor = subtitleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = subtitleLabel.textColor = [UIColor whiteColor];
    titleLabel.font = [UIFont boldSystemFontOfSize:16];
    titleLabel.shadowColor = [UIColor darkGrayColor];
    subtitleLabel.font = [UIFont systemFontOfSize:12];
    
    titleLabel.text = self.viewController.title.length ? self.viewController.title : self.viewController.navigationItem.title;
    ASFSession *s = [ASFSession sharedSession];
    subtitleLabel.text = s.sessionStarted ? s.adLogon : @"Not Logged In";
    
    [titleView addSubview:titleLabel];
    [titleView addSubview:subtitleLabel];
    
    return titleView;
}

- (void) viewDidLoad
{
    [(UIObservingTextView *)self.displayName observeKeyPath:@"authorization.profile.displayName" onObject:[ASFSession sharedSession]];
    [(UIObservingTextView *)self.accessToken observeKeyPath:@"authorization.accessToken" onObject:[ASFSession sharedSession]];
    [(UIObservingTextView *)self.expiration observeKeyPath:@"authorization.expiration" onObject:[ASFSession sharedSession]];
    [(UIObservingTextView *)self.refreshToken observeKeyPath:@"authorization.refreshToken" onObject:[ASFSession sharedSession]];
    [(UIObservingTextView *)self.authorizationUrl observeKeyPath:@"configuration.authenticationEndpoint" onObject:[ASFSession sharedSession]];
    [(UIObservingTextView *)self.profileUrl observeKeyPath:@"configuration.profileEndpoint" onObject:[ASFSession sharedSession]];
}

- (void) hideRequestProgressIndicator
{
    double delayInSeconds = 0.5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        self.downloadProgressView.hidden = YES;
    });
}

#pragma mark - Public methods

- (void) initializeData
{
    self.requestedUrl.text = @"Select a button above";
    self.httpsResponse.text = @"";
    self.dataBecomingAvailable.text = @"";
    self.dataBecomingUnavailable.text = @"";
    // The view will be updated when viewWillAppear is triggered
}

- (void) updateData
{
    static NSArray *consentStatuses = nil;
    
    // Check if the view should be updated
    if ([self.delegate respondsToSelector:@selector(securityViewManagerShouldUpdateData:)] && ![self.delegate securityViewManagerShouldUpdateData:self]) {
        return;
    }
    if ([self.delegate respondsToSelector:@selector(securityViewManagerWillUpdateData:)]) {
        [self.delegate securityViewManagerWillUpdateData:self];
    }
    
    ASFSession *s = [ASFSession sharedSession];
    
    // Local data
    self.cachedPasswordData.text = [s getCachedPassword];
    self.urlSchemes.text = [s.urlSchemes componentsJoinedByString:@", "];
    self.applicationKey.text = s.licenseKey;
    self.secretKey.text = s.secretKey;
    self.deviceIdentifier.text = [ASFKitDevice uniqueIdentifier];
    self.fileAttributes.text = [NSString stringWithFormat:@"%@",[[NSFileManager defaultManager] attributesOfItemAtPath:[NSString stringWithFormat:@"%@/abbvie.png",[ASFUtilities getDocumentsDirectory]] error:nil]];
    
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
        NSArray *fetchLog = [NSArray arrayWithContentsOfFile:[[ASFUtilities getDocumentsDirectory] stringByAppendingPathComponent:kPOCBackgroundFetchLog]];
        self.backgroundFetchLog.text = [fetchLog componentsJoinedByString:@"\n------------------------------------\n"];
    } else {
        self.backgroundFetchLog.text = NSLocalizedString(@"Background fetch is only supported on iOS 7 and later", nil);
    }
    
    
    // Server data
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        consentStatuses = @[NSLocalizedString(@"Unknown", nil), NSLocalizedString(@"Accepted", nil), NSLocalizedString(@"Not Accepted", nil), NSLocalizedString(@"Needs Updating", nil)];
    });
    self.activityLogUserConsentStatus.text = consentStatuses[[ASFActivityLog currentConsentStatus]];
    self.activityLogStatus.text = [ASFActivityLog activityLoggingIsAvailable] ? NSLocalizedString(@"Enabled", nil) : NSLocalizedString(@"Disabled", nil);
    
    // Token details
    self.timeToReceiveResponse.text = [NSString stringWithFormat:@"%.2f seconds", s.timeToReceiveAuthenticationResponse];
    self.timeToVerifyCredentials.text = [NSString stringWithFormat:@"%.2f seconds", s.timeToVerifyCredentials];
    self.timeToRetrieveProfile.text = [NSString stringWithFormat:@"%.2f seconds", s.timeToRetrieveProfile];
    
    // Protection Status
    self.frameworkVersion.text = s.frameworkVersion;
    ASFIntegrityCheckResult *result = [ASFSession sharedSession].integrityCheckController.lastResult;
    if (result) {
        switch (result.status)
        {
            case ASFIntegrityCheckPassed:
            {
                NSArray *skippedChecks = [result.passedChecks filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"didEncounterTemporaryProblem = YES"]];
                self.lastIntegrityCheckResult.text = [NSString stringWithFormat:@"Passed (%zd check(s) skipped) at %@", skippedChecks.count, result.completedDate];
                break;
            }
            case ASFIntegrityCheckFailed:
            {
                self.lastIntegrityCheckResult.text = [NSString stringWithFormat:@"%zd checks failed at %@", result.failedChecks.count, result.completedDate];
                break;
            }
        }
    }
    
    self.configurationProfileStatus.text = [s verifyConfigurationWithError:nil] ? @"Yes" : @"No";
    
    NSDictionary *entitlements = [ASFUtilities currentEntitlements];
    self.keychainAccessGroup.text = [entitlements[@"keychain-access-groups"] componentsJoinedByString:@", "];
    self.dataProtection.text = entitlements[@"com.apple.developer.default-data-protection"];
    
    NSString *service = s.configuration.keychainServiceName;
    self.keychainData.text = [NSString stringWithFormat:@"%@", [ASFKeychain load:service]];
    self.publicKeychainData.text = [NSString stringWithFormat:@"%@", [ASFKeychain load:[@"Public" stringByAppendingString:service]]];
    
    UIView *titleView = [self createTitleView];
    self.viewController.navigationItem.titleView = titleView;
    CGRect frame = titleView.frame;
    frame.size.width = self.viewController.navigationController.navigationBar.frame.size.width * .7;
    titleView.frame = frame;
    
    if (s.sessionStarted)
    {
        self.loginToggleButton.title = @"Logout";
        self.simulateExpirationButton.enabled = YES;
    }
    else
    {
        self.loginToggleButton.title = @"Login";
        self.simulateExpirationButton.enabled = NO;
    }
    
    if ([self.delegate respondsToSelector:@selector(securityViewManagerDidUpdateData:)]) {
        [self.delegate securityViewManagerDidUpdateData:self];
    }
}

- (void) closeModal
{
    [self.viewController dismissViewControllerAnimated:YES completion:NULL];
}

- (void) onDataAvailable:(NSNotification *)notification
{
    self.dataBecomingAvailable.text = [[NSDate date] stringWithFormat:DateFormat];
}
- (void) onDataUnavailable:(NSNotification *)notification
{
    self.dataBecomingUnavailable.text = [[NSDate date] stringWithFormat:DateFormat];
}

- (void) toggleLogin
{
    if ([ASFSession sharedSession].sessionStarted)
    {
        [[ASFSession sharedSession] endSession];
    }
    else
    {
        [[ASFSession sharedSession] startSession];
    }
}

- (void) simulateExpiration
{
    [[ASFSession sharedSession] simulateExpiration];
}

#pragma mark - Accessors
- (void) setViewController:(UIViewController *)viewController
{
    _viewController = viewController;
    viewController.navigationItem.leftBarButtonItem = self.loginToggleButton;
    viewController.navigationItem.rightBarButtonItem = self.simulateExpirationButton;
    [self updateData];
}

# pragma mark - Interface actions
- (IBAction) openFileBrowser:(UIButton *)sender
{
    FileViewController *vc = [[FileViewController alloc] initWithPath:[NSString stringWithFormat:@"%@/../",[ASFUtilities getDocumentsDirectory]]];
    [self presentModalOrPopoverWithViewController:vc fromSender:sender wrappedInNavigationController:YES];
}

- (IBAction) openWebBrowser:(id)sender
{
    WebViewViewController *vc = [WebViewViewController new];
    [self.viewController presentViewController:[[UINavigationController alloc] initWithRootViewController:vc] animated:YES completion:NULL];
}

- (IBAction) makeHTTPsRequest:(id) sender
{
    NSURL *url = [NSURL URLWithString:[[self class] echoURLForCurrentUser]];
    [ASFURLRequest requestWithURL:url delegate:self];
    
    // Create an activity statement
    [ASFActivityLog logActionForCurrentUser:@"accessed" withObjectName:@"secure data" ofType:@"json" completionHandler:^(ASFActivityLogStoreResult result, NSArray *statements, NSError *error) {
        if (!result) {
            [[[UIAlertView alloc] initWithTitle:error.localizedDescription message:error.localizedFailureReason delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
        }
    }];
}

- (IBAction) makeHTTPsRequestInvalidCertificate: (id) sender
{
    NSString *href = [[[self class] echoURLForCurrentUser] stringByReplacingOccurrencesOfString:@"mobileappserver.net" withString:@"192.237.166.183"];
    NSURL *url = [NSURL URLWithString:href];
    [ASFURLRequest requestWithURL:url delegate:self];
    
    // Create an activity statement
    [ASFActivityLog logActionForCurrentUser:@"accessed" withObjectName:@"insecure data" ofType:@"json" completionHandler:^(ASFActivityLogStoreResult result, NSArray *statements, NSError *error) {
        if (!result) {
            [[[UIAlertView alloc] initWithTitle:error.localizedDescription message:error.localizedFailureReason delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
        }
    }];
}

- (IBAction) downloadFile:(id)sender
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://mobileappserver.net/demo/abbvie.png?%.0f", [NSDate date].timeIntervalSinceReferenceDate]];
    [ASFURLRequest downloadFileFromURL:url toFilePath:[NSString stringWithFormat:@"%@/abbvie.png",[ASFUtilities getDocumentsDirectory]] delegate:self];
    
    // Create an activity statement
    [ASFActivityLog logActionForCurrentUser:@"downloaded" withObjectName:@"Butterfly Photo" ofType:@"image" completionHandler:^(ASFActivityLogStoreResult result, NSArray *statements, NSError *error) {
        if (!result) {
            [[[UIAlertView alloc] initWithTitle:error.localizedDescription message:error.localizedFailureReason delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
        }
    }];
}

- (IBAction) viewProfile:(UIButton *)sender
{
    ProfileDetailsViewController *profileDetails = [ProfileDetailsViewController new];
    [self presentModalOrPopoverWithViewController:profileDetails fromSender:sender wrappedInNavigationController:NO];
}

- (IBAction) viewActivityStream:(UIButton *)sender
{
    ActivityStreamViewController *activityStreamViewController = [ActivityStreamViewController new];
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:activityStreamViewController];
    nav.modalPresentationStyle = UIModalPresentationFormSheet;
    activityStreamViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(closeModal)];
    [self.viewController presentViewController:nav animated:YES completion:NULL];
}

- (IBAction) viewPDF:(id)sender
{
    NSURL *pdfURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"SecureCodingGuide" ofType:@"pdf"]];
    PSPDFDocument *document = [PSPDFDocument documentWithURL:pdfURL];
    ASFPDFViewController *pdfViewController = [[ASFPDFViewController alloc] initWithDocument:document];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:pdfViewController];
    [self.viewController presentViewController:nav animated:YES completion:nil];
}

- (IBAction) viewUserMap:(UIButton *)sender
{
    ASFMapViewController *mapViewController = [[ASFMapViewController alloc] initWithProfile:[ASFSession sharedSession].authorization.profile];
    [self presentModalOrPopoverWithViewController:mapViewController fromSender:sender wrappedInNavigationController:NO];
}

- (IBAction) clearBackgroundFetchLog:(id)sender
{
    [[NSFileManager defaultManager] removeItemAtPath:[[ASFUtilities getDocumentsDirectory] stringByAppendingPathComponent:kPOCBackgroundFetchLog] error:nil];
    [self updateData];
}

- (void) presentModalOrPopoverWithViewController:(UIViewController *)viewController fromSender:(UIButton *)sender wrappedInNavigationController:(BOOL)wrapPopoverInNavigationController
{
    if (IS_IPAD)
    {
        if (wrapPopoverInNavigationController) {
            viewController = [[UINavigationController alloc] initWithRootViewController:viewController];
        }
        
        popover = [[UIPopoverController alloc] initWithContentViewController:viewController];
        popover.delegate = self;
        [popover presentPopoverFromRect:sender.frame inView:sender.superview permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else
    {
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:viewController];
        viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(closeModal)];
        [self.viewController presentViewController:nav animated:YES completion:NULL];
    }
}

#pragma mark - Other
+ (NSString *) echoURLForCurrentUser
{
    NSString *href = @"https://mobileappserver.net/demo/echo.php";
    NSDictionary *data = [[ASFSession sharedSession].authorization.profile dictionaryWithValuesForKeys:@[@"upi",@"displayName"]];
    NSMutableArray *query = [NSMutableArray array];
    [data enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSString *value, BOOL *stop) {
        value = [[value description] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [query addObject:[NSString stringWithFormat:@"%@=%@", [key stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding], value]];
    }];
    return [href stringByAppendingFormat:@"?%@", [query componentsJoinedByString:@"&"]];
}

#pragma mark - ASFURLRequestDelegate
- (BOOL) requestShouldStartAutomatically
{
    return YES;
}
- (void) requestDidStart:(ASFURLRequest *)request
{
    self.requestedUrl.text = request.URL.absoluteString;
    self.httpsResponse.text = @"Requesting URL...";
}
- (void) requestDidFinish:(ASFURLRequest *)request
{
    if (request.downloadFilepath)
    {
        self.httpsResponse.text = [NSString stringWithFormat:@"Downloaded file to: %@",request.downloadFilepath];
        self.pictureResponse.image = [UIImage imageWithData:request.responseData];
        [self updateData];
    }
    else
    {
        NSString *responseString = [[NSString alloc] initWithData:request.responseData encoding:NSUTF8StringEncoding];
        self.httpsResponse.text = responseString;
    }
    
    [self hideRequestProgressIndicator];
}
- (void)request:(ASFURLRequest *)request didMakeProgress:(float)percentComplete
{
    self.httpsResponse.text = [NSString stringWithFormat:@"Receiving response... (%.0f%%)", percentComplete * 100.0];
    self.downloadProgressView.hidden = NO;
    [self.downloadProgressView setProgress:percentComplete animated:YES];
}

- (void) request:(ASFURLRequest *)request didFailWithError:(NSError *)error
{
    self.httpsResponse.text = [NSString stringWithFormat:@"Error: %@",error];
    [self hideRequestProgressIndicator];
}

#pragma mark - UIPopoverDelegate methods
- (void) popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    popover = nil;
}

@end
