//
//  UIObservingTextView.h
//  poc
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIObservingTextView : UITextView

@property (nonatomic, strong) NSString *format;

- (void) observeKeyPath:(NSString *)keyPath onObject:(NSObject *)object;

@end
