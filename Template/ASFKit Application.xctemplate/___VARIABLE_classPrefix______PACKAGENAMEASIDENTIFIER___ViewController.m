//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright ___ORGANIZATIONNAME___ ___YEAR___. All rights reserved.
//

#import "___VARIABLE_classPrefix______PACKAGENAMEASIDENTIFIER___ViewController.h"
#import <ASFKit/ASFKit.h>

@interface ___FILEBASENAMEASIDENTIFIER___ ()
@property (nonatomic, strong) IBOutlet UIBarButtonItem *loginButton;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *statusButton;
@end

@implementation ___FILEBASENAMEASIDENTIFIER___

#pragma mark - View lifecycle

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void) viewDidLoad
{
    self.statusButton.enabled = NO;
    [super viewDidLoad];
}

# pragma mark - Interface actions
/// <summary>
/// Updates the interfaced based on whether the user is logged in or not.
/// </summary>
- (void) updateInterface
{
    ASFSession *session = [ASFSession sharedSession];
    
    if (session.sessionStarted)
    {
        self.statusButton.title = [NSString stringWithFormat:NSLocalizedString(@"Logged in: %@", nil), session.displayName];
        self.statusButton.enabled = YES;
        self.loginButton.title = NSLocalizedString(@"Log Out", nil);
    }
    else
    {
        self.statusButton.title = NSLocalizedString(@"Not Logged In", nil);
        self.statusButton.enabled = NO;
        self.loginButton.title = NSLocalizedString(@"Log In", nil);
    }
}

/// <summary>
/// Receives the event when the user presses the "Login" or "Logout" button in the bottom right of the screen.
/// Determines if the session has been started and uses that to determine if it should end the user's session or start a new one.
/// </summary>
- (IBAction) toggleLogin:(id)sender
{
    if([ASFSession sharedSession].sessionStarted)
    {
        [[ASFSession sharedSession] endSession];
    }
    else
    {
        [[ASFSession sharedSession] startSession];
    }
}

/// <summary>
/// Displays an UIAlertView containing the basic information about the current session.
/// </summary>
- (IBAction) displayStatus:(id)sender
{
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ASFSession Status", nil) message:[[ASFSession sharedSession] description] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil] show];
}

@end
