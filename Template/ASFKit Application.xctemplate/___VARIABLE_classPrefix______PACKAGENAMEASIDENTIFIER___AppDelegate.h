//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright ___ORGANIZATIONNAME___ ___YEAR___. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ___VARIABLE_classPrefix______PACKAGENAMEASIDENTIFIER___ViewController;
@interface ___FILEBASENAMEASIDENTIFIER___ : NSObject

@property (nonatomic, retain) IBOutlet UIWindow *window;

@property (nonatomic, retain) IBOutlet ___VARIABLE_classPrefix______PACKAGENAMEASIDENTIFIER___ViewController *viewController;

@end