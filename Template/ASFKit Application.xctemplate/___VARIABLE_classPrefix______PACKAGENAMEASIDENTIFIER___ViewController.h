//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright ___ORGANIZATIONNAME___ ___YEAR___. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ___FILEBASENAMEASIDENTIFIER___ : UIViewController

/// <summary>Updates interface labels based on the ASFSession's current status.</summary>
- (void) updateInterface;

@end
