ASFKit 2.0 Source Code
======================
(c) 2013 AbbVie

System Requirements
-------------------
### iOS Library and Applications
In order to build the iOS components of ASFKit, your system must meet the following requirements:

* OS X 10.8
* Xcode 4.6

### Licensing Service
In order to run the server-side components of ASFKit, your system must meet the following requirements:

* PHP 5.3
* Cake 2.3
* MySQL 5.2

Project Structure
-------------
* **Distribution** - Contains the PackageMaker script and additional resources included in the public distribution of ASFKit.
* **Documentation** - Contains both public and support documentation for ASFKit.
* **Source** -- Contains the source files for all components of ASFKit.
	* **ASFKit** -- Source files for the iOS framework.
	* **PoC** -- Source files for the PoC apps.
	* **Server** -- Source files for the licensing service.
* **Template** -- Contains the Xcode 4 project template files.

Class Documentation
-------------------
All public classes and most private classes are documented following the JavaDoc standards. Class documentation can be generated using [Doxygen](http://www.doxygen.nl) or [Appledoc](http://gentlebytes.com/appledoc/).

The **Public Class Documentation** build scheme relies on Appledoc to build the Xcode Docset files. To install Appledoc, open Terminal and run:

    ./Documentation/install-appledoc.sh


Known Issues
--------------
When building the documentation, Xcode will report build warnings about invalid cross-references. These can be ignored.

Troubleshooting
--------------
Refer to the support guide for troubleshooting tips.

Bug Reports
-------------
If you encounter bugs in the ASFKit framework, source code files, or the documentation, please report them to:

Daniel Pfeiffer  
Float Mobile Learning  
dpfeiffer@floatlearning.com

